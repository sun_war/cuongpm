#!/bin/bash

# Path and folder
pathMount="/magentonfs/"
versionBuild="magento-${BUILD_NUMBER}"
sourceCode="${pathMount}feca_magento/"
versionBuildSource="${pathMount}${versionBuild}"

echo $sourceCode
echo $versionBuildSource

# Make new folder
echo "Make new folder"
mkdir "${versionBuildSource}"

# Copy file to new folder
echo "Copy file to new folder"
cp -af "${sourceCode}".* "${versionBuildSource}"

cd "${versionBuildSource}"

# Pull new source
echo "Pull new source"
git pull origin master

# Deploy mangento
echo "Deploy mangento"
sudo -u apache php -dmemory_limit=2G bin/magento module:enable --all
sudo -u apache php -dmemory_limit=2G bin/magento c:c
sudo -u apache php -dmemory_limit=2G bin/magento s:up
sudo -u apache php -dmemory_limit=2G  bin/magento s:d:c
sudo -u apache php -dmemory_limit=2G  bin/magento s:st:d -f

# Symlink from new folder to source code folder
ls -sf "${versionBuildSource}/"* "${sourceCode}"
