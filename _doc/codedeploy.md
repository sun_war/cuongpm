## Setup
Install ruby
```angularjs
sudo yum install ruby -y
```

Install wget
 ```angularjs
wget https://aws-codedeploy-us-east-2.s3.us-east-2.amazonaws.com/latest/install
wget https://aws-codedeploy-ap-southeast-1.s3.ap-southeast-1.amazonaws.com/latest/install
```

Install codedeploy
```angularjs
chmod +x ./install
sudo ./install auto
```

Start and check
```angularjs
sudo service codedeploy-agent start
sudo service codedeploy-agent status
```

## Github
- `scripts/prepare.sh`
```angularjs
#!/usr/bin/env bash

aws s3 cp s3://magento-deployment-configuration/env.php /var/www/magento/app/etc/env.php
chown -R magento:apache /var/www/magento
```
- `scripts/deploy.sh`
```angularjs
#!/bin/bash
cd /var/www/magento

chown -R root:root /var/www/magento/composer.phar
php composer.phar install

chown -R magento:apache /var/www/magento

sudo -u magento php -dmemory_limit=2G bin/magento module:enable --all
sudo -u magento php -dmemory_limit=2G bin/magento c:e
sudo -u magento php -dmemory_limit=2G bin/magento c:c
sudo -u magento php -dmemory_limit=2G bin/magento s:up
sudo -u magento php -dmemory_limit=2G bin/magento s:d:c
sudo -u magento php -dmemory_limit=2G bin/magento setup:static-content:deploy --jobs 10 -f th_TH en_US --theme Magento/backend --theme Feca/FecaAsia2
```

- appspec.yml
```angularjs
# This is an appspec.yml template file for use with an EC2/On-Premises deployment in CodeDeploy.
# The lines in this template starting with the hashtag symbol are
#   instructional comments and can be safely left in the file or
#   ignored.
# For help completing this file, see the "AppSpec File Reference" in the
#   "CodeDeploy User Guide" at
#   https://docs.aws.amazon.com/codedeploy/latest/userguide/app-spec-ref.html
version: 0.0
# Specify "os: linux" if this revision targets Amazon Linux,
#   Red Hat Enterprise Linux (RHEL), or Ubuntu Server
#   instances.
# Specify "os: windows" if this revision targets Windows Server instances.
# (You cannot specify both "os: linux" and "os: windows".)
os: linux
# os: windows
# During the Install deployment lifecycle event (which occurs between the
#   BeforeInstall and AfterInstall events), copy the specified files
#   in "source" starting from the root of the revision's file bundle
#   to "destination" on the Amazon EC2 instance.
# Specify multiple "source" and "destination" pairs if you want to copy
#   from multiple sources or to multiple destinations.
# If you are not copying any files to the Amazon EC2 instance, then remove the
#   "files" section altogether. A blank or incomplete "files" section
#   may cause associated deployments to fail.
files:
- source: /
  destination: /var/www/magento

permissions:
- object: /var/www/magento
  pattern: "**"
  owner: magento
  type:
  - file

# For deployments to Amazon Linux, Ubuntu Server, or RHEL instances,
#   you can specify a "permissions"
#   section here that describes special permissions to apply to the files
#   in the "files" section as they are being copied over to
#   the Amazon EC2 instance.
#   For more information, see the documentation.
# If you are deploying to Windows Server instances,
#   then remove the
#   "permissions" section altogether. A blank or incomplete "permissions"
#   section may cause associated deployments to fail.
hooks:
  BeforeInstall:
  - location: scripts/prepare.sh
    runas: root
  AfterInstall:
  - location: scripts/deploy.sh
    runas: root
```
