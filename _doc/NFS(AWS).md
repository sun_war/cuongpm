# Storage
- S3: store data
- EBS: auto setup driver for machine
- EFS: auto storage scalable 

# NFS(AWS)

## Setup

Install
```angularjs
sudo yum update -y
sudo yum install nfs-utils -y
```

Make folder share
```angularjs
/nfs/share
```
Edit file /etc/exports to create mountpoint expor
```angularjs
/var/nfs/share 10.10.0.0/24(rw,no_root_squash)
```
Start NFS Server
```angularjs
sudo systemctl start rpcbind nfs-server
```
Enable start with OS server
```angularjs
sudo systemctl enable rpcbind nfs-server
```
Check port use for NFS
```angularjs
rpcinfo -p
```
Check mount point on server
```angularjs
showmount -e localhost
```

Add run bash to auto scale
```
mount -t nfs -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport fs-08682849.efs.ap-southeast-1.amazonaws.com:/ /var/www/mg2/
cd /var/www/mg2/

mount -t nfs -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport fs-b6773af7.efs.ap-southeast-1.amazonaws.com:/ /mg2/
mount -t nfs -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport fs-1f763b5e.efs.ap-southeast-1.amazonaws.com:/ /no/

chmod go+rw .
```

vi `/etc/fstab`
```angularjs
fs-08682849.efs.ap-southeast-1.amazonaws.com:/ /opt/efs-mount nfs nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 0 0

fs-5d75381c.efs.ap-southeast-1.amazonaws.com:/ /mg2 nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 0 0
```
Check
```angularjs
 df -h
```
Pass
```angularjs
Cwfec2019
```

Command
```angularjs
git /var/www/mg2/ pull

php /var/www/mg2/bin/magento module:enable --all


rm -rf /var/www/mg2/generated/
rm -rf /var/www/mg2/var/
rm -rf /var/www/mg2/pub/static/
rm -rf /var/www/mg2/var/generation
rm -rf /var/www/mg2/var/cache
rm -rf /var/www/mg2/var/composer_home
rm -rf /var/www/mg2/var/page_cache
rm -rf /var/www/mg2/var/view_preprocessed

php /var/www/mg2/bin/magento c:c
php /var/www/mg2/bin/magento s:up
php /var/www/mg2/bin/magento s:d:c
php /var/www/mg2/bin/magento s:st:d -f
```

```angularjs
chmod g+s
chmod -R g+rw
```

```angularjs
echo ${BUILD_NUMBER}
```

Bash
```angularjs
#!/bin/bash

# Path and folder
pathMount="/magentonfs/"
versionBuild="magento-${BUILD_NUMBER}"
sourceCode="${pathMount}/feca_magento/"
versionBuildSource="${pathMount}${versionBuild}"

# switch acc magento
sudo su magento

# Make new folder
mkdir "${versionBuildSource}"

# Copy file to new folder
#cp -rf "${sourceCode}"* "${versionBuildSource}"
cp -rf "${sourceCode}".* "${versionBuildSource}"

cd "${versionBuildSource}"

# Pull new source
git pull

# Deploy mangento
php -dmemory_limit=2G module:enable --all
php -dmemory_limit=2G c:c
php -dmemory_limit=2G s:up
#php -dmemory_limit=2G s:d:c
php -dmemory_limit=2G s:st:d -f

# Symlink from new folder to source code folder 
ls -sf "${versionBuildSource}/"* "${sourceCode}"
```


```angularjs
#!/bin/bash

# Path and folder
pathMount="/magentonfs/"
versionBuild="magento-${BUILD_NUMBER}"
sourceCode="${pathMount}feca_magento/"
versionBuildSource="${pathMount}${versionBuild}"

echo $sourceCode
echo $versionBuildSource

# Make new folder
echo "Make new folder"
#mkdir "${versionBuildSource}"

# Copy file to new folder
echo "Copy file to new folder"
#cp -af "${sourceCode}".* "${versionBuildSource}"

cd "${versionBuildSource}"

# Pull new source
echo "Pull new source"
git pull origin master

# Deploy mangento
echo "Deploy mangento"
sudo -u apache php -dmemory_limit=2G bin/magento module:enable --all
sudo -u apache php -dmemory_limit=2G bin/magento c:c
sudo -u apache php -dmemory_limit=2G bin/magento s:up
sudo -u apache php -dmemory_limit=2G  bin/magento s:d:c
sudo -u apache php -dmemory_limit=2G  bin/magento s:st:d -f

# Symlink from new folder to source code folder 
ls -s "${versionBuildSource}/"* "${sourceCode}"
```
- Symlink sử dụng file tuyệt đối

```angularjs
<VirtualHost *:80>
DocumentRoot /magento/
    ServerName localhost
    ServerAlias localhost
    SetEnv MAGE_MODE "developer"
    SetEnv MAGE_RUN_TYPE "website"
    SetEnv MAGE_RUN_CODE "feca"

    <Directory /magento/>
         Options FollowSymLinks
         AllowOverride All
         Order allow,deny
         Allow from all
         Require all granted
    </Directory>
    # ErrorLog /var/www/error.log
    #  CustomLog /var/www/requests.log combined

     DirectoryIndex  index.php
</VirtualHost>
```

````angularjs
semanage port -a -t ssh_port_t -p tcp 2222
````

View size folder
```angularjs
$ du -sh ostechnix
```
