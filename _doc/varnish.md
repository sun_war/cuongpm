# Varnish on CentOS 7
## Introduction
- Install
- Config
- Logging
- View Statistic cache

## 1, Install
In this step, we will install Varnish high-performance HTTP accelerator. It's available on the EPEL (Extra Packages for Enterprise Linux) repository, so we need to install the repo before installing Varnish.
Install EPEL repository using the following command.
```angular2html
yum -y install epel-release
```
Now install Varnish from the EPEL repository using yum command below.
```angular2html
yum -y install varnish
```
After the installation is complete, start Varnish and add it to launch at system boot using the systemctl commands below.
```angular2html
systemctl start varnish
systemctl enable varnish
```
Check status
```angular2html
systemctl status varnish
```
Status view this:
```angular2html
[ec2-user@ip-172-31-30-129 nginx]$ systemctl status varnish
● varnish.service - Varnish Cache, a high-performance HTTP accelerator
   Loaded: loaded (/usr/lib/systemd/system/varnish.service; enabled; vendor preset: disabled)
   Active: active (running) since Mon 2019-12-09 08:46:16 UTC; 37min ago
  Process: 18541 ExecStart=/usr/sbin/varnishd -a :80 -f /etc/varnish/default.vcl -s malloc,256m (code=exited, status=0/SUCCESS)
 Main PID: 18543 (varnishd)
    Tasks: 217
   Memory: 114.8M
   CGroup: /system.slice/varnish.service
           ├─18543 /usr/sbin/varnishd -a :80 -f /etc/varnish/default.vcl -s malloc,256m
           └─18553 /usr/sbin/varnishd -a :80 -f /etc/varnish/default.vcl -s malloc,256m

```
By default, Varnish will use port 6081 and 6082. Check it using the netstat command below.
```angular2html
netstat -plntu
```
Result this
```angular2html
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 127.0.0.1:38773         0.0.0.0:*               LISTEN      -
tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      -
tcp        0      0 0.0.0.0:80              0.0.0.0:*               LISTEN      -
tcp        0      0 0.0.0.0:8080            0.0.0.0:*               LISTEN      -

```
## 2, Configure 

### Varnish as a reverse proxy for Apache
So Varnish is installed, and now we will configure it as a reverse proxy for the Apache web server. Varnish will run on HTTP port 80 by default.
Go to the Varnish configuration directory and edit the default configuration in `default.vcl`.

```angular2html
vi  /etc/varnish/default.vcl
```

Define the default backend section. We are using Apache web server running on port 8080, so the configuration as below.
```angular2html
backend default {
     .host = "127.0.0.1";
     .port = "8080";
 }
```
Next, configure Varnish to run with HTTP port 80. Edit the 'varnish.params' file present in the Varnish configuration directory.
```angular2html
vim varnish.params
```
In this file, change the value of the 'VARNISH_LISTEN_PORT' line to HTTP port 80.
```angular2html
VARNISH_LISTEN_PORT=80
```
Save and exit.
Now restart Varnish and check stats using the netstat command.

### Varnish for NLB(AWS)
You can also group several backend into a group of backends. These groups are called directors. This will give you increased performance and resilience.
Have all private IP of NLB: 
```angular2html
- 10.10.3.10 
- 10.10.4.10 
```
You can define several backends and group them together in a director. This requires you to load a VMOD, a Varnish module.
Go to the Varnish configuration directory and edit the default configuration in `default.vcl`
```angular2html
import directors;    # load the directors

backend server1 {
    .host = "10.10.3.10";
    .port = "80";
}
backend server2 {
    .host = "10.10.4.10";
    .port = "80";
}

sub vcl_init {
    new bar = directors.round_robin();
    bar.add_backend(server1);
    bar.add_backend(server2);
}
```
Save and exit.
Now restart Varnish and check stats using the netstat command.
```angular2html
sudo systemctl daemon-reload
sudo systemctl restart varnish
```

## 3, Logging
Start the service on boot
```angular2html
service varnishncsa start
```
Show log with command
```angular2html
varnishncsa
```
Result logging this
```angular2html
[ec2-user@ip-172-31-30-129 nginx]$ sudo varnishncsa
118.70.13.208 - - [09/Dec/2019:08:51:59 +0000] "GET http://54.179.166.162/ HTTP/1.1" 304 0 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36"
118.70.13.208 - - [09/Dec/2019:08:52:01 +0000] "GET http://54.179.166.162/ HTTP/1.1" 304 0 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36"
118.70.13.208 - - [09/Dec/2019:08:52:02 +0000] "GET http://54.179.166.162/ HTTP/1.1" 304 0 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36"
118.70.13.208 - - [09/Dec/2019:08:52:03 +0000] "GET http://54.179.166.162/ HTTP/1.1" 304 0 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36"
118.70.13.208 - - [09/Dec/2019:08:52:03 +0000] "GET http://54.179.166.162/ HTTP/1.1" 304 0 "-" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36"
```
##4, Detailed statistics
This is also a command that Varnish will show up in real time when you use it. The varnishstat command displays detailed statistics during the cache processing of Varnish such as how many pages are cached, the HIT and MISS ratio of the cache, the number of error connections between the frontend (Varnish) and the backend server
```angular2html
varnishstat 
```

```angular2html
Uptime mgt:      0+00:42:25                                                                                                   Hitrate n:       10      100      101
Uptime child:    0+00:42:26                                                                                                      avg(n):   0.0000   0.0000   0.0000

    NAME                                                                             CURRENT        CHANGE       AVERAGE        AVG_10       AVG_100      AVG_1000
MGT.uptime                                                                        0+00:42:25
MAIN.uptime                                                                       0+00:42:26
MAIN.sess_conn                                                                             6          0.00          0.00          0.00          0.00          0.00
MAIN.client_req                                                                           14          0.00          0.01          0.00          0.00          0.00
MAIN.cache_hit                                                                             8          0.00          0.00          0.00          0.00          0.00
MAIN.cache_miss                                                                            6          0.00          0.00          0.00          0.00          0.00
MAIN.backend_conn                                                                          3          0.00          0.00          0.00          0.00          0.00
MAIN.backend_reuse                                                                         3          0.00          0.00          0.00          0.00          0.00
MAIN.backend_recycle                                                                       6          0.00          0.00          0.00          0.00          0.00
MAIN.fetch_length                                                                          6          0.00          0.00          0.00          0.00          0.00
MAIN.pools                                                                                 2          0.00           .            2.00          2.00          2.00
MAIN.threads                                                                             200          0.00           .          200.00        200.00        200.00
MAIN.threads_created                                                                     200          0.00          0.08          0.00          0.00          0.00
MAIN.n_objectcore                                                                          1          0.00           .            1.00          1.00          1.00
MAIN.n_objecthead                                                                          1          0.00           .            1.00          1.00          1.00
MAIN.n_backend                                                                             1          0.00           .            1.00          1.00          1.00
MAIN.n_expired                                                                             6          0.00          0.00          0.00          0.00          0.00
MAIN.s_sess                                                                                6          0.00          0.00          0.00          0.00          0.00
MAIN.s_fetch                                                                               6          0.00          0.00          0.00          0.00          0.00
MAIN.s_req_hdrbytes                                                                     6.33K         0.00          2.55          0.00          0.00          0.00
MAIN.s_resp_hdrbytes                                                                    3.69K         0.00          1.48          0.00          0.00          0.00
MAIN.s_resp_bodybytes                                                                  16.21K         0.00          6.52          0.00          0.00          0.00
MAIN.sess_closed_err                                                                       5          0.00          0.00          0.00          0.00          0.00
MAIN.backend_req                                                                           6          0.00          0.00          0.00          0.00          0.00
MAIN.n_vcl                                                                                 1          0.00           .            1.00          1.00          1.00
MAIN.bans                                                                                  1          0.00           .            1.00          1.00          1.00
SMA.s0.c_req                                                                              12          0.00          0.00          0.00          0.00          0.00
SMA.s0.c_bytes                                                                         21.96K         0.00          8.83          0.00          0.00          0.00
SMA.s0.c_freed                                                                         21.96K         0.00          8.83          0.00          0.00          0.00
SMA.s0.g_space                                                                        256.00M         0.00           .          256.00M       256.00M       256.00M
VBE.boot.default.bereq_hdrbytes                                                         2.21K         0.00          0.89          0.00          0.00          0.00
VBE.boot.default.beresp_hdrbytes                                                        1.34K         0.00          0.54          0.00          0.00          0.00
VBE.boot.default.beresp_bodybytes                                                      20.17K         0.00          8.11          0.00          0.00          0.00
VBE.boot.default.req                                             
```

## Read more
 -  [document Varnish office](https://varnish-cache.org/docs)
 
 
