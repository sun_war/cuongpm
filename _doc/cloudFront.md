# [Cloud Front](https://www.mageplaza.com/kb/setup-amazon-cloudfront-cdn-in-magento-2.html)

Amazon CloudFront is a fast content delivery network (CDN) provided by Amazon to securely deliver data, videos, applications, and API to customers around the world with low latency, high transfer speeds. By installing Amazon CloudFront CDN in Magento 2, you assist customers in accessing the closest server instead of VPS or Hosting of your website.

To integrate Amazon CloudFront with Magento 2 through 4 steps, as below:

- Step 1: Create An AWS Account To Start Setting Up Amazon CloudFront CDN In Magento 2
- Step 2: Create A Distribution
- Step 3: Set Up Access-control-allow-origin For The Static Files And Media
- Step 4: Create A New URL For Static And Media

## Step 1: Create An AWS Account To Start Setting Up Amazon CloudFront CDN In Magento 2
After successfully signing up an account at `https://aws.amazon.com/`, please activate your account and enter your credit number in the payment section. Then, expand the Service menu and choose CloudFront. After that, click to the Create Distribution button.

## Step 2: Create A Distribution
Choose one delivery method (Web or RTMP) that is most suitable for your situation. Then, click to the Get Started button. Here, I select the Web as the delivery method.


In the Origin Settings tab, take the following actions:
Origin Domain name: Enter your domain website
Origin Path: Enter the path of static. At your Magento website, two relevant Distributions for Static and Media folders needs to be created. For example: /pub/static
Origin Protocol Policy: Select the origin protocol policy based on your web setting. If your Magento website is using SSL, please select HTTPS only. However, if you want both HTTP and HTTPS to be compatible with Distribution, please choose Match Viewer instead.

In the Default Cache Behavior Settings tab, take the following sections:
Viewer Protocol Policy: Select the policy you want the viewers to use to access your content. From our view, you should use the Viewer Protocol Policy relevant to Origin Protocol Policy.

Allowed HTTP Methods: Select suitable methods. From our view, the last option GET, HEAD, OPTIONS, PUT, POST, PATCH, DELETE is the most compatible with your Magento website.

In the Distribution Settings tab, take the following actions:
In Alternate Domain Names (CNAMEs) field: Enter the subdomain which you want to use to replace the domain name. If you choose this option, you need to access the configuration of the domain then insert the subdomain pointing to (*.cloudfront.net)


Finally, click to the Create Distribution button

Now, you need to wait for around 15 to 40 minutes for CloudFront service to complete the deployment. Then, you will see the following display:


Click to the relevant ID link to check the detail of the distribution created.


You can access the links to review whether the settings are successful or not. For instance:

Origin Path: `https://mageplaza.net/pub/static/version158642331/frontend/Magento/luma/en_US/mage/calendar.css`
The origin path `https://mageplaza.net/pub/static/` will be replaced with the domain name created at the CloudFront `http://d5472poob0l2.cloudfront.net`.
Please access to check: `http://d5472poob0l2.cloudfront.net/version1543319270/frontend/Magento/luma/en_US/mage/calendar.css`
Image

If there is no difference between the file content and the origin file, you have set up Amazon CloudFront CDN successfully.
In case you want to change the domain `http://d5472poob0l2.cloudfront.net` into the subdomain of your Magento 2 website, please navigate to the configuration of the domain and create a CNAME of the subdomain pointing to the above domain:

Then check the assess to `https://static.mageplaza.net/version158642331/frontend/Magento/luma/en_US/mage/calendar.css`

### Step 3: Set Up Access-control-allow-origin For The Static Files And Media
pub/static
The path to change the configuration of pub/static is `pub/static/.htaccess`

The origin file

```angularjs
<IfModule mod_headers.c>
   <FilesMatch .*\.(ico|jpg|jpeg|png|gif|svg|js|css|swf|eot|ttf|otf|woff|woff2)$>
       Header append Cache-Control public
   </FilesMatch>
       Header append Cache-Control no-store
   </FilesMatch>
</IfModule>
```
Please change to

```angularjs
<IfModule mod_headers.c>
<FilesMatch .*\.(ico|jpg|jpeg|png|gif|svg|js|css|swf|eot|ttf|otf|woff|woff2|html|json)$>
   Header set Cache-Control "max-age=604800, public"
   Header set Access-Control-Allow-Origin "*"
   Header set Access-Control-Allow-Methods "GET, OPTIONS"
   Header set Access-Control-Max-Age "604800"
   Header set Access-Control-Allow-Headers "Host, Content-Type, Origin, Accept"
</FilesMatch>
   <FilesMatch .*\.(zip|gz|gzip|bz2|csv|xml)$>
       Header append Cache-Control no-store
   </FilesMatch>
</IfModule>
```
Please note that the html and json extensions will be changed by the extension of the file. Hence, you need to review the configuration Catalog Store, which may relate to security issues, to ensure that the html extension works.

pub/media
Please use the path pub/media/.htaccess to change the header of media files.

The original file is as follow:

```angularjs
<IfModule mod_headers.c>
   <FilesMatch .*\.(ico|jpg|jpeg|png|gif|svg|js|css|swf|eot|ttf|otf|woff|woff2)$>
       Header append Cache-Control public
   </FilesMatch>
   <FilesMatch .*\.(zip|gz|gzip|bz2|csv|xml)$>
       Header append Cache-Control no-store
   </FilesMatch>
</IfModule>
Please change the content of <IfModule mod_headers.c> into:

<FilesMatch .*\.(ico|jpg|jpeg|png|gif|svg|js|css|swf|eot|ttf|otf|woff|woff2)$>
   Header append Cache-Control max-age=604800
   Header set Access-Control-Allow-Origin "*"
</FilesMatch>
```

After modifying the configurations of pub/static and pub/media, your Magento 2 website is ready for the new distribution of static files and media from Amazon CloudFront.

### Step 4: Create A New URL For Static And Media
Please navigate to Configuration > Web > Base URLs and Base URLs (Secure). Enter the domain created into Base URL for Static View Files and Base URL for User Media Files.

Following the above instructions, now, you have almost finished the process of setting up AWS CloudFront CDN for Magento 2. Remember to save all configurations file and clear the cache Go to the Homepage to check the result. Thanks to Amazon CloudFront, all statistic files and media are replaced with the new domain. After setting up Amazon CloudFront in Magento 2, the loading page is improved immediately.