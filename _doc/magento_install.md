#!/bin/bash

cd /magento
composer install

npm install

php -d memory_limit=3G ./bin/magento setup:install \
--admin-firstname=gotanda \
--admin-lastname=test \
--admin-email=sugai@gotandadenshi.jp \
--admin-user=admin \
--admin-password=gotanda2373 \
--base-url=http://web-alb-1727740799.ap-southeast-1.elb.amazonaws.com \
--backend-frontname=admin \
--db-name=magento \
--db-user=admin \
--db-password=Artemis-2019 \
--use-rewrites=1 \
--language=th_TH \
--currency=THB \
--timezone=Asia/Bangkok \
--cleanup-database \
--session-save=redis \
--session-save-redis-host=127.0.0.1 \
--session-save-redis-port=6379 \
--session-save-redis-timeout=3 \
--session-save-redis-db=1 \
--session-save-redis-compression-threshold=2048 \
--session-save-redis-compression-lib=gzip \
--session-save-redis-max-concurrency=10 \
--session-save-redis-break-after-frontend=5 \
--session-save-redis-break-after-adminhtml=30 \
--session-save-redis-first-lifetime=600 \
--session-save-redis-bot-first-lifetime=60 \
--session-save-redis-bot-lifetime=7200 \
--session-save-redis-disable-locking=0 \
--session-save-redis-min-lifetime=60 \
--session-save-redis-max-lifetime=2592000 \
--cache-backend=redis \
--cache-backend-redis-server=127.0.0.1 \
--cache-backend-redis-port=6379 \
--cache-backend-redis-db=2

# set encryption key
sed --in-place -e ':a' -e 'N' -e '$!ba' -e "s/'crypt' => \[\\n        'key' => '[a-z0-9]*'\\n    \],/'crypt' => \[\\n        'key' => 'ed71316f73e0b3bb2e3ebba961470c9c'\\n    \],/g" /magento/app/etc/env.php

cd /magento/var/restore/

# 前提 var/restore にバックアップファイルが一つだけ置いてある状態。
# リストアするファイルのUNIX_TIME部分を変数として取得する。
# TODO エラー処理など

BACKUP_FILE_NAME=`ls -al| grep '_media' |grep '.tgz'| sed -E 's/.* (([0-9]*)_media(|_.*).tgz)/\1/g'`
BACKUP_UNIX_TIME=`ls -al| grep '_media' |grep '.tgz'| sed -E 's/.* (([0-9]*)_media(|_.*).tgz)/\2/g'`

tar -zxvf ./${BACKUP_FILE_NAME} > /dev/null
rm -rf /magento/var/restore/pub/media/catalog/product/cache > /dev/null
cp -rf /magento/var/restore/pub/media /magento/pub > /dev/null

mysql -h test-fec-instance-1.ce4roblr7nax.ap-southeast-1.rds.amazonaws.com -u admin -pArtemis-2019 magento < /magento/var/restore/var/${BACKUP_UNIX_TIME}_db.sql
mysql -h test-fec-instance-1.ce4roblr7nax.ap-southeast-1.rds.amazonaws.com -u admin -pArtemis-2019 magento < /magento/dev/tools/install_scripts/all_env_delete_personal_information.sql
mysql -h test-fec-instance-1.ce4roblr7nax.ap-southeast-1.rds.amazonaws.com -u admin -pArtemis-2019 magento < /magento/dev/tools/install_scripts/local_vagrant_overwrite_config.sql
mysql -h test-fec-instance-1.ce4roblr7nax.ap-southeast-1.rds.amazonaws.com -u admin -pArtemis-2019 magento < /magento/dev/tools/install_scripts/all_env_modify_admin_authorization_rule.sql


# モジュールのアップデート、コンパイルコマンド一式
cd /magento
php -dmemory_limit=2G ./bin/magento cache:flush
php -dmemory_limit=2G ./bin/magento indexer:reindex
php -dmemory_limit=2G ./bin/magento setup:upgrade
php -dmemory_limit=2G ./bin/magento setup:di:compile
php -dmemory_limit=2G ./bin/magento setup:static-content:deploy --jobs 10 -f th_TH en_US --theme Magento/backend --theme Feca/FecaAsia2
php -dmemory_limit=2G ./bin/magento maintenance:status
php -dmemory_limit=2G ./bin/magento maintenance:disable
php -dmemory_limit=2G ./bin/magento maintenance:status

# cron に登録
crontab /magento/dev/tools/install_scripts/local_vagrant_magento_crontab

rm -rf /magento/var/restore/pub
rm -rf /magento/var/restore/var
