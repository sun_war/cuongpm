 # Php 7.2 on Centos7
 
 [Document](https://www.cyberciti.biz/faq/how-to-install-php-7-2-on-centos-7-rhel-7/)
 
 ## Install 
Turn on EPEL repo on a CentOS and RHEL 7.x sever by typing the following command:
```angularjs
sudo yum -y install epel-release
```
 
Turn remi repo too:
```angularjs
sudo yum install http://rpms.remirepo.net/enterprise/remi-release-7.rpm -y
```

Install yum-utils packages too:
```angularjs
sudo yum install yum-utils
```
Enable remi repo, run:
```angularjs
sudo yum-config-manager --enable remi-php72
sudo yum update -y
```
Install php 
```angularjs
sudo yum install php72 -y
```
Install Lib php
 ```angularjs
sudo yum install php-xml php-soap  php-bcmath php-bz2 php-calendar php-ctype php-curl php-dom php-exif php-fileinfo php-ftp php-gd php-gettext php-iconv php-intl php-json php-mbstring php-mysqlnd php-pdo php-phar php-posix php-shmop php-simplexml php-sockets php-sqlite3 php-sysvmsg php-sysvsem php-sysvshm php-tokenizer php-xml php-xmlwriter php-xsl php-mysqli php-pdo_mysql php-pdo_sqlite php-wddx php-xmlreader php-zip -y
```
```angularjs
sudo yum -y install php libapache2-mod-php php-common php-gmp php-curl php-soap php-bcmath php-intl php-mbstring php-xmlrpc php-mcrypt php-mysql php-gd php-xml php-cli php-zip
yum install php72-php-xml -y
```
Fix php use
```angularjs
 cp /usr/bin/php72 /usr/bin/php
```
Check PHP version:
```angularjs
php --version
```

List installed modules
```angularjs
php72 --modules
```
