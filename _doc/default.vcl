vcl 4.0;

import directors;
import std;

# Default backend definition. Set this to point to your content server.
    backend default1 {
.host = "10.10.2.22";
.port = "80";
.first_byte_timeout = 600s;
.probe = {
        .url = "/pub/health_check.php";
.timeout = 2s;
.interval = 5s;
.window = 10;
.threshold = 5;
}
}

backend default2 {
.host = "10.10.3.22";
.port = "80";
.first_byte_timeout = 600s;
.probe = {
        .url = "/pub/health_check.php";
.timeout = 2s;
.interval = 5s;
.window = 10;
.threshold = 5;
}
}



#acl purge {
#    "localhost";
#}

sub vcl_init {
    new bar = directors.round_robin();
    bar.add_backend(default1);
    bar.add_backend(default2);
}


sub vcl_backend_response {
#   unset beresp.http.foo;

    set beresp.ttl = 1d;

    if ((bereq.method == "GET" && bereq.url ~ "\.(css|js|xml|gif|jpg|jpeg|swf|png|zip|ico|img|wmf|txt)$") ||
    bereq.url ~ "\.(minify).*\.(css|js).*" ||
    bereq.url ~ "\.(css|js|xml|gif|jpg|jpeg|swf|png|zip|ico|img|wmf|txt)\?ver") {
        unset beresp.http.Set-Cookie;
        set beresp.ttl = 5d;
    }

# Unset all cache control headers bar Age.
        unset beresp.http.etag;
    unset beresp.http.Cache-Control;
    unset beresp.http.Pragma;

# Unset headers we never want someone to see on the front end
    unset beresp.http.Server;
    unset beresp.http.X-Powered-By;

# Set how long the client should keep the item by default
    set beresp.http.cache-control = "max-age = 300";

# Set how long the client should keep the item by default
    set beresp.http.cache-control = "max-age = 300";

# Override browsers to keep styling and dynamics for longer
        if (bereq.url ~ ".minify.*\.(css|js).*") { set beresp.http.cache-control = "max-age = 604800"; }
    if (bereq.url ~ "\.(css|js).*") { set beresp.http.cache-control = "max-age = 604800"; }

# Override the browsers to cache longer for images than for main content
    if (bereq.url ~ ".(xml|gif|jpg|jpeg|swf|css|js|png|zip|ico|img|wmf|txt)$") {
        set beresp.http.cache-control = "max-age = 604800";
    }

# We're done here, send the data to the browser
    return (deliver);
}


sub vcl_recv {
# Happens before we check if we have this in cache already.
#
# Typically you clean up the request here, removing cookies you don't need,
# rewriting the request, etc.

# Properly handle different encoding types
    if (req.http.Accept-Encoding) {
        if (req.url ~ "\.(jpg|jpeg|png|gif|gz|tgz|bz2|tbz|mp3|ogg|swf|woff)$") {
        # No point in compressing these
            unset req.http.Accept-Encoding;
        } elsif (req.http.Accept-Encoding ~ "gzip") {
            set req.http.Accept-Encoding = "gzip";
        } elsif (req.http.Accept-Encoding ~ "deflate") {
            set req.http.Accept-Encoding = "deflate";
        } else {
        # unknown algorithm (aka crappy browser)
            unset req.http.Accept-Encoding;
        }
    }

# Cache files with these extensions
    if (req.url ~ "\.(js|css|jpg|jpeg|png|gif|gz|tgz|bz2|tbz|mp3|ogg|swf|woff)$") {
        unset req.http.cookie;
        return (hash);
    }

# Dont cache anything thats on the blog page or thats a POST request
    if (req.url ~ "^/blog" || req.method == "POST" || req.url ~ "^/admin" || req.url ~ "^/login" || req.url ~ "^/catalogsearch" || req.url ~ "^/checkout") {
        return (pass);
    }

# This is Laravel specific, we have session-monster which sets a no-session header if we dont really need the set session cookie.
# Check for this and unset the cookies if not required
# Except if its a POST request
    if (req.http.X-No-Session ~ "yeah" && req.method != "POST") {
        unset req.http.cookie;
    }

    return (hash);
}

sub vcl_deliver {
# Happens when we have all the pieces we need, and are about to send the
# response to the client.
#
# You can do accounting or modifying the final object here.

# Lets not tell the world we are using Varnish in the same principle we set server_tokens off in Nginx
    unset resp.http.Via;
    unset resp.http.X-Varnish;
}
