# Magento

## Config PHP
Change limit memory at `/ect/php.ini`. Min is 1G
```angularjs
memory_limit = 2048M
```
```angularjs
sudo vi /etc/httpd/conf/httpd.conf
```
## Config vhost
```angularjs
vi /etc/httpd/conf.d/magento.conf
```

```angularjs
<VirtualHost *:80>
    ServerName localhost
    ServerAlias localhost

    DocumentRoot /var/www/mg2
    <Directory /var/www/mg2/>
        Options Indexes FollowSymLinks MultiViews
        AllowOverride All
    </Directory>
#    ErrorLog /var/www/error.log
#    CustomLog /var/www/requests.log combined
</VirtualHost>
```

```angularjs
tail -f /var/log/httpd/error_log
```

```angularjs
sudo vi /etc/selinux/config
```
Change file `app/ect/env.php`
```angularjs
    'session' => [
        'save' => 'files',
        'save_path' => '/tmp'
    ]
```
## Mysql
Connect with pass `Artemis-2019`
```angularjs
mysql -h test-fec.cluster-ce4roblr7nax.ap-southeast-1.rds.amazonaws.com -u admin -p;
```

Get config
```angularjs
select * from core_config_data;
```

Update domain at table `core_config_data`
```angularjs
update core_config_data set value="http://cuongpm.ml/" where path="web/unsecure/base_url";

update core_config_data set value="http://cuongpm.ml/" where path="web/secure/base_url";

update core_config_data set value="" where path="web/unsecure/base_static_url";

update core_config_data set value="" where path="web/secure/base_static_url";

update core_config_data set value="" where path="web/unsecure/base_media_url";

update core_config_data set value="" where path="web/secure/base_media_url";
```