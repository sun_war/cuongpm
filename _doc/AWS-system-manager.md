# AWS system manager
## IAM User for AWS
- Choose Users/Add user.
- For User name, enter Administrator.
- Select the check box next to AWS Management Console access. Then select Custom password.
- Choose Next: Permissions.
- Under Set permissions, choose Add user to group.
- Choose Create group.
- In the Create group dialog box, for Group name enter Administrators.
- Choose Filter policies, and then select AWS managed -job function to filter the table contents.
- In the policy list, select the check box for AdministratorAccess. Then choose Create group.
- Back in the list of groups, select the check box for your new group. Choose Refresh if necessary to see the group in the list.
- Choose Next: Tags.
- (Optional) Add metadata to the user by attaching tags as key-value pairs. For more information about using tags in IAM, see Tagging IAM Entities in the IAM User Guide.
- Choose Next: Review to see the list of group memberships to be added to the new user. When you are ready to proceed, choose Create user.

## Non-Admin IAM Users and Groups for Systems Manager
- 
