# NFS

## Scenario
- NFS Server IP address: 192.168.2.100
- NFS Client IP address: 192.168.2.101
## Setup and config
### NFS server
Package `nfs-utils`
```angularjs
sudo yum install nfs-utils
```

Make folder share
```angularjs
/var/nfs/share
```

Edit `/etc/exports`
```angularjs
/var/nfs/share 192.168.2.0/24(rw,no_root_squash)
```

Start NFS Server
```angularjs
sudo systemctl start rpcbind nfs-server
```

Set NFS Server start with server
```angularjs
sudo systemctl enable rpcbind nfs-server
```

Check port use by NFS
```angularjs
rpcinfo -p
```

Config Firewall to access
```angularjs
sudo firewall-cmd --permanent --add-service=nfs
sudo firewall-cmd --permanent --add-service=mountd
sudo firewall-cmd --permanent --add-service=rpc-bind
sudo firewall-cmd --permanent --add-port=2049/tcp
sudo firewall-cmd --permanent --add-port=2049/udp
sudo firewall-cmd --reload
```

Check mount point on server
```angularjs
showmount -e localhost
```

### NFS Client
 Install `nfs-utils` và `nfs-utils-lib`
```angularjs
sudo yum install nfs-utils nfs-utils-lib
```

Check mount point on NFS Server from client, use command `showmount -e <NFS_Server_IP>`
```angularjs
showmount -e 192.168.2.100
```

Create and mount folder to mount to NFS Server
```angularjs
mkdir -p /var/vinasupport.com/share
mount -t nfs 192.168.1.101:/var/nfs/share /var/vinasupport.com/share
```
To check the mounted information on the client use the following command
```angularjs
nfsstat -m
```

To automatically mount to the NFS Server when server reboot, fix file `/etc/fstab` adding the following line at end of the file
```angularjs
192.168.2.100:/var/nfs/share /var/vinasupport.com/share nfs rw,sync,hard,intr 0 0
```

## Important file configuration of the NFS
- This is the main configuration of the NFS, containing information of the list of share file and folder on NFS server
```angularjs
/etc/exports
```
- To automatically mount a folder NFS on system when reboot
```angularjs
/etc/fstab 
```
- File config of the NFS to management port listening of rpc and other servers
```angularjs
/etc/sysconfig/nfs
```

## Some command are often used
- `showmount -e` : View folders share on sytem
- `showmount -e` <server-ip or hostname>: View folder share on a Remote Server
- `showmount -d` : View folder
- `exportfs -v` :  View folder and option
- `exportfs -a` : Exports all folder share on /etc/exports
- `exportfs -u` : Unexports all folder share on /etc/exports
- `exportfs -r` : Refresh after edit /etc/exports

Fix lỗi: refused mount request from 192.168.1.108 for /home/me (/home/me): illegal port 64112

Nếu bạn gặp lỗi trên vì cổng trên 1024 thì cần thêm option “insecure” vào file /etc/exports, sau đó chạy exportfs -r để refresh lại.