# Jenkin

Java
```angularjs
yum install java-11-amazon-corretto -y
```

Edit host
```angularjs
sed -i "s/localhost localhost.localdomain localhost4 localhost4.localdomain4/jenkins jenkins.localdomain localhost4 localhost4.localdomain4/g" /etc/hosts
```

Add host name is jenkins
```angularjs
echo "jenkins" > /etc/hostname
```

Add host name to network
```angularjs
echo "HOSTNAME=jenkins.localdomain" >> /etc/sysconfig/network
```

```angularjs
hostnamectl set-hostname jenkins.localdomain
```
Setup
```angularjs
cd /home/ec2-user/
wget -O /etc/yum.repos.d/jenkins.repo http://pkg.jenkins.io/redhat/jenkins.repo
rpm --import https://pkg.jenkins.io/redhat/jenkins.io.key
yum install jenkins -y
systemctl start jenkins
systemctl enable jenkins
```

Check
```angularjs
netstat -ntpl
```

Get password
```angularjs
 cat /var/lib/jenkins/secrets/initialAdminPassword
```