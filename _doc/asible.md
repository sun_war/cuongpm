##Running Ansible Playbooks using EC2 Systems Manager Run Command and State Manager
## 1. Setup 
### 1.1 Config aws cli
Install unzip
```angularjs
sudo yum install -y unzip
```
Download files.
```angularjs
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
```
Unzip the installer
```angularjs
unzip awscliv2.zip
```
Run the install program.
```angularjs
sudo ./aws/install
```
Confirm the installation
```angularjs
aws --version
```

Config
```angularjs
aws configure
```
Setting user attach policies `AmazonEC2RoleforSSM`
## 1.2 Install SSM Agent

```angularjs
sudo yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm
sudo systemctl enable amazon-ssm-agent
sudo systemctl start amazon-ssm-agent
sudo systemctl status amazon-ssm-agent
```

## 1.3 Install Ansible
For RedHat 7 you can install Ansible by enabling the epel repo. Use the following commands:
```angularjs
sudo rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo yum -y install ansible
ansible-galaxy install weareinteractive.vsftpd
```

## 2. State Manager Walkthrough

### Example Setup nginx
vi nginx.yml
```angularjs
- hosts: local
  connection: local
  become: yes
  become_user: root
  tasks:
   - name: Install Nginx
     apt:
       name: nginx
       state: installed
       update_cache: true
     notify:
      - Start Nginx
 
  handlers:
   - name: Start Nginx
     service:
       name: nginx
       state: started
```
Run play box
```angularjs
ansible-playbook nginx.yml
```
Result
```angularjs

PLAY [localhost] ****************************************************************************************************************************************

TASK [Gathering Facts] **********************************************************************************************************************************
ok: [localhost]

TASK [Install Nginx] ************************************************************************************************************************************
fatal: [localhost]: FAILED! => {"changed": false, "msg": "value of state must be one of: absent, build-dep, fixed, latest, present, got: installed"}

PLAY RECAP **********************************************************************************************************************************************
localhost                  : ok=1    changed=0    unreachable=0    failed=1    skipped=0    rescued=0    ignored=0  
```

## Setup Control Machine
Generate ssh-key
```angularjs
ssh-keygen
```

Config hosts
```angularjs
vi hosts
```
```angularjs
[webserver]
ami ansible_ssh_user=centos ansible_ssh_port=2222 ansible_ssh_host=13.250.42.76
```

Copy to server
```angularjs
ssh-copy-id -i ~/.ssh/mykey user@host
# or
cat ~/.ssh/id_rsa.pub | ssh -i fec.pem centos@13.250.42.76 -p 2222 "cat - >> ~/.ssh/authorized_keys2"
```

Ping to local
```angularjs
ansible -i ./hosts --connection=local all -m ping
```

Ping to server
```angularjs
 ansible -i hosts ami -m ping
```

Check setup
```angularjs
ansible -i hosts all  -m setup
```

Run command to node
```angularjs
ansible -i hosts all -m service -a "name=httpd state=started"
ansible -i ./hosts ami -b --become-user=root -m shell -a "touch /var/www/xyz.abc"
```



