Config access key
```angularjs
aws2 configure
```

Create new repo
```angularjs
 aws2 codecommit create-repository --repository-name {repository-name} --repository-description "{repository-description}"
```

Result
```angularjs
{
    "repositoryMetadata": {
        "accountId": "083795662752",
        "repositoryId": "7123d206-ffaf-4bb7-bf58-76c6475bf5a9",
        "repositoryName": "demorepo",
        "repositoryDescription": "my demo repo",
        "lastModifiedDate": "2019-12-31T03:14:53.818000+00:00",
        "creationDate": "2019-12-31T03:14:53.818000+00:00",
        "cloneUrlHttp": "https://git-codecommit.ap-southeast-1.amazonaws.com/v1/repos/demorepo",
        "cloneUrlSsh": "ssh://git-codecommit.ap-southeast-1.amazonaws.com/v1/repos/demorepo",
        "Arn": "arn:aws:codecommit:ap-southeast-1:083795662752:demorepo"
    }
}
```

Get info repo
```angularjs
aws2 codecommit get-repository --repository-name {repository-name}
```

Get all branch
```angularjs
aws2 codecommit list-branches --repository-name demorepo {repository-name}
```

Get commit
```angularjs
aws2 codecommit get-commit --repository-name {repository-name} --commit-id {commit-id}
```

Create new branch 
```angularjs
create-branch --repository-name {repository-name} --branch-name {branch-name} --commit-id {commit-id}
```

```angularjs
aws2 codecommit update-default-branch --repository-name {repository-name} --default-branch-name {branch-name}
```

List command
```angularjs
list-repositories
create-repository
get-repository
list-branches
create-branch
get-branch
get-commit
delete-branch
delete-repository
```
