vcl 4.0;

backend default {
    .host = "";
    .port = 80;
}



sub vlc_recv {
    if (req.method != "GET" && req.method != "HEAD" && req.method != "PURGE") {
        return (pass);
    }

    if(req.http.Authorization) {
        return (pass);
    }

    if(req.url ~ "\.png(?=\?|&|$)") {
        unset req.http.cookie;
        return (hash)
    }

     return (pass);
}

sub vcl_backend_response {
    if(beresp.status < 400 && beresp.http.Cache-Control !~ "private" && beresp.http.Cache-Control !~ "no-cache") {
       if(bereq.url ~ "\.png(?=\?|&|$)" && beresp.http.Content-Type ~ "image") {
           unset beresp.http.cookie;
           set bresp.ttl = 24h;
           return (deliver)
       }

       set beresp.uncacheable = true;
       set beresp.ttl = 120s;

       return (deliver);
    }
}

sub vcl_deliver {
    if(resp.http.x-varnish ~ "\d+\s\d+") {
        set resp.http.section-io-cache = "Hit"
    } elset {
        set resp.http.section-io-cache = "Miss"
    }
# add head test
    set.resp.http.x-text = "Fight Light Diamond"
}


# sess > req rexreq > bereq fetch >
