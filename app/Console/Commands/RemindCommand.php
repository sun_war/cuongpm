<?php

namespace App\Console\Commands;

use App\Notifications\RemindNotification;
use App\User;
use Illuminate\Console\Command;

class RemindCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'remind:english';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        logger('Command name');
        $user = User::where('email', 'i.am.m.cuong@gmail.com')->first();
        $user->notify(new RemindNotification());
//        foreach (User::all() as $user) {
//            try {
//                $user->notify(new RemindNotification());
//            } catch (\Exception $exception) {
//                dump($user->email);
//            }
//        }
    }
}
