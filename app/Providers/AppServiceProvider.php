<?php

namespace App\Providers;

use ACL\ACLServiceProvider;
use BlockChain\Providers\BlockChainServiceProvider;
use Bugger\BuggerServiceProvider;
use Bugger\EventBuggerServiceProvider;
use Illuminate\Support\Facades\DB;
use Landing\LandingServiceProvider;
use ECommerce\Providers\ECommerceServiceProvider;
use English\EnglishServiceProvider;
use Games\GameServiceRepository;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use IO\IOServiceProvider;
use Laravel\Passport\Passport;
use Media\Providers\MediaServiceProvider;
use Modularization\ModularizationServiceProvider;
use PassportHmac\Providers\PassportHmacServiceProvider;
use Patterns\PatternsServiceProvider;
use Profile\ProfileServiceProvider;
use ReCaptcha\ReCaptchaServiceProvider;
use Studio\StudioServiceProvider;
use Test\TestServiceProvider;
use Tutorial\Providers\TutorialServiceProvider;
use Vincent\Providers\VincentServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Schema::defaultStringLength(191);

        Passport::tokensExpireIn(now()->addDays(15));
        Passport::refreshTokensExpireIn(now()->addDays(30));
        Passport::pruneRevokedTokens();

//        Resource::withoutWrapping();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->share('asset_url', config('app.asset_url'));

        $this->app->register(ACLServiceProvider::class);
        $this->app->register(BlockChainServiceProvider::class);
        $this->app->register(BuggerServiceProvider::class);

        $this->app->register(EventBuggerServiceProvider::class);
        $this->app->register(ECommerceServiceProvider::class);
        $this->app->register(GameServiceRepository::class);

        $this->app->register(LandingServiceProvider::class);

        $this->app->register(IOServiceProvider::class);
        $this->app->register(MediaServiceProvider::class);
        $this->app->register(ModularizationServiceProvider::class);

        $this->app->register(PatternsServiceProvider::class);
        $this->app->register(ProfileServiceProvider::class);
        $this->app->register(PassportHmacServiceProvider::class);

        $this->app->register(ReCaptchaServiceProvider::class);

        $this->app->register(StudioServiceProvider::class);
        $this->app->register(TestServiceProvider::class);
        $this->app->register(TutorialServiceProvider::class);
        $this->app->register(VincentServiceProvider::class);

        DB::enableQueryLog();

        DB::listen(function ($query) {
            $ignoreKyes = ['insert into `jobs`', 'select * from `jobs`'];

            foreach ($ignoreKyes as $key) {
                if (substr($query->sql, 0, strlen($key)) === $key) {
                    return;
                }
            }

            logger()->debug('SQL', [
                'sql' => $query->sql,
                'bindings' => $query->bindings,
                'runtime' => $query->time
            ]);
        });
    }
}
