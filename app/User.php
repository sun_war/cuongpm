<?php

namespace App;

use ACL\Models\Role;
use ACL\Models\VerifyUser;
use ACL\Http\Repositories\PermissionRepository;
use ACL\Http\Repositories\RoleRepository;
use App\Uploads\UserUpload;
use Uploader\UploadAble;
use BlockChain\Models\Account;
use BlockChain\Models\Crypto;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Cache;
use Laravel\Passport\HasApiTokens;
use Modularization\MultiInheritance\ModelsTrait;
use Profile\Models\UserProfile;
use SMartins\PassportMultiauth\HasMultiAuthApiTokens;


class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
    use ModelsTrait;
//    use HasMultiAuthApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = ['first_name', 'last_name', 'code', 'email', 'phone_number', 'sex', 'password', 'birthday',
        'address', AVATAR_COL, 'remember_token', 'is_active', 'last_login', 'last_logout'];


    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['full_name'];

    public function scopeFilter($query, $input)
    {
        if (isset($input['email'])) {
            $query->where('email', 'LIKE', '%' . $input['email'] . '%');
        }
        return $query;
    }

    function getFullNameAttribute() {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user', 'user_id', 'role_id');
    }

    public function isSuperAdmin()
    {
        return $this->roles()->where('name', 'admin')->count();
    }

    public function hasRole($name, RoleRepository $repository)
    {
        return $repository->is($name);
    }

    public function hasPermission($name, PermissionRepository $repository)
    {
        return $repository->is($name);
    }

    public function getAuthIdentifier()
    {
        return $this->getKey();
    }

    const OTP_CACHE_LIVE_TIME = 0.5; // 30s

    public function verifyUser()
    {
        return $this->hasOne(VerifyUser::class, 'user_id');
    }

    public function verifyOtp($authenticationCode)
    {
        $googleAuthenticator = new \PHPGangsta_GoogleAuthenticator();
        $result = $googleAuthenticator->verifyCode($this->google_authentication, $authenticationCode, 0);

        if ($result) {
            $key = "OTPCode:{$this->id}:{$authenticationCode}";

            if (Cache::get($key) !== $authenticationCode) {
                Cache::put($key, $authenticationCode, User::OTP_CACHE_LIVE_TIME);
            }
        }

        return $result;
    }

    public function account()
    {
        return $this->hasOne(Account::class, 'user_id');
    }

    public function crypto()
    {
        return $this->hasOne(Crypto::class, 'user_id');
    }

    public function profile()
    {
        return $this->hasOne(UserProfile::class, 'user_id');
    }

    protected $checkbox = ['is_active'];

    public function modelUploader()
    {
        return UserUpload::class;
    }
}
