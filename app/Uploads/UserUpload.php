<?php
/**
 * Created by PhpStorm.
 * User: cuongpm
 * Date: 6/19/19
 * Time: 10:14 AM
 */

namespace App\Uploads;


use Uploader\UploadModel;

class UserUpload extends UploadModel
{
    public $fileUpload = [AVATAR_COL => 1];

    public $pathUpload = [AVATAR_COL => '/images/users'];

    public $thumbImage = [
        AVATAR_COL => [
            '/thumbs/' => [
                [200, 200], [300, 300], [400, 400]
            ]
        ]
    ];
}