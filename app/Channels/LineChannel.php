<?php

namespace App\Channels;

use Illuminate\Notifications\Notification;
use App\Http\Services\LineNotifyService;
use Log;

class LineChannel
{
    /**
     * Send the given notification.
     *
     * @param  mixed $notifiable
     * @param  \Illuminate\Notifications\Notification $notification
     * @return void
     */
    private $lineNotifyService;

    public function send($notifiable, Notification $notification)
    {

        $this->lineNotifyService = new LineNotifyService();

        $message = $notification->toLine($notifiable);

        $this->lineNotifyService->sendNotification($message, $notifiable->id);
        // Send notification to the $notifiable instance...
    }
}