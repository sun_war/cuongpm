<?php

namespace GQL;

use Illuminate\Support\ServiceProvider;

class GQLServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'acl');
        $this->loadRoutesFrom(__DIR__ . '/routers/web.php');
        $this->loadRoutesFrom(__DIR__ . '/routers/api.php');
        $this->loadRoutesFrom(__DIR__ . '/routers/admin.php');
        $this->loadMigrationsFrom(__DIR__ .'/database/migrations');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
