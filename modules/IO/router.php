<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 3/18/19
 * Time: 10:29 AM
 */
Route::group(
    [
        'namespace' => 'IO\Http\Controllers',
        'middleware' => ['web', 'auth'],
    ],
    function () {
        Route::get('send-msg', 'MessageController@index')->name('msg');
        Route::post('send', 'MessageController@send')->name('send');

        Route::get('/post', 'PostController@index');

        Route::get('/groups', 'GroupController@index');
        Route::post('/groups/{id}/notify', 'GroupController@notify')->name('notify');

        Route::resource('messages' , 'MessageController');
    }
);

//Route::group(
//    [
//        'namespace' => 'IO\Http\Controllers\API',
//        'middleware' => ['api'],
//        'prefix' => ['api'],
//        'name' => 'api.'
//    ],
//    function () {
//        Route::resource('messages' , 'MessageApiController');
//    }
//);
