/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */
import Echo from "laravel-echo"
window.io = require('socket.io-client');

window.echo = new Echo({
    broadcaster: 'socket.io',
    host: window.location.hostname + ':6001',
});


require('./chat/group');
require('./chat/message');


window.echo.channel('aml')
  .listen('.BoughtAmlEvent', (res) => {
    const msg = `BoughtAmlEvent amount(${res.amount}) AML please write code to update frontend`;
    alert(msg);
  })
  .listen('.TransactionAmlCurrentEvent', (res) => {
    const msg = `TransactionAmlCurrentEvent amount(${res.amount}) AML please write code to update frontend`;
    alert(msg);
  });

const userId = $('meta[name=user-id]').attr('content');
window.echo.private(`channel-private.${userId}`)
  .listen('.test', (res) => {
    alert(res.data)
  });
