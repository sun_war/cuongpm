@extends('edu::layouts.app')
@section('content')
    <div class="container-fluid">
        <form class=" text-info form-group" action="{{route('parity.play')}}" method="POST" id="gameForm">

            <div class="row">
                <div class="col-lg-12">
                    <h1 class="text-center"><strong>Chẵn lẻ</strong></h1>
                </div>
                <div class="col-md-4 col-md-offset-4">
                    <div class="btn-group btn-group-justified form-group " role="group" aria-label="...">
                        @for($i = 1; $i < 7; $i++)
                            <div class="btn-group" role="group">
                                <button type="button"
                                        class="resultBtn btn btn-default btn-xs {{isset($number) && $number === $i ? 'btn-danger' : ''}}">{{$i}}</button>
                            </div>
                        @endfor
                    </div>
                </div>
                <div class="col-md-4 col-md-offset-4">
                    {!! csrf_field() !!}
                    <ul class="list-group">
                        <li class="list-group-item active">Đỗ thánh thử tài</li>
                        <li class="list-group-item bettingInput {{isset($result) && $result == 1 ? 'list-group-item-success' : ''}}">
                            Chẵn
                            <input required type="radio" {{ isset($betting) && $betting === '1' ? 'checked' : ''}} value="1"
                                   class="pull-right" name="betting">
                        </li>
                        <li class="list-group-item bettingInput {{isset($result) && $result == 0 ? 'list-group-item-success' : ''}}"> Lẻ
                            <input required type="radio"
                                   {{ isset($betting) && $betting  === '0' ? 'checked' : ''}} value="0" class="pull-right"
                                   name="betting">
                        </li>
                    </ul>
                    <div class="form-group">
                        <ul class="list-group">
                            <li class="list-group-item active">Đặt coin</li>
                            <li class="list-group-item ">
                                <input type="number" id="coin" value="{{ isset($coin) ? $coin : 1 }}"
                                       class="form-control form-group" placeholder="coin" name="coin">
                                <strong id="number"></strong>
                            </li>
                            {{--<li class="list-group-item"><input type="checkbox" value="1" placeholder="coin" name="coin">--}}
                                {{--Tuyệt chiêu siêu vô địch đắc thắng thủ--}}
                            {{--</li>--}}
                        </ul>
                        <lable class="text-success">Leverage(Kai o ken)</lable>
                        <input type="range" min="1" max="100" value="1" name="leverage" id="leverage">
                        <input type="number" class="form-control" min="1" max="100" value="1" id="leverageText">
                    </div>
                    <div class="text-center form-group">
                        <button class="btn btn-success btn-icon" id="playBtn">Play <i class="fa fa-bitcoin"></i></button>
                    </div>
                </div>
            </div>
            <input type="hidden" name="code" id="code">
            @include('gm::layouts.component.items')
        </form>
    </div>

@endsection

@push('head')
    <style>
        .main-content {
            background: url('https://i.pinimg.com/originals/6d/e0/9a/6de09a00df91e01b276aa5a70fcb92ea.jpg') !important;
        }
    </style>
@endpush

@push('js')

    <script src="{{asset('build/forceForm.js')}}"></script>
    <script>
        const leverage = '#leverage';
        const leverageText = '#leverageText';

        let opts = {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-bottom-full-width",
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        const game = {
            play: () => {
                const moneyTotal = '#moneyTotal';
                const itemSpecial = '.itemSpecial';
                const codeInput = '#code';
                $(itemSpecial).click( function () {
                    const self = $(this);
                    $(itemSpecial).css('background', 'white');
                    const codeVal = $(code).val();
                    let id = self.attr('id');
                    if(codeVal === id)
                    {
                        id = '';
                    } else {
                        self.css('background', 'green');
                    }
                    $(codeInput).val(id);
                });
                const playBtn = '#playBtn';
                const gameForm = '#gameForm';
                const url = $(gameForm).attr('action');
                const resultBtn = '.resultBtn';
                const bettingInput = '.bettingInput';
                $(playBtn).click(function (e) {
                    e.preventDefault();
                    const self = $(this);
                    const data = $(gameForm).serialize();
                    $.ajax({
                        url: url,
                        data: data,
                        method: 'POST',
                        beforeSend: ()=> {
                            self.prop('disabled', true);
                        },
                        success: function (data) {
                            console.log(data)
                            $(moneyTotal).html(format.number(data.money, moneyTotal));
                            self.prop('disabled', false);
                            toastr.info(data.msg, "", opts);
                            $(resultBtn).removeClass('btn-success');
                            $(resultBtn).eq( data.number - 1 ).addClass('btn-success');
                            $(bettingInput).removeClass('list-group-item-success');
                            if(data.number % 2 === 0) {
                                $(bettingInput + ':even').addClass('list-group-item-success');
                            } else {
                                $(bettingInput + ':odd').addClass('list-group-item-success');
                            }
                        },
                        error: function () {
                            self.prop('disabled', false);
                            alert('ERROR')
                        }
                    });
                });
            }
        };
        game.play();
        $('#coin').magicFormatNumber('#number')
    </script>
@endpush