<div class="row">
    <div class="col-lg-12">
        <h2><strong> Sử dụng vật phẩm</strong></h2>
    </div>
    @foreach(\ECommerce\Models\Product::where('category_id', 1)->get() as $item)
        <div class="col-lg-2 col-md-3 col-sm-4"
             data-toggle="popover"
             data-trigger="hover"
             data-placement="top"
             data-html=true
             data-content="{{$item->intro}} {{$item->details}}"
                data-original-title="{{$item->name}}"
        >
            <div class="thumbnail form-group text-center itemSpecial"  id="{{$item->code}}">
                <img class="img-responsive" src="{{\ViewFa::thumb($item->image, [400, 300])}}" alt="">
                <h5><strong>{{$item->name}}</strong></h5>
            </div>
        </div>
    @endforeach
</div>
