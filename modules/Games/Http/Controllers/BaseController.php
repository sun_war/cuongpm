<?php
/**
 * Created by PhpStorm.
 * User: CPM
 * Date: 6/11/2018
 * Time: 8:37 PM
 */

namespace Games\Http\Controllers;


use Games\Core\Factories\EffectFactory;
use Illuminate\Http\Request;
use Logger\Supports\Events\CoinLogEvent;

class BaseController
{
    protected $coin, $win, $betting, $money, $code, $msg, $user, $number, $view;

    public function __construct(Request $request)
    {
        $this->coin = (double)$request->get(COIN);
        $this->betting = $request->get(BETTING);
        $this->code = $request->get('code');
    }

    public function index()
    {
        return view($this->view);
    }

    public function play(Request $request)
    {
        if ($this->valid()) {
            $this->law();
            $this->setMsg();
        }
        if ($request->ajax()) {
            return response()->json([
                'msg' => $this->msg,
                'win' => $this->win,
                'money' => auth()->user()->coin,
                'number' => $this->number
            ], 200);
        }
        return view($this->view, ['number' => $this->number, 'betting' => $this->betting])
            ->with(\request()->all());
    }

    protected function valid()
    {
        $this->money = auth()->user()->coin;
        if (\request()->get(BETTING) === null) {
            $this->msg .= "Bạn hãy đặt cược";
            return false;
        }
        if ($this->money <= 0) {
            $this->msg .= "Số coin của đổ hiệp đã hết. Vui lòng đào coin để chơi tiếp";
            return false;
        }
        if ($this->coin > $this->money) {
            $this->msg .= 'Độ hiệp không thể đặt số coin lớn hơn tải sản hiện có';
            return false;
        }
        return true;
    }

    protected function mark($code, $coin)
    {
        $data = app(EffectFactory::class)
            ->product($code, $coin, $this->win);
        $this->coin = $data[COIN];
        $this->msg .= $data['msg'];
        $this->log();
        return $data;
    }

    protected function log()
    {
        $log = [
            COIN_COL => auth()->user()->coin,
            DEAL_COL => (double)$this->coin,
            'created_by' => auth()->id(),
        ];
//        event(new CoinLogEvent($log));
    }

    protected function setMsg()
    {
        if ($this->coin > 0) {
            $status = SUCCESS;
            $this->msg .= " Chúc mừng đỗ thánh đã giành chiến thắng. Giành được " . number_format($this->coin) . " coin";
        } else {
            $status = ERROR;
            $this->msg .= " Chúc đỗ hiệp may mắn lần sau";
        }
        session()->flash($status, $this->msg);
    }
//
//    private function law()
//    {
//        $this->mark($this->code, $this->coin);
//    }
}
