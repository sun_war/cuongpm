<?php
/**
 * Created by PhpStorm.
 * User: JK
 * Date: 2/23/2018
 * Time: 7:31 PM
 */

namespace Games\Http\Controllers;

use Illuminate\Http\Request;

class ParityController extends BaseController
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        $this->view = 'gm::parity.index';;
    }

    public function law()
    {
        $this->number = rand(1, 6);
        $result = ($this->number % 2 === 0);
        $this->win = (boolean)$this->betting === $result;
        if ($this->win === false) {
            $this->coin = 0 - $this->coin;
        }
        $this->mark($this->code, $this->coin);
    }
}