<?php

namespace Game\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LottRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            BETTING => 'request',
            COIN => 'min:1|max:' . auth()->user()->coin
        ];
    }
}
