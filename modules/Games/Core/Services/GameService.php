<?php
/**
 * Created by PhpStorm.
 * User: cuongpm
 * Date: 6/5/18
 * Time: 3:18 PM
 */

namespace Games\Core\Services;

use Games\Core\Factories\EffectFactory;
use Logger\Supports\Events\CoinLogEvent;

class GameService
{
    protected $win, $msg, $coin;

    public function mark($code, $coin)
    {
        $data = app(EffectFactory::class)
            ->product($code, $coin, $this->win);
        $this->coin = $data[COIN];
//        $this->log();
        return $data;
    }

    public function log()
    {
        $log = [
            COIN_COL => auth()->user()->coin,
            DEAL_COL => (double)$this->coin,
            'created_by' => auth()->id(),
        ];
//        event(new CoinLogEvent($log));
    }

}
