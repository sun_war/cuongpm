<?php
/**
 * Created by PhpStorm.
 * User: cuongpm
 * Date: 5/31/18
 * Time: 12:47 PM
 */

namespace Games\Core\Factories;


class EffectFactory
{
    private $user, $msg, $coinPlay, $coin;

    public function __construct()
    {

    }

    public function product($code, $coinPlay, $win)
    {
        $this->user = auth()->user();
        $this->coin = $this->user->coin;
        $this->coinPlay = $coinPlay;
        switch ($code) {
            case 'cardOfSoulSalvation':
                $this->cardOfSoulSalvation($win);
                break;
            case 'angelOfVincent':
                $this->angelOfVincent($win);
                break;
            case 'ringsOfVincent':
                $this->ringsOfVincent($win);
                break;
            case 'holyWomenOfObsessions':
                $this->holyWomenOfObsessions($win);
                break;
            case 'deathMatch':
                $this->deathMatch($win);
                break;
            case 'Kaioken':
                $this->Kaioken($win);
                break;
            default:
                $this->end();
        }
        return [ 'coin' => $this->coinPlay, 'msg' => $this->msg];
    }

    public function cardOfSoulSalvation($win)
    {
        if (((double)$this->coinPlay + $this->coin) == 0 && $win === false) {
            $this->coinPlay /= 2;
            $this->msg .= "Đỗ thánh vừa kích hoạt thẻ cứu thế linh hồn, giảm thiệt hại " . number_format($this->coinPlay);
        }
        $this->end();
        return ['msg' => $this->msg, 'coin' => $this->coinPlay];
    }

    public function angelOfVincent($win)
    {
        if ($win) {
            $this->msg .= 'Thiên thần của vincent đã được kích hoạt hiệu ứng đặc biệt, phần thưởng được X2 .';
            $this->coinPlay = $this->coinPlay * 2;
        }
        $this->end();
        return ['msg' => $this->msg, 'coin' => $this->coinPlay];
    }

    public function ringsOfVincent($win)
    {
        if ($win === false) {
            $this->msg .= 'Nhẫn thần của vincent tái xuất giang hồ, bảo toàn tài sản cho chủ nhân. ';
            $this->coinPlay = $this->coinPlay / 2;
        }
        $this->end();
        return ['msg' => $this->msg, 'coin' => $this->coinPlay];
    }

    public function holyWomenOfObsessions($win)
    {
        if ($win === false) {
            $this->coinPlay = $this->coin - $this->coinPlay;
            $this->msg .= "Thánh nữ của ám ảnh đảo nghịch càn khôn biết thua thành thắng. " . number_format($this->coinPlay);
        } else {
            $this->coinPlay = $this->coinPlay / -2;
            $this->msg .= "Thánh nữ của ám ảnh đang ở chế độ ám ảnh khi người chơi thắng bạn vừa mất  " . number_format($this->coinPlay);
        }

        $this->end();
        return ['msg' => $this->msg, 'coin' => $this->coinPlay];
    }

    public function deathMatch($win)
    {
        if ($win === false) {
            return 0;
        }
        $this->end();
        return ['msg' => $this->msg, 'coin' => $this->coinPlay];
    }

    public function Kaioken($win)
    {
        if ($win === false) {
            $this->coinPlay = $this->coin - $this->coinPlay;
            $this->msg .= "Kaioken thất bại -" . number_format($this->coinPlay);
        } else {
            $this->coinPlay += $this->coinPlay * request('leverage');
            $this->msg .= "Kaioken thành công +" . number_format($this->coinPlay);
        }
    }

    private function end()
    {
        $this->user = auth()->user();
        $this->user->coin += $this->coinPlay;
        $this->user->save();
    }
}