<?php

namespace PassportHmac\Models;


use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class HmacSecret extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'hmac_secrets';
    public $fillable = ['user_id', 'scope', 'secret'];

    public $fileUpload = ['image' => 1];
    protected $pathUpload = ['image' => '/images/hmac_secrets'];

    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [

            ]
        ]
    ];
}

