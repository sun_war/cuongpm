<?php

namespace PassportHmac\Models;


use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class CrazyCourse extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'crazy_courses';
    public $fillable = ['name', 'img', 'description', 'is_active', 'created_by', 'updated_by'];

    public $fileUpload = ['image' => 1];
    protected $pathUpload = ['image' => '/images/crazy_courses'];

    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [

            ]
        ]
    ];
}

