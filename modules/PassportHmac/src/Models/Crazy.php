<?php

namespace PassportHmac\Models;


use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Crazy extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'crazies';
    public $fillable = ['name', 'audio', 'created_by', 'updated_by', 'is_active', 'crazy_course_id'];

    public $fileUpload = ['image' => 1];
    protected $pathUpload = ['image' => '/images/crazies'];

    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [

            ]
        ]
    ];
}

