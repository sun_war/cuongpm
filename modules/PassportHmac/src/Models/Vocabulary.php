<?php

namespace PassportHmac\Models;


use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Vocabulary extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'vocabularies';
    public $fillable = ['word', 'type', 'pronounce', 'meaning', 'image', 'description', 'is_active'];

    public $fileUpload = ['image' => 1];
    protected $pathUpload = ['image' => '/images/vocabularies'];

    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [

            ]
        ]
    ];
}

