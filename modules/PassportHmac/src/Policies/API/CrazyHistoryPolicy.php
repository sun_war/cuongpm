<?php

namespace PassportHmac\Policies;

use PassportHmac\Models\CrazyHistory;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CrazyHistoryPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    private $roleRepository, $permissionRepository;


    public function before($user, $ability)
    {
        // do something
    }

    public function view(User $user, CrazyHistory $crazy_histories)
    {
        if ($this->permissionRepository->is('view_crazy_history')) {
            return true;
        }

        return $user->id === $crazy_histories->user_id;
    }

    public function create(User $user)
    {
        if ($this->permissionRepository->is('create_crazy_history')) {
            return true;
        }

        return false;
    }

    public function update(User $user, CrazyHistory $crazy_histories)
    {
        if ($this->permissionRepository->is('update_crazy_history')) {
            return true;
        }

        return $user->id === $crazy_histories->user_id;
    }

    public function delete($user, CrazyHistory $crazy_histories)
    {
        if ($this->permissionRepository->is('delete_crazy_history')) {
            return true;
        }

        return $user->id === $crazy_histories->user_id;
    }

}
