<?php

namespace PassportHmac\Policies;

use PassportHmac\Models\CrazyDetail;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CrazyDetailPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    private $roleRepository, $permissionRepository;


    public function before($user, $ability)
    {
        // do something
    }

    public function view(User $user, CrazyDetail $crazy_details)
    {
        if ($this->permissionRepository->is('view_crazy_detail')) {
            return true;
        }

        return $user->id === $crazy_details->user_id;
    }

    public function create(User $user)
    {
        if ($this->permissionRepository->is('create_crazy_detail')) {
            return true;
        }

        return false;
    }

    public function update(User $user, CrazyDetail $crazy_details)
    {
        if ($this->permissionRepository->is('update_crazy_detail')) {
            return true;
        }

        return $user->id === $crazy_details->user_id;
    }

    public function delete($user, CrazyDetail $crazy_details)
    {
        if ($this->permissionRepository->is('delete_crazy_detail')) {
            return true;
        }

        return $user->id === $crazy_details->user_id;
    }

}
