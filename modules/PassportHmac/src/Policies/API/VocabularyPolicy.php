<?php

namespace PassportHmac\Policies;

use PassportHmac\Models\Vocabulary;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class VocabularyPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    private $roleRepository, $permissionRepository;


    public function before($user, $ability)
    {
        // do something
    }

    public function view(User $user, Vocabulary $vocabularies)
    {
        if ($this->permissionRepository->is('view_vocabulary')) {
            return true;
        }

        return $user->id === $vocabularies->user_id;
    }

    public function create(User $user)
    {
        if ($this->permissionRepository->is('create_vocabulary')) {
            return true;
        }

        return false;
    }

    public function update(User $user, Vocabulary $vocabularies)
    {
        if ($this->permissionRepository->is('update_vocabulary')) {
            return true;
        }

        return $user->id === $vocabularies->user_id;
    }

    public function delete($user, Vocabulary $vocabularies)
    {
        if ($this->permissionRepository->is('delete_vocabulary')) {
            return true;
        }

        return $user->id === $vocabularies->user_id;
    }

}
