<?php

namespace PassportHmac\Policies;

use PassportHmac\Models\HmacSecret;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class HmacSecretPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    private $roleRepository, $permissionRepository;


    public function before($user, $ability)
    {
        // do something
    }

    public function view(User $user, HmacSecret $hmac_secrets)
    {
        if ($this->permissionRepository->is('view_hmac_secret')) {
            return true;
        }

        return $user->id === $hmac_secrets->user_id;
    }

    public function create(User $user)
    {
        if ($this->permissionRepository->is('create_hmac_secret')) {
            return true;
        }

        return false;
    }

    public function update(User $user, HmacSecret $hmac_secrets)
    {
        if ($this->permissionRepository->is('update_hmac_secret')) {
            return true;
        }

        return $user->id === $hmac_secrets->user_id;
    }

    public function delete($user, HmacSecret $hmac_secrets)
    {
        if ($this->permissionRepository->is('delete_hmac_secret')) {
            return true;
        }

        return $user->id === $hmac_secrets->user_id;
    }

}
