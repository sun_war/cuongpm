<?php

namespace PassportHmac\Policies;

use PassportHmac\Models\News;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class NewsPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    private $roleRepository, $permissionRepository;


    public function before($user, $ability)
    {
        // do something
    }

    public function view(User $user, News $news)
    {
        if ($this->permissionRepository->is('view_news')) {
            return true;
        }

        return $user->id === $news->user_id;
    }

    public function create(User $user)
    {
        if ($this->permissionRepository->is('create_news')) {
            return true;
        }

        return false;
    }

    public function update(User $user, News $news)
    {
        if ($this->permissionRepository->is('update_news')) {
            return true;
        }

        return $user->id === $news->user_id;
    }

    public function delete($user, News $news)
    {
        if ($this->permissionRepository->is('delete_news')) {
            return true;
        }

        return $user->id === $news->user_id;
    }

}
