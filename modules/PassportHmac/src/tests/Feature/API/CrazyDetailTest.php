<?php
/**
 * Created by cuongpm/modularization.
 * Author: Fight Light Diamond i.am.m.cuong@gmail.com
 * MIT: 2e566161fd6039c38070de2ac4e4eadd8024a825
 */

namespace PassportHmac\Tests\Feature\API;


use Modularization\MultiInheritance\TestTrait;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CrazyDetailTest extends TestCase
{
	use TestTrait;

	public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct();
    }

    public function setAuth()
    {
        $this->setUsername( config('modularization.test.user_account.username'));
        $this->setPassword( config('modularization.test.user_account.password'));
        $this->setProvider('users');
    }

    private function getId()
    {
        return \PassportHmac\Models\CrazyDetail::value('id');
    }

    public function getServer()
    {
        return $this->transformHeadersToServerVars($this->getHeader());
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $server = $this->getServer();
		$params = [

		];

		$response = $this->call('GET', route('api.crazy-details.index'), $params, [], [], $server);

        $response->assertStatus(200);
    }

    public function testStore()
    {
        $params = [ 'crazy_id' => rand(1, 9), 'no' => rand(1, 9), 'sentence' => rand(1, 9), 'meaning' => rand(1, 9), 'created_by' => rand(1, 9), 'updated_by' => rand(1, 9), 'is_active' => rand(1, 9), 'time' => rand(1, 9),  ];
        $response = $this->post(route('api.crazy-details.store'), $params, $this->getHeader());

        $response->assertStatus(201);
    }

    public function testShow()
    {
        $response = $this->get(route('api.crazy-details.show', $this->getId()), $this->getHeader());

        $response->assertStatus(200);
    }

    public function testUpdate()
    {
        $params = [ 'crazy_id' => rand(1, 9), 'no' => rand(1, 9), 'sentence' => rand(1, 9), 'meaning' => rand(1, 9), 'created_by' => rand(1, 9), 'updated_by' => rand(1, 9), 'is_active' => rand(1, 9), 'time' => rand(1, 9),  ];
        $response = $this->put(route('api.crazy-details.update', $this->getId()), $params, $this->getHeader());

        $response->assertStatus(200);
    }

    public function testDestroy()
    {
        $response = $this->delete(route('api.crazy-details.destroy', $this->getId()), [], $this->getHeader());

        $response->assertStatus(200);
    }
}
