<?php
/**
 * Created by cuongpm/modularization.
 * Author: Fight Light Diamond i.am.m.cuong@gmail.com
 * MIT: 2e566161fd6039c38070de2ac4e4eadd8024a825
 */

namespace PassportHmac\Tests\Feature\API;


use Modularization\MultiInheritance\TestTrait;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class VocabularyTest extends TestCase
{
	use TestTrait;

	public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct();
    }

    public function setAuth()
    {
        $this->setUsername( config('modularization.test.user_account.username'));
        $this->setPassword( config('modularization.test.user_account.password'));
        $this->setProvider('users');
    }

    private function getId()
    {
        return \PassportHmac\Models\Vocabulary::value('id');
    }

    public function getServer()
    {
        return $this->transformHeadersToServerVars($this->getHeader());
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $server = $this->getServer();
		$params = [

		];

		$response = $this->call('GET', route('api.vocabularies.index'), $params, [], [], $server);

        $response->assertStatus(200);
    }

    public function testStore()
    {
        $params = [ 'word' => rand(1, 9), 'type' => rand(1, 9), 'pronounce' => rand(1, 9), 'meaning' => rand(1, 9), 'image' => rand(1, 9), 'description' => rand(1, 9), 'is_active' => rand(1, 9),  ];
        $response = $this->post(route('api.vocabularies.store'), $params, $this->getHeader());

        $response->assertStatus(201);
    }

    public function testShow()
    {
        $response = $this->get(route('api.vocabularies.show', $this->getId()), $this->getHeader());

        $response->assertStatus(200);
    }

    public function testUpdate()
    {
        $params = [ 'word' => rand(1, 9), 'type' => rand(1, 9), 'pronounce' => rand(1, 9), 'meaning' => rand(1, 9), 'image' => rand(1, 9), 'description' => rand(1, 9), 'is_active' => rand(1, 9),  ];
        $response = $this->put(route('api.vocabularies.update', $this->getId()), $params, $this->getHeader());

        $response->assertStatus(200);
    }

    public function testDestroy()
    {
        $response = $this->delete(route('api.vocabularies.destroy', $this->getId()), [], $this->getHeader());

        $response->assertStatus(200);
    }
}
