<?php
/**
 * Created by cuongpm/modularization.
 * Author: Fight Light Diamond i.am.m.cuong@gmail.com
 * MIT: 2e566161fd6039c38070de2ac4e4eadd8024a825
 */

namespace PassportHmac\Tests\Feature\Admin;


use Modularization\MultiInheritance\TestTrait;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CrazyDetailTest extends TestCase
{
	use TestTrait;

	public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct();
    }

    public function setAuth()
    {
        $this->setUsername(config('modularization.test.admin_account.username'));
        $this->setPassword(config('modularization.test.admin_account.password'));
        $this->setProvider('admins');
    }

    private function getId()
    {
        return \PassportHmac\Models\CrazyDetail::value('id');
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testIndex()
    {
        $server = $this->getServer();
        $params = [

        ];

        $response = $this->call('GET', route('admin.services.index'), $params, [], [], $server);

        $response->assertStatus(200);
    }

    public function testStore()
    {
        $params = [ 'crazy_id' => rand(1, 9), 'no' => rand(1, 9), 'sentence' => rand(1, 9), 'meaning' => rand(1, 9), 'created_by' => rand(1, 9), 'updated_by' => rand(1, 9), 'is_active' => rand(1, 9), 'time' => rand(1, 9),  ];
        $response = $this->post(route('admin.crazy-details.store'), $params, $this->getHeader());

        $response->assertStatus(201);
    }

    public function testShow()
    {
        $response = $this->get(route('admin.crazy-details.show', $this->getId()), $this->getHeader());

        $response->assertStatus(200);
    }

    public function testUpdate()
    {
        $params = [ 'crazy_id' => rand(1, 9), 'no' => rand(1, 9), 'sentence' => rand(1, 9), 'meaning' => rand(1, 9), 'created_by' => rand(1, 9), 'updated_by' => rand(1, 9), 'is_active' => rand(1, 9), 'time' => rand(1, 9),  ];
        $response = $this->put(route('admin.crazy-details.update', $this->getId()), $params, $this->getHeader());

        $response->assertStatus(200);
    }

    public function testDestroy()
    {
        $response = $this->delete(route('admin.crazy-details.destroy', $this->getId()), [], $this->getHeader());

        $response->assertStatus(200);
    }
}
