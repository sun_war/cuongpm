<?php

namespace PassportHmac\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class NewsUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
'intro' => 'required',
'content' => 'required',
'views' => 'required',
'source' => 'required',
'last_view' => 'required',
'active' => 'required',
'hot' => 'required',
'created_by' => 'required',
'updated_by' => 'required',

        ];
    }

    public function messages()
    {
        return [
            
        ];
    }
}
