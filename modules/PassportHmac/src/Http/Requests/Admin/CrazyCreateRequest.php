<?php

namespace PassportHmac\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CrazyCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
'audio' => 'required',
'created_by' => 'required',
'updated_by' => 'required',
'is_active' => 'required',
'crazy_course_id' => 'required',

        ];
    }

    public function messages()
    {
        return [
            
        ];
    }
}
