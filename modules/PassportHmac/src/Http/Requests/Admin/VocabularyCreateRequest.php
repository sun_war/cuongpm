<?php

namespace PassportHmac\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class VocabularyCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'word' => 'required',
'type' => 'required',
'pronounce' => 'required',
'meaning' => 'required',
'image' => 'required',
'description' => 'required',
'is_active' => 'required',

        ];
    }

    public function messages()
    {
        return [
            
        ];
    }
}
