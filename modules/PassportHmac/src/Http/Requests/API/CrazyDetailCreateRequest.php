<?php

namespace PassportHmac\Http\Requests\API;

use Illuminate\Foundation\Http\FormRequest;

class CrazyDetailCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'crazy_id' => 'required',
'no' => 'required',
'sentence' => 'required',
'meaning' => 'required',
'created_by' => 'required',
'updated_by' => 'required',
'is_active' => 'required',
'time' => 'required',

        ];
    }

    public function messages()
    {
        return [
            
        ];
    }
}
