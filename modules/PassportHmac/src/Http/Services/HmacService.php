<?php


use Illuminate\Support\Facades\DB;

class HmacService
{
    const READ = 4;
    const TRADE = 2;
    const WITHDRAW = 1;
    const RTW = 7;
    const R_W = 5;
    const RT_ = 6;
    const _TW = 3;

    public function hmac($data, $key)
    {
        return hash_hmac($this->getAlgo(), $data, $key);
    }

    public function getAlgo()
    {
        return config('passport-hmac.alog', 'sha256');
    }

    public function getKey($scope)
    {
        return DB::table('hmac_secrets')
            ->where('user_id', auth('api')->id())
            ->where('scope', $scope)
            ->value('secret');
    }

    public function compare($params, $hres)
    {
        $hparams = $this->hmac($params, $this->getKey()) ;

        return $hparams === $hres;
    }
}