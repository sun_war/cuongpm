<?php
/**
 * Created by cuongpm/modularization.
 * Author: Fight Light Diamond i.am.m.cuong@gmail.com
 * MIT: 2e566161fd6039c38070de2ac4e4eadd8024a825
 *
 */

namespace PassportHmac\Http\Services\Admin;

use PassportHmac\Http\Repositories\NewsRepository;

class NewsService
{
    private $repository;

    public function __construct(NewsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index($input)
    {
        $input['relationship'] = null;
        $input['sort'] = 'id|desc';

        return $this->repository->myPaginate($input);
    }

    public function create()
    {
        return [];
    }

    public function store($input)
    {
        return $this->repository->store($input);
    }

    public function show($id)
    {
       return $this->repository->find($id);
    }

    public function edit($id)
    {
       return $this->repository->find($id);
    }

    public function update($input, $id)
    {
        $news = $this->repository->find($id);

        return $this->repository->change($input, $news);
    }

    public function destroy($id)
    {
        $news = $this->repository->find($id);

        if (! empty($news)) {
            $this->repository->delete($id);
        }

        return $news;
    }
}