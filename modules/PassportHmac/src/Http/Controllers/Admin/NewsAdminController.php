<?php

namespace PassportHmac\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use PassportHmac\Http\Requests\Admin\NewsCreateRequest;
use PassportHmac\Http\Requests\Admin\NewsUpdateRequest;
use PassportHmac\Http\Resources\Admin\NewsResource;
use PassportHmac\Http\Resources\Admin\NewsResourceCollection;
use PassportHmac\Http\Services\Admin\NewsService;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class NewsAdminController extends Controller
{
    private $service;

    public function __construct(NewsService $service)
    {
        $this->service = $service;
    }

	/**
     * Paginate
     * @group News
     * @authenticated
     *
     * @queryParam id required The fund id. Example: 1
     *
     * @response {
     * "data": [
     *   {
     *    "id": 10,
     *    "created_at": "2019-09-04 10:43:47",
     *    "updated_at": "2019-09-04 10:43:47"
     *   },
     *   {
     *    "id": 9,
     *    "created_at": "2019-09-04 08:56:43",
     *    "updated_at": "2019-09-04 08:56:43"
     *   }
     *  ],
     *  "links": {
     *     "first": "{url}?page=1",
     *     "last": "{url}?page=1",
     *     "prev": null,
     *     "next": null
     *  },
     *  "meta": {
     *     "current_page": 1,
     *     "from": 1,
     *     "last_page": 1,
     *     "path": "{url}",
     *     "per_page": 10,
     *     "to": 2,
     *     "total": 2
     *   }
     * }
     */
    public function index(Request $request)
    {
        try {
            $input = $request->all();
            $data = $this->service->index($input);

           return new NewsResourceCollection($data);
        } catch (\Exception $exception) {
            throw new HttpException(500, $exception->getMessage());
        }
    }

	/**
     * Create
     * @group News
     * @authenticated
     *
     * @bodyParam is_active int required The is active. Example: 1
     *
     * @response {
     *  "is_active": 0,
     *  "updated_at": "2019-09-05 02:34:34",
     *  "created_at": "2019-09-05 02:34:34",
     *  "id": 11
     * }
     *
     */
    public function store(NewsCreateRequest $request)
    {
        try {
            $input = $request->all();
            $news = $this->service->store($input);

            return new NewsResource($news);
        } catch (\Exception $exception) {
            throw new HttpException(500, $exception->getMessage());
        }
    }

	/**
     * Show
     * @group News
     * @authenticated
     *
     *
     * @response {
     *  "is_active": 0,
     *  "updated_at": "2019-09-05 02:34:34",
     *  "created_at": "2019-09-05 02:34:34",
     *  "id": 11
     * }
     *
     */
    public function show($id)
    {
        try {
            $news = $this->service->show($id);

            return new NewsResource($news);
        } catch (\Exception $exception) {
            throw new HttpException(500, $exception->getMessage());
        }
    }

	/**
     * Update
     * @group News
     * @authenticated
     *
     * @bodyParam is_active int optional The is active. Example: 1
     *
     * @response {
     *  "is_active": 0,
     *  "updated_at": "2019-09-05 02:34:34",
     *  "created_at": "2019-09-05 02:34:34",
     *  "id": 11
     * }
     *
     */
    public function update(NewsUpdateRequest $request, $id)
    {
        $input = $request->all();
        try {
            $data = $this->service->update($input, $id);

            return new NewsResource($data);
        } catch (\Exception $exception) {
            throw new HttpException(500, $exception->getMessage());
        }
    }

	/**
     * Destroy
     * @group News
     * @authenticated
     *
     * @response {
     *  "is_active": 0,
     *  "updated_at": "2019-09-05 02:34:34",
     *  "created_at": "2019-09-05 02:34:34",
     *  "id": 11
     * }
     *
     */
    public function destroy($id)
    {
        try {
            $data = $this->service->destroy($id);

            return new NewsResource($data);
        } catch (\Exception $exception) {
            throw new HttpException(500, $exception->getMessage());
        }
    }
}
