<?php

namespace PassportHmac\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use PassportHmac\Http\Requests\Admin\HmacSecretCreateRequest;
use PassportHmac\Http\Requests\Admin\HmacSecretUpdateRequest;
use PassportHmac\Http\Resources\Admin\HmacSecretResource;
use PassportHmac\Http\Resources\Admin\HmacSecretResourceCollection;
use PassportHmac\Http\Services\Admin\HmacSecretService;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class HmacSecretAdminController extends Controller
{
    private $service;

    public function __construct(HmacSecretService $service)
    {
        $this->service = $service;
    }

	/**
     * Paginate
     * @group HmacSecret
     * @authenticated
     *
     * @queryParam id required The fund id. Example: 1
     *
     * @response {
     * "data": [
     *   {
     *    "id": 10,
     *    "created_at": "2019-09-04 10:43:47",
     *    "updated_at": "2019-09-04 10:43:47"
     *   },
     *   {
     *    "id": 9,
     *    "created_at": "2019-09-04 08:56:43",
     *    "updated_at": "2019-09-04 08:56:43"
     *   }
     *  ],
     *  "links": {
     *     "first": "{url}?page=1",
     *     "last": "{url}?page=1",
     *     "prev": null,
     *     "next": null
     *  },
     *  "meta": {
     *     "current_page": 1,
     *     "from": 1,
     *     "last_page": 1,
     *     "path": "{url}",
     *     "per_page": 10,
     *     "to": 2,
     *     "total": 2
     *   }
     * }
     */
    public function index(Request $request)
    {
        try {
            $input = $request->all();
            $data = $this->service->index($input);

           return new HmacSecretResourceCollection($data);
        } catch (\Exception $exception) {
            throw new HttpException(500, $exception->getMessage());
        }
    }

	/**
     * Create
     * @group HmacSecret
     * @authenticated
     *
     * @bodyParam is_active int required The is active. Example: 1
     *
     * @response {
     *  "is_active": 0,
     *  "updated_at": "2019-09-05 02:34:34",
     *  "created_at": "2019-09-05 02:34:34",
     *  "id": 11
     * }
     *
     */
    public function store(HmacSecretCreateRequest $request)
    {
        try {
            $input = $request->all();
            $hmacSecret = $this->service->store($input);

            return new HmacSecretResource($hmacSecret);
        } catch (\Exception $exception) {
            throw new HttpException(500, $exception->getMessage());
        }
    }

	/**
     * Show
     * @group HmacSecret
     * @authenticated
     *
     *
     * @response {
     *  "is_active": 0,
     *  "updated_at": "2019-09-05 02:34:34",
     *  "created_at": "2019-09-05 02:34:34",
     *  "id": 11
     * }
     *
     */
    public function show($id)
    {
        try {
            $hmacSecret = $this->service->show($id);

            return new HmacSecretResource($hmacSecret);
        } catch (\Exception $exception) {
            throw new HttpException(500, $exception->getMessage());
        }
    }

	/**
     * Update
     * @group HmacSecret
     * @authenticated
     *
     * @bodyParam is_active int optional The is active. Example: 1
     *
     * @response {
     *  "is_active": 0,
     *  "updated_at": "2019-09-05 02:34:34",
     *  "created_at": "2019-09-05 02:34:34",
     *  "id": 11
     * }
     *
     */
    public function update(HmacSecretUpdateRequest $request, $id)
    {
        $input = $request->all();
        try {
            $data = $this->service->update($input, $id);

            return new HmacSecretResource($data);
        } catch (\Exception $exception) {
            throw new HttpException(500, $exception->getMessage());
        }
    }

	/**
     * Destroy
     * @group HmacSecret
     * @authenticated
     *
     * @response {
     *  "is_active": 0,
     *  "updated_at": "2019-09-05 02:34:34",
     *  "created_at": "2019-09-05 02:34:34",
     *  "id": 11
     * }
     *
     */
    public function destroy($id)
    {
        try {
            $data = $this->service->destroy($id);

            return new HmacSecretResource($data);
        } catch (\Exception $exception) {
            throw new HttpException(500, $exception->getMessage());
        }
    }
}
