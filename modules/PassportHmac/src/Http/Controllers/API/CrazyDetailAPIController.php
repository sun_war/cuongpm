<?php

namespace PassportHmac\Http\Controllers\API;


use App\Http\Controllers\Controller;
use PassportHmac\Http\Requests\API\CrazyDetailCreateRequest;
use PassportHmac\Http\Requests\API\CrazyDetailUpdateRequest;
use PassportHmac\Http\Resources\API\CrazyDetailResource;
use PassportHmac\Http\Resources\Admin\CrazyDetailResourceCollection;
use PassportHmac\Http\Services\API\CrazyDetailService;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class CrazyDetailAPIController extends Controller
{
    private $service;

    public function __construct(CrazyDetailService $service)
    {
        $this->service = $service;
    }

    /**
     * Paginate
     * @group CrazyDetail
     * @authenticated
     *
     * @queryParam id required The fund id. Example: 1
     *
     * @response {
     * "data": [
     *   {
     *    "id": 10,
     *    "created_at": "2019-09-04 10:43:47",
     *    "updated_at": "2019-09-04 10:43:47"
     *   },
     *   {
     *    "id": 9,
     *    "created_at": "2019-09-04 08:56:43",
     *    "updated_at": "2019-09-04 08:56:43"
     *   }
     *  ],
     *  "links": {
     *     "first": "{url}?page=1",
     *     "last": "{url}?page=1",
     *     "prev": null,
     *     "next": null
     *  },
     *  "meta": {
     *     "current_page": 1,
     *     "from": 1,
     *     "last_page": 1,
     *     "path": "{url}",
     *     "per_page": 10,
     *     "to": 2,
     *     "total": 2
     *   }
     * }
     */
    public function index(Request $request)
    {
        try {
            $input = $request->all();
            $data = $this->service->index($input);

            return new CrazyDetailResourceCollection($data);
        } catch (\Exception $exception) {
            throw new HttpException(500, $exception->getMessage());
        }
    }

	/**
     * Create
     * @group CrazyDetail
     * @authenticated
     *
     * @bodyParam is_active int required The is active. Example: 1
     *
     * @response {
     *  "is_active": 0,
     *  "updated_at": "2019-09-05 02:34:34",
     *  "created_at": "2019-09-05 02:34:34",
     *  "id": 11
     * }
     *
     */
    public function store(CrazyDetailCreateRequest $request)
    {
        try {
            $input = $request->all();
            $crazyDetail = $this->service->store($input);

            return new CrazyDetailResource($crazyDetail);
        } catch (\Exception $exception) {
            throw new HttpException(500, $exception->getMessage());
        }
    }

	/**
     * Show
     * @group CrazyDetail
     * @authenticated
     *
     *
     * @response {
     *  "is_active": 0,
     *  "updated_at": "2019-09-05 02:34:34",
     *  "created_at": "2019-09-05 02:34:34",
     *  "id": 11
     * }
     *
     */
    public function show($id)
    {
        try {
            $crazyDetail = $this->service->show($id);

            return new CrazyDetailResource($crazyDetail);
        } catch (\Exception $exception) {
            throw new HttpException(500, $exception->getMessage());
        }
    }

	/**
     * Update
     * @group CrazyDetail
     * @authenticated
     *
     * @bodyParam is_active int optional The is active. Example: 1
     *
     * @response {
     *  "is_active": 0,
     *  "updated_at": "2019-09-05 02:34:34",
     *  "created_at": "2019-09-05 02:34:34",
     *  "id": 11
     * }
     *
     */
    public function update(CrazyDetailUpdateRequest $request, $id)
    {
        $input = $request->all();
        try {
            $data = $this->service->update($input, $id);

            return new CrazyDetailResource($data);
        } catch (\Exception $exception) {
            throw new HttpException(500, $exception->getMessage());
        }
    }

	/**
     * Destroy
     * @group CrazyDetail
     * @authenticated
     *
     * @response {
     *  "is_active": 0,
     *  "updated_at": "2019-09-05 02:34:34",
     *  "created_at": "2019-09-05 02:34:34",
     *  "id": 11
     * }
     *
     */
    public function destroy($id)
    {
        try {
            $data = $this->service->destroy($id);

            return new CrazyDetailResource($data);
        } catch (\Exception $exception) {
            throw new HttpException(500, $exception->getMessage());
        }
    }
}
