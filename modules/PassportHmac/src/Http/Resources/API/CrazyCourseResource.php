<?php

namespace PassportHmac\Http\Resources\API;

use Illuminate\Http\Resources\Json\Resource;

class CrazyCourseResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
