<?php

namespace PassportHmac\Http\Repositories;


use Modularization\src\MultiInheritance\RepositoryInterfaceExtra;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface HmacSecretRepository
 * @package namespace App\Repositories;
 */
interface HmacSecretRepository extends RepositoryInterface, RepositoryInterfaceExtra
{
    public function myPaginate($input);

    public function store($input);

    public function change($input, $data);

    public function delete($data);

    public function import($file);
}