<?php

namespace PassportHmac\Http\Repositories;


use Modularization\src\MultiInheritance\RepositoryInterfaceExtra;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface VocabularyRepository
 * @package namespace App\Repositories;
 */
interface VocabularyRepository extends RepositoryInterface, RepositoryInterfaceExtra
{
    public function myPaginate($input);

    public function store($input);

    public function change($input, $data);

    public function delete($data);

    public function import($file);
}