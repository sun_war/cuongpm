<?php

namespace ECommerce\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Reseller extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'resellers';
    public $fillable = ['price', 'product_id', 'created_by', 'qty'];

    public $fileUpload = ['image' => 1];
    protected $pathUpload = ['image' => '/images/resellers'];
    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [

            ]
        ]
    ];
}

