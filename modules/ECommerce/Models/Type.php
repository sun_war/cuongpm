<?php

namespace ECommerce\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Type extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'types';
    public $fillable = ['name', IMAGE_COL, 'is_active', DESCRIPTION_COL];

    public function scopeFilter($query, $input)
    {
        if (isset($input['name'])) {
            $query->where('name', 'LIKE', '%' . $input['name'] . '%');
        }
        if (isset($input[IMAGE_COL])) {
            $query->where(IMAGE_COL, $input[IMAGE_COL]);
        }
        if (isset($input['is_active'])) {
            $query->where('is_active', $input['is_active']);
        }
        if (isset($input[DESCRIPTION_COL])) {
            $query->where(DESCRIPTION_COL, $input[DESCRIPTION_COL]);
        }
        return $query;
    }

    public $fileUpload = [IMAGE_COL => 1];
    protected $pathUpload = [IMAGE_COL => '/images/types'];
    protected $thumbImage = [
        IMAGE_COL => [
            '/thumbs/' => [
                [200, 200], [300, 300], [400, 400]
            ]
        ]
    ];
    protected $checkbox = ['is_active'];
}

