<?php

namespace ECommerce\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class OrderDetail extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'order_details';
    public $fillable = ['order_id', 'product_id', QUANTITY_COL, PRICE_COL, 'created_by'];

    public function scopeFilter($query, $input)
    {
        if (isset($input['order_id'])) {
            $query->where('order_id', $input['order_id']);
        }
        if (isset($input['product_id'])) {
            $query->where('product_id', $input['product_id']);
        }
        if (isset($input[QUANTITY_COL])) {
            $query->where(QUANTITY_COL, $input[QUANTITY_COL]);
        }
        if (isset($input[PRICE_COL])) {
            $query->where(PRICE_COL, $input[PRICE_COL]);
        }
        return $query;
    }

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    public $fileUpload = ['image' => 1];
    protected $pathUpload = ['image' => '/images/order_details'];
    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [
                [200, 200], [300, 300], [400, 400]
            ]
        ]
    ];
    protected $checkbox = ['is_active'];
}

