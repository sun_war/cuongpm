<?php

namespace ECommerce\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Order extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'orders';
    public $fillable = ['created_by', 'updated_by', 'is_active'];

    public function scopeFilter($query, $input)
    {
        if (isset($input['created_by'])) {
            $query->where('created_by', $input['created_by']);
        }
        if (isset($input['is_active'])) {
            $query->where('is_active', $input['is_active']);
        }
        if (isset($input['updated_by'])) {
            $query->where('updated_by', $input['updated_by']);
        }
        return $query;
    }

    public function details()
    {
        return $this->hasMany(OrderDetail::class, 'order_id');
    }

    public $fileUpload = ['image' => 1];
    protected $pathUpload = ['image' => '/images/orders'];
    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [
                [200, 200], [300, 300], [400, 400]
            ]
        ]
    ];
    protected $checkbox = ['is_active'];
}

