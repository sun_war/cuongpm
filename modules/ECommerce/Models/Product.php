<?php

namespace ECommerce\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Product extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'products';
    public $fillable = ['brand_id', 'name', PRICE_COL, IMAGE_COL, INTRO_COL, DETAILS_COL, 'code',
        DISCOUNT_COL, VIEWS_COL, TOTAL_COL, RECOMMENDED_COL, 'is_active', 'type_id', 'category_id'];

    public function scopeFilter($query, $input)
    {
        if (isset($input['brand_id'])) {
            $query->where('brand_id', $input['brand_id']);
        }
        if (isset($input['name'])) {
            $query->where('name', 'LIKE', '%' . $input['name'] . '%');
        }
        if (isset($input['code'])) {
            $query->where('code', 'LIKE', '%' . $input['code'] . '%');
        }
        if (isset($input[PRICE_COL])) {
            $query->where(PRICE_COL, $input[PRICE_COL]);
        }
        if (isset($input[IMAGE_COL])) {
            $query->where(IMAGE_COL, $input[IMAGE_COL]);
        }
        if (isset($input[INTRO_COL])) {
            $query->where(INTRO_COL, $input[INTRO_COL]);
        }
        if (isset($input[DETAILS_COL])) {
            $query->where(DETAILS_COL, $input[DETAILS_COL]);
        }
        if (isset($input[DISCOUNT_COL])) {
            $query->where(DISCOUNT_COL, $input[DISCOUNT_COL]);
        }
        if (isset($input[VIEWS_COL])) {
            $query->where(VIEWS_COL, $input[VIEWS_COL]);
        }
        if (isset($input[TOTAL_COL])) {
            $query->where(TOTAL_COL, $input[TOTAL_COL]);
        }
        if (isset($input[RECOMMENDED_COL])) {
            $query->where(RECOMMENDED_COL, $input[RECOMMENDED_COL]);
        }
        if (isset($input['type_id'])) {
            $query->where('type_id', $input['type_id']);
        }
        if (isset($input['is_active'])) {
            $query->where('is_active', $input['is_active']);
        }
        if (isset($input['category_id'])) {
            $query->where('category_id', $input['category_id']);
        }
        if (isset($input['prices'])) {
            $prices = explode('-', $input['prices']);
            $query->where(PRICE_COL, '>=', $prices[0]);
            $query->where(PRICE_COL, '<=', $prices[1]);
        }
        return $query;
    }

    public $fileUpload = [IMAGE_COL => 1];
    protected $pathUpload = [IMAGE_COL => '/images/products'];
    protected $thumbImage = [
        IMAGE_COL => [
            '/thumbs/' => [
                [200, 150], [300, 225], [400, 300]
            ]
        ]
    ];
    protected $checkbox = ['is_active'];
}

