<?php

namespace ECommerce\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ResellerCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'qty' => 'reseller:' . request('product_id'),
        ];
    }

    public function messages()
    {
        return [
            'qty.reseller' => 'Qty too much'
        ];
    }
}
