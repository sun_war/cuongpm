<?php

namespace ECommerce\Http\Controllers;

use App\Http\Controllers\Controller;
use Modularization\Facades\InputFa;
use ECommerce\Models\Category;
use ECommerce\Http\Requests\CategoryCreateRequest;
use ECommerce\Http\Requests\CategoryUpdateRequest;
use ECommerce\Http\Repositories\CategoryRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class CategoryController extends Controller
{
    use ControllersTrait;
    private $repository;
    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['categories'] = $this->repository->myPaginate($input);
        if($request->ajax())
        {
            return view('eco::category.table', $data)->render();
        }
        return view('eco::category.index', $data);
    }

    public function create()
    {
        return view('eco::category.create');
    }

    public function store(CategoryCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('category.index');
    }

    public function show($id)
    {
        $category = $this->repository->find($id);
        if(empty($category))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('eco::category.show', compact('category'));
    }

    public function edit($id)
    {
        $category = $this->repository->find($id);
        if(empty($category))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('eco::category.update', compact('category'));
    }

    public function update(CategoryUpdateRequest $request, $id)
    {
        $input = $request->all();
        $category = $this->repository->find($id);
        if(empty($category))
        {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $category);
        session()->flash('success', 'update success');
        return redirect()->route('category.index');
    }

    public function destroy($id)
    {
        $category = $this->repository->find($id);
        if(empty($category))
        {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
