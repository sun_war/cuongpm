<?php

namespace ECommerce\Http\Controllers;

use App\Http\Controllers\Controller;
use ECommerce\Http\Requests\OrderCreateRequest;
use ECommerce\Http\Requests\OrderUpdateRequest;
use ECommerce\Http\Repositories\OrderRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class OrderController extends Controller
{
    use ControllersTrait;
    private $repository;
    public function __construct(OrderRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['orders'] = $this->repository->myPaginate($input);
        if($request->ajax())
        {
            return view('eco::order.table', $data)->render();
        }
        return view('eco::order.index', $data);
    }

    public function create()
    {
        return view('eco::order.create');
    }

    public function store(OrderCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('order.index');
    }

    public function show($id)
    {
        $order = $this->repository->find($id);
        if(empty($order))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('eco::order.show', compact('order'));
    }

    public function edit($id)
    {
        $order = $this->repository->find($id);
        if(empty($order))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('eco::order.update', compact('order'));
    }

    public function update(OrderUpdateRequest $request, $id)
    {
        $input = $request->all();
        $order = $this->repository->find($id);
        if(empty($order))
        {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $order);
        session()->flash('success', 'update success');
        return redirect()->route('order.index');
    }

    public function destroy($id)
    {
        $order = $this->repository->find($id);
        if(empty($order))
        {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
