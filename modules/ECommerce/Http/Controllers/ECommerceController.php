<?php

namespace ECommerce\Http\Controllers;


use ECommerce\Models\Products;
use ECommerce\Http\Repositories\OrderRepository;
use Illuminate\Http\Request;


class ECommerceController extends BaseController
{

    private $repository;

    public function __construct(OrderRepository $repository)
    {
        $this->repository = $repository;
    }

    public function home()
    {
        return view('eco::home');
    }

    public function order(Request $request)
    {
        $input = $request->all();
        $input['created_by'] = auth()->id();
        $data['orders'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('eco::my-order.table', $data)->render();
        }
        return view('eco::my-order.index', $data);
    }

    public function sell() {

    }
}

