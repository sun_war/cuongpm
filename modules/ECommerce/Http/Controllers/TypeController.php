<?php

namespace ECommerce\Http\Controllers;

use App\Http\Controllers\Controller;
use Modularization\Facades\InputFa;
use ECommerce\Models\Type;
use ECommerce\Http\Requests\TypeCreateRequest;
use ECommerce\Http\Requests\TypeUpdateRequest;
use ECommerce\Http\Repositories\TypeRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class TypeController extends Controller
{
    use ControllersTrait;
    private $repository;
    public function __construct(TypeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['types'] = $this->repository->myPaginate($input);
        if($request->ajax())
        {
            return view('eco::type.table', $data)->render();
        }
        return view('eco::type.index', $data);
    }

    public function create()
    {
        return view('eco::type.create');
    }

    public function store(TypeCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('type.index');
    }

    public function show($id)
    {
        $type = $this->repository->find($id);
        if(empty($type))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('eco::type.show', compact('type'));
    }

    public function edit($id)
    {
        $type = $this->repository->find($id);
        if(empty($type))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('eco::type.update', compact('type'));
    }

    public function update(TypeUpdateRequest $request, $id)
    {
        $input = $request->all();
        $type = $this->repository->find($id);
        if(empty($type))
        {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $type);
        session()->flash('success', 'update success');
        return redirect()->route('type.index');
    }

    public function destroy($id)
    {
        $type = $this->repository->find($id);
        if(empty($type))
        {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
