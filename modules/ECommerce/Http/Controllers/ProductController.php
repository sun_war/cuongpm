<?php

namespace ECommerce\Http\Controllers;

use App\Http\Controllers\Controller;
use Landing\Models\About;
use Landing\Models\Slide;
use Modularization\Facades\InputFa;
use ECommerce\Models\Product;
use ECommerce\Http\Requests\ProductCreateRequest;
use ECommerce\Http\Requests\ProductUpdateRequest;
use ECommerce\Http\Repositories\ProductRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class ProductController extends Controller
{
    use ControllersTrait;
    private $repository;
    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['products'] = $this->repository->myPaginate($input);
        if($request->ajax())
        {
            return view('eco::product.table', $data)->render();
        }
        return view('eco::product.index', $data);
    }

    public function create()
    {
        return view('eco::product.create');
    }

    public function store(ProductCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('product.index');
    }

    public function show($id)
    {
        $product = $this->repository->find($id);
        if(empty($product))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('eco::product.show', compact('product'));
    }

    public function edit($id)
    {
        $product = $this->repository->find($id);
        if(empty($product))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('eco::product.update', compact('product'));
    }

    public function update(ProductUpdateRequest $request, $id)
    {
        $input = $request->all();
        $product = $this->repository->find($id);
        if(empty($product))
        {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $product);
        session()->flash('success', 'update success');
        return redirect()->route('product.index');
    }

    public function destroy($id)
    {
        $product = $this->repository->find($id);
        if(empty($product))
        {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }

    public function getShowProduct(Request $request){
        $input = $request->all();
        $input[PER_PAGE] = 9;
        $products = $this->repository->myPaginate($input);
        if($request->ajax()) {
            return view('eco::product.component.product-list', compact('products'))->render();
        }
        $slides = app(Slide::class)->where('is_active', 1)->orderBy(NO_COL)->get();
        $about = app(About::class)
            ->where('is_active', 1)
            ->orderBy('id', 'DESC')
            ->first();

        return view('eco::product.showProduct', compact('products', 'slides', 'about'));
    }

    function detail($id){
        return view('eco::product.detail')->with('product', $this->repository->find($id));
    }
}
