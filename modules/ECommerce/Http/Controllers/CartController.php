<?php
/**
 * Created by PhpStorm.
 * User: 340
 * Date: 5/23/2015
 * Time: 9:56 AM
 */

namespace ECommerce\Http\Controllers;

use App\Http\Controllers\Controller;
use ECommerce\Http\Requests\OrderCreateRequest;
use ECommerce\Models\Product;
use ECommerce\Http\Repositories\OrderRepository;
use ECommerce\Http\Repositories\ProductRepository;
use Illuminate\Support\Facades\Input;

class CartController extends Controller
{
    private $repository, $orderRepository;

    public function __construct(ProductRepository $repository, OrderRepository $orderRepository)
    {
        $this->repository = $repository;
        $this->orderRepository = $orderRepository;
    }

    public function index()
    {
        return view('eco::order.cart', compact('product'));
    }

    /***********Xử lý giỏ hàng************/
    public function add($id)
    {
        $product = $this->repository->find($id);
        if (empty($product)) {
            \session()->flash('error', 'Product not found');
            return \back();
        }
        if (\session()->has('cart.' . $id)) {
            $qty = \session()->get('cart.' . $id);
            $qty = $qty + 1;
            \session()->put('cart.' . $id, $qty);
        } else {
            \session()->put('cart.' . $id, 1);
        }
        return view('eco::order.cart', compact('product'));
    }

    function delete()
    {
        \session()->forget('cart');
        return view('eco::order.cart');
    }

    function getDeleteProduct($id)
    {
        \session()->forget('cart.' . $id);
        $n = new Product();
        return view('eco::order.cart')
            ->with('product', $n->getInvolveProduct($id));
    }

    function getQuitCart($id)
    {
        \session()->flush();
        $n = new Product();
        return view('eco::order.cart')
            ->with('product', $n->getInvolveProduct($id));
    }

    function order(OrderCreateRequest $request)
    {
        $cart = \session()->get('cart');
        if (empty($cart)) {
            \session()->flash('error', 'Cart empty');
            return \back();
        }
        $input = $request->all();
        $input['cart'] = $cart;
        $result = $this->orderRepository->store($input);
        if ($result) {
            \session()->flash('success', 'Bạn đã đặt hàng thành công');
        }
        return \redirect()->route('eco.product.show');
    }

    function change()
    {
        foreach (\session()->get('cart') as $id => $value) {
            if (Input::get($id) == 0) {
                \session()->forget('cart.' . $id);
            } else {
                \session()->put('cart.' . $id, Input::get($id));
            }
        }
        return view('eco::order.cart');
    }
} 