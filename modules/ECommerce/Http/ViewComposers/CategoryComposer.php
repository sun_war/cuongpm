<?php
/**
 * Created by PhpStorm.
 * User: CPM
 * Date: 5/24/2018
 * Time: 9:44 PM
 */

namespace ECommerce\Http\ViewComposers;


use ECommerce\Http\Repositories\CategoryRepository;
use Illuminate\View\View;

class CategoryComposer
{
    private $repository;
    public function __construct(CategoryRepository $repository)
    {
       $this->repository = $repository;
    }

    public function compose(View $view)
    {
        $categoryCompose = $this->repository->pluck('name', 'id');
        $view->with(compact('categoryCompose'));
    }
}