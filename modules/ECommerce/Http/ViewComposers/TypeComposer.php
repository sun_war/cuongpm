<?php
/**
 * Created by PhpStorm.
 * User: CPM
 * Date: 5/24/2018
 * Time: 9:44 PM
 */

namespace ECommerce\Http\ViewComposers;


use ECommerce\Http\Repositories\TypeRepository;
use Illuminate\View\View;

class TypeComposer
{
    private $repository;
    public function __construct(TypeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function compose(View $view)
    {
        $typeCompose = $this->repository->pluck('name', 'id');
        $view->with(compact('typeCompose'));
    }
}