<?php
/**
 * Created by PhpStorm.
 * User: cuongpm
 * Date: 7/23/18
 * Time: 10:23 AM
 */
Route::group(['namespace' => 'ECommerce\Http\Controllers', 'middleware' => ['web', 'auth:admin'], 'prefix' => 'e'], function () {
    Route::resource('resellers' , 'ResellerController');
});

Route::group(['middleware' => ['web'], 'namespace' => 'ECommerce\Http\Controllers'], function () {
    Route::get('/e', 'ProductController@getShowProduct')->name('eco.product.show');
});

Route::group(['prefix' => 'eco', 'middleware' => ['web'], 'namespace' => 'ECommerce\Http\Controllers'], function () {
    Route::get('/', array(
            'as' => 'eco',
            'uses' => "HomeController@home"
        )
    );
    Route::get('product/detail/{id}', 'ProductController@detail')->name('eco.product.detail');
    Route::get('cart/add/{id}', 'CartController@add')->name('cart.add');
    Route::get('cart-delete', 'CartController@delete')->name('cart.delete');
    Route::post('cart-change', 'CartController@change')->name('cart.change');

    Route::group(['middleware' => 'auth'], function () {

        Route::get('cart-order', 'CartController@order')->name('cart.order');
        Route::get('my-order', 'ECommerceController@order')->name('my-cart');

    });
    Route::group(['middleware' => 'auth:admin'], function () {
        Route::group(['middleware' => 'role:admin'], function () {
            Route::resource('product', 'ProductController');
            Route::resource('brand' , 'BrandController');
            Route::resource('category' , 'CategoryController');
            Route::resource('order' , 'OrderController');
            Route::resource('order-detail' , 'OrderDetailController');
            Route::resource('cart', 'CartController');
            Route::resource('product' , 'ProductController');
            Route::resource('type' , 'TypeController');
        });

    });
});

