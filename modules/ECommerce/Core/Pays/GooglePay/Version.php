<?php
/**
 * Created by PhpStorm.
 * User: cuongpm
 * Date: 8/27/18
 * Time: 10:33 AM
 */

namespace ECommerce\Core\Pays\GooglePay;


class Version
{
    const BASE_REQUEST = [
        'apiVersion' => 2,
        'apiVersionMinor' => 0
    ];
}