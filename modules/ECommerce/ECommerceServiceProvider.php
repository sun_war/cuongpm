<?php

namespace ECommerce\Providers;

use ECommerce\Http\ViewComposers\BrandComposer;
use ECommerce\Http\ViewComposers\CategoryComposer;
use ECommerce\Http\ViewComposers\TypeComposer;
use ECommerce\Http\Repositories\BrandRepository;
use ECommerce\Http\Repositories\BrandRepositoryEloquent;
use ECommerce\Http\Repositories\CategoryRepository;
use ECommerce\Http\Repositories\CategoryRepositoryEloquent;
use ECommerce\Http\Repositories\OrderDetailRepository;
use ECommerce\Http\Repositories\OrderDetailRepositoryEloquent;
use ECommerce\Http\Repositories\OrderRepository;
use ECommerce\Http\Repositories\OrderRepositoryEloquent;
use ECommerce\Http\Repositories\ProductRepository;
use ECommerce\Http\Repositories\ProductRepositoryEloquent;
use ECommerce\Http\Repositories\ResellerRepository;
use ECommerce\Http\Repositories\ResellerRepositoryEloquent;
use ECommerce\Http\Repositories\TypeRepository;
use ECommerce\Http\Repositories\TypeRepositoryEloquent;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class ECommerceServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__ . '/router.php');
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'eco');

        view()->composer([
            'eco::product.index',
            'eco::product.create',
            'eco::product.update',
            'eco::layouts.app'
        ], CategoryComposer::class);

        view()->composer([
            'eco::product.index',
            'eco::product.create',
            'eco::product.update',
            'eco::layouts.app'
        ], TypeComposer::class);

        view()->composer([
            'eco::product.index',
            'eco::product.create',
            'eco::product.update',
            'eco::layouts.app'
        ], BrandComposer::class);

        Validator::extend('reseller', function ($attribute, $value, $parameters, $validator) {
            $userId = auth()->id();

            $qtyOrder = app(OrderDetailRepository::class)->filterCount([
                'created_by' => $userId,
                'product_id' => $parameters
            ], QUANTITY_COL);

            $qtyReseller = app(ResellerRepository::class)->filterCount([
                'created_by' => $userId,
                'product_id' => $parameters
            ], QUANTITY_COL);

            $validator->errors()->add('reseller', 'Something is wrong with this field!');

            return $qtyOrder >= $qtyReseller + $value;
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BrandRepository::class, BrandRepositoryEloquent::class);
        $this->app->bind(CategoryRepository::class, CategoryRepositoryEloquent::class);
        $this->app->bind(OrderDetailRepository::class, OrderDetailRepositoryEloquent::class);
        $this->app->bind(OrderRepository::class, OrderRepositoryEloquent::class);
        $this->app->bind(ProductRepository::class, ProductRepositoryEloquent::class);
        $this->app->bind(ResellerRepository::class, ResellerRepositoryEloquent::class);
        $this->app->bind(TypeRepository::class, TypeRepositoryEloquent::class);
    }
}
