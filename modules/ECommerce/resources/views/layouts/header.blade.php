<header class="navbar navbar-fixed-top"><!-- set fixed position by adding class "navbar-fixed-top" -->
    <div class="navbar-inner">
        <!-- logo -->
        <div class="navbar-brand">
            <a href="{{route('eco.product.show')}}" style="color: whitesmoke">
                <strong>{{config('app.name')}}</strong>
            </a>
        </div>
        <!-- main menu -->
        <ul class="navbar-nav">
            <li class="has-sub">
                <a>
                    <i class="entypo-doc-text"></i>
                    <span class="title">Danh mục sản phẩm</span>
                </a>
                <ul>
                    @foreach($categoryCompose as $id => $name)
                        <li>
                            <a href="#{{$id}}">
                                <span class="title">{{$name}}</span>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </li>
            <li>
                <form id="formFilter" class="navbar-form navbar-right" action="{{route('eco.product.show')}}" method="GET">
                    <select name="brand_id" id="" class="form-control selectFilter">
                        <option value="">Thương hiệu</option>
                        @foreach($brandCompose as $id => $name)
                            <option value="{{$id}}">{{$name}}</option>
                        @endforeach
                    </select>
                    <select name="prices" id="" class="form-control selectFilter">
                        <option value="">Mức giá</option>
                        <option value="0-1000000">0 - 1,000,000 $</option>
                        <option value="1000000-5000000">1,000,000 $ - 5,000,000 $</option>
                        <option value="5000000-25000000">5,000,000 $ - 25,000,000 $</option>
                        <option value="2500000-10000000">30,000,000 $ - 100,000,000 $</option>
                        <option value="10000000-99000000000">100,000,000 $ - Cao nhất</option>
                    </select>
                    <input  id="name" name="name" class="form-control inputFilter" placeholder="Tìm kiếm...">
                    <input type="hidden">
                </form>
            </li>
        </ul>

        <ul class="navbar-nav navbar-right">
            <li>
                <a href="{{route('cart.index')}}">
                    <span><i class="fa fa-cart-plus"></i></span>
                </a>
            </li>
            <li>
                <a href="{{route('contact')}}">
                    <span><i class="entypo-mail"></i></span>
                </a>
            </li>
            @if(auth()->check())
                <li class="opened active has-sub">
                    <a href="#">
                         <span  style="color: goldenrod">
                            <strong>
                                {{auth()->user()->last_name}}
                            </strong>
                        </span>
                        <i class="entypo-user-add"></i>
                    </a>
                    <ul class="visible">
                        <li>
                            <a href="{{route('acl.profile')}}">
                                <i class="entypo-user-add"></i> {{__('auth.profile')}}
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a onclick="$('#logoutForm').submit()">
                        <i class="fa fa-sign-out"></i>
                    </a>
                    <form id="logoutForm" style="display: none" method="post" action="{{route('logout')}}" >
                        {!! csrf_field() !!}
                    </form>
                </li>
            @else
                <li>
                    <a href="{{asset('login')}}"> {{__('auth.login')}} <span class="fa fa-sign-in"></span></a>
                </li>
                <li class="sep"></li>
                <li>
                    <a href="{{asset('register')}}">{{__('auth.register')}} <span class="fa entypo-user-add-plus"></span></a>
                </li>
            @endif
            <!-- mobile only -->
            <li class="visible-xs">
                <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                <div class="horizontal-mobile-menu visible-xs">
                    <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                        <i class="entypo-menu"></i>
                    </a>
                </div>
            </li>
        </ul>
    </div>
</header>

@push('js')
    <script src="{{asset('build/form-filter.js')}}"></script>
@endpush