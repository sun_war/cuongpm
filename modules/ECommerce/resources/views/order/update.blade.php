@extends('layouts.app')
@section('content')
    <div class="row">
        <ol class="breadcrumb bc-3">
            <li>
                <a href="/"><i class="fa fa-home"></i></a>
            </li>
            <li>
                <a href="{{route('order.index')}}">{{trans('table.orders')}}</a>
            </li>
            <li class="active">
                <strong>{{__('action.update')}}</strong>
            </li>
        </ol>
        <form action="{{route('order.update', $order->id)}}" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            {{method_field('PUT')}}
            <div class="form-group col-lg-6">
                <label for="total">{{trans('label.total')}}</label>
                <input type="number" class="form-control" name="total" id="total" value="{{$order->total}}">
            </div>
            <div class="form-group col-lg-6">
                <label for="created_by">{{trans('label.created_by')}}</label>
                <input type="number" class="form-control" name="created_by" id="created_by"
                       value="{{$order->created_by}}">
            </div>
            <div class="form-group col-lg-6">
                <label for="is_active">{{trans('label.is_active')}}</label>
                <input  class="form-control" name="is_active" id="is_active" value="{{$order->is_active}}">
            </div>
            <div class="form-group col-lg-6">
                <label for="updated_by">{{trans('label.updated_by')}}</label>
                <input type="number" class="form-control" name="updated_by" id="updated_by"
                       value="{{$order->updated_by}}">
            </div>

            <div class="col-lg-12 form-group">
                <button class="btn btn-primary">{{trans('button.done')}}</button>
                <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
                <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
                <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
            </div>
        </form>
    </div>
@endsection
@push('js')
    <script>
        Menu('#ECommerceMenu', '#orderMenu')
    </script>
@endpush