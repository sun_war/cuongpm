<table class="table">
    <thead>
    <tr>
        <th>{{__('label.image')}}</th>
        <th>{{trans('label.created_by')}}</th>
        <th>{{trans('label.name')}}</th>
        <th>{{trans('label.price')}}</th>
        <th>{{trans('label.quantity')}}</th>
        <th>{{trans('label.created_at')}}</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($orders as $row)
        <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
            <td></td>
            <td>{{$row->creatorName()}}</td>
            <th colspan="3"></th>
            <td>{{$row->created_at}}</td>
            <td class="text-right">
                <form method="POST" action="{{route('order.destroy', $row->id)}}">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button class="btn btn-danger btn-xs destroyBtn">
                        <i class="fa fa-trash"></i>
                    </button>
                </form>
            </td>
        </tr>
        <tr>
            <th>{{__('label.details')}}</th>
            <th colspan="6"></th>
        </tr>
        @foreach($row->details as $detail)
            <tr>
                <th>
                    <img class="img-responsive" src="{{$detail->getImage(IMAGE_COL)}}" alt="">
                </th>
                <th></th>
                <td>{{$detail->product->name}}</td>
                <td>{{$detail->price}} <i class="fa fa-bitcoin"></i></td>
                <td>{{$detail->quantity}}</td>
                <td></td>
                <td class="text-right">
                    <button data-id="{{$detail->product->id}}" data-name="{{$detail->product->name}}"
                            data-toggle="modal" data-target="#myModal" class="btn btn-primary btn-xs btnSell">
                        <i class="fa fa-sellsy"></i> sell
                    </button>
                </td>
            </tr>
        @endforeach
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$orders->links()}}
</div>
