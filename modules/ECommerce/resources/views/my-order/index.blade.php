@extends('eco::layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3>Chi tiết hóa đơn</h3>
            </div>
            <div class="col-lg-12" id="table">
                @include('eco::my-order.table')
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <form class="modal-content" method="POST" action="{{route('resellers.store')}}">
                {{csrf_field()}}
                <input type="hidden" name="product_id" id="sellProductId">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Sell</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Product</label>
                        <input readonly id="productName" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Price</label>
                        <input type="number" name="price" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Qty</label>
                        <input type="number" value="1" name="qty" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary">{{__('button.done')}}</button>
                </div>
            </form>
        </div>
    </div>

@endsection

@push('js')
    <script>
        const btnSell = '.btnSell';
        const sellProductId = '#sellProductId';
        const productName = '#productName';
        $(document).on('click', btnSell, function () {
            const self = $(this);
            const id = self.attr('data-id');
            const name = self.attr('data-name');
            $(productName).val(name);
            $(sellProductId).val(id);
        });
    </script>
@endpush
