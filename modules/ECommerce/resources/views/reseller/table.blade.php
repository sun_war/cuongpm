<table class="table table-hover">
    <thead>
    <tr>
        <th>{{trans('label.price')}}</th>
<th>{{trans('label.product_id')}}</th>
<th>{{trans('label.created_by')}}</th>
<th>{{trans('label.qty')}}</th>

        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($resellers as $row)
    <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
        <td>{{$row->price}}</td>
<td>{{$row->product_id}}</td>
<td>{{$row->created_by}}</td>
<td>{{$row->qty}}</td>

        <td class="text-right">
            <form method="POST" action="{{route('resellers.destroy', $row->id)}}">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <button class="btn btn-danger btn-xs destroyBtn">
                    <i class="fa fa-trash"></i>
                </button>
                <a href="{{route('resellers.edit', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="{{route('resellers.show', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-eye"></i>
                </a>
            </form>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$resellers->links()}}
</div>
