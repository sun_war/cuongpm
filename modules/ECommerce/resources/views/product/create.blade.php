@extends('layouts.app')
@section('content')
    <div class="row">
        <ol class="breadcrumb bc-3">
            <li>
                <a href="/"><i class="fa fa-home"></i></a>
            </li>
            <li>
                <a href="{{route('product.index')}}">{{trans('table.products')}}</a>
            </li>
            <li class="active">
                <strong>{{__('action.create')}}</strong>
            </li>
        </ol>

        <form action="{{route('product.store')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group col-lg-4">
                <label for="name">{{trans('label.name')}}</label>
                <input  class="form-control" name="name" id="name">
            </div>
            <div class="form-group col-lg-4">
                <label for="name">{{trans('label.code')}}</label>
                <input  class="form-control" name="code" id="code">
            </div>
            <div class="form-group col-lg-4">
                <label for="is_active">{{__('table.categories')}}</label>
                <select name="category_id" class="form-control selectFilter" id="category_id">
                    @foreach($categoryCompose as $id => $name)
                        <option value="{{$id}}">{{$name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-4">
                <label for="brand_id">{{trans('table.brands')}}</label>
                <select class="form-control" name="brand_id" id="brand_id">
                    @foreach($brandCompose as $id => $name)
                        <option value="{{$id}}">{{$name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-4">
                <label for="price">{{trans('label.price')}}</label>
                <input type="number" class="form-control" name="price" id="price">
                <h5><strong id="number" class="bold text-success"></strong></h5>
            </div>
            {{--<div class="form-group col-lg-3">--}}
            {{--<label for="discount">{{trans('label.discount')}}</label>--}}
            {{--<input type="number" value="0" class="form-control" name="discount" id="discount">--}}
            {{--</div>--}}
            <div class="form-group col-lg-4">
                <label for="total">{{trans('label.total')}}</label>
                <input type="number" class="form-control" name="total" id="total">
            </div>
            <div class="form-group col-lg-12">
                <label for="intro">{{trans('label.intro')}}</label>
                <textarea class="form-control ckeditor" name="intro" id="intro"></textarea>
            </div>
            <div class="form-group col-lg-12">
                <label for="details">{{trans('label.details')}}</label>
                <textarea class="form-control ckeditor" name="details" id="details"></textarea>
            </div>
            <div class="form-group col-lg-12">
                @include('layouts.components.image', ['name' => 'image', 'image' => ''])
            </div>
            <div class="form-group col-lg-6">
                <label for="recommended">{{trans('label.recommended')}}</label>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" checked value="1" name="recommended" id="recommended">
                    </label>
                </div>
            </div>
            <div class="form-group col-lg-6">
                @include('mod::layouts.components.is-active')
            </div>

            <div class="col-lg-12">
                <button class="btn btn-primary">{{trans('button.done')}}</button>
                <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
                <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
                <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
            </div>
        </form>
    </div>
@endsection

@push('js')
    <script src="{{asset('build/forceForm.js')}}"></script>
    <script>
        $('#price').magicFormatNumber('#number')
    </script>
    <script>
        Menu('#ECommerceMenu', '#productMenu')
    </script>
@endpush
