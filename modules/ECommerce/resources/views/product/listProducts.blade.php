<h3><strong>Sản phẩm liên quan</strong></h3>
<div class="row">
@foreach($products as $product)
      <div class="col-sm-4 col-md-3">
        <div class="thumbnail">
          <img class="img-responsive" src="{{ \ViewFa::thumb($product->image, [400, 400])}}" alt="{{$product->name}}">
          <div class="caption">
            <h3>{{$product->name}}</h3>
            <p>{{number_format($product->price)}} Đ</p>
            <p><a href='{{route('cart.add', $product->id)}}' class="btn btn-primary" role="button">Mua</a>
            <a href="{{route('product.detail', $product->id)}}" class="btn btn-default" role="button">Chi tiết</a></p>
          </div>
        </div>
      </div>
@endforeach
</div>