@extends('layouts.app')
@section('content')
    <ol class="breadcrumb bc-3">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('product.index')}}">{{trans('table.products')}}</a>
        </li>
        <li class="active">
            <strong>Table</strong>
        </li>
    </ol>
    <form class="form-group row" id="formFilter" action="{{route('product.index')}}" method="POST">
        <div class="col-sm-1 form-group">
            <label for="per_page">{{__('label.per_page')}}</label>
            <select name="per_page" class="form-control selectFilter" id="per_page">
                <option value="10">10</option>
                <option value="20">20</option>
                <option value="30">30</option>
                <option value="40">40</option>
                <option value="50">50</option>
            </select>
        </div>
        <div class="col-sm-3 form-group">
            <label for="name">{{__('label.name')}}</label>
            <input name="name" class="form-control inputFilter" id="name">
        </div>
        <div class="col-sm-2 form-group">
            <label for="is_active">{{__('table.brands')}}</label>
            <select name="brand_id" class="form-control selectFilter" id="brand_id">
                <option value="">All</option>
                @foreach($brandCompose as $id => $name)
                    <option value="{{$id}}">{{$name}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-sm-2 form-group">
            <label for="is_active">{{__('table.categories')}}</label>
            <select name="category_id" class="form-control selectFilter" id="category_id">
                <option value="">All</option>
                @foreach($categoryCompose as $id => $name)
                    <option value="{{$id}}">{{$name}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-sm-2 form-group">
            <label for="is_active">{{__('label.price')}}</label>
            <select name="prices" class="form-control selectFilter" id="prices">
                <option value="">Mức giá</option>
                <option value="0-10000000">0-10000000</option>
                <option value="10000000-20000000">10000000-20000000</option>
                <option value="20000000-30000000">20000000-30000000</option>
                <option value="30000000-40000000">30000000-40000000</option>
                <option value="40000000-99000000000">40000000 - Cao nhất</option>
            </select>
        </div>
        <div class="col-sm-1 form-group">
            <label for="is_active">{{__('label.is_active')}}</label>
            <select name="is_active" class="form-control selectFilter" id="is_active">
                <option value="">All</option>
                <option value="1">Active</option>
                <option value="0">Inactive</option>
            </select>
        </div>
        <div class="col-sm-1 form-group">
            <label>{{__('label.action')}}</label>
            <a class="btn btn-primary btn-block" href="{{route('product.create')}}"><i class="fa fa-plus"></i></a>
        </div>
    </form>
    <div class="form-group" id="table">
        @include('eco::product.table')
    </div>
@endsection

@push('js')
    <script src="{{asset('build/form-filter.js')}}"></script>
    <script>
        Menu('#ECommerceMenu', '#productMenu')
    </script>
@endpush