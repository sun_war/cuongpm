@foreach($products as $product)
    <div class="col-sm-6 col-md-4 col-lg-4 col-xs-12">
        <div class="thumbnail text-center">
            <a href="{{route('eco.product.detail', $product->id)}}"></a>
            <img class="img-responsive" src="{{ \ViewFa::thumb($product->image, [400, 300])}}" alt="{{$product->name}}">
            <div class="caption">
                <h4>{{$product->name}}</h4>
                <h4>{{number_format($product->price)}} Đ</h4>
                <div>
                    <a href='{{route('cart.add', $product->id)}}'  class="btn btn-primary btn-icon">Mua <i class="fa fa-cart-plus"></i></a>
                    <a href="{{route('eco.product.detail',$product->id )}}" class="btn btn-info btn-icon">Chi tiết  <i class="fa fa-link"></i></a>
                </div>
            </div>
        </div>
    </div>
@endforeach
<div class="col-lg-12 form-group text-center">
    {{$products->links()}}
</div>
