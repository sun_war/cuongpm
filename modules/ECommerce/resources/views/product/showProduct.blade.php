@extends('eco::layouts.app')
@section('title' , __('table.products'))
@section('content')
    {{--<div class="col-lg-12">--}}
        {{--<form method="get" class="search-bar form-group" action="" enctype="application/x-www-form-urlencoded">--}}

            {{--<div class="input-group">--}}
                {{--<input  class="form-control input-lg" name="search" placeholder="Điền tên sản phẩm ...">--}}

                {{--<div class="input-group-btn">--}}
                    {{--<button type="submit" class="btn btn-lg btn-primary btn-icon">--}}
                        {{--Tìm kiếm--}}
                        {{--<i class="entypo-search"></i>--}}
                    {{--</button>--}}
                {{--</div>--}}
            {{--</div>--}}

        {{--</form>--}}
    {{--</div>--}}
{{--    @include('edu::layouts.components.slides')--}}
    <div class="container">
        <div class="row">
            <div class="feature-block col-lg-12">
                <h3>
                    <i class="fa fa-cart-plus"></i>
                    Sản phẩm
                </h3>
            </div>
        </div>
        <div class="row" id="table" class="form-group">
            @include('eco::product.component.product-list')
        </div>
    </div>

    <section>
        <div class="container">
            <div class="row">
                <div class="feature-block col-lg-12">
                    <h3>
                        <i class="entypo-cog"></i>
                        Thông tin về chúng tôi
                    </h3>
                </div>
                <div class="col-lg-12 col-xs-12 col-sm-12">
                    <div class="embed-responsive embed-responsive-16by9">
                        {!! $about->map !!}
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="footer-widgets">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h3><a href="#">{{config('app.name')}}</a></h3>
                    <p>
                        VINCENT cung cấp các giá trị phục vụ cho khách hàng
                    </p>
                </div>
            </div>
        </div>
    </section>
@endsection
