@extends('layouts.app')
@section('content')
    <div class="row">
        <ol class="breadcrumb bc-3">
            <li>
                <a href="/"><i class="fa fa-home"></i></a>
            </li>
            <li>
                <a href="{{route('order-detail.index')}}">{{trans('table.order_details')}}</a>
            </li>
            <li class="active">
                <strong>{{__('action.create')}}</strong>
            </li>
        </ol>

        <form action="{{route('order-detail.store')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group col-lg-6">
                <label for="order_id">{{trans('label.order_id')}}</label>
                <input type="number" class="form-control" name="order_id" id="order_id">
            </div>
            <div class="form-group col-lg-6">
                <label for="product_id">{{trans('label.product_id')}}</label>
                <input type="number" class="form-control" name="product_id" id="product_id">
            </div>
            <div class="form-group col-lg-6">
                <label for="quantity">{{trans('label.quantity')}}</label>
                <input type="number" class="form-control" name="quantity" id="quantity">
            </div>
            <div class="form-group col-lg-6">
                <label for="price">{{trans('label.price')}}</label>
                <input type="number" class="form-control" name="price" id="price">
            </div>

            <div class="col-lg-12">
                <button class="btn btn-primary">{{trans('button.done')}}</button>
                <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
                <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
                <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
            </div>
        </form>
    </div>
@endsection
@push('js')
    <script>
        Menu('#ECommerceMenu', '#orderDetailMenu')
    </script>
@endpush