<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProducts extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products',function($table){
            $table->bigIncrements('id');
            $table->unsignedTinyInteger('brand_id')->nullable();
            $table->unsignedTinyInteger('category_id')->nullable();
            $table->unsignedTinyInteger('type_id')->nullable();
            $table->string('name',48);
            $table->string('code', 48)->nullabel();
            $table->bigInteger('price')->nullable();
            $table->string('image')->nullable();;
            $table->text('intro')->nullable();;
            $table->text('details')->nullable();;
            $table->unsignedTinyInteger('discount')->default(0);
            $table->bigInteger('views')->default(0);
            $table->bigInteger('total')->default(0);
            $table->tinyInteger('recommended')->default(0);
            $table->tinyInteger('is_active')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
	}
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('products');
	}

}
