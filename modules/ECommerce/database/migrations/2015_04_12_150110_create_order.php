<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrder extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders',function($table){
            $table->bigIncrements('id');
            $table->unsignedInteger('created_by')->unsigned();
            $table->unsignedInteger('updated_by')->unsigned();
            $table->unsignedInteger('total')->default(1);
            $table->string('is_active')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('orders');
	}

}
