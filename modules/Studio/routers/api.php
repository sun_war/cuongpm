<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 3/22/19
 * Time: 2:39 PM
 */

Route::name('api.')
    ->namespace('Studio\Http\Controllers\API')
    ->prefix('api/studio')
    ->middleware(['api', 'auth:api'])
    ->group(function () {
        Route::resource('studio-follows', 'StudioFollowApiController');
        Route::resource('studio-rates', 'StudioRateApiController');
        Route::resource('studio-reports' , 'StudioReportApiController');
        Route::resource('studios' , 'StudioApiController');
    });
