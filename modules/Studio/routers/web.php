<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 3/22/19
 * Time: 2:39 PM
 */

Route::name('studio.')
    ->namespace('Studio\Http\Controllers')
    ->middleware(['web', 'locale.db'])
    ->prefix('studio')
    ->group(function () {
        Auth::routes();
    });

Route::group(['namespace' => 'Studio\Http\Controllers',
    'middleware' => ['web', 'auth:admin'], 'prefix' => 'admin'],
    function () {
        Route::resource('studio-follows', 'StudioFollowController');
        Route::resource('studio-rates', 'StudioRateController');
        Route::resource('studio-reports' , 'StudioReportController');
        Route::resource('studios' , 'StudioController');
    });
