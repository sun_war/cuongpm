<?php

namespace Studio\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Studio\Http\Repositories\StudioRepository;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    private $repository;

    /**
     * Create a new controller instance.
     *
     * @param StudioRepository $repository
     */
    public function __construct(StudioRepository $repository)
    {
        $this->middleware('guest:studio');
        $this->repository = $repository;
    }

    public function showRegistrationForm()
    {
        return view('studio::auth.register');
    }

    public function register(Request $request)
    {
        $input = $request->input();
        DB::beginTransaction();
        $validator = $this->validator($input);
        if($validator->fails()) {
            return back()->withErrors($validator->errors());
        }
        $user = $this->create($input);
        Auth::guard('studio')->login($user);
        DB::commit();
        session()->flash('success', 'Register successfully, please verify email');
        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|email|max:255|unique:studios',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $input
     * @return User
     */
    protected function create(array $input)
    {
        return $this->repository->store($input);
    }
}
