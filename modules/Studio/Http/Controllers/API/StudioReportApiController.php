<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/11/18
 * Time: 9:54 AM
 */

namespace Studio\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Studio\Http\Requests\StudioReportCreateRequest;
use Studio\Http\Requests\StudioReportUpdateRequest;
use Studio\Http\Resources\StudioReportResource;
use Studio\Http\Repositories\StudioReportRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

/**
 * @group Studio
 *
 * APIs for studio
 */

class StudioReportApiController  extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(StudioReportRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data = $this->repository->myPaginate($input);
        return new StudioReportResource($data);
    }

    public function create()
    {
        return view('studio::studio-report.create');
    }

    public function store(StudioReportCreateRequest $request)
    {
        $input = $request->all();
        $studioReport = $this->repository->store($input);
        return new StudioReportResource($studioReport);
    }

    public function show($id)
    {
        $studioReport = $this->repository->find($id);
        if (empty($studioReport)) {
            return new StudioReportResource([$studioReport]);
        }
        return new StudioReportResource($studioReport);
    }

    public function edit($id)
    {
        $studioReport = $this->repository->find($id);
        if (empty($studioReport)) {
            return new StudioReportResource([$studioReport]);
        }
        return new StudioReportResource($studioReport);
    }

    public function update(StudioReportUpdateRequest $request, $id)
    {
        $input = $request->all();
        $studioReport = $this->repository->find($id);
        if (empty($studioReport)) {
            return new StudioReportResource([$studioReport]);
        }
        $data = $this->repository->change($input, $studioReport);
        return new StudioReportResource($data);
    }

    public function destroy($id)
    {
        $studioReport = $this->repository->find($id);
        if (empty($studioReport)) {
            return new StudioReportResource($studioReport);
        }
        $data = $this->repository->delete($id);
        return new StudioReportResource([$data]);
    }
}