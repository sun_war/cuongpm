<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/11/18
 * Time: 9:54 AM
 */

namespace Studio\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Studio\Http\Requests\StudioRateCreateRequest;
use Studio\Http\Requests\StudioRateUpdateRequest;
use Studio\Http\Resources\StudioRateResource;
use Studio\Http\Repositories\StudioRateRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

/**
 * @group Studio
 *
 * APIs for studio
 */

class StudioRateApiController  extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(StudioRateRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data = $this->repository->myPaginate($input);
        return new StudioRateResource($data);
    }

    public function create()
    {
        return view('studio::studio-rate.create');
    }

    public function store(StudioRateCreateRequest $request)
    {
        $input = $request->all();
        $studioRate = $this->repository->store($input);
        return new StudioRateResource($studioRate);
    }

    public function show($id)
    {
        $studioRate = $this->repository->find($id);
        if (empty($studioRate)) {
            return new StudioRateResource([$studioRate]);
        }
        return new StudioRateResource($studioRate);
    }

    public function edit($id)
    {
        $studioRate = $this->repository->find($id);
        if (empty($studioRate)) {
            return new StudioRateResource([$studioRate]);
        }
        return new StudioRateResource($studioRate);
    }

    public function update(StudioRateUpdateRequest $request, $id)
    {
        $input = $request->all();
        $studioRate = $this->repository->find($id);
        if (empty($studioRate)) {
            return new StudioRateResource([$studioRate]);
        }
        $data = $this->repository->change($input, $studioRate);
        return new StudioRateResource($data);
    }

    public function destroy($id)
    {
        $studioRate = $this->repository->find($id);
        if (empty($studioRate)) {
            return new StudioRateResource($studioRate);
        }
        $data = $this->repository->delete($id);
        return new StudioRateResource([$data]);
    }
}