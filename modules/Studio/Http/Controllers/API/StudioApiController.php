<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/11/18
 * Time: 9:54 AM
 */

namespace Studio\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Studio\Http\Requests\StudioCreateRequest;
use Studio\Http\Requests\StudioUpdateRequest;
use Studio\Http\Resources\StudioResource;
use Studio\Http\Repositories\StudioRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

/**
 * @group Studio
 *
 * APIs for studio
 */
class StudioApiController  extends Controller
{
    use ControllersTrait;
    /**
     * @var StudioRepository
     */
    private $repository;

    /**
     * StudioApiController constructor.
     * @param StudioRepository $repository
     */
    public function __construct(StudioRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return StudioResource
     */
    public function index(Request $request)
    {
        $input = $request->all();
        $data = $this->repository->myPaginate($input);
        return new StudioResource($data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('studio::studio.create');
    }

    /**
     * @param StudioCreateRequest $request
     * @return StudioResource
     */
    public function store(StudioCreateRequest $request)
    {
        $input = $request->all();
        $studio = $this->repository->store($input);
        return new StudioResource($studio);
    }

    /**
     * @param $id
     * @return StudioResource
     */
    public function show($id)
    {
        $studio = $this->repository->find($id);
        if (empty($studio)) {
            return new StudioResource([$studio]);
        }
        return new StudioResource($studio);
    }

    /**
     * @param $id
     * @return StudioResource
     */
    public function edit($id)
    {
        $studio = $this->repository->find($id);
        if (empty($studio)) {
            return new StudioResource([$studio]);
        }
        return new StudioResource($studio);
    }

    /**
     * @param StudioUpdateRequest $request
     * @param $id
     * @return StudioResource
     */
    public function update(StudioUpdateRequest $request, $id)
    {
        $input = $request->all();
        $studio = $this->repository->find($id);
        if (empty($studio)) {
            return new StudioResource([$studio]);
        }
        $data = $this->repository->change($input, $studio);
        return new StudioResource($data);
    }

    /**
     * @param $id
     * @return StudioResource
     */
    public function destroy($id)
    {
        $studio = $this->repository->find($id);
        if (empty($studio)) {
            return new StudioResource($studio);
        }
        $data = $this->repository->delete($id);
        return new StudioResource([$data]);
    }
}