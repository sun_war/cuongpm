<?php

namespace Studio\Http\Controllers;

use App\Http\Controllers\Controller;
use Studio\Http\Requests\StudioFollowCreateRequest;
use Studio\Http\Requests\StudioFollowUpdateRequest;
use Studio\Http\Repositories\StudioFollowRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class StudioFollowController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(StudioFollowRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['studioFollows'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('studio::studio-follow.table', $data)->render();
        }
        return view('studio::studio-follow.index', $data);
    }

    public function create()
    {
        return view('studio::studio-follow.create');
    }

    public function store(StudioFollowCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('studio-follows.index');
    }

    public function show($id)
    {
        $studioFollow = $this->repository->find($id);
        if (empty($studioFollow)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('studio::studio-follow.show', compact('studioFollow'));
    }

    public function edit($id)
    {
        $studioFollow = $this->repository->find($id);
        if (empty($studioFollow)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('studio::studio-follow.update', compact('studioFollow'));
    }

    public function update(StudioFollowUpdateRequest $request, $id)
    {
        $input = $request->all();
        $studioFollow = $this->repository->find($id);
        if (empty($studioFollow)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $studioFollow);
        session()->flash('success', 'update success');
        return redirect()->route('studio-follows.index');
    }

    public function destroy($id)
    {
        $studioFollow = $this->repository->find($id);
        if (empty($studioFollow)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
