<?php

namespace Studio\Http\Controllers;

use App\Http\Controllers\Controller;
use Studio\Http\Requests\UserCreateRequest;
use Studio\Http\Requests\UserUpdateRequest;
use Studio\Http\Repositories\UserRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class UserController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['users'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('studio::user.table', $data)->render();
        }
        return view('studio::user.index', $data);
    }

    public function create()
    {
        return view('studio::user.create');
    }

    public function store(UserCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('users.index');
    }

    public function show($id)
    {
        $user = $this->repository->find($id);
        if (empty($user)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('studio::user.show', compact('user'));
    }

    public function edit($id)
    {
        $user = $this->repository->find($id);
        if (empty($user)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('studio::user.update', compact('user'));
    }

    public function update(UserUpdateRequest $request, $id)
    {
        $input = $request->all();
        $user = $this->repository->find($id);
        if (empty($user)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $user);
        session()->flash('success', 'update success');
        return redirect()->route('users.index');
    }

    public function destroy($id)
    {
        $user = $this->repository->find($id);
        if (empty($user)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
