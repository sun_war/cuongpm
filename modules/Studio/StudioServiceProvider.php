<?php
/**
 * Created by PhpStorm.
 * User: cuongpm
 * Date: 7/23/18
 * Time: 10:30 AM
 */
namespace Studio;
use Illuminate\Support\ServiceProvider;
use SMartins\PassportMultiauth\Providers\MultiauthServiceProvider;
use Studio\Http\Repositories\StudioFollowRepository;
use Studio\Http\Repositories\StudioFollowRepositoryEloquent;
use Studio\Http\Repositories\StudioRateRepository;
use Studio\Http\Repositories\StudioRateRepositoryEloquent;
use Studio\Http\Repositories\StudioReportRepository;
use Studio\Http\Repositories\StudioReportRepositoryEloquent;
use Studio\Http\Repositories\StudioRepository;
use Studio\Http\Repositories\StudioRepositoryEloquent;

class StudioServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'studio');
        $this->loadRoutesFrom(__DIR__ . '/routers/api.php');
        $this->loadRoutesFrom(__DIR__ . '/routers/web.php');
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(StudioFollowRepository::class, StudioFollowRepositoryEloquent::class);
        $this->app->bind(StudioRateRepository::class, StudioRateRepositoryEloquent::class);
        $this->app->bind(StudioReportRepository::class, StudioReportRepositoryEloquent::class);
        $this->app->bind(StudioRepository::class, StudioRepositoryEloquent::class);

//        $this->app->register(MultiauthServiceProvider::class);
    }
}