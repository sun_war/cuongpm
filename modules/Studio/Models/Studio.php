<?php

namespace Studio\Models;

use Illuminate\Notifications\Notifiable;
use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;
use SMartins\PassportMultiauth\HasMultiAuthApiTokens;


class Studio extends Authenticatable
{
    use TransformableTrait;
    use ModelsTrait;
    use SoftDeletes;
    use HasMultiAuthApiTokens;
    use Notifiable;

    public $table = 'studios';
    public $fillable = ['email', 'name', 'phone_number', 'facebook', 'website', 'image', 'slogan', 'city', 'address', 'status'];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public $fileUpload = ['image' => 1];
    protected $pathUpload = ['image' => '/images/studios'];
    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [

            ]
        ]
    ];
}

