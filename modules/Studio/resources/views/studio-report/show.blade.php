@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('studio-reports.index')}}">{{trans('table.studio_reports')}}</a>
    </li>
    <li class="active">
        <strong>Show</strong>
    </li>
</ol>
<div>
    <table class="table">
        <tbody>
            <tr><th>{{__('label.studio_id')}}</th>
<td>{!! $studio_report->studio_id !!}</td>
</tr><tr><th>{{__('label.created_by')}}</th>
<td>{!! $studio_report->created_by !!}</td>
</tr><tr><th>{{__('label.content')}}</th>
<td>{!! $studio_report->content !!}</td>
</tr>
        </tbody>
    </table>
</div>
@endsection

@push('js')
<script>
    Menu('#StudioMenu', '#studioReportMenu')
</script>
@endpush
