@extends('studio::auth.layout')
@section('content')
    {{--<div class="login-form">--}}
        <div class="login-content">
            <form role="form" method="POST" action="{{ route('studio.register') }}" class="text-left">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="">Name</label>
                    <input id="name" type="name" class="form-control" name="name" value="{{ old('name') }}" required
                           autofocus>
                    @if ($errors->has('email'))
                        <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                    @endif
                </div>
                <div class="form-group">
                    <lable>Email</lable>
                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required
                           autofocus>
                    @if ($errors->has('email'))
                        <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                    @endif

                </div>
                <div class="form-group">
                   <lable>Password</lable>
                        <input id="password" type="password" class="form-control" name="password" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                        @endif

                </div>
                <div class="form-group">
                    <label for="">Password confirmation</label>
                        <input id="password_confirmation" type="password" class="form-control"
                               name="password_confirmation" required>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                        @endif
                </div>
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-primary  btn-login">
                        <i class="fa fa-sign-in"></i>
                        Register
                    </button>
                </div>
            </form>
        </div>
    {{--</div>--}}
@endsection