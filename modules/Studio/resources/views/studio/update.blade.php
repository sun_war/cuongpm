@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('studios.index')}}">{{trans('table.studios')}}</a>
    </li>
    <li class="active">
        <strong>{{__('action.update')}}</strong>
    </li>
</ol>
<div class="row">
    <form action="{{route('studios.update', $studio->id)}}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        {{method_field('PUT')}}
        <div class="form-group col-lg-12">
    <label for="email">{{trans('label.email')}}</label>
    <input required class="form-control" name="email" id="email" value="{{$studio->email}}">
</div>
<div class="form-group col-lg-12">
    <label for="name">{{trans('label.name')}}</label>
    <input required class="form-control" name="name" id="name" value="{{$studio->name}}">
</div>
<div class="form-group col-lg-12">
    <label for="phone_number">{{trans('label.phone_number')}}</label>
    <input required class="form-control" name="phone_number" id="phone_number" value="{{$studio->phone_number}}">
</div>
<div class="form-group col-lg-12">
    <label for="facebook">{{trans('label.facebook')}}</label>
    <input required class="form-control" name="facebook" id="facebook" value="{{$studio->facebook}}">
</div>
<div class="form-group col-lg-12">
    <label for="website">{{trans('label.website')}}</label>
    <input required class="form-control" name="website" id="website" value="{{$studio->website}}">
</div>
<div class="form-group col-lg-12">
    <label for="image">{{trans('label.image')}}</label>
    <input required type="file" name="image" id="image">
    <p class="help-block">Example block-level help text here.</p>
</div>
<div class="form-group col-lg-12">
    <label for="slogan">{{trans('label.slogan')}}</label>
    <textarea class="form-control" name="slogan" id="slogan">{{$studio->slogan}}</textarea>
</div>
<div class="form-group col-lg-12">
    <label for="city">{{trans('label.city')}}</label>
    <input required class="form-control" name="city" id="city" value="{{$studio->city}}">
</div>
<div class="form-group col-lg-12">
    <label for="address">{{trans('label.address')}}</label>
    <input required class="form-control" name="address" id="address" value="{{$studio->address}}">
</div>
<div class="form-group col-lg-12">
    <label for="status">{{trans('label.status')}}</label>
    <div class="checkbox">
        <label>
            <input required type="checkbox" {{$studio->status !== 1 ?: 'checked'}} name="status" id="status" value="1">
        </label>
    </div>
</div>

        <div class="col-lg-12 form-group">
            <button class="btn btn-primary">{{trans('button.done')}}</button>
            <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
            <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
            <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
        </div>
    </form>
</div>
@endsection

@push('js')
    <script>
        Menu('#StudioMenu', '#studioMenu')
    </script>
@endpush
