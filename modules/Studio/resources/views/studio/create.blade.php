@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('studios.index')}}">{{trans('table.studios')}}</a>
    </li>
    <li class="active">
        <strong>{{__('action.create')}}</strong>
    </li>
</ol>
<div class="row">
    <form action="{{route('studios.store')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group col-lg-12">
    <label for="email">{{trans('label.email')}}</label>
    <input required class="form-control" name="email" id="email">
</div>

<div class="form-group col-lg-12">
    <label for="name">{{trans('label.name')}}</label>
    <input required class="form-control" name="name" id="name">
</div>

<div class="form-group col-lg-12">
    <label for="phone_number">{{trans('label.phone_number')}}</label>
    <input required class="form-control" name="phone_number" id="phone_number">
</div>

<div class="form-group col-lg-12">
    <label for="facebook">{{trans('label.facebook')}}</label>
    <input required class="form-control" name="facebook" id="facebook">
</div>

<div class="form-group col-lg-12">
    <label for="website">{{trans('label.website')}}</label>
    <input required class="form-control" name="website" id="website">
</div>

<div class="form-group col-lg-12">
    <label for="image">{{trans('label.image')}}</label>
    <input type="file" required name="image" id="image">
    <p class="help-block">Example block-level help text here.</p>
</div><div class="form-group col-lg-12">
    <label for="slogan">{{trans('label.slogan')}}</label>
    <textarea class="form-control" name="slogan" id="slogan"></textarea>
</div>
<div class="form-group col-lg-12">
    <label for="city">{{trans('label.city')}}</label>
    <input required class="form-control" name="city" id="city">
</div>

<div class="form-group col-lg-12">
    <label for="address">{{trans('label.address')}}</label>
    <input required class="form-control" name="address" id="address">
</div>

<div class="form-group col-lg-12">
    <label for="status">{{trans('label.status')}}</label>
    <div class="checkbox">
        <label>
            <input type="checkbox" required checked value="1" name="status" id="status">
        </label>
    </div>
</div>


        <div class="col-lg-12">
            <button class="btn btn-primary">{{trans('button.done')}}</button>
            <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
            <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
            <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
        </div>
    </form>
</div>
@endsection

@push('js')
<script>
    Menu('#StudioMenu', '#adaAccountMenu')
</script>
@endpush