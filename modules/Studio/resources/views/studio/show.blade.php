@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('studios.index')}}">{{trans('table.studios')}}</a>
    </li>
    <li class="active">
        <strong>Show</strong>
    </li>
</ol>
<div>
    <table class="table">
        <tbody>
            <tr><th>{{__('label.email')}}</th>
<td>{!! $studio->email !!}</td>
</tr><tr><th>{{__('label.name')}}</th>
<td>{!! $studio->name !!}</td>
</tr><tr><th>{{__('label.phone_number')}}</th>
<td>{!! $studio->phone_number !!}</td>
</tr><tr><th>{{__('label.facebook')}}</th>
<td>{!! $studio->facebook !!}</td>
</tr><tr><th>{{__('label.website')}}</th>
<td>{!! $studio->website !!}</td>
</tr><tr><th>{{__('label.image')}}</th>
<td>{!! $studio->image !!}</td>
</tr><tr><th>{{__('label.slogan')}}</th>
<td>{!! $studio->slogan !!}</td>
</tr><tr><th>{{__('label.city')}}</th>
<td>{!! $studio->city !!}</td>
</tr><tr><th>{{__('label.address')}}</th>
<td>{!! $studio->address !!}</td>
</tr><tr><th>{{__('label.status')}}</th>
<td>{!! $studio->status !!}</td>
</tr>
        </tbody>
    </table>
</div>
@endsection

@push('js')
<script>
    Menu('#StudioMenu', '#studioMenu')
</script>
@endpush
