<?php

namespace ACL\Http\Repositories;

use App\User;
use Modularization\MultiInheritance\RepositoriesTrait;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;


/**
 * Class UserRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    use RepositoriesTrait;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    public function myPaginate($input)
    {
        isset($input[PER_PAGE]) ?: $input[PER_PAGE] = 10;
        return $this->makeModel()
            ->filter($input)
            ->with('roles')
            ->paginate($input[PER_PAGE]);
    }

    public function store($input)
    {
        $input = $this->standardized($input, $this->makeModel());
        $user = $this->create($input);
        $user->roles()->attack($input['role_ids']);
        return $user;
    }

    public function change($input, $data)
    {
        $input = $this->standardized($input, $data);
        $user = $this->update($input, $data->id);

        if (isset($input['role_ids'])) {
            $user->roles()->sync($input['role_ids']);
        }

        return $user;
    }

    public function destroy($data)
    {
        return $this->delete($data->id);
        // TODO: Implement destroy() method.
    }

    private function standardized($input, $data)
    {
        return $data->uploader($input);
    }

    /**
     * Boot up the repository, ping criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getListByRole($name = [], $field = 'name')
    {
        return $this->model->leftJoin('role_user', 'role_user.user_id', '=', 'users.id')
            ->leftJoin('roles', 'roles.id', '=', 'role_user.role_id')
            ->whereIn('roles.name', $name)
            ->pluck('users.' . $field, 'users.id');
    }
}
