<?php

namespace ACL\Http\Controllers\API\Auth;


use ACL\Http\Resources\LoginResource;
use App\User;
use Exception;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use League\OAuth2\Server\Exception\OAuthServerException;
use Psr\Http\Message\ServerRequestInterface;
use \Laravel\Passport\Http\Controllers\AccessTokenController as ATC;

/**
 * @group ACL
 *
 * APIs for acl
 */
class LoginController extends ATC
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */

    protected $redirectTo = '/';

    /**
     * Login
     *
     * @param ServerRequestInterface $request
     * @return LoginResource|\Illuminate\Http\JsonResponse
     */
    public function login(ServerRequestInterface $request)
    {
        try {
            $username = $request->getParsedBody()['username'];
            $user = User::where('email', $username)->first();
            $tokenResponse = parent::issueToken($request);
            $content = $tokenResponse->getContent();
            $data = json_decode($content, true);

            if (isset($data["error"])) {
                throw new OAuthServerException('The user credentials were incorrect.', 6, 'invalid_credentials', 422);
            }

            $user = collect($user);
            $user->put('access_token', $data['access_token']);
            $user->put('token_type', $data['token_type']);
            $user->put('expires_in', $data['expires_in']);
            $user->put('refresh_token', $data['refresh_token']);

            return new LoginResource($user);
        } catch (Exception $e) {
            return response()->json(['errors' => [$e->getMessage()]], $e->getCode());
        }
    }
}
