<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 2/15/19
 * Time: 11:11 AM
 */

Route::name('api.')
    ->namespace('ACL\Http\Controllers\API')
    ->prefix('api')
    ->middleware(['api', 'cors'])
    ->group( function () {
        Route::post('/login', 'Auth\LoginController@login')->name('login');
        Route::post('/register', 'Auth\RegisterController@register')->name('register');
        Route::post('/forgot-password', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('forgot-password');
        Route::post('/reset-password', 'Auth\ResetPasswordController@reset')->name('reset-password');
//        Route::post('oauth/token', 'Auth\AccessTokenController@issueToken')->name('passport.token');
    });

Route::name('api.')
    ->namespace('ACL\Http\Controllers\API')
    ->prefix('api')
    ->middleware(['api'])
    ->group( function () {
        Route::resource('verify-users' , 'VerifyUserApiController');
    });
