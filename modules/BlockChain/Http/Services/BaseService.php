<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 10/17/18
 * Time: 9:47 AM
 */

namespace BlockChain\Http\Services;

use GuzzleHttp\Client;

class BaseService
{
    protected $uri = 'https://api-ropsten.etherscan.io/api';
    protected $contentType = 'application/json';
    protected $client;
    protected $account;

    protected $Erc20Service = [
        'network' => 'ropsten',
        'broadcastTransactionUrl' => 'https://ropsten.etherscan.io',
        'tokens' => [
            'MGC' => [
                'symbol' => 'MGC',
                'name' => 'MGC004',
                'decimal' => 18,
                'address' => '0xebf0c068cc1dd9b343e92bc2cc09a2ca272d6511',
                'transactionFee' => 0.1
            ]
        ]
    ];

    public function __construct()
    {
        $this->account = \request('address', NULL);

        $this->client = new Client([
            'base_url' => [$this->uri],
            'defaults' => [
                'headers' => [
                    'content-type' => $this->contentType,
                    'Accept' => $this->contentType
                ],
            ],
        ]);
    }

    protected function getApi($network)
    {
        switch ($network) {
            case 'mainnet':
                $ApiUrl = 'https://api.etherscan.io/api';
                break;
            case 'ropsten':
                $ApiUrl = 'https://api-ropsten.etherscan.io/api';
                break;
            default:
                $ApiUrl = 'https://ropsten.etherscan.io/api';
        }
        return $ApiUrl;
    }

    protected function send($data)
    {
        $res = $this->client->get($this->uri, ['query' => $data]);
        return json_decode($res->getBody(), true);
    }
}