<?php
/**
 * Created by PhpStorm.
 * User: cuongpm
 * Date: 4/24/19
 * Time: 10:49 AM
 */

namespace BlockChain\Http\Facades;


use Illuminate\Support\Facades\Facade;

class BlockChainFacade extends Facade
{
  public static function getFacadeAccessor()
  {
    return 'BlockChain';
  }
}