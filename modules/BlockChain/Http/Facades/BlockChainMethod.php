<?php
/**
 * Created by PhpStorm.
 * User: cuongpm
 * Date: 4/24/19
 * Time: 10:40 AM
 */

namespace BlockChain\Http\Facades;
use GuzzleHttp\Client;

class BlockChainMethod
{
    public function getUrlPrice($coin, $currency = 'USD') {
        return "https://min-api.cryptocompare.com/data/pricemultifull?fsyms={$coin}&tsyms={$currency}";
    }

    public function getPrice($coin, $currency = 'USD') {
        $url = $this->getUrlPrice($coin, $currency);
        $client = new Client();
        $res = $client->get($url);
        return json_decode($res->getBody(), true);
    }

    public function getPriceCurrent($coin, $currency = 'USD') {
        $res = $this->getPrice($coin, $currency);
        return $this->filterPrice($res);
    }

    private function filterPrice($res, $filter = 'PRICE') {
      $res = $res['RAW'];
      $data = [];
      foreach ($res as $coinKey => $prices) {
        foreach ($prices as $currencyKey => $price) {
          $data[$coinKey][$currencyKey] = $price[$filter];
        }
      }
      return $data;
    }

    public function mathCoin($price, $money) {
        return $money/$price;
    }

    public function buyCoin($price, $money, $bonus = 0) {
        $coin = $this->mathCoin($price, $money);
        $total = $coin;
        $coin_bonus = 0;
        if($bonus > 0) {
            $total = $this->mathCoin($bonus, $money);
            $coin_bonus = $total - $coin;
        }
        return compact('coin', 'total', 'coin_bonus');
    }
}