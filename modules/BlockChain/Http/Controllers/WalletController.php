<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 3/6/19
 * Time: 11:27 AM
 */

namespace BlockChain\Http\Controllers;


use App\Http\Controllers\Controller;
use BlockChain\Http\Repositories\AccountRepository;
use BlockChain\Http\Repositories\CryptoRepository;
use BlockChain\Http\Services\AccountService;
use BlockChain\Http\Services\TransactionService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class WalletController extends Controller
{
    private $accountRepo, $cryptoRepo;

    public function __construct(AccountRepository $accountRepo, CryptoRepository $cryptoRepo)
    {
        $this->accountRepo = $accountRepo;
        $this->cryptoRepo = $cryptoRepo;
    }

    public function index()
    {
        $account = auth()->user()->account;
        $total = 0;
        if($account) {
            $address = $account->address;
            $total = app(AccountService::class)->getCoinAmount('ETH', $address);
            $histories = app(TransactionService::class)->getHistory($address);
        }
        return view('bc::index', compact('account', 'total', 'histories', 'address'));
    }

    /**
     * get user's public RSA
     */
    public function getKey()
    {
        $key = \request('key', 'public_key');
        $user = auth()->user();
        $crypto = $user->crypto;
        if (empty($crypto)) {
            $crypto = $this->crypto();
        }
        return response()->json($crypto->$key);
    }

    /**
     * RSA create
     */
    public function crypto()
    {
        $input['user_id'] = auth()->id();
        $input = app(AccountService::class)->setKey($input);
        return $this->cryptoRepo->store($input);
    }
}
