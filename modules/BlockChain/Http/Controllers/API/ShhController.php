<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/6/18
 * Time: 9:57 AM
 */

namespace BlockChain\Http\Controllers\API;


class ShhController extends BaseController
{
    public function version()
    {
        $params = [];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "shh_version",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function post()
    {
        $params = [
            'from' => "0x04f96a5e25610293e42a73908e93ccc8c4d4dc0edcfa9fa872f50cb214e08ebf61a03e245533f97284d442460f2998cd41858798ddfd4d661997d3940272b717b1",
            'to' => "0x3e245533f97284d442460f2998cd41858798ddf04f96a5e25610293e42a73908e93ccc8c4d4dc0edcfa9fa872f50cb214e08ebf61a0d4d661997d3940272b717b1",
            'topics' => ["0x776869737065722d636861742d636c69656e74", "0x4d5a695276454c39425154466b61693532"],
            'payload' => "0x7b2274797065223a226d6",
            'priority' => "0x64",
            'ttl' => "0x64",
        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "db_getString",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function newIdentinty()
    {
        $params = [];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "shh_newIdentinty",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function hasIdentity()
    {
        $params = [
            '0x04f96a5e25610293e42a73908e93ccc8c4d4dc0edcfa9fa872f50cb214e08ebf61a03e245533f97284d442460f2998cd41858798ddfd4d661997d3940272b717b1'
        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "shh_hasIdentity",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function newGroup()
    {
        $params = [

        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "shh_newIdentinty",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function addToGroup()
    {
        $params = [
            '0x04f96a5e25610293e42a73908e93ccc8c4d4dc0edcfa9fa872f50cb214e08ebf61a03e245533f97284d442460f2998cd41858798ddfd4d661997d3940272b717b1'
        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "shh_hasIdentity",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function newFilter()
    {
        $params = [
            "topics" => ['0x12341234bf4b564f'],
            "to" => "0x04f96a5e25610293e42a73908e93ccc8c4d4dc0edcfa9fa872f50cb214e08ebf61a03e245533f97284d442460f2998cd41858798ddfd4d661997d3940272b717b1"
        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "shh_newFilter",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function uninstallFilter()
    {
        $params = [
            '0x7'
        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "shh_uninstallFilter",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function getFilterChanges()
    {
        $params = [
            '0x7'
        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "shh_getFilterChanges",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }

    public function getMessages()
    {
        $params = [
            '0x7'
        ];
        $data = [
            'jsonrpc' => "2.0",
            "method" => "shh_getMessages",
            "params" => $params,
            "id" => 1
        ];
        return $this->send($data);
    }
}