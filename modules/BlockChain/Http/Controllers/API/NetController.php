<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/6/18
 * Time: 9:55 AM
 */

namespace BlockChain\Http\Controllers\API;


class NetController extends BaseController
{
    public function getVersion()
    {
        $data = [
            'jsonrpc' => "2.0",
            "method" => "net_version",
            "params" => [],
            "id" => 67
        ];

        return $this->send($data);
    }

    public function listen() {
        $data = [
            'jsonrpc' => "2.0",
            "method" => "net_listening",
            "params" => [],
            "id" => 67
        ];

        return $this->send($data);
    }

    public function countPeer()
    {
        $data = [
            'id' => 74,
            'jsonrpc' => "2.0",
            'method' => 'net_peerCount',
            'params' => [],
        ];

        return $this->send($data);
    }
}