<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 10/17/18
 * Time: 10:46 AM
 */

namespace BlockChain\Http\Controllers\API;


use BlockChain\Http\Services\AccountService;

class AccountController
{
    private $service;

    public function __construct(AccountService $service)
    {
        $this->service = $service;
    }

    public function getTransactions()
    {
        return $this->service->getTransactions();
    }

    public function getBalance()
    {
        return $this->service->getBalance();
    }

    public function getList()
    {
        return $this->service->getList();
    }

    public function getPrice()
    {
        return $this->service->getPrice();
    }
}