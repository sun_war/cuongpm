<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 3/15/19
 * Time: 4:31 PM
 */

namespace BlockChain\Http\Controllers\Auth;


use BlockChain\Http\Repositories\AccountRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class RegisterController
{
    private $accountRepo;

    public function __construct(AccountRepository $accountRepo)
    {
        $this->accountRepo = $accountRepo;
    }

    public function register(Request $request)
    {
        $input = $request->all();

        $user = auth()->user();
        $account = $user->account;
        if (empty($account)) {
            $input['user_id'] = $user->id;
            $input[PASSWORD_COL] = Hash::make($input[PASSWORD_COL]);
            $user = $this->accountRepo->store($input);
        }
        session()->flash('registerBC', 1);
        return response()->json($user);
    }
}
