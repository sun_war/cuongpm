<?php

namespace BlockChain\Http\Controllers;

use App\Http\Controllers\Controller;
use BlockChain\Http\Requests\CryptoCreateRequest;
use BlockChain\Http\Requests\CryptoUpdateRequest;
use BlockChain\Http\Repositories\CryptoRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class CryptoController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(CryptoRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['cryptos'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('bc::crypto.table', $data)->render();
        }
        return view('bc::crypto.index', $data);
    }

    public function create()
    {
        return view('bc::crypto.create');
    }

    public function store(CryptoCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('cryptos.index');
    }

    public function show($id)
    {
        $crypto = $this->repository->find($id);
        if (empty($crypto)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('bc::crypto.show', compact('crypto'));
    }

    public function edit($id)
    {
        $crypto = $this->repository->find($id);
        if (empty($crypto)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('bc::crypto.update', compact('crypto'));
    }

    public function update(CryptoUpdateRequest $request, $id)
    {
        $input = $request->all();
        $crypto = $this->repository->find($id);
        if (empty($crypto)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $crypto);
        session()->flash('success', 'update success');
        return redirect()->route('cryptos.index');
    }

    public function destroy($id)
    {
        $crypto = $this->repository->find($id);
        if (empty($crypto)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
