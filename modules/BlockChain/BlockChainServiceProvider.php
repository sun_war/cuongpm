<?php

namespace BlockChain\Providers;

use BlockChain\Http\Facades\BlockChainMethod;
use BlockChain\Http\Repositories\AccountRepository;
use BlockChain\Http\Repositories\AccountRepositoryEloquent;
use BlockChain\Http\Repositories\CryptoRepository;
use BlockChain\Http\Repositories\CryptoRepositoryEloquent;
use Illuminate\Support\ServiceProvider;

class BlockChainServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'bc');
        $this->loadRoutesFrom(__DIR__ . '/router.php');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('BlockChain', BlockChainMethod::class);

        $this->app->bind(AccountRepository::class, AccountRepositoryEloquent::class);
        $this->app->bind(CryptoRepository::class, CryptoRepositoryEloquent::class);
    }
}
