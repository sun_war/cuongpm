const CryptoJS = require("crypto-js");
const axios = require('axios');
const ethers = require('ethers');
const {Wallet} = ethers;
const {JSEncrypt} = require('jsencrypt');
const {Security} = require('../common/security');
const QRious = require('qrious');
const createAccountBtn = '#createAccountBtn';
const ethCreateForm = '#ethCreateForm';
const loginForm = '#loginForm';

class Auth {
    async register(publicKey) {
        const password = $('#account_password').val();

        const wallet = Wallet.createRandom();
        const mnemonicHash = CryptoJS.SHA256(wallet.mnemonic).toString();
        const privateKeyHash = CryptoJS.SHA256(wallet.privateKey).toString();

        const security = new Security();
        const keystore = security.createKeystore(wallet.privateKey, password);
        security.saveKey(publicKey, wallet.privateKey);
        security.saveMnemonic(publicKey, wallet.mnemonic);

        const data = {
            address: wallet.address,
            password: password,
            private_key: privateKeyHash,
            mnemonic: mnemonicHash,
            keystore: JSON.stringify(keystore)
        };
        const createRoute = $(ethCreateForm).attr('action');
        const res = await axios.post(createRoute, data);
        if(res.data) {
            location.reload();
        } else {
            alert('Lỗi')
        }

    }

    async login(url, dataJson) {
        const security = new Security();
        const res = await axios.post(url, dataJson);
        const wallet = await security.getKeystore(JSON.parse(res.data.keystore), dataJson.password);
        const publicKey = await security.getKey();

        security.saveKey(publicKey, wallet.privateKey);
    }
}

$(createAccountBtn).click(async function () {
    const security = new Security();
    const publicKey = await security.getKey();

    const auth = new Auth();
    auth.register(publicKey);
});

$(loginForm).submit(async function (e) {
    e.preventDefault();

    const self = $(this);
    const url = self.attr('action');

    const data = self.serialize();
    const dataJson = serializeToJson(data);

    const auth = new Auth();
    await auth.login(url, dataJson);
    location.reload();
});

function serializeToJson(data) {
    const dataArray = data.split('&');
    const dataJson = {};
    for (let value of dataArray) {
        const val = value.split('=');
        dataJson[val[0]] = val[1];
    }
    return dataJson;
}

const showMnemonic = parseInt($('html').attr('data-mnemonic'));
if (showMnemonic !== 0) {
    showMnemonicAfterRegister();
}

async function showMnemonicAfterRegister() {
    const security = new Security();
    const privateKey = await security.getKey({key: 'private_key'});

    const mnemonic = security.decryptMnemonic(privateKey);
    $('#mnemonicTxt').val(mnemonic);

    ( function() {
          new QRious({
            element: document.getElementById('qrCoeRecovery'),
            mime: 'image/png',
            value: mnemonic,
            size: '200',
        });

    })();
    $('#backupPhrase').modal('show');
}

