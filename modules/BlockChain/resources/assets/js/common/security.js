const Web3 = require('web3');
const {providerUrl} = require('../configs/network');
const web3 = new Web3(providerUrl);
const axios = require('axios');
const keyRoute = '#keyRoute';
class Security {
    async getKey(params = {}) {
        const keyRoute = $(keyRoute).val();
        const res = await axios.post(keyRoute, params);
        return res.data;
    }

    createKeystore(privateKey, password) {
        return web3.eth.accounts.encrypt(privateKey, password.toString());
    }

    getKeystore(keystore, password) {
        return web3.eth.accounts.decrypt(keystore, password.toString());
    }

    saveKey(publicKey, key) {
        const encrypt = new JSEncrypt();
        encrypt.setPublicKey(publicKey);
        return localStorage.privateKey = encrypt.encrypt(key);
    };

    saveMnemonic(publicKey, mnemonic) {
        const encrypt = new JSEncrypt();
        encrypt.setPublicKey(publicKey);
        localStorage.mnemonic = encrypt.encrypt(mnemonic);
    }

    decryptKey(privateKey) {
        const decrypt = new JSEncrypt();
        decrypt.setPrivateKey(privateKey);
        return decrypt.decrypt(localStorage.privateKey);
    }

    decryptMnemonic(privateKey) {
        const decrypt = new JSEncrypt();
        decrypt.setPrivateKey(privateKey);
        return decrypt.decrypt(localStorage.mnemonic);
    }

    async getPrivateKey() {
        const prk = await this.getKey({key: 'private_key'});
        return this.decryptKey(prk);
    }
}


export {Security};