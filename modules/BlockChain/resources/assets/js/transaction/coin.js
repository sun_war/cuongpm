import {Security} from "../common/security";

const EthService = {
    network: 'ropsten',
    providerUrl: 'http://ropsten.infura.io',
    broadcastTransactionUrl: 'https://ropsten.etherscan.io',
};

const Erc20Service = {
    network: 'ropsten',
    providerUrl: 'http://ropsten.infura.io',
    broadcastTransactionUrl: 'https://ropsten.etherscan.io',
    tokens: {
        mgc4: {
            symbol: 'mgc4',
            name: 'MGC004',
            decimal: 18,
            address: '0xebf0c068cc1dd9b343e92bc2cc09a2ca272d6511',
            transactionFee: 0.0005,
        },
    },
};

const ethereum_address = require('ethereum-address');
const axios = require('axios');
const ethers = require('ethers');
const Web3 = require('web3');
const web3 = new Web3(EthService.providerUrl);

const {JSEncrypt} = require('jsencrypt');
const amountTransactionInput = '#amountTransactionInput';
const toAddressInput = '#toAddressInput';

Erc20Service.getContractABI = async (contractAddress) => {
    try {
        const url = `${Erc20Service.providerUrl}/api?module=contract&action=getabi&address=${contractAddress}&apikey=YourApiKeyToken`;
        const response = await axios.get(url);

        if (response.data.status === '0') {
            toastr.error(response.data.message);
            throw new Error(response.data.message);
        }
        return response.data.result;
    } catch (error) {
        return 0;
    }
};

const transactionForm = '#transactionForm';
$(transactionForm).submit(async function (e) {
    e.preventDefault();
    const sendAddress = $('#toAddressInput').val();
    const receiveAddress = $('#toAddressInput').val();
    const amount = $('#amountTransactionInput').val();
    const fee = $('#feeInput').val();

    const security = new Security();
    const privateKey = security.getPrivateKey();

    const eth = new ETH();
    eth.transaction(sendAddress, receiveAddress, privateKey, amount, fee);

    toastr.success('Send success');
    $(this).trigger('reset');
    $('#transactionModal').modal('hide');
});

class ETH {
    async getPrice(fsyms = 'ETH', tsyms = 'USD') {
        const apiUrl = `https://min-api.cryptocompare.com/data/pricemultifull?fsyms=${fsyms}&tsyms=${tsyms}`;
        const res = await axios.get(apiUrl);
        return res.data.RAW[fsyms][tsyms]['PRICE'];
    }

    async mathPrice(amount) {
        const price = await this.getPrice();
        return parseFloat(price) * parseFloat(amount);
    }

    async getFee() {
        const url = 'https://ethgasstation.info/json/ethgasAPI.json';
        return await axios.get(url);
    }

    async transaction(sendAddress, receiveAddress, privateKey, amount, fee) {
        try {
            const wallet = new ethers.Wallet(privateKey, new ethers.providers.EtherscanProvider(EthService.network));
            const nonce = await wallet.getTransactionCount();
            let transactionInfo = {
                nonce: nonce,
                gasLimit: 21000,
                gasPrice: ethers.utils.parseUnits(fee.toString(), 'gwei'),
                to: receiveAddress,
                value: web3.utils.toHex(web3.utils.toWei(amount.toString(), 'ether')),
            };
            const transaction = await wallet.sendTransaction(transactionInfo);
            console.log(transaction);
            transaction.transactionUrl = EthService.broadcastTransactionUrl + '/tx/' + transaction.hash;
        } catch (e) {
        }
    };

    async transactionToken(sendAddress, receiveAddress, privateKey, amount, fee, coin) {
        try {
            const transactionInfo = {
                gasLimit: 21000,
                gasPrice: ethers.utils.parseUnits(`${fee}`, 'gwei'),
            };
            const wallet = new ethers.Wallet(privateKey, new ethers.providers.EtherscanProvider(Erc20Service.network));
            coin = coin.toLowerCase();
            const contractAddress = Erc20Service.tokens[coin].address;
            const contractAbiFragment = await Erc20Service.getContractABI(contractAddress);
            const contract = new ethers.Contract(contractAddress, contractAbiFragment, new ethers.providers.EtherscanProvider(Erc20Service.network));
            const contractWithSigner = contract.connect(wallet);

            const decimals = Erc20Service.tokens[coin].decimal;
            const numberOfTokens = ethers.utils.parseUnits(amount.toString(), decimals);
            await contractWithSigner.transfer(receiveAddress, numberOfTokens, transactionInfo);
        } catch (error) {

        }
    };
}

const feeSelect = '#feeSelect';

$(feeSelect).change(async function () {
    const eth = new ETH();
    const self = $(this);
    const name = self.val();
    const fees = await eth.getFee();
    const fee = parseFloat(fees.data[name]) / 10;
    $('#feeInput').val(fee);
});

$(amountTransactionInput).change(async function () {
    const eth = new ETH();
    const self = $(this);
    const price = await eth.mathPrice(self.val());
    $('#usdInput').val(price);
});

$(toAddressInput).change(function () {
    const self = $(this);
    const address = self.val();
    if (ethereum_address.isAddress(address)) {
        toastr.success('Address vail');
    } else {
        toastr.error('Address invail');
    }
});

