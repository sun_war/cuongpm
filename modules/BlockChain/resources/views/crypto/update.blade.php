@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('cryptos.index')}}">{{trans('table.cryptos')}}</a>
    </li>
    <li class="active">
        <strong>{{__('action.update')}}</strong>
    </li>
</ol>
<div class="row">
    <form action="{{route('cryptos.update', $crypto->id)}}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        {{method_field('PUT')}}
        <div class="form-group col-lg-12">
    <label for="private_key">{{trans('label.private_key')}}</label>
    <textarea class="form-control" name="private_key" id="private_key">{{$crypto->private_key}}</textarea>
</div>
<div class="form-group col-lg-12">
    <label for="public_key">{{trans('label.public_key')}}</label>
    <textarea class="form-control" name="public_key" id="public_key">{{$crypto->public_key}}</textarea>
</div>

        <div class="col-lg-12 form-group">
            <button class="btn btn-primary">{{trans('button.done')}}</button>
            <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
            <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
            <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
        </div>
    </form>
</div>
@endsection

@push('js')
    <script>
        Menu('#BlockChainMenu', '#cryptoMenu')
    </script>
@endpush
