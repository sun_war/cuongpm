@extends('edu::layouts.app')
@section('content')
    <div class="row">
        <h1>Wallet / Blockchain app platform</h1>
        @if(empty($account))
            <div class="col-lg-6 col-lg-offset-3 form-group">
                <h2>Tạo tài khoản Blockchain</h2>
                <form action="{{route('eth.register')}}" id="ethCreateForm" method="POST">
                    <div class="form-group">
                        <label for="">Password for account</label>
                        <input type="password" name="password" class="form-control" required id="account_password">
                    </div>
                    <div class="form-group">
                        <label for="">Password confirmation</label>
                        <input type="password" name="password_confirmation" class="form-control" required>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" required name="agreed">
                            I have read and agreed to the Terms of Service & Privacy Policy
                        </label>
                    </div>
                    <button type="button" class="btn btn-primary btn-icon" id="createAccountBtn">
                        <i class="fa fa-plus-circle"></i> Create
                    </button>
                </form>
            </div>
        @elseif(auth('eth')->check())
            <div class="col-lg-5 form-group">
                <h2>Thông tin</h2>
                <table class="table table-bordered table-hover">
                    <tr>
                        <th class="active">Address</th>
                        <td>{{$account[ADDRESS_COL]}}</td>
                    </tr>
                    <tr>
                        <th class="active">Total</th>
                        <td>{{$total}} ETH</td>
                    </tr>
                    <tr>
                        <th class="active">Transaction</th>
                        <td class="text-right">
                            <button class="btn btn-xs btn-primary" data-toggle="modal" data-target="#transactionModal">
                                <i class="fa fa-send-o"></i>
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <th class="active">Private key</th>
                        <td class="text-right">
                            <button class="btn btn-xs btn-primary" data-toggle="modal" data-target="#privateKeyModal">
                                <i class="fa fa-key"></i>
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <th class="active">Google OTP <span class="label label-danger">Disable</span></th>
                        <td class="text-right">
                            <button class="btn btn-xs btn-primary" data-toggle="modal" data-target="#privateKeyModal">
                                <i class="fa fa-google-wallet"></i> Enable
                            </button>
                        </td>
                    </tr>
                    <tr>
                        <th class="active">Sign out</th>
                        <td class="text-right">
                            <form method="POST" action="{{route('eth.logout')}}" style="display: inline">
                                {{csrf_field()}}
                            <button class="btn btn-xs btn-primary">
                                <i class="fa fa-sign-out"></i>
                            </button>
                            </form>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-lg-7 form-group">
                <h2>Lịch sử giao dịch</h2>
                @if(count($histories))
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th>Type</th>
                        <th>Address</th>
                        <th>Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($histories as $history)
                        <tr>
                            <td>
                                @if(strtoupper($history['from']) === strtoupper($address))
                                    Send
                                @else
                                    Received
                                @endif
                            </td>
                            <td>{{$history['to']}}</td>
                            <td>{{$history['value']/ pow(10, 18)}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @else
                    <div class="alert-warning alert">
                        Chưa có giao dịch
                    </div>
                @endif
            </div>
        @else
            <div class="col-lg-6 col-lg-offset-3 form-group">
                <form action="{{route('eth.login')}}" method="POST" id="loginForm" class="form-group">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="">Password</label>
                        <input type="password" name="password" class="form-control">
                    </div>
                    <button class="btn btn-primary">Login</button>
                </form>
            </div>
        @endif
        <input type="hidden" value="{{route('eth.key')}}" id="keyRoute">
        @include('bc::layouts.modal.private-key')
        @include('bc::layouts.modal.transaction')
        @include('bc::layouts.modal.backup-phrase')
    </div>
@endsection

@push('js')
    <script type="text/javascript" src="{{asset('modules/BlockChain/js/app.js')}}?v={{rand(0, 9999)}}"></script>
@endpush