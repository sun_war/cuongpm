<div class="modal fade" id="privateKeyModal" tabindex="-1" role="dialog" aria-labelledby="privateKeyModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="privateKeyModalLabel">Private key</h4>
            </div>
            <div class="modal-body">
                <div class="input-group form-group">
                    <input id="privateTxt" class="form-control" readonly value="">
                    <span class="input-group-btn">
                        <button class="btn btn-info" type="button" id="copyPrivateKeyBtn">
                            <i class="fa fa-copy"></i>
                        </button>
                    </span>
                </div>
                <div class="text-center form-group">
                    <img style="display: inline !important;" id="privateKeyQr" class="img-responsive center">
                </div>
            </div>
        </div>
    </div>
</div>