<div class="modal fade" id="transactionModal" tabindex="-1" role="dialog" aria-labelledby="transactionModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="transactionModalLabel">Transaction</h4>
            </div>
            <div class="modal-body">
                <form action="" id="transactionForm">
                    <div class="form-group">
                        <label for="">To</label>
                        <input id="toAddressInput" class="form-control" name="to">
                    </div>
                    <div class="form-group">
                        <label for="">Amount</label>
                        <input type="number" name="amount" id="amountTransactionInput" class="form-control"
                               step="0.0000001"
                               min="0.0000001">
                    </div>
                    <div class="form-group">
                        <label for="">USD</label>
                        <input readonly id="usdInput" name="usd" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="">Transaction Fee</label>
                        <select id="feeSelect" class="form-control">
                            <option></option>
                            <option value="safeLow">Slowly</option>
                            <option value="fast">Regular</option>
                            <option value="fastest">Fast</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input class="form-control" name="fee" readonly="" id="feeInput">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary btn-icon">
                            <i class="fa fa-send-o"></i>Send
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>