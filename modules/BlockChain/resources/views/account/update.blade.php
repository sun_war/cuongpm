@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('accounts.index')}}">{{trans('table.accounts')}}</a>
    </li>
    <li class="active">
        <strong>{{__('action.update')}}</strong>
    </li>
</ol>
<div class="row">
    <form action="{{route('accounts.update', $account->id)}}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        {{method_field('PUT')}}
        <div class="form-group col-lg-12">
    <label for="mnemonic">{{trans('label.mnemonic')}}</label>
    <input required class="form-control" name="mnemonic" id="mnemonic" value="{{$account->mnemonic}}">
</div>
<div class="form-group col-lg-12">
    <label for="private_key">{{trans('label.private_key')}}</label>
    <input required class="form-control" name="private_key" id="private_key" value="{{$account->private_key}}">
</div>
<div class="form-group col-lg-12">
    <label for="keystore">{{trans('label.keystore')}}</label>
    <input required class="form-control" name="keystore" id="keystore" value="{{$account->keystore}}">
</div>
<div class="form-group col-lg-12">
    <label for="status">{{trans('label.status')}}</label>
    <div class="checkbox">
        <label>
            <input required type="checkbox" {{$account->status !== 1 ?: 'checked'}} name="status" id="status" value="1">
        </label>
    </div>
</div>

        <div class="col-lg-12 form-group">
            <button class="btn btn-primary">{{trans('button.done')}}</button>
            <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
            <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
            <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
        </div>
    </form>
</div>
@endsection

@push('js')
    <script>
        Menu('#BlockChainMenu', '#accountMenu')
    </script>
@endpush
