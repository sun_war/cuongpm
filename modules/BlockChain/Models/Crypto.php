<?php

namespace BlockChain\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Crypto extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;
//    use SoftDeletes;

    public $table = 'cryptos';
    public $fillable = ['user_id', 'private_key', 'public_key'];
    public $network = 'ropsten';
    public $broadcastTransactionUrl = 'https://ropsten.etherscan.io';

    public $tokens = [
        'mgc4' => [
            'symbol' => 'mgc4',
            'name' => 'MGC004',
            'decimal' => 18,
            'address' => '0xebf0c068cc1dd9b343e92bc2cc09a2ca272d6511',
            'transactionFree' => 0.1
        ]
    ];

    function getApiUrl()
    {
        switch ($this->network) {
            case 'mainnet':
                $ApiUrl = 'https://api.etherscan.io';
                break;
            case 'ropsten':
                $ApiUrl = 'https://api-ropsten.etherscan.io';
                break;
            default:
                $ApiUrl = 'https://ropsten.etherscan.io';
        }
        return $ApiUrl;
    }
}

