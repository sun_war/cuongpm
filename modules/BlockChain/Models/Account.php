<?php

namespace BlockChain\Models;

use App\User;
use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Account extends Authenticatable
{
    use TransformableTrait;
    use ModelsTrait;
    use SoftDeletes;

    public $table = 'accounts';
    public $fillable = ['user_id', 'address', 'mnemonic', 'private_key', 'keystore', 'status', 'password'];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
}

