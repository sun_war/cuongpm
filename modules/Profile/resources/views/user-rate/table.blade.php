<table class="table table-hover">
    <thead>
    <tr>
        <th>{{trans('label.user_id')}}</th>
        <th>{{trans('label.created_by')}}</th>
        <th>{{trans('label.start')}}</th>
        <th>{{trans('label.description')}}</th>

        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($userRates as $row)
        <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
            <td>{{$row->user->email}}</td>
            <td>{{$row->creatorName('email')}}</td>
            <td>{{$row->start}}</td>
            <td>{{$row->description}}</td>

            <td class="text-right">
                <form method="POST" action="{{route('user-rates.destroy', $row->id)}}">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button class="btn btn-danger btn-xs destroyBtn">
                        <i class="fa fa-trash"></i>
                    </button>
                    <a href="{{route('user-rates.edit', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{route('user-rates.show', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-eye"></i>
                    </a>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$userRates->links()}}
</div>
