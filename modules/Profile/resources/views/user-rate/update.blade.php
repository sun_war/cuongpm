@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('user-rates.index')}}">{{trans('table.user_rates')}}</a>
    </li>
    <li class="active">
        <strong>{{__('action.update')}}</strong>
    </li>
</ol>
<div class="row">
    <form action="{{route('user-rates.update', $userRate->id)}}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        {{method_field('PUT')}}
        <div class="form-group col-lg-12">
    <label for="created_by">{{trans('label.created_by')}}</label>
    <input required type="number" class="form-control" name="created_by" id="created_by" value="{{$userRate->created_by}}">
</div>
<div class="form-group col-lg-12">
    <label for="start">{{trans('label.start')}}</label>
    <input required type="number" class="form-control" name="start" id="start" value="{{$userRate->start}}">
</div>
<div class="form-group col-lg-12">
    <label for="description">{{trans('label.description')}}</label>
    <input required type="number" class="form-control" name="description" id="description" value="{{$userRate->description}}">
</div>

        <div class="col-lg-12 form-group">
            <button class="btn btn-primary">{{trans('button.done')}}</button>
            <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
            <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
            <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
        </div>
    </form>
</div>
@endsection

@push('js')
    <script>
        Menu('#ProfileMenu', '#userRateMenu')
    </script>
@endpush
