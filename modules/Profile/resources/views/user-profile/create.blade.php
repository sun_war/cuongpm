@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('user-profiles.index')}}">{{trans('table.user_profiles')}}</a>
    </li>
    <li class="active">
        <strong>{{__('action.create')}}</strong>
    </li>
</ol>
<div class="row">
    <form action="{{route('user-profiles.store')}}" method="post" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="form-group col-lg-12">
    <label for="height">{{trans('label.height')}}</label>
    <input type="number" required class="form-control" name="height" id="height">
</div>
<div class="form-group col-lg-12">
    <label for="weight">{{trans('label.weight')}}</label>
    <input type="number" required class="form-control" name="weight" id="weight">
</div>
<div class="form-group col-lg-12">
    <label for="round_one">{{trans('label.round_one')}}</label>
    <input type="number" required class="form-control" name="round_one" id="round_one">
</div>
<div class="form-group col-lg-12">
    <label for="round_two">{{trans('label.round_two')}}</label>
    <input type="number" required class="form-control" name="round_two" id="round_two">
</div>
<div class="form-group col-lg-12">
    <label for="round_three">{{trans('label.round_three')}}</label>
    <input type="number" required class="form-control" name="round_three" id="round_three">
</div>
<div class="form-group col-lg-12">
    <label for="image_one">{{trans('label.image_one')}}</label>
    <textarea class="form-control" name="image_one" id="image_one"></textarea>
</div>
<div class="form-group col-lg-12">
    <label for="image_two">{{trans('label.image_two')}}</label>
    <textarea class="form-control" name="image_two" id="image_two"></textarea>
</div>
<div class="form-group col-lg-12">
    <label for="image_three">{{trans('label.image_three')}}</label>
    <textarea class="form-control" name="image_three" id="image_three"></textarea>
</div>
<div class="form-group col-lg-12">
    <label for="website">{{trans('label.website')}}</label>
    <input required class="form-control" name="website" id="website">
</div>

<div class="form-group col-lg-12">
    <label for="hobby">{{trans('label.hobby')}}</label>
    <input required class="form-control" name="hobby" id="hobby">
</div>

<div class="form-group col-lg-12">
    <label for="slogan">{{trans('label.slogan')}}</label>
    <input required class="form-control" name="slogan" id="slogan">
</div>


        <div class="col-lg-12">
            <button class="btn btn-primary">{{trans('button.done')}}</button>
            <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
            <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
            <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
        </div>
    </form>
</div>
@endsection

@push('js')
<script>
    Menu('#ProfileMenu', '#adaAccountMenu')
</script>
@endpush