@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('user-profiles.index')}}">{{trans('table.user_profiles')}}</a>
    </li>
    <li class="active">
        <strong>Show</strong>
    </li>
</ol>
<div>
    <table class="table">
        <tbody>
            <tr><th>{{__('label.height')}}</th>
<td>{!! $user_profile->height !!}</td>
</tr><tr><th>{{__('label.weight')}}</th>
<td>{!! $user_profile->weight !!}</td>
</tr><tr><th>{{__('label.round_one')}}</th>
<td>{!! $user_profile->round_one !!}</td>
</tr><tr><th>{{__('label.round_two')}}</th>
<td>{!! $user_profile->round_two !!}</td>
</tr><tr><th>{{__('label.round_three')}}</th>
<td>{!! $user_profile->round_three !!}</td>
</tr><tr><th>{{__('label.image_one')}}</th>
<td>{!! $user_profile->image_one !!}</td>
</tr><tr><th>{{__('label.image_two')}}</th>
<td>{!! $user_profile->image_two !!}</td>
</tr><tr><th>{{__('label.image_three')}}</th>
<td>{!! $user_profile->image_three !!}</td>
</tr><tr><th>{{__('label.website')}}</th>
<td>{!! $user_profile->website !!}</td>
</tr><tr><th>{{__('label.hobby')}}</th>
<td>{!! $user_profile->hobby !!}</td>
</tr><tr><th>{{__('label.slogan')}}</th>
<td>{!! $user_profile->slogan !!}</td>
</tr>
        </tbody>
    </table>
</div>
@endsection

@push('js')
<script>
    Menu('#ProfileMenu', '#userProfileMenu')
</script>
@endpush
