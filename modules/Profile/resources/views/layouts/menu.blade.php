<li class="has-sub root-level" id="ProfileMenu"><a>
        <i class="fa fa-file"></i>
        <span class="title">{{__('menu.model')}}
        </span>
    </a>
    <ul>
        <li id="userAlbumsMenu">
            <a href="{{route('user-albums.index')}}">
                <span class="title">{{__('table.user_albums')}}</span>
            </a>
        </li>
        <li id="userFollowsMenu">
            <a href="{{route('admin.user-follows.index')}}">
                <span class="title">{{__('table.user_follows')}}</span>
            </a>
        </li>
        <li id="userImagesMenu">
            <a href="{{route('user-images.index')}}">
                <span class="title">{{__('table.user_images')}}</span>
            </a>
        </li>
        <li id="userRatesMenu">
            <a href="{{route('user-rates.index')}}">
                <span class="title">{{__('table.user_rates')}}</span>
            </a>
        </li>
        <li id="userProfilesMenu">
            <a href="{{route('user-profiles.index')}}">
                <span class="title">{{__('table.user_profiles')}}</span>
            </a>
        </li>
    </ul>
</li>