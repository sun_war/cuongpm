@extends('edu::layouts.app')
@section('content')
    <h2>Review (Rờ vêu)</h2>
    <p>Chúng tôi luôn mang trải nghiệp chân thực</p>
    <div class="grid">
        @include('profile::frontend.layouts.image-content')
    </div>
    <input type="hidden" id="page" value="{{request('page', 1)}}">
@endsection

@push('css')
    <style>
        .grid-item {
            width: 250px;
        }
    </style>
@endpush

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/masonry/3.3.2/masonry.pkgd.min.js"></script>
    <script>
        $(window).scroll(function () {
            let k = 0;
            const top = $(window).scrollTop();
            const doHeight = $(document).height();
            const wiHeight = $(window).height();
            const height = doHeight - wiHeight;

            if (
                top === height
            ) {
                if (k < $('#page').val()) {
                    k = $('#page').val();
                    page = parseInt(k) + 1;
                    const data = {page: page};
                    $.ajax({
                        url: '/review',
                        data: data,
                        success: function (data) {
                            if (data) {
                                $('.grid').after(data);
                                $('#page').val(page);
                            }

                        }
                    })
                }
            }
        });

        $(function () {
            $('.grid').masonry({
                itemSelector: '.grid-item',
                columnWidth: 0
            });
        });
    </script>
@endpush