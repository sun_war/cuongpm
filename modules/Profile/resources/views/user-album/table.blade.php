<table class="table table-hover">
    <thead>
    <tr>
        <th>{{trans('label.title')}}</th>
<th>{{trans('label.description')}}</th>
<th>{{trans('label.image')}}</th>
<th>{{trans('label.views')}}</th>
<th>{{trans('label.like')}}</th>

        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($userAlbums as $row)
    <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
        <td>{{$row->title}}</td>
<td>{{$row->description}}</td>
<td>{{$row->image}}</td>
<td>{{$row->views}}</td>
<td>{{$row->like}}</td>

        <td class="text-right">
            <form method="POST" action="{{route('user-albums.destroy', $row->id)}}">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <button class="btn btn-danger btn-xs destroyBtn">
                    <i class="fa fa-trash"></i>
                </button>
                <a href="{{route('user-albums.edit', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="{{route('user-albums.show', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-eye"></i>
                </a>
            </form>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$userAlbums->links()}}
</div>
