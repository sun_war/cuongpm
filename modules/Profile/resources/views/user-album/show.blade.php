@extends('layouts.app')
@section('content')
    <ol class="breadcrumb bc-3">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('user-albums.index')}}">{{trans('table.user_albums')}}</a>
        </li>
        <li class="active">
            <strong>Show</strong>
        </li>
    </ol>
    <div>
        <table class="table">
            <tbody>
            <tr>
                <th>{{__('label.title')}}</th>
                <td>{!! $user_album->title !!}</td>
            </tr>
            <tr>
                <th>{{__('label.description')}}</th>
                <td>{!! $user_album->description !!}</td>
            </tr>
            <tr>
                <th>{{__('label.image')}}</th>
                <td>{!! $user_album->image !!}</td>
            </tr>
            <tr>
                <th>{{__('label.views')}}</th>
                <td>{!! $user_album->views !!}</td>
            </tr>
            <tr>
                <th>{{__('label.like')}}</th>
                <td>{!! $user_album->like !!}</td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection

@push('js')
    <script>
        Menu('#ProfileMenu', '#userAlbumMenu')
    </script>
@endpush
