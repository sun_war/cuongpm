@extends('edu::layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1 form-group">
            <div class="input-group">
                <input class="form-control" placeholder="Search for username...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
              </span>
            </div><!-- /input-group -->
        </div><!-- /.col-lg-6 -->
    </div>
    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">
            <button type="button" class="btn btn-primary btn-xs btn-icon" data-toggle="modal" data-target="#createAlbum">
                <i class="fa fa-plus"></i>  New Album
            </button>
            @foreach($userAlbums as $album)
                <form class="form-group text-right" method="POST" action="{{route('api.user-albums.destroy', $album['id'])}}">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button type="button" class="btn btn-primary btn-xs  btn-icon" data-toggle="modal" data-target="#createImage">
                        <i class="fa fa-plus"></i> New Image
                    </button>
                    <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#updateImage">
                        <i class="fa fa-edit"></i>
                    </button>
                    <button class="btn btn-primary btn-xs">
                        <i class="fa fa-trash"></i>
                    </button>
                </form>
                <h4><a href="{{route('user-albums.show', $album['id'])}}">{{$album->title}}</a></h4>
                <p>{{$album->description}}</p>
                {{--<div class="form-group">--}}
                {{--<img class="img-responsive" src="{{$album->getImage($album[IMAGE_COL])}}" alt="{{$album->title}}">--}}
                {{--</div>--}}
                <span class="btn btn-xs btn-white" id="likeImage" data-id="{{$album->id}}" data-route="{{route('api.user-image-likes.store')}}">
                    <i class="fa fa-heart" style="color: red"></i>
                </span>
                <span data-toggle="modal" data-target="#likerImage" class="btn btn-white btn-xs" id="numLike">69</span>
                {{--@include('profile::user-image.modals.update', compact('image'))--}}
                {{--@include('profile::user-image.modals.liker', compact('image'))--}}
            @endforeach
            <div class="text-center">
                {{$userAlbums->links()}}
            </div>
        </div>
    </div>
    @include('profile::user-image.modals.create')
    @include('profile::user-album.modals.create')
@endsection
@push('css')

@endpush
@push('js')
    <script>
        const likeImage = '#likeImage';
        const numLike = '#numLike';
        $(document).on('click', likeImage, function () {
            const self = $(this);
            const id = self.attr('data-id');
            const noLike = parseInt($(numLike).text());
            $(numLike).text(noLike + 1);
            const data = {
                user_image_id: id
            };
            const url = self.attr('data-route');
            $.ajax({
                url: url,
                data: data,
                method: 'POST',
                success: function (result) {
                    console.log(result);
                }
            });
        });
    </script>
@endpush
