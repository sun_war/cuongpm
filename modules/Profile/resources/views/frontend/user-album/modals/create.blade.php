<div class="modal fade" id="createAlbum" tabindex="-1" role="dialog" aria-labelledby="createAlbumLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="createAlbumLabel">{{__('label.album')}}</h4>
            </div>
            <div class="modal-body">
                <form class="form-group" action="{{route('user-albums.store')}}" method="post"
                      enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="title">{{trans('label.title')}}</label>
                        <input required class="form-control" name="title" id="title">
                    </div>
                    <div class="form-group">
                        <label for="description">{{trans('label.description')}}</label>
                        <textarea required class="form-control" rows="10" name="description" id="description"></textarea>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary">{{trans('button.done')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>