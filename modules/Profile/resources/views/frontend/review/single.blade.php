@extends('edu::layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <img id="singleImage" src="{{$userImage->getImage(IMAGE_COL)}}" alt="" class="img-responsive"
                 style=" display: block;
                  margin-left: auto;
                  margin-right: auto;">
        </div>
        <input type="hidden" id="userImageId" value="{{$userImage->id}}">
    </div>
@endsection

@push('js')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui-touch-punch/0.2.3/jquery.ui.touch-punch.min.js"></script>
    <script>
        let isLike = true;
        let isLoad = true;
        const singleImage = '#singleImage';
        const singleImageRoute = '#singleImageRoute';
        const p = $(singleImage).position();
        const userImageId = '#userImageId';

        $(singleImage).draggable({
            revert: true,
            start: function () {
                isLike = true;
                isLoad = true;
            },
            drag: function () {
                const position = $(this).position();
                const left = p.left - position.left;
                const top = p.top - position.top;
                let id = parseInt($(userImageId).val());
                let params = {};
                // if (left > 100 && isLike) {
                //     isLike = false;
                // }
                if (left < -100 && isLike) {
                    toastr.success('LIKE');
                    isLike = false;
                }
                if (top > 30 && isLoad) {
                    params = {
                        id: id - 1,
                        type: 0
                    };
                    isLoad = false;
                    getImage(params);
                }
                if (top < -30 && isLoad) {
                    params = {
                        id: id + 1,
                        type: 1
                    };
                    isLoad = false;
                    getImage(params);
                }
            },
            stop: function () {
                isLike = true;
                isLoad = true;
            }
        });

        function getImage(params) {
            console.log(params);
            $.ajax({
                url: $(singleImageRoute).val(),
                data: params,
                success: function (data) {
                    if (data.id !== undefined) {
                        $(singleImage).attr('src', data.image);
                        $(userImageId).val(data.id);
                    }
                }
            })
        }
    </script>
@endpush