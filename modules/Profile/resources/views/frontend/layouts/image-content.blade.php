<div class="grid">
    @foreach($userImages as $image)
        <div class="grid-item col-lg-4 col-sm-6 col-xs-12 form-group">
            <div class="thumbnail">
                <a href="{{route('review.single', $image->id)}}">
                    <img src="{{$image->getImage(IMAGE_COL)}}" style="width: 100%">
                </a>
                <div class="caption">
                    <h5>{{$image->title}}</h5>
                    <p>
                        <i class="fa fa-heart" style="color: firebrick"></i>
                        {{number_format(rand(1111, 9999))}}
                    </p>
                </div>
            </div>
        </div>
    @endforeach
</div>
