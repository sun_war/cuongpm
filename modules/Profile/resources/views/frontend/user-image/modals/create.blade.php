<div class="modal fade" id="createImage" tabindex="-1" role="dialog" aria-labelledby="createImageLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="createImageLabel">{{__('label.image')}}</h4>
            </div>
            <div class="modal-body">
                <form class="form-group" action="{{route('user-images.store')}}" method="post"
                      enctype="multipart/form-data">
                    {{csrf_field()}}
                    @isset($user_album_id)
                        <input type="hidden" value="{{$user_album_id}}" name="{{'user_album_id'}}">
                    @endisset
                    <div class="form-group">
                        <label for="title">{{trans('label.title')}}</label>
                        <input required class="form-control" name="title" id="title">
                    </div>
                    <div class="form-group">
                        <label for="description">{{trans('label.description')}}</label>
                        <textarea required class="form-control" rows="10" name="description" id="description"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="image">{{trans('label.image')}}</label>
                        <input type="file" required name="image" id="image">
                        <p class="help-block">Example block-level help text here.</p>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary">{{trans('button.done')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
