<div class="modal fade" id="likerImage" tabindex="-1" role="dialog" aria-labelledby="likerImageLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                {{--<h4 class="modal-title" id="likerImageLabel">--}}
                    <i class="fa fa-heart" style="color: red"></i> {{count($image->users)}}
                {{--</h4>--}}
            </div>
            <div class="modal-body">
                @foreach($image->users as $user)
                    <div class="row">
                        <div class="col-xs-3">
                            <img class="img-responsive img-circle" src="{{$user->getImage(AVATAR_COL)}}" alt="">
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="form-group">
                                {{$user->first_name . ' ' . $user->last_name}}
                            </div>

                            <button class="btn btn-xs btn-primary btn-icon"><i class="fa fa-plus"></i> Add friend</button>
                        </div>
                    </div>

                    {{--<div>--}}
                        {{--{{dump($user)}}--}}
                    {{--</div>--}}
                @endforeach
            </div>
        </div>
    </div>
</div>