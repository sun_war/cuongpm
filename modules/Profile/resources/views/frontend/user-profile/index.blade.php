@extends('edu::layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li class="active">
            <strong>Thông tin cá nhân</strong>
        </li>
    </ol>
    <div class="row">
        <div class="col-sm-3 col-md-2 form-group">
            @if(trim(auth()->user()->avatar) !== '')
                <div class="form-group">
                    <img class="img-responsive img-circle img-thumbnail"
                         src="{{ asset('storage/' . auth()->user()->avatar )}}">
                </div>
            @endif
        </div>
        <div class="col-md-10 col-sm-9 form-group">
            <table class="table table-bordered form-group">
                <tr>
                    <th>
                        {{__('label.height')}}
                    </th>
                    <td>{{$profile[HEIGHT_COL]}}</td>
                </tr>
                <tr>
                    <th>
                        {{__('label.weight')}}
                    </th>
                    <td>{{$profile[WEIGHT_COL]}}</td>
                </tr>
                <tr>
                    <th>
                        {{__('label.round_one')}}
                    </th>
                    <td>{{$profile->round_one}}</td>
                </tr>
                <tr>
                    <th>
                        {{__('label.round_two')}}
                    </th>
                    <td>{{$profile->round_two}}</td>
                </tr>
                <tr>
                    <th>
                        {{__('label.round_three')}}
                    </th>
                    <td>{{$profile->round_three}}</td>
                </tr>
                <tr>
                    <th>
                        {{__('label.website')}}
                    </th>
                    <td>{{$profile->website}}</td>
                </tr>
                <tr>
                    <th>
                        {{__('label.hobby')}}
                    </th>
                    <td>{{$profile->hobby}}</td>
                </tr>
                <tr>
                    <th>
                        {{__('label.slogan')}}
                    </th>
                    <td>{{$profile->slogan}}</td>
                </tr>
            </table>
            <div class="row">
                <div class="col-lg-4 col-md-4 form-group">
                    <a href="{{$profile->getImage($profile->image_one)}}">
                        <img src="{{$profile->getImage($profile->image_one)}}" alt="" class="img-responsive">
                    </a>

                </div>
                <div class="col-lg-4  col-md-4  form-group">
                    <a href="{{$profile->getImage($profile->image_two)}}">
                        <img src="{{$profile->getImage($profile->image_two)}}" alt="" class="img-responsive">
                    </a>
                </div>
                <div class="col-lg-4  col-md-4  form-group">
                    <a href="{{$profile->getImage($profile->image_three)}}">
                        <img src="{{$profile->getImage($profile->image_three)}}" alt="" class="img-responsive">
                    </a>
                </div>
            </div>
            <button class="btn btn-default" data-toggle="modal" data-target="#profile">
                <i class="fa fa-edit"></i> {{__('button.edit')}}
            </button>
            <a class="btn btn-default" href="{{route('user-images.index')}}"><i class="fa fa-image"></i></a>
        </div>
    </div>
@endsection
@push('css')
    <link rel="stylesheet"
          href="{{asset('bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css')}}"/>
@endpush
@push('js')
    <script type="text/javascript" src="{{asset('bower_components/moment/min/moment.min.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js')}}">

    </script>
@endpush