<div class="row">
    <div class="col-lg-10 col-lg-offset-1">
        <button type="button" class="btn btn-primary btn-xs btn-icon" data-toggle="modal" data-target="#createAlbum">
            <i class="fa fa-plus"></i> New Album
        </button>
        <a href="{{route('user-albums.index')}}" class="btn btn-primary btn-xs btn-icon">
            <i class="fa fa-link"></i> Album
        </a>
        <button type="button" class="btn btn-primary btn-xs btn-icon" data-toggle="modal" data-target="#createImage">
            <i class="fa fa-plus"></i> New Image
        </button>
        @foreach($images as $image)
            <form class="form-group text-right" method="POST"
                  action="{{route('user-images.destroy', $image['id'])}}">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#updateImage">
                    <i class="fa fa-edit"></i>
                </button>
                <button class="btn btn-primary btn-xs">
                    <i class="fa fa-trash"></i>
                </button>
            </form>
            <h4>{{$image->title}}</h4>
            <p>{{$image->description}}</p>
            <div class="form-group text-center">
                <img class="img-responsive center" src="{{$image->getImage(IMAGE_COL)}}"
                     alt="{{$image->title}}">
            </div>
            <div class="text-center">
                     <span class="btn btn-xs btn-white" id="likeImage" data-id="{{$image->id}}"
                           data-route="{{route('user-image-likes.store')}}">
                        <i class="fa fa-heart" style="color: red"></i>
                    </span>
                <span data-toggle="modal" data-target="#likerImage" class="btn btn-white btn-xs"
                      id="numLike">{{(int)$image->users_count}}</span>
            </div>
            @include('profile::user-image.modals.update', compact('image'))
            @include('profile::user-image.modals.liker', compact('image'))
        @endforeach
        <div class="text-center">
            {{$images->links()}}
        </div>
    </div>
</div>
