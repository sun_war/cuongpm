<table class="table table-hover">
    <thead>
    <tr>
        <th>{{trans('label.user_images_id')}}</th>
<th>{{trans('label.created_by')}}</th>

        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($userImageLikes as $row)
    <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
        <td>{{$row->user_images_id}}</td>
<td>{{$row->created_by}}</td>

        <td class="text-right">
            <form method="POST" action="{{route('user-image-likes.destroy', $row->id)}}">
                {{csrf_field()}}
                {{method_field('DELETE')}}
                <button class="btn btn-danger btn-xs destroyBtn">
                    <i class="fa fa-trash"></i>
                </button>
                <a href="{{route('user-image-likes.edit', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-edit"></i>
                </a>
                <a href="{{route('user-image-likes.show', $row->id)}}" class="btn btn-info btn-xs">
                    <i class="fa fa-eye"></i>
                </a>
            </form>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$userImageLikes->links()}}
</div>
