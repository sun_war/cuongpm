<?php

namespace Profile;

use Illuminate\Support\ServiceProvider;
use Profile\Http\Repositories\UserAlbumRepository;
use Profile\Http\Repositories\UserAlbumRepositoryEloquent;
use Profile\Http\Repositories\UserFollowRepository;
use Profile\Http\Repositories\UserFollowRepositoryEloquent;
use Profile\Http\Repositories\UserImageLikeRepository;
use Profile\Http\Repositories\UserImageLikeRepositoryEloquent;
use Profile\Http\Repositories\UserImageRepository;
use Profile\Http\Repositories\UserImageRepositoryEloquent;
use Profile\Http\Repositories\UserProfileRepository;
use Profile\Http\Repositories\UserProfileRepositoryEloquent;
use Profile\Http\Repositories\UserRateRepository;
use Profile\Http\Repositories\UserRateRepositoryEloquent;

class ProfileServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/routers/api.php');
        $this->loadRoutesFrom(__DIR__ . '/routers/admin.php');
        $this->loadRoutesFrom(__DIR__ . '/routers/web.php');
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'profile');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserAlbumRepository::class, UserAlbumRepositoryEloquent::class);
        $this->app->bind(UserFollowRepository::class, UserFollowRepositoryEloquent::class);
        $this->app->bind(UserImageLikeRepository::class, UserImageLikeRepositoryEloquent::class);
        $this->app->bind(UserImageRepository::class, UserImageRepositoryEloquent::class);
        $this->app->bind(UserRateRepository::class, UserRateRepositoryEloquent::class);
        $this->app->bind(UserProfileRepository::class, UserProfileRepositoryEloquent::class);
    }
}
