<?php

namespace Profile\Models;

use App\User;
use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class UserRate extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;
//    use SoftDeletes;

    public $table = 'user_rates';
    public $fillable = ['user_id', 'created_by', 'start', 'description'];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }

    public $fileUpload = ['image' => 1];
    protected $pathUpload = ['image' => '/images/user_rates'];
    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [

            ]
        ]
    ];
}

