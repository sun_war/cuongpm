<?php

namespace Profile\Models;

use App\User;
use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class UserImage extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;
    use SoftDeletes;

    public $table = 'user_images';
    public $fillable = ['created_by', 'title', 'description', 'image', 'user_album_id'];

    public function like() {
        return $this->hasMany(UserImageLike::class, 'user_image_id');
    }

    public function users() {
        return $this->belongsToMany(User::class, USER_IMAGE_LIKES_TB,
            'user_image_id', 'created_by');
    }

    public $fileUpload = ['image' => 1];
    protected $pathUpload = ['image' => '/images/user_images'];
    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [

            ]
        ]
    ];
}

