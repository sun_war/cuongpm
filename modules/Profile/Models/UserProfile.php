<?php

namespace Profile\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class UserProfile extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;
//    use SoftDeletes;

    public $table = 'user_profiles';
    public $fillable = ['user_id', 'height', 'weight', 'round_one', 'round_two', 'round_three',
        IMAGE_ONE_COL, IMAGE_TWO_COL, IMAGE_THREE_COL, 'website', 'hobby', 'slogan'];

    public $fileUpload = [
        IMAGE_ONE_COL => 1,
        IMAGE_TWO_COL => 1,
        IMAGE_THREE_COL => 1,
    ];
    protected $pathUpload = [
        IMAGE_ONE_COL => '/images/user_profiles',
        IMAGE_TWO_COL => '/images/user_profiles',
        IMAGE_THREE_COL => '/images/user_profiles',
    ];
    protected $thumbImage = [
        IMAGE_ONE_COL => [
            '/thumbs/' => [

            ]
        ]
    ];
}

