<?php

namespace Profile\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class UserAlbum extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;
//    use SoftDeletes;

    public $table = 'user_albums';
    public $fillable = ['title', 'description', 'image', 'views', 'like'];

    public function images() {
        return $this->hasMany(UserImage::class, 'user_album_id');
    }

    public $fileUpload = ['image' => 1];
    protected $pathUpload = ['image' => '/images/user_albums'];
    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [

            ]
        ]
    ];
}

