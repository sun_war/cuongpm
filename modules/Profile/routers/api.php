<?php
/**
 * Created by PhpStorm.
 * User: cuongpm
 * Date: 5/9/19
 * Time: 11:21 AM
 */


Route::name('api.')
    ->namespace('Profile\Http\Controllers\API')
    ->prefix('api/profile')
    ->middleware(['api', 'auth:api'])
    ->group(function () {
        Route::resource('user-albums' , 'UserAlbumApiController');
        Route::resource('user-follows' , 'UserFollowApiController');
        Route::resource('user-image-likes' , 'UserImageLikeApiController');
        Route::resource('user-images', 'UserImageApiController');
        Route::resource('user-rates' , 'UserRateApiController');
        Route::resource('user-profiles' , 'UserProfileApiController');
    });
