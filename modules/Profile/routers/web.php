<?php
/**
 * Created by PhpStorm.
 * User: cuongpm
 * Date: 5/9/19
 * Time: 11:21 AM
 */


Route::namespace('Profile\Http\Controllers')
    ->middleware(['web', 'auth'])
    ->group(function () {
        Route::resource('user-rates' , 'UserRateController');
        Route::resource('user-images', 'UserImageController');
        Route::resource('user-albums', 'UserAlbumController');
        Route::resource('user-image-likes' , 'UserImageLikeApiController');
        Route::resource('user-images', 'UserImageController');
        Route::resource('user-profiles', 'UserProfileController');
    });

Route::namespace('Profile\Http\Controllers')
    ->middleware(['web'])
    ->group(function () {
        Route::get('review' , 'ReviewController@index')->name('review');
        Route::get('review/single/{id}' , 'ReviewController@single')->name('review.single');
    });
