<?php
/**
 * Created by PhpStorm.
 * User: cuongpm
 * Date: 5/9/19
 * Time: 11:21 AM
 */


Route::name('admin.')
    ->namespace('Profile\Http\Controllers\Admin')
    ->prefix('admin/profile')
    ->middleware(['api', 'auth:api'])
    ->group(function () {
        Route::resource('user-albums' , 'UserAlbumController');
        Route::resource('user-follows' , 'UserFollowController');
        Route::resource('user-image-likes' , 'UserImageLikeController');
        Route::resource('user-images', 'UserImageController');
        Route::resource('user-rates' , 'UserRateController');
        Route::resource('user-profiles' , 'UserProfileController');
    });
