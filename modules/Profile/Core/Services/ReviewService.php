<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 3/23/19
 * Time: 8:35 PM
 */

namespace Profile\Core\Services;


use Profile\Http\Repositories\UserImageRepository;

class ReviewService
{
    private $repository;

    public function __construct(UserImageRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index($request)
    {
        $input = $request->all();
        $input[PER_PAGE] = 21;
        return $this->repository->myPaginate($input);
    }

    public function single($request)
    {
        $id = $request->id;
        $type = $request->type;
        $order = 'DESC';
        $operator = '<';
        if ($type) {
            $operator = '>';
            $order = 'ASC';
        }
        return $this->repository->getSingleById($id, $operator, $order);
    }
}