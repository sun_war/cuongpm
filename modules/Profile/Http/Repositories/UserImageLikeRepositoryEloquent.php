<?php

namespace Profile\Http\Repositories;


use Modularization\MultiInheritance\RepositoriesTrait;

use Illuminate\Support\Facades\Cache;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Profile\Models\UserImageLike;

/**
 * Class NewsRepositoryEloquent
 * @package namespace App\Repositories;
 */
class UserImageLikeRepositoryEloquent extends BaseRepository implements UserImageLikeRepository
{
    use RepositoriesTrait;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return UserImageLike::class;
    }

    public function myPaginate($input)
    {
        isset($input[PER_PAGE]) ?: $input[PER_PAGE] = 10;
        return $this->makeModel()
            ->filter($input)
            ->orderBy('id', 'DESC')
            ->paginate($input[PER_PAGE]);
    }

    public function store($input)
    {
        $input['created_by'] = auth()->id();
        $like = $this->filterFirst($input);
        if($like) {
            return $this->filterDelete($input);
        }
        return $this->create($input);
    }

    public function edit($id)
    {
        $userImageLike = $this->find($id);
        if (empty($userImageLike)) {
            return $userImageLike;
        }
        return compact('userImageLike');
    }

    public function change($input, $data)
    {
        $input['updated_by'] = auth()->id();
        return $this->update($input, $data->id);
 }

    private function standardized($input, $data)
    {
        return $data->uploads($input);
    }

    public function destroy($data)
    {
        return $this->delete($data->id);
    }

    /**
     * Boot up the repository, ping criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
