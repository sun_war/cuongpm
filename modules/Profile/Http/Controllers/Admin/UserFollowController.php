<?php

namespace Profile\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Profile\Http\Requests\UserFollowCreateRequest;
use Profile\Http\Requests\UserFollowUpdateRequest;
use Profile\Http\Repositories\UserFollowRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class UserFollowController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(UserFollowRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['userFollows'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('profile::user-follow.table', $data)->render();
        }
        return view('profile::user-follow.index', $data);
    }

    public function create()
    {
        return view('profile::user-follow.create');
    }

    public function store(UserFollowCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('admin.user-follows.index');
    }

    public function show($id)
    {
        $userFollow = $this->repository->find($id);
        if (empty($userFollow)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('profile::user-follow.show', compact('userFollow'));
    }

    public function edit($id)
    {
        $userFollow = $this->repository->find($id);
        if (empty($userFollow)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('profile::user-follow.update', compact('userFollow'));
    }

    public function update(UserFollowUpdateRequest $request, $id)
    {
        $input = $request->all();
        $userFollow = $this->repository->find($id);
        if (empty($userFollow)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $userFollow);
        session()->flash('success', 'update success');
        return redirect()->route('admin.user-follows.index');
    }

    public function destroy($id)
    {
        $userFollow = $this->repository->find($id);
        if (empty($userFollow)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
