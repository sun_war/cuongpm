<?php

namespace Profile\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Profile\Http\Requests\UserImageCreateRequest;
use Profile\Http\Requests\UserImageUpdateRequest;
use Profile\Http\Repositories\UserImageRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class UserImageController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(UserImageRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['userImages'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('profile::user-image.table', $data)->render();
        }
        return view('profile::user-image.index', $data);
    }

    public function create()
    {
        return view('profile::user-image.create');
    }

    public function store(UserImageCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('user-images.index');
    }

    public function show($id)
    {
        $userImage = $this->repository->find($id);
        if (empty($userImage)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('profile::user-image.show', compact('userImage'));
    }

    public function edit($id)
    {
        $userImage = $this->repository->find($id);
        if (empty($userImage)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('profile::user-image.update', compact('userImage'));
    }

    public function update(UserImageUpdateRequest $request, $id)
    {
        $input = $request->all();
        $userImage = $this->repository->find($id);
        if (empty($userImage)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $userImage);
        session()->flash('success', 'update success');
        return redirect()->route('user-images.index');
    }

    public function destroy($id)
    {
        $userImage = $this->repository->find($id);
        if (empty($userImage)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
