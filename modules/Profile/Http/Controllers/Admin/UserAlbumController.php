<?php

namespace Profile\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Profile\Http\Requests\UserAlbumCreateRequest;
use Profile\Http\Requests\UserAlbumUpdateRequest;
use Profile\Http\Repositories\UserAlbumRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class UserAlbumController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(UserAlbumRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['userAlbums'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('profile::user-album.table', $data)->render();
        }
        return view('profile::user-album.index', $data);
    }

    public function create()
    {
        return view('profile::user-album.create');
    }

    public function store(UserAlbumCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('user-albums.index');
    }

    public function show($id)
    {
        $userAlbum = $this->repository->find($id);
        if (empty($userAlbum)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('profile::user-album.show', compact('userAlbum'));
    }

    public function edit($id)
    {
        $userAlbum = $this->repository->find($id);
        if (empty($userAlbum)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('profile::user-album.update', compact('userAlbum'));
    }

    public function update(UserAlbumUpdateRequest $request, $id)
    {
        $input = $request->all();
        $userAlbum = $this->repository->find($id);
        if (empty($userAlbum)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $userAlbum);
        session()->flash('success', 'update success');
        return redirect()->route('user-albums.index');
    }

    public function destroy($id)
    {
        $userAlbum = $this->repository->find($id);
        if (empty($userAlbum)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
