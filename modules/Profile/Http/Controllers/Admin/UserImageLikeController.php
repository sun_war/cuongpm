<?php

namespace Profile\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Profile\Http\Requests\UserImageLikeCreateRequest;
use Profile\Http\Requests\UserImageLikeUpdateRequest;
use Profile\Http\Repositories\UserImageLikeRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class UserImageLikeController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(UserImageLikeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['userImageLikes'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('profile::user-image-like.table', $data)->render();
        }
        return view('profile::user-image-like.index', $data);
    }

    public function create()
    {
        return view('profile::user-image-like.create');
    }

    public function store(UserImageLikeCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('user-image-likes.index');
    }

    public function show($id)
    {
        $userImageLike = $this->repository->find($id);
        if (empty($userImageLike)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('profile::user-image-like.show', compact('userImageLike'));
    }

    public function edit($id)
    {
        $userImageLike = $this->repository->find($id);
        if (empty($userImageLike)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('profile::user-image-like.update', compact('userImageLike'));
    }

    public function update(UserImageLikeUpdateRequest $request, $id)
    {
        $input = $request->all();
        $userImageLike = $this->repository->find($id);
        if (empty($userImageLike)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $userImageLike);
        session()->flash('success', 'update success');
        return redirect()->route('user-image-likes.index');
    }

    public function destroy($id)
    {
        $userImageLike = $this->repository->find($id);
        if (empty($userImageLike)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
