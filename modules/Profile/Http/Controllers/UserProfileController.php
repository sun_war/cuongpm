<?php

namespace Profile\Http\Controllers;

use App\Http\Controllers\Controller;
use Profile\Http\Requests\UserProfileCreateRequest;
use Profile\Http\Requests\UserProfileUpdateRequest;
use Profile\Http\Repositories\UserProfileRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class UserProfileController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(UserProfileRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $user = auth()->user();
        $profile = $user->profile;
        if(empty($profile)) {
            $profile = $user->profile()->create([]);
        }
        return view('profile::user-profile.index', compact('profile'));
    }

    public function show($id)
    {
        $userProfile = $this->repository->find($id);
        if (empty($userProfile)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('profile::user-profile.show', compact('userProfile'));
    }

    public function edit($id)
    {
        $userProfile = $this->repository->find($id);
        if (empty($userProfile)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('profile::user-profile.update', compact('userProfile'));
    }

    public function update(UserProfileUpdateRequest $request, $id)
    {
        $input = $request->all();
        $userProfile = $this->repository->find($id);
        if (empty($userProfile)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $userProfile);
        session()->flash('success', 'update success');
        return redirect()->route('user-profiles.index');
    }
}
