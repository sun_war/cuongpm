<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 1/25/19
 * Time: 10:58 AM
 */

namespace Profile\Http\Controllers;


use Illuminate\Http\Request;
use Profile\Http\Requests\UserAlbumCreateRequest;
use Profile\Http\Repositories\UserAlbumRepository;

class UserAlbumController
{
    private $repository;
    public function __construct(UserAlbumRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $input['user_id'] = auth()->id();
        $data['userAlbums'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('profile::user-album.table', $data)->render();
        }
        return view('profile::user-album.index', $data);
    }

    public function store(UserAlbumCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return back();
    }

    public function show($id)
    {
        $userAlbum = $this->repository->find($id);
        if (empty($userAlbum)) {
            session()->flash('error', 'not found');
            return back();
        }
        $images = $userAlbum->images()->paginate();;
        return view('profile::user-album.show', compact('userAlbum', 'images'));
    }
}
