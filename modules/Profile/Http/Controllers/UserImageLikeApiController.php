<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/11/18
 * Time: 9:54 AM
 */

namespace Profile\Http\Controllers;

use App\Http\Controllers\Controller;
use Profile\Http\Requests\UserImageLikeCreateRequest;
use Profile\Http\Requests\UserImageLikeUpdateRequest;
use Profile\Http\Resources\UserImageLikeResource;
use Profile\Http\Repositories\UserImageLikeRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class UserImageLikeApiController  extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(UserImageLikeRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data = $this->repository->myPaginate($input);
        return new UserImageLikeResource($data);
    }

    public function create()
    {
        return view('profile::user-image-like.create');
    }

    public function store(UserImageLikeCreateRequest $request)
    {
        $input = $request->all();
        $userImageLike = $this->repository->store($input);
        return new UserImageLikeResource($userImageLike);
    }

    public function show($id)
    {
        $userImageLike = $this->repository->find($id);
        return new UserImageLikeResource($userImageLike);
    }

    public function edit($id)
    {
        $userImageLike = $this->repository->find($id);
        if (empty($userImageLike)) {
            return new UserImageLikeResource($userImageLike);
        }
        return new UserImageLikeResource($userImageLike);
    }

    public function update(UserImageLikeUpdateRequest $request, $id)
    {
        $input = $request->all();
        $userImageLike = $this->repository->find($id);
        if (empty($userImageLike)) {
            return new UserImageLikeResource($userImageLike);
        }
        $data = $this->repository->change($input, $userImageLike);
        return new UserImageLikeResource($data);
    }

    public function destroy($id)
    {
        $userImageLike = $this->repository->find($id);
        if (empty($userImageLike)) {
            return new UserImageLikeResource($userImageLike);
        }
        $data = $this->repository->delete($id);
        return new UserImageLikeResource($data);
    }
}