<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/11/18
 * Time: 9:54 AM
 */

namespace Profile\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Profile\Http\Requests\UserRateCreateRequest;
use Profile\Http\Requests\UserRateUpdateRequest;
use Profile\Http\Resources\UserRateResource;
use Profile\Http\Repositories\UserRateRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

/**
 * @group Profile
 *
 * APIs for profile
 */
class UserRateApiController  extends Controller
{
    use ControllersTrait;
    /**
     * @var UserRateRepository
     */
    private $repository;

    /**
     * UserRateApiController constructor.
     * @param UserRateRepository $repository
     */
    public function __construct(UserRateRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return UserRateResource
     */
    public function index(Request $request)
    {
        $input = $request->all();
        $data = $this->repository->myPaginate($input);
        return new UserRateResource($data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('profile::user-rate.create');
    }

    /**
     * @param UserRateCreateRequest $request
     * @return UserRateResource
     */
    public function store(UserRateCreateRequest $request)
    {
        $input = $request->all();
        $userRate = $this->repository->store($input);
        return new UserRateResource($userRate);
    }

    /**
     * @param $id
     * @return UserRateResource
     */
    public function show($id)
    {
        $userRate = $this->repository->find($id);
        if (empty($userRate)) {
            return new UserRateResource([$userRate]);
        }
        return new UserRateResource($userRate);
    }

    /**
     * @param $id
     * @return UserRateResource
     */
    public function edit($id)
    {
        $userRate = $this->repository->find($id);
        if (empty($userRate)) {
            return new UserRateResource([$userRate]);
        }
        return new UserRateResource($userRate);
    }

    /**
     * @param UserRateUpdateRequest $request
     * @param $id
     * @return UserRateResource
     */
    public function update(UserRateUpdateRequest $request, $id)
    {
        $input = $request->all();
        $userRate = $this->repository->find($id);
        if (empty($userRate)) {
            return new UserRateResource([$userRate]);
        }
        $data = $this->repository->change($input, $userRate);
        return new UserRateResource($data);
    }

    /**
     * @param $id
     * @return UserRateResource
     */
    public function destroy($id)
    {
        $userRate = $this->repository->find($id);
        if (empty($userRate)) {
            return new UserRateResource($userRate);
        }
        $data = $this->repository->delete($id);
        return new UserRateResource([$data]);
    }
}