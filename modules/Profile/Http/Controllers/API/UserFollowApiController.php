<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/11/18
 * Time: 9:54 AM
 */

namespace Profile\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Profile\Http\Requests\UserFollowCreateRequest;
use Profile\Http\Requests\UserFollowUpdateRequest;
use Profile\Http\Resources\UserFollowResource;
use Profile\Http\Repositories\UserFollowRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

/**
 * @group Profile
 *
 * APIs for profile
 */
class UserFollowApiController  extends Controller
{
    use ControllersTrait;
    /**
     * @var UserFollowRepository
     */
    private $repository;

    /**
     * UserFollowApiController constructor.
     * @param UserFollowRepository $repository
     */
    public function __construct(UserFollowRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return UserFollowResource
     */
    public function index(Request $request)
    {
        $input = $request->all();
        $data = $this->repository->myPaginate($input);
        return new UserFollowResource($data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('profile::user-follow.create');
    }

    /**
     * @param UserFollowCreateRequest $request
     * @return UserFollowResource
     */
    public function store(UserFollowCreateRequest $request)
    {
        $input = $request->all();
        $userFollow = $this->repository->store($input);
        return new UserFollowResource($userFollow);
    }

    /**
     * @param $id
     * @return UserFollowResource
     */
    public function show($id)
    {
        $userFollow = $this->repository->find($id);
        if (empty($userFollow)) {
            return new UserFollowResource([$userFollow]);
        }
        return new UserFollowResource($userFollow);
    }

    /**
     * @param $id
     * @return UserFollowResource
     */
    public function edit($id)
    {
        $userFollow = $this->repository->find($id);
        if (empty($userFollow)) {
            return new UserFollowResource([$userFollow]);
        }
        return new UserFollowResource($userFollow);
    }

    /**
     * @param UserFollowUpdateRequest $request
     * @param $id
     * @return UserFollowResource
     */
    public function update(UserFollowUpdateRequest $request, $id)
    {
        $input = $request->all();
        $userFollow = $this->repository->find($id);
        if (empty($userFollow)) {
            return new UserFollowResource([$userFollow]);
        }
        $data = $this->repository->change($input, $userFollow);
        return new UserFollowResource($data);
    }

    /**
     * @param $id
     * @return UserFollowResource
     */
    public function destroy($id)
    {
        $userFollow = $this->repository->find($id);
        if (empty($userFollow)) {
            return new UserFollowResource($userFollow);
        }
        $data = $this->repository->delete($id);
        return new UserFollowResource([$data]);
    }
}