<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/11/18
 * Time: 9:54 AM
 */

namespace Profile\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Profile\Http\Requests\UserAlbumCreateRequest;
use Profile\Http\Requests\UserAlbumUpdateRequest;
use Profile\Http\Resources\UserAlbumResource;
use Profile\Http\Repositories\UserAlbumRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

/**
 * @group Profile
 *
 * APIs for profile
 */
class UserAlbumApiController  extends Controller
{
    use ControllersTrait;
    /**
     * @var UserAlbumRepository
     */
    private $repository;

    /**
     * UserAlbumApiController constructor.
     * @param UserAlbumRepository $repository
     */
    public function __construct(UserAlbumRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return UserAlbumResource
     */
    public function index(Request $request)
    {
        $input = $request->all();
        $data = $this->repository->myPaginate($input);
        return new UserAlbumResource($data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('profile::user-album.create');
    }

    /**
     * @param UserAlbumCreateRequest $request
     * @return UserAlbumResource
     */
    public function store(UserAlbumCreateRequest $request)
    {
        $input = $request->all();
        $userAlbum = $this->repository->store($input);
        return new UserAlbumResource($userAlbum);
    }

    /**
     * @param $id
     * @return UserAlbumResource
     */
    public function show($id)
    {
        $userAlbum = $this->repository->find($id);
        if (empty($userAlbum)) {
            return new UserAlbumResource([$userAlbum]);
        }
        $userAlbum->imageList = $userAlbum->images;
        return new UserAlbumResource($userAlbum);
    }

    /**
     * @param $id
     * @return UserAlbumResource
     */
    public function edit($id)
    {
        $userAlbum = $this->repository->find($id);
        if (empty($userAlbum)) {
            return new UserAlbumResource([$userAlbum]);
        }
        return new UserAlbumResource($userAlbum);
    }

    /**
     * @param UserAlbumUpdateRequest $request
     * @param $id
     * @return UserAlbumResource
     */
    public function update(UserAlbumUpdateRequest $request, $id)
    {
        $input = $request->all();
        $userAlbum = $this->repository->find($id);
        if (empty($userAlbum)) {
            return new UserAlbumResource([$userAlbum]);
        }
        $data = $this->repository->change($input, $userAlbum);
        return new UserAlbumResource($data);
    }

    /**
     * @param $id
     * @return UserAlbumResource
     */
    public function destroy($id)
    {
        $userAlbum = $this->repository->find($id);
        if (empty($userAlbum)) {
            return new UserAlbumResource($userAlbum);
        }
        $data = $this->repository->delete($id);
        return new UserAlbumResource([$data]);
    }
}