<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/11/18
 * Time: 9:54 AM
 */

namespace Profile\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Profile\Http\Requests\UserImageLikeCreateRequest;
use Profile\Http\Requests\UserImageLikeUpdateRequest;
use Profile\Http\Resources\UserImageLikeResource;
use Profile\Http\Repositories\UserImageLikeRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

/**
 * @group Profile
 *
 * APIs for profile
 */
class UserImageLikeApiController  extends Controller
{
    use ControllersTrait;
    /**
     * @var UserImageLikeRepository
     */
    private $repository;

    /**
     * UserImageLikeApiController constructor.
     * @param UserImageLikeRepository $repository
     */
    public function __construct(UserImageLikeRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return UserImageLikeResource
     */
    public function index(Request $request)
    {
        $input = $request->all();
        $data = $this->repository->myPaginate($input);
        return new UserImageLikeResource($data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('profile::user-image-like.create');
    }

    /**
     * @param UserImageLikeCreateRequest $request
     * @return UserImageLikeResource
     */
    public function store(UserImageLikeCreateRequest $request)
    {
        $input = $request->all();
        $userImageLike = $this->repository->store($input);
        return new UserImageLikeResource($userImageLike);
    }

    /**
     * @param $id
     * @return UserImageLikeResource
     */
    public function show($id)
    {
        $userImageLike = $this->repository->find($id);
        return new UserImageLikeResource($userImageLike);
    }

    /**
     * @param $id
     * @return UserImageLikeResource
     */
    public function edit($id)
    {
        $userImageLike = $this->repository->find($id);
        if (empty($userImageLike)) {
            return new UserImageLikeResource($userImageLike);
        }
        return new UserImageLikeResource($userImageLike);
    }

    /**
     * @param UserImageLikeUpdateRequest $request
     * @param $id
     * @return UserImageLikeResource
     */
    public function update(UserImageLikeUpdateRequest $request, $id)
    {
        $input = $request->all();
        $userImageLike = $this->repository->find($id);
        if (empty($userImageLike)) {
            return new UserImageLikeResource($userImageLike);
        }
        $data = $this->repository->change($input, $userImageLike);
        return new UserImageLikeResource($data);
    }

    /**
     * @param $id
     * @return UserImageLikeResource
     */
    public function destroy($id)
    {
        $userImageLike = $this->repository->find($id);
        if (empty($userImageLike)) {
            return new UserImageLikeResource($userImageLike);
        }
        $data = $this->repository->delete($id);
        return new UserImageLikeResource($data);
    }
}