<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/11/18
 * Time: 9:54 AM
 */

namespace Profile\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Profile\Http\Requests\UserImageCreateRequest;
use Profile\Http\Requests\UserImageUpdateRequest;
use Profile\Http\Resources\UserImageResource;
use Profile\Http\Repositories\UserImageRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

/**
 * @group Profile
 *
 * APIs for profile
 */
class UserImageApiController  extends Controller
{
    use ControllersTrait;
    /**
     * @var UserImageRepository
     */
    private $repository;

    /**
     * UserImageApiController constructor.
     * @param UserImageRepository $repository
     */
    public function __construct(UserImageRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return UserImageResource
     */
    public function index(Request $request)
    {
        $input = $request->all();
        $data = $this->repository->myPaginate($input);
        return new UserImageResource($data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('profile::user-image.create');
    }

    /**
     * @param UserImageCreateRequest $request
     * @return UserImageResource
     */
    public function store(UserImageCreateRequest $request)
    {
        $input = $request->all();
        $userImage = $this->repository->store($input);
        return new UserImageResource($userImage);
    }

    /**
     * @param $id
     * @return UserImageResource
     */
    public function show($id)
    {
        $userImage = $this->repository->find($id);
        return new UserImageResource($userImage);
    }

    /**
     * @param $id
     * @return UserImageResource
     */
    public function edit($id)
    {
        $userImage = $this->repository->find($id);
        if (empty($userImage)) {
            return new UserImageResource([$userImage]);
        }
        return new UserImageResource($userImage);
    }

    /**
     * @param UserImageUpdateRequest $request
     * @param $id
     * @return UserImageResource
     */
    public function update(UserImageUpdateRequest $request, $id)
    {
        $input = $request->all();
        $userImage = $this->repository->find($id);
        if (empty($userImage)) {
            return new UserImageResource($userImage);
        }
        $data = $this->repository->change($input, $userImage);
        return new UserImageResource($data);
    }

    /**
     * @param $id
     * @return UserImageResource
     */
    public function destroy($id)
    {
        $userImage = $this->repository->find($id);
        if (empty($userImage)) {
            return new UserImageResource($userImage);
        }
        $data = $this->repository->delete($id);
        return new UserImageResource([$data]);
    }
}