<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/11/18
 * Time: 9:54 AM
 */

namespace Profile\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Profile\Http\Requests\UserProfileCreateRequest;
use Profile\Http\Requests\UserProfileUpdateRequest;
use Profile\Http\Resources\UserProfileResource;
use Profile\Http\Repositories\UserProfileRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

/**
 * @group Profile
 *
 * APIs for profile
 */
class UserProfileApiController  extends Controller
{
    use ControllersTrait;
    /**
     * @var UserProfileRepository
     */
    private $repository;

    /**
     * UserProfileApiController constructor.
     * @param UserProfileRepository $repository
     */
    public function __construct(UserProfileRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return UserProfileResource
     */
    public function index(Request $request)
    {
        $input = $request->all();
        $data = $this->repository->myPaginate($input);
        return new UserProfileResource($data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('profile::user-profile.create');
    }

    /**
     * @param UserProfileCreateRequest $request
     * @return UserProfileResource
     */
    public function store(UserProfileCreateRequest $request)
    {
        $input = $request->all();
        $userProfile = $this->repository->store($input);
        return new UserProfileResource($userProfile);
    }

    /**
     * @param $id
     * @return UserProfileResource
     */
    public function show($id)
    {
        $userProfile = $this->repository->find($id);
        if (empty($userProfile)) {
            return new UserProfileResource([$userProfile]);
        }
        return new UserProfileResource($userProfile);
    }

    /**
     * @param $id
     * @return UserProfileResource
     */
    public function edit($id)
    {
        $userProfile = $this->repository->find($id);
        if (empty($userProfile)) {
            return new UserProfileResource([$userProfile]);
        }
        return new UserProfileResource($userProfile);
    }

    /**
     * @param UserProfileUpdateRequest $request
     * @param $id
     * @return UserProfileResource
     */
    public function update(UserProfileUpdateRequest $request, $id)
    {
        $input = $request->all();
        $userProfile = $this->repository->find($id);
        if (empty($userProfile)) {
            return new UserProfileResource([$userProfile]);
        }
        $data = $this->repository->change($input, $userProfile);
        return new UserProfileResource($data);
    }

    /**
     * @param $id
     * @return UserProfileResource
     */
    public function destroy($id)
    {
        $userProfile = $this->repository->find($id);
        if (empty($userProfile)) {
            return new UserProfileResource($userProfile);
        }
        $data = $this->repository->delete($id);
        return new UserProfileResource([$data]);
    }
}