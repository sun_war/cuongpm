<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 3/9/19
 * Time: 9:47 PM
 */

namespace Profile\Http\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Profile\Core\Services\ReviewService;
use Profile\Models\UserImage;
use Profile\Http\Repositories\UserImageRepository;

class ReviewController extends Controller
{
    private $service;

    public function __construct(ReviewService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $userImages = $this->service->index($request);
        if ($request->ajax()) {
            return view('profile::frontend.layouts.image-content', compact('userImages'))->render();
        }
        return view('profile::index', compact('userImages'));
    }

    public function single(Request $request)
    {
        $userImage = $this->service->single($request);
        if ($request->ajax()) {
            $data = null;
            if($userImage) {
                $data = $userImage->toArray();
                $data['image'] = $userImage->getImage(IMAGE_COL);
            }
            return response()->json($data);
        }
        return view('profile::frontend.review.single', compact('userImage'));
    }
}