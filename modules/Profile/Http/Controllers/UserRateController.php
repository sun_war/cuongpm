<?php

namespace Profile\Http\Controllers;

use App\Http\Controllers\Controller;
use Profile\Http\Requests\UserRateCreateRequest;
use Profile\Http\Requests\UserRateUpdateRequest;
use Profile\Http\Repositories\UserRateRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class UserRateController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(UserRateRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['userRates'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('profile::user-rate.table', $data)->render();
        }
        return view('profile::user-rate.index', $data);
    }

    public function create()
    {
        return view('profile::user-rate.create');
    }

    public function store(UserRateCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'rate success');
        return back();
    }

    public function show($id)
    {
        $userRate = $this->repository->find($id);
        if (empty($userRate)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('profile::user-rate.show', compact('userRate'));
    }

    public function edit($id)
    {
        $userRate = $this->repository->find($id);
        if (empty($userRate)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('profile::user-rate.update', compact('userRate'));
    }

    public function update(UserRateUpdateRequest $request, $id)
    {
        $input = $request->all();
        $userRate = $this->repository->find($id);
        if (empty($userRate)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $userRate);
        session()->flash('success', 'update success');
        return redirect()->route('user-rates.index');
    }

    public function destroy($id)
    {
        $userRate = $this->repository->find($id);
        if (empty($userRate)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
