<?php

namespace Profile\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserRateResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
