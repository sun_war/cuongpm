<?php

namespace Profile\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class UserImageResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        try {
            return parent::toArray($request);
        } catch (\Exception $exception) {
            return response()->json($this->resource);
        }
    }
}
