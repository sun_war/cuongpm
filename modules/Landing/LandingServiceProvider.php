<?php
/**
 * Created by PhpStorm.
 * User: JK
 * Date: 1/27/2018
 * Time: 11:39 AM
 */
namespace Landing;
use Landing\Http\ViewComposers\ConfigComposer;
use Landing\Http\Repositories\AboutRepository;
use Landing\Http\Repositories\AboutRepositoryEloquent;
use Landing\Http\Repositories\ApplicationRepository;
use Landing\Http\Repositories\ApplicationRepositoryEloquent;
use Landing\Http\Repositories\ConfigRepository;
use Landing\Http\Repositories\ConfigRepositoryEloquent;
use Landing\Http\Repositories\ContactRepository;
use Landing\Http\Repositories\ContactRepositoryEloquent;
use Landing\Http\Repositories\IntroduceRepository;
use Landing\Http\Repositories\IntroduceRepositoryEloquent;
use Landing\Http\Repositories\MemberRepository;
use Landing\Http\Repositories\MemberRepositoryEloquent;
use Landing\Http\Repositories\ProductRepository;
use Landing\Http\Repositories\ProductRepositoryEloquent;
use Landing\Http\Repositories\QuotationRepository;
use Landing\Http\Repositories\QuotationRepositoryEloquent;
use Landing\Http\Repositories\ServiceRepository;
use Landing\Http\Repositories\ServiceRepositoryEloquent;
use Landing\Http\Repositories\ShortNewsRepository;
use Landing\Http\Repositories\ShortNewsRepositoryEloquent;
use Landing\Http\Repositories\SiteSettingRepository;
use Landing\Http\Repositories\SiteSettingRepositoryEloquent;
use Landing\Http\Repositories\SlideRepository;
use Landing\Http\Repositories\SlideRepositoryEloquent;
use Landing\Http\Repositories\TitleRepository;
use Landing\Http\Repositories\TitleRepositoryEloquent;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Landing\Facades\ViewFun;

class LandingServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Blade::directive('isActive', function ($is_active) {
            return "<?php echo $is_active ? '<i class=\"fa fa-check text-success\"></i>' : '<i class=\"fa fa-ban text-danger\"></i>' ?>";
        });

        Blade::directive('sort', function ($field) {
            return "<?php echo app('input')->setSort(with{$field}); ?>";
        });

        Blade::directive('menu', function ($group) {
            return "<?php echo ViewFa::isActive($group); ?>";
        });

        Blade::directive('roleName', function ($user) {
            return "<?php echo FoFormat::roleName($user); ?>";
        });

        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        $this->loadRoutesFrom(__DIR__ . '/router.php');
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'cpn');

        view()->composer([
            'edu::layouts.app',
            'en::layouts.app',
        ], ConfigComposer::class);

    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/config/menu.php', 'menu');
        $this->app->bind('ViewFa', ViewFun::class);

        $this->app->bind(AboutRepository::class, AboutRepositoryEloquent::class);
        $this->app->bind(ApplicationRepository::class, ApplicationRepositoryEloquent::class);
        $this->app->bind(ConfigRepository::class, ConfigRepositoryEloquent::class);
        $this->app->bind(ContactRepository::class, ContactRepositoryEloquent::class);

        $this->app->bind(IntroduceRepository::class, IntroduceRepositoryEloquent::class);
        $this->app->bind(MemberRepository::class, MemberRepositoryEloquent::class);
        $this->app->bind(ProductRepository::class, ProductRepositoryEloquent::class);
        $this->app->bind(QuotationRepository::class, QuotationRepositoryEloquent::class);
        $this->app->bind(SiteSettingRepository::class, SiteSettingRepositoryEloquent::class);
        $this->app->bind(SlideRepository::class, SlideRepositoryEloquent::class);
        $this->app->bind(ShortNewsRepository::class, ShortNewsRepositoryEloquent::class);
        $this->app->bind(ServiceRepository::class, ServiceRepositoryEloquent::class);
        $this->app->bind(TitleRepository::class, TitleRepositoryEloquent::class);
    }
}