<?php

namespace Landing\Http\Repositories;


use Modularization\MultiInheritance\RepositoriesTrait;

use Illuminate\Support\Facades\Cache;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Landing\Models\SiteSetting;

/**
 * Class NewsRepositoryEloquent
 * @package namespace App\Repositories;
 */
class SiteSettingRepositoryEloquent extends BaseRepository implements SiteSettingRepository
{
    use RepositoriesTrait;

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return SiteSetting::class;
    }

    public function myPaginate($input)
    {
        isset($input[PER_PAGE]) ?: $input[PER_PAGE] = 10;
        return $this->makeModel()
            ->filter($input)
            ->orderBy('id', 'DESC')
            ->paginate($input[PER_PAGE]);
    }

    public function store($input)
    {
        $input['created_by'] = auth()->id();
        $input = $this->standardized($input, $this->makeModel());
        return $this->create($input);
    }

    public function edit($id)
    {
        $siteSetting = $this->find($id);
        if (empty($siteSetting)) {
            return $siteSetting;
        }
        $siteSetting->social = json_decode($siteSetting->social, true);
        return compact('siteSetting');
    }

    public function change($input, $data)
    {
        $input['updated_by'] = auth()->id();
        $input = $this->standardized($input, $data);
        return $this->update($input, $data->id);
 }

    private function standardized($input, $data)
    {
        $socialArray = json_decode($data['social'], true);
        $input = $this->buildSocial($input, $socialArray);
        return $data->uploads($input);
    }

    private function buildSocial($input, $socialArray) {
        $socials = [];
        foreach (SOCIALS as $SOCIAL) {
            foreach (SOCIAL_ATTRIBUTES as $ATTRIBUTE => $type) {
                $socials[$SOCIAL][$ATTRIBUTE] = $socialArray[$SOCIAL][$ATTRIBUTE];
                if(isset($input[$SOCIAL][$ATTRIBUTE])) {
                    $socials[$SOCIAL][$ATTRIBUTE] = $this->setSocial($input[$SOCIAL][$ATTRIBUTE]);
                }
            }
        }
        $input['social'] = json_encode($socials);
        return $input;
    }

    private function setSocial($input)
    {
        if(is_file($input)) {
            return app('UploadFa')
                ->file($input, storage_path('images/site-setting'));
        } else {
            return $input;
        }
    }

    public function destroy($data)
    {
        return $this->delete($data->id);
    }

    /**
     * Boot up the repository, ping criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
