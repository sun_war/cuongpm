<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/27/18
 * Time: 3:50 PM
 */

namespace Landing\Http\ViewComposers;


use Landing\Http\Repositories\ConfigRepository;
use Illuminate\View\View;

class ConfigComposer
{
    private $repository;

    public function __construct(ConfigRepository $repository)
    {
        $this->repository = $repository;
    }

    public function compose(View $view)
    {
        try {
            $list = $this->repository->filterFirst([LOCALE_COL => \App::getLocale()]);
            if (empty($list)) {
                $list = $this->repository->makeModel()->first();
            }
        } catch (\Exception $exception) {
            $list = [];
        }

        $view->with(['configCompose' => $list]);
    }
}