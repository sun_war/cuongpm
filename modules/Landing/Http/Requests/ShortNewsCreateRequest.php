<?php

namespace Landing\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ShortNewsCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'required',
            'title' => 'required',
            'content' => 'required',
        ];
    }

    public function messages()
    {
        return [

        ];
    }
}
