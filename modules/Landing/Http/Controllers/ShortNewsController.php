<?php

namespace Landing\Http\Controllers;

use App\Http\Controllers\Controller;
use Landing\Http\Requests\ShortNewsCreateRequest;
use Landing\Http\Requests\ShortNewsUpdateRequest;
use Landing\Http\Repositories\ShortNewsRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class ShortNewsController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(ShortNewsRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $input[LOCALE_COL] = \App::getLocale();
        $data['shortNews'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('cpn::short-news.table', $data)->render();
        }
        return view('cpn::short-news.index', $data);
    }

    public function create()
    {
        return view('cpn::short-news.create');
    }

    public function store(ShortNewsCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('short-news.index');
    }

    public function show($id)
    {
        $shortNews = $this->repository->find($id);
        if (empty($shortNews)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('cpn::short-news.show', compact('shortNews'));
    }

    public function edit($id)
    {
        $shortNews = $this->repository->find($id);
        if (empty($shortNews)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('cpn::short-news.update', compact('shortNews'));
    }

    public function update(ShortNewsUpdateRequest $request, $id)
    {
        $input = $request->all();
        $shortNews = $this->repository->find($id);
        if (empty($shortNews)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $shortNews);
        session()->flash('success', 'update success');
        return redirect()->route('short-news.index');
    }

    public function destroy($id)
    {
        $shortNews = $this->repository->find($id);
        if (empty($shortNews)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
