<?php

namespace Landing\Http\Controllers;

use App\Http\Controllers\Controller;
use Landing\Http\Requests\ProductCreateRequest;
use Landing\Http\Requests\ProductUpdateRequest;
use Landing\Http\Repositories\ProductRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class ProductController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $input[LOCALE_COL] = \App::getLocale();
        $data['products'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('cpn::product.table', $data)->render();
        }
        return view('cpn::product.index', $data);
    }

    public function create()
    {
        return view('cpn::product.create');
    }

    public function store(ProductCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('products.index');
    }

    public function show($id)
    {
        $product = $this->repository->find($id);
        if (empty($product)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('cpn::product.show', compact('product'));
    }

    public function edit($id)
    {
        $product = $this->repository->find($id);
        if (empty($product)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('cpn::product.update', compact('product'));
    }

    public function update(ProductUpdateRequest $request, $id)
    {
        $input = $request->all();
        $product = $this->repository->find($id);
        if (empty($product)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $product);
        session()->flash('success', 'update success');
        return redirect()->route('products.index');
    }

    public function destroy($id)
    {
        $product = $this->repository->find($id);
        if (empty($product)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
