<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/11/18
 * Time: 9:54 AM
 */

namespace Landing\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Landing\Http\Requests\AboutCreateRequest;
use Landing\Http\Requests\AboutUpdateRequest;
use Landing\Http\Resources\AboutResource;
use Landing\Http\Repositories\AboutRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class AboutApiController  extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(AboutRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['abouts'] = $this->repository->myPaginate($input);
        return new AboutResource($data);
    }

    public function create()
    {
        return view('cpn::about.create');
    }

    public function store(AboutCreateRequest $request)
    {
        $input = $request->all();
        $about = $this->repository->store($input);
        return new AboutResource($about);
    }

    public function show($id)
    {
        $about = $this->repository->find($id);
        return new AboutResource($about);
    }

    public function edit($id)
    {
        $about = $this->repository->find($id);
        if (empty($about)) {
            return new AboutResource($about);
        }
        return new AboutResource($about);
    }

    public function update(AboutUpdateRequest $request, $id)
    {
        $input = $request->all();
        $about = $this->repository->find($id);
        if (empty($about)) {
            return new AboutResource($about);
        }
        $data = $this->repository->change($input, $about);
        return new AboutResource($data);
    }

    public function destroy($id)
    {
        $about = $this->repository->find($id);
        if (empty($about)) {
            return new AboutResource($about);
        }
        $data = $this->repository->delete($id);
        return new AboutResource($data);
    }
}