<?php

namespace Landing\Http\Controllers;

use App\Http\Controllers\Controller;
use Landing\Http\Requests\IntroduceCreateRequest;
use Landing\Http\Requests\IntroduceUpdateRequest;
use Landing\Http\Repositories\IntroduceRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class IntroduceController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(IntroduceRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $input[LOCALE_COL] = \App::getLocale();
        $data['introduces'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('cpn::introduce.table', $data)->render();
        }
        return view('cpn::introduce.index', $data);
    }

    public function create()
    {
        return view('cpn::introduce.create');
    }

    public function store(IntroduceCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('introduces.index');
    }

    public function show($id)
    {
        $introduce = $this->repository->find($id);
        if (empty($introduce)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('cpn::introduce.show', compact('introduce'));
    }

    public function edit($id)
    {
        $introduce = $this->repository->find($id);
        if (empty($introduce)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('cpn::introduce.update', compact('introduce'));
    }

    public function update(IntroduceUpdateRequest $request, $id)
    {
        $input = $request->all();
        $introduce = $this->repository->find($id);
        if (empty($introduce)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $introduce);
        session()->flash('success', 'update success');
        return redirect()->route('introduces.index');
    }

    public function destroy($id)
    {
        $introduce = $this->repository->find($id);
        if (empty($introduce)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
