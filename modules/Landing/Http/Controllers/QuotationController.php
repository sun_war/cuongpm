<?php

namespace Landing\Http\Controllers;

use App\Http\Controllers\Controller;
use Modularization\Facades\InputFa;
use Landing\Models\Quotation;
use Landing\Http\Requests\QuotationCreateRequest;
use Landing\Http\Requests\QuotationUpdateRequest;
use Landing\Http\Repositories\QuotationRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class QuotationController extends Controller
{
    use ControllersTrait;
    private $repository;
    public function __construct(QuotationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $input[LOCALE_COL] = \App::getLocale();
        $data['quotations'] = $this->repository->makeModel()
            ->orderBy(NO_COL, 'ASC')
            ->filter($input)->get();
        if($request->ajax())
        {
            return view('cpn::quotation.table', $data)->render();
        }
        return view('cpn::quotation.index', $data);
    }

    public function create()
    {
        return view('cpn::quotation.create');
    }

    public function store(QuotationCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('quotation.index');
    }

    public function show($id)
    {
        $quotation = $this->repository->find($id);
        if(empty($quotation))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('cpn::quotation.show', compact('quotation'));
    }

    public function edit($id)
    {
        $quotation = $this->repository->find($id);
        if(empty($quotation))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('cpn::quotation.update', compact('quotation'));
    }

    public function update(QuotationUpdateRequest $request, $id)
    {
        $input = $request->all();
        $quotation = $this->repository->find($id);
        if(empty($quotation))
        {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $quotation);
        session()->flash('success', 'update success');
        return redirect()->route('quotation.index');
    }

    public function destroy($id)
    {
        $quotation = $this->repository->find($id);
        if(empty($quotation))
        {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
