<?php

namespace Landing\Http\Controllers;

use App\Http\Controllers\Controller;
use Landing\Http\Requests\SiteSettingCreateRequest;
use Landing\Http\Requests\SiteSettingUpdateRequest;
use Landing\Http\Repositories\SiteSettingRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class SiteSettingController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(SiteSettingRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['siteSettings'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('cpn::site-setting.table', $data)->render();
        }
        return view('cpn::site-setting.index', $data);
    }

    public function create()
    {
        return view('cpn::site-setting.create');
    }

    public function store(SiteSettingCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('site-settings.index');
    }

    public function show($id)
    {
        $siteSetting = $this->repository->find($id);
        if (empty($siteSetting)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('cpn::site-setting.show', compact('siteSetting'));
    }

    public function edit($id)
    {
        $data = $this->repository->edit($id);
        if (empty($data)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('cpn::site-setting.update', $data);
    }

    public function update(SiteSettingUpdateRequest $request, $id)
    {
        $input = $request->all();
        $siteSetting = $this->repository->find($id);
        if (empty($siteSetting)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $siteSetting);
        session()->flash('success', 'update success');
        return redirect()->route('site-settings.index');
    }

    public function destroy($id)
    {
        $siteSetting = $this->repository->find($id);
        if (empty($siteSetting)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
