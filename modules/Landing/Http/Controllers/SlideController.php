<?php

namespace Landing\Http\Controllers;

use App\Http\Controllers\Controller;
use Modularization\Facades\InputFa;
use Landing\Models\Slide;
use Landing\Http\Requests\SlideCreateRequest;
use Landing\Http\Requests\SlideUpdateRequest;
use Landing\Http\Repositories\SlideRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class SlideController extends Controller
{
    use ControllersTrait;
    private $repository;
    public function __construct(SlideRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['slides'] = $this->repository->makeModel()
            ->orderBy(NO_COL, 'ASC')
            ->filter($input)->get();
        if($request->ajax())
        {
            return view('cpn::slide.table', $data)->render();
        }
        return view('cpn::slide.index', $data);
    }

    public function create()
    {
        return view('cpn::slide.create');
    }

    public function store(SlideCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('slide.index');
    }

    public function show($id)
    {
        $slide = $this->repository->find($id);
        if(empty($slide))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('cpn::slide.show', compact('slide'));
    }

    public function edit($id)
    {
        $slide = $this->repository->find($id);
        if(empty($slide))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('cpn::slide.update', compact('slide'));
    }

    public function update(SlideUpdateRequest $request, $id)
    {
        $input = $request->all();
        $slide = $this->repository->find($id);
        if(empty($slide))
        {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $slide);
        session()->flash('success', 'update success');
        return redirect()->route('slide.index');
    }

    public function destroy($id)
    {
        $slide = $this->repository->find($id);
        if(empty($slide))
        {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }

}
