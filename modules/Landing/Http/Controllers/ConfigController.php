<?php

namespace Landing\Http\Controllers;

use App\Http\Controllers\Controller;
use Landing\Http\Requests\ConfigCreateRequest;
use Landing\Http\Requests\ConfigUpdateRequest;
use Landing\Http\Repositories\ConfigRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class ConfigController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(ConfigRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['configs'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('cpn::config.table', $data)->render();
        }
        return view('cpn::config.index', $data);
    }

    public function create()
    {
        return view('cpn::config.create');
    }

    public function store(ConfigCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('configs.index');
    }

    public function show($id)
    {
        $config = $this->repository->find($id);
        if (empty($config)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('cpn::config.show', compact('config'));
    }

    public function edit($id)
    {
        $config = $this->repository->find($id);
        if (empty($config)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('cpn::config.update', compact('config'));
    }

    public function update(ConfigUpdateRequest $request, $id)
    {
        $input = $request->all();
        $config = $this->repository->find($id);
        if (empty($config)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $config);
        session()->flash('success', 'update success');
        return redirect()->route('configs.index');
    }

    public function destroy($id)
    {
        $config = $this->repository->find($id);
        if (empty($config)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
