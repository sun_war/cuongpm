<?php

namespace Landing\Http\Controllers;

use App\Http\Controllers\Controller;
use Landing\Http\Requests\ApplicationCreateRequest;
use Landing\Http\Requests\ApplicationUpdateRequest;
use Landing\Http\Repositories\ApplicationRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class ApplicationController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(ApplicationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $input[LOCALE_COL] = \App::getLocale();
        $data['applications'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('cpn::application.table', $data)->render();
        }
        return view('cpn::application.index', $data);
    }

    public function create()
    {
        return view('cpn::application.create');
    }

    public function store(ApplicationCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('applications.index');
    }

    public function show($id)
    {
        $application = $this->repository->find($id);
        if (empty($application)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('cpn::application.show', compact('application'));
    }

    public function edit($id)
    {
        $application = $this->repository->find($id);
        if (empty($application)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('cpn::application.update', compact('application'));
    }

    public function update(ApplicationUpdateRequest $request, $id)
    {
        $input = $request->all();
        $application = $this->repository->find($id);
        if (empty($application)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $application);
        session()->flash('success', 'update success');
        return redirect()->route('applications.index');
    }

    public function destroy($id)
    {
        $application = $this->repository->find($id);
        if (empty($application)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
