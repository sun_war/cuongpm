<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Configs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('icon');
            $table->string('title_meta')->nullable();
            $table->string('keywords')->nullable();
            $table->text('description_meta');
            $table->string('image_meta')->nullable();
            $table->string('logo')->nullable();
            $table->text('face_script')->nullable();
            $table->text('google_script')->nullable();
            $table->tinyInteger('is_active')->default(0);
            $table->string('locale')->default('en');
            $table->unsignedInteger('created_by')->nullable();
            $table->unsignedInteger('updated_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configs');
    }
}
