<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContact extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('contacts', function($table){
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email');
            $table->string('phone_number', 24);
            $table->unsignedInteger('created_by')->nullable();
            $table->text('message');
            $table->timestamps();
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('contacts');
	}

}
