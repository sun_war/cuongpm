@extends('layouts.app')
@section('content')
    <div class="row">
        <ol class="breadcrumb bc-3">
            <li>
                <a href="/"><i class="fa fa-home"></i></a>
            </li>
            <li>
                <a href="{{route('about.index')}}">{{trans('table.abouts')}}</a>
            </li>
            <li class="active">
                <strong>{{__('action.update')}}</strong>
            </li>
        </ol>
        <form action="{{route('about.update', $about->id)}}" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            {{method_field('PUT')}}
            <div class="form-group col-lg-6">
                <label for="phone_number">{{trans('label.phone_number')}}</label>
                <input class="form-control" required name="phone_number" id="phone_number"
                       value="{{$about->phone_number}}">
            </div>
            <div class="form-group col-lg-6">
                <label for="address">{{trans('label.address')}}</label>
                <input class="form-control" name="address" required id="address" value="{{$about->address}}">
            </div>
            <div class="form-group col-lg-6">
                <label for="email">{{trans('label.email')}}</label>
                <input class="form-control" name="email" required id="email" value="{{$about->email}}">
            </div>
            <div class="form-group col-lg-6">
                <label for="map">{{trans('label.map')}}</label>
                <input class="form-control" name="map" required id="map" value="{{$about->map}}">
            </div>
            <div class="form-group col-lg-12">
                <label for="info">{{trans('label.info')}}</label>
                <textarea class="form-control ckeditor" required name="info" id="info">{{$about->info}}</textarea>
            </div>
            <div class="form-group col-lg-12">
                <label for="locale">{{trans('label.locale')}}</label>
                <select required class="form-control" name="locale" id="locale">
                    @foreach(LOCALES as $LOCALE => $name)
                        <option {{$about->locale === $LOCALE ? 'selected' : ''}} value="{{$LOCALE}}">{{$name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-6">
                <label for="is_active">{{trans('label.is_active')}}</label>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" {{$about->is_active !== 1 ?: 'checked'}}
                        name="is_active" id="is_active"
                               value="1">
                    </label>
                </div>
            </div>

            <div class="col-lg-12 form-group">
                <button class="btn btn-primary">{{trans('button.done')}}</button>
                <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
                <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
                <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
            </div>
        </form>
    </div>
@endsection
@push('js')
    <script>
        Menu('#componentMenu', '#aboutMenu')
    </script>
@endpush