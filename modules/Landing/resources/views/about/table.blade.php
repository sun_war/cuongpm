<table class="table">
    <thead>
    <tr>

        <th>{{trans('label.phone_number')}}</th>
        <th>{{trans('label.address')}}</th>
        <th>{{trans('label.email')}}</th>
        {{--<th>{{trans('label.map')}}</th>--}}
        {{--<th>{{trans('label.info')}}</th>--}}
        {{--<th>{{trans('label.is_active')}}</th>--}}

        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($abouts as $row)
        <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">

            <td>{{$row->phone_number}}</td>
            <td>{{$row->address}}</td>
            <td>{{$row->email}}</td>
            {{--<td>{{$row->map}}</td>--}}
            {{--<td>{{$row->info}}</td>--}}
            {{--<td>{{$row->is_active}}</td>--}}

            <td class="text-right" width="100px">
                <form method="POST" action="{{route('about.destroy', $row->id)}}">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button class="btn btn-danger btn-xs destroyBtn">
                        <i class="fa fa-trash"></i>
                    </button>
                    <a href="{{route('about.edit', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{route('about.show', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-eye"></i>
                    </a>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$abouts->links()}}
</div>
