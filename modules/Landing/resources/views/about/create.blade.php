@extends('layouts.app')
@section('content')
    <ol class="breadcrumb bc-3">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('about.index')}}">{{trans('table.abouts')}}</a>
        </li>
        <li class="active">
            <strong>{{__('action.create')}}</strong>
        </li>
    </ol>
    <div class="row">
        <form action="{{route('about.store')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group col-lg-6">
                <label for="phone_number">{{trans('label.phone_number')}}</label>
                <input class="form-control" required name="phone_number" id="phone_number">
            </div>
            <div class="form-group col-lg-6">
                <label for="email">{{trans('label.email')}}</label>
                <input class="form-control" required name="email" id="email">
            </div>
            <div class="form-group col-lg-12">
                <label for="address">{{trans('label.address')}}</label>
                <textarea class="form-control ckeditor"  required name="address" id="address"></textarea>
            </div>
            <div class="form-group col-lg-12">
                <label for="map">{{trans('label.map')}}</label>
                <textarea class="form-control ckeditor"  required name="map" id="map"></textarea>
            </div>
            <div class="form-group col-lg-12">
                <label for="info">{{trans('label.info')}}</label>
                <textarea class="form-control ckeditor" required name="info" id="info"></textarea>
            </div>
            @include('cpn::vendor.locale-select')
            <div class="form-group col-lg-6">
                <label for="is_active">{{trans('label.is_active')}}</label>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" checked value="1" name="is_active" id="is_active">
                    </label>
                </div>
            </div>
            <div class="col-lg-12">
                <button class="btn btn-primary">{{trans('button.done')}}</button>
                <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
                <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
                <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
            </div>
        </form>
    </div>
@endsection
@push('js')
    <script>
        Menu('#componentMenu', '#aboutMenu')
    </script>
@endpush