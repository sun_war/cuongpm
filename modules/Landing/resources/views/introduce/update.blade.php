@extends('layouts.app')
@section('content')
    <ol class="breadcrumb bc-3">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('introduces.index')}}">{{trans('table.introduces')}}</a>
        </li>
        <li class="active">
            <strong>{{__('action.update')}}</strong>
        </li>
    </ol>
    <div class="row">
        <form action="{{route('introduces.update', $introduce->id)}}" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            {{method_field('PUT')}}
            <div class="form-group col-lg-6">
                <label for="image" style="display: block">{{trans('label.image')}}</label>
                <div class="form-group">
                    <div class="fileinput fileinput-newform-group" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" data-trigger="fileinput">
                            <img class="img-responsive" src="{{$introduce->image ? asset('storage') . $introduce->image : 'http://placehold.it/200x150'}}" alt="...">
                        </div>
                        <div class="fileinput-preview fileinput-exists thumbnail img-responsive" ></div>
                        <div>
                            <span class="btn btn-white btn-file">
                                <span class="fileinput-new">Select image</span>
                                <span class="fileinput-exists">Change</span>
                                <input type="file" name="image" id="image" accept="image/*">
                            </span>
                            <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group col-lg-12">
                <label for="title">{{trans('label.title')}}</label>
                <input required class="form-control" name="title" id="title" value="{{$introduce->title}}">
            </div>
            <div class="form-group col-lg-12">
                <label for="content">{{trans('label.content')}}</label>
                <textarea class="form-control ckeditor"  name="content" id="content">{{$introduce->content}}</textarea>
            </div>
            <div class="form-group col-lg-12">
                <label for="link">{{trans('label.link')}}</label>
                <input required class="form-control" name="link" id="link" value="{{$introduce->link}}">
            </div>
            <div class="form-group col-lg-12">
                <label for="locale">{{trans('label.locale')}}</label>
                <select required class="form-control" name="locale" id="locale">
                    @foreach(LOCALES as $LOCALE => $name)
                        <option {{$introduce->locale === $LOCALE ? 'selected' : ''}} value="{{$LOCALE}}">{{$name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group col-lg-12">
                <label for="is_active">{{trans('label.is_active')}}</label>
                <div class="checkbox">
                    <label>
                        <input required type="checkbox" {{$introduce->is_active !== 1 ?: 'checked'}} name="is_active"
                               id="is_active" value="1">
                    </label>
                </div>
            </div>
            {{--<div class="form-group col-lg-6">--}}
                {{--<label for="no">{{trans('label.no')}}</label>--}}
                {{--<div class="checkbox">--}}
                    {{--<label>--}}
                        {{--<input required type="checkbox" {{$introduce->no !== 1 ?: 'checked'}} name="no" id="no"--}}
                               {{--value="1">--}}
                    {{--</label>--}}
                {{--</div>--}}
            {{--</div>--}}

            <div class="col-lg-12 form-group">
                <button class="btn btn-primary">{{trans('button.done')}}</button>
                <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
                <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
                <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
            </div>
        </form>
    </div>
@endsection

@push('js')
    <script>
        Menu('#componentMenu', '#introduceMenu')
    </script>
@endpush
