@extends('layouts.app')
@section('content')
    <ol class="breadcrumb bc-3">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('introduces.index')}}">{{trans('table.introduces')}}</a>
        </li>
        <li class="active">
            <strong>Show</strong>
        </li>
    </ol>
    <div>
        <table class="table">
            <tbody>
            <tr>
                <th>{{__('label.image')}}</th>
                <td>
                    <img src="{{$introduce->getImage(IMAGE_COL)}}" width="300px" alt="">
                </td>
            </tr>
            <tr>
                <th>{{__('label.title')}}</th>
                <td>{!! $introduce->title !!}</td>
            </tr>
            <tr>
                <th>{{__('label.content')}}</th>
                <td>{!! $introduce->content !!}</td>
            </tr>
            <tr>
                <th>{{__('label.link')}}</th>
                <td>{!! $introduce->link !!}</td>
            </tr>
            <tr>
                <th>{{__('label.is_active')}}</th>
                <td>{!! $introduce->is_active ?
                '<button class="btn btn-xs btn-success"><i class="fa fa-check"></i></button>' :
                '<button class="btn btn-xs btn-default"><i class="fa fa-ban"></i></button>' !!}</td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection

@push('js')
    <script>
        Menu('#componentMenu', '#introduceMenu')
    </script>
@endpush
