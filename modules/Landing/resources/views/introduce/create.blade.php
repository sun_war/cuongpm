@extends('layouts.app')
@section('content')
    <ol class="breadcrumb bc-3">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('introduces.index')}}">{{trans('table.introduces')}}</a>
        </li>
        <li class="active">
            <strong>{{__('action.create')}}</strong>
        </li>
    </ol>
    <div class="row">
        <form action="{{route('introduces.store')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group col-lg-6">
                <label for="image" style="display: block">{{trans('label.image')}}</label>
                <div class="fileinput fileinput-newform-group" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" data-trigger="fileinput">
                        <img class="img-responsive" src="http://placehold.it/200x150" alt="...">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail img-responsive" ></div>
                    <div>
                    <span class="btn btn-white btn-file">
                        <span class="fileinput-new">Select image</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="file" name="image" id="image" accept="image/*">
                    </span>
                        <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                </div>
            </div>
            <div class="form-group col-lg-12">
                <label for="title">{{trans('label.title')}}</label>
                <input required class="form-control" name="title" id="title">
            </div>

            <div class="form-group col-lg-12">
                <label for="content">{{trans('label.content')}}</label>
                <textarea class="form-control ckeditor"  name="content" id="content"></textarea>
            </div>
            <div class="form-group col-lg-12">
                <label for="link">{{trans('label.link')}}</label>
                <input required class="form-control" name="link" id="link">
            </div>

            @include('cpn::vendor.locale-select')

            <div class="form-group col-lg-12">
                <label for="is_active">{{trans('label.is_active')}}</label>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" required checked value="1" name="is_active" id="is_active">
                    </label>
                </div>
            </div>

            {{--<div class="form-group col-lg-12">--}}
                {{--<label for="no">{{trans('label.no')}}</label>--}}
                {{--<div class="checkbox">--}}
                    {{--<label>--}}
                        {{--<input type="checkbox" required checked value="1" name="no" id="no">--}}
                    {{--</label>--}}
                {{--</div>--}}
            {{--</div>--}}


            <div class="col-lg-12">
                <button class="btn btn-primary">{{trans('button.done')}}</button>
                <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
                <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
                <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
            </div>
        </form>
    </div>
@endsection

@push('js')
    <script>
        Menu('#componentMenu', '#introduceMenu')
    </script>
@endpush