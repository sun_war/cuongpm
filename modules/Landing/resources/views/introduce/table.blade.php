<table class="table table-hover">
    <thead>
    <tr>
        <th>{{trans('label.image')}}</th>
        <th>{{trans('label.title')}}</th>
        <th>{{trans('label.content')}}</th>
        {{--<th>{{trans('label.link')}}</th>--}}
        {{--<th>{{trans('label.is_active')}}</th>--}}
        {{--<th>{{trans('label.no')}}</th>--}}

        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($introduces as $row)
        <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
            <td>
                <a href="{{route('services.edit', $row->id)}}" class="member-img">
                    <img width="150px" src="{{$row->image ? asset('storage') . $row->image : 'http://placehold.it/200x150'}}" class="img-rounded img-responsive"/>
                </a>
            </td>
            <td>{{$row->title}}</td>
            <td>{!! str_limit($row->content, 100) !!}</td>
            {{--<td>{{$row->link}}</td>--}}
            {{--<td>{{$row->is_active}}</td>--}}
            {{--<td>{{$row->no}}</td>--}}

            <td class="text-right" width="100px">
                <form method="POST" action="{{route('introduces.destroy', $row->id)}}">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button class="btn btn-danger btn-xs destroyBtn">
                        <i class="fa fa-trash"></i>
                    </button>
                    <a href="{{route('introduces.edit', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{route('introduces.show', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-eye"></i>
                    </a>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$introduces->links()}}
</div>
