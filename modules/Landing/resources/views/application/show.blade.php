@extends('layouts.app')
@section('content')
    <ol class="breadcrumb bc-3">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('applications.index')}}">{{trans('table.applications')}}</a>
        </li>
        <li class="active">
            <strong>Show</strong>
        </li>
    </ol>
    <div>
        <table class="table">
            <tbody>
            <tr>
                <th>{{__('label.title')}}</th>
                <td>{!! $application->title !!}</td>
            </tr>
            <tr>
                <th>{{__('label.content')}}</th>
                <td>{!! $application->content !!}</td>
            </tr>
            <tr>
                <th>{{__('label.is_active')}}</th>
                <td>{!! $application->is_active ?
                '<button class="btn btn-xs btn-success"><i class="fa fa-check"></i></button>' :
                '<button class="btn btn-xs btn-default"><i class="fa fa-ban"></i></button>' !!}</td>
            </tr>
            {{--<tr>--}}
                {{--<th>{{__('label.no')}}</th>--}}
                {{--<td>{!! $application->no !!}</td>--}}
            {{--</tr>--}}
            </tbody>
        </table>
    </div>
@endsection

@push('js')
    <script>
        Menu('#componentMenu', '#applicationMenu')
    </script>
@endpush
