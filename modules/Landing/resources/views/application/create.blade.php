@extends('layouts.app')
@section('content')
    <ol class="breadcrumb bc-3">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('applications.index')}}">{{trans('table.applications')}}</a>
        </li>
        <li class="active">
            <strong>{{__('action.create')}}</strong>
        </li>
    </ol>
    <div class="row">
        <form action="{{route('applications.store')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group col-lg-12">
                <label for="title">{{trans('label.title')}}</label>
                <input required class="form-control" name="title" id="title">
            </div>

            <div class="form-group col-lg-12">
                <label for="content">{{trans('label.content')}}</label>
                <textarea required class="form-control ckeditor" name="content" id="content"></textarea>
            </div>

            <div class="form-group col-lg-12">
                <label for="is_active">{{trans('label.is_active')}}</label>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" required checked value="1" name="is_active" id="is_active">
                    </label>
                </div>
            </div>

            @include('cpn::vendor.locale-select')

            {{--<div class="form-group col-lg-6">--}}
                {{--<label for="no">{{trans('label.no')}}</label>--}}
                {{--<div class="checkbox">--}}
                    {{--<label>--}}
                        {{--<input type="checkbox" required checked value="1" name="no" id="no">--}}
                    {{--</label>--}}
                {{--</div>--}}
            {{--</div>--}}

            <div class="col-lg-12">
                <button class="btn btn-primary">{{trans('button.done')}}</button>
                <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
                <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
                <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
            </div>
        </form>
    </div>
@endsection

@push('js')
    <script>
        Menu('#componentMenu', '#applicationMenu')
    </script>
@endpush