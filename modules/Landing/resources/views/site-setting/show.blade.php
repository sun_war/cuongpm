@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('site-settings.index')}}">{{trans('table.site_settings')}}</a>
    </li>
    <li class="active">
        <strong>Show</strong>
    </li>
</ol>
<div>
    <table class="table">
        <tbody>
            <tr><th>{{__('label.page_name')}}</th>
<td>{!! $site_setting->page_name !!}</td>
</tr><tr><th>{{__('label.site_email')}}</th>
<td>{!! $site_setting->site_email !!}</td>
</tr><tr><th>{{__('label.site_phone_number')}}</th>
<td>{!! $site_setting->site_phone_number !!}</td>
</tr><tr><th>{{__('label.language')}}</th>
<td>{!! $site_setting->language !!}</td>
</tr><tr><th>{{__('label.copyright')}}</th>
<td>{!! $site_setting->copyright !!}</td>
</tr><tr><th>{{__('label.social')}}</th>
<td>{!! $site_setting->social !!}</td>
</tr><tr><th>{{__('label.created_by')}}</th>
<td>{!! $site_setting->created_by !!}</td>
</tr><tr><th>{{__('label.updated_by')}}</th>
<td>{!! $site_setting->updated_by !!}</td>
</tr>
        </tbody>
    </table>
</div>
@endsection

@push('js')
<script>
    Menu('#ComponentMenu', '#siteSettingMenu')
</script>
@endpush
