@extends('layouts.app')
@section('content')
    <ol class="breadcrumb bc-3">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('site-settings.index')}}">{{trans('table.site_settings')}}</a>
        </li>
        <li class="active">
            <strong>{{__('action.update')}}</strong>
        </li>
    </ol>
    <div class="row">
        <form action="{{route('site-settings.update', $siteSetting->id)}}" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            {{method_field('PUT')}}
            <div class="form-group col-lg-12">
                <label for="page_name">{{trans('label.page_name')}}</label>
                <input required class="form-control" name="page_name" id="page_name"
                       value="{{$siteSetting->page_name}}">
            </div>
            <div class="form-group col-lg-12">
                <label for="site_email">{{trans('label.site_email')}}</label>
                <input required class="form-control" name="site_email" id="site_email"
                       value="{{$siteSetting->site_email}}">
            </div>
            <div class="form-group col-lg-12">
                <label for="site_phone_number">{{trans('label.site_phone_number')}}</label>
                <input required class="form-control" name="site_phone_number" id="site_phone_number"
                       value="{{$siteSetting->site_phone_number}}">
            </div>
            <div class="form-group col-lg-12">
                <label for="language">{{trans('label.language')}}</label>
                <input required class="form-control" name="language" id="language" value="{{$siteSetting->language}}">
            </div>
            <div class="form-group col-lg-12">
                <label for="copyright">{{trans('label.copyright')}}</label>
                <input required class="form-control" name="copyright" id="copyright"
                       value="{{$siteSetting->copyright}}">
            </div>
            @foreach(SOCIALS as $SOCIAL)
                <div class="">
                    <div class="col-lg-12">
                        <h4>{{$SOCIAL}}</h4>
                    </div>
                    @foreach(SOCIAL_ATTRIBUTES as $ATTRIBUTE => $type)
                        {{$siteSetting[$SOCIAL][$ATTRIBUTE]}}
                        <div class="form-group col-lg-3">
                            <label for="social">{{trans('label.' . $ATTRIBUTE)}}</label>
                            <input type="{{$type}}" name="{{$SOCIAL}}[{{$ATTRIBUTE}}]"
                               value="{{
                                        isset($siteSetting['social'][$SOCIAL][$ATTRIBUTE]) ?
                                        $siteSetting['social'][$SOCIAL][$ATTRIBUTE] : NULL
                                }}"
                                   class="form-control" id="social" />
                        </div>
                    @endforeach
                </div>
            @endforeach

            <div class="col-lg-12 form-group">
                <button class="btn btn-primary">{{trans('button.done')}}</button>
                <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
                <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
                <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
            </div>
        </form>
    </div>
@endsection

@push('js')
    <script>
        Menu('#ComponentMenu', '#siteSettingMenu')
    </script>
@endpush
