@extends('layouts.app')
@section('content')
    <ol class="breadcrumb">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('quotation.index')}}">{{trans('table.quotations')}}</a>
        </li>
        <li class="active">
            <strong>Table</strong>
        </li>
    </ol>
    <form class="form-group row" id="formFilter" action="{{route('quotation.index')}}" method="POST">
        {{--<div class="col-sm-8 form-group">--}}
            {{--<label for="name">{{__('label.search')}}</label>--}}
            {{--<input name="name" class="form-control inputFilter" id="name">--}}
        {{--</div>--}}
        <div class="col-sm-2 form-group">
            <label for="is_active">{{__('label.is_active')}}</label>
            <select name="is_active" class="form-control selectFilter" id="is_active">
                <option value="">All</option>
                <option value="1">Active</option>
                <option value="0">Inactive</option>
            </select>
        </div>
        <div class="col-sm-2 form-group">
            <label>{{__('label.action')}}</label>
            <a class="btn btn-primary btn-block" href="{{route('quotation.create')}}"><i class="fa fa-plus"></i></a>
        </div>
    </form>
    <div class="form-group" id="table">
        @include('cpn::quotation.table')
    </div>
    <input type="hidden" id="sortRoute" value="{{route('quotation.sort')}}">
@endsection

@push('js')
    <script src="{{asset('build/form-filter.js')}}"></script>
    <script src="{{asset('build/forceSort.js')}}"></script>
    <script>
        forceSort(sortable, '.no');
        const sortBtn = '#sortBtn';
        const idInputs = '.ids';
        const sortRoute = "#sortRoute";
        $(sortBtn).click(function () {
            let ids = [];
            $(idInputs).each(function () {
                ids.push($(this).val());
            });
            const data = {ids: ids}
            console.log(ids);
            $.ajax({
                url: $(sortRoute).val(),
                method: 'POST',
                data: data,
                success: (data) => {
                    const opts = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-top-right",
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    toastr.success("", "Sắp xếp slides thành công", opts);
                },
                error: () => {
                    const opts = {
                        "closeButton": true,
                        "debug": false,
                        "positionClass": "toast-top-right",
                        "onclick": null,
                        "showDuration": "300",
                        "hideDuration": "1000",
                        "timeOut": "5000",
                        "extendedTimeOut": "1000",
                        "showEasing": "swing",
                        "hideEasing": "linear",
                        "showMethod": "fadeIn",
                        "hideMethod": "fadeOut"
                    };
                    toastr.error("Lỗi máy chủ, vui lòng liên hệ kĩ thuật viên", opts)
                }
            })
        })
    </script>
    <script>
        Menu('#componentMenu', '#quotationMenu')
    </script>
@endpush