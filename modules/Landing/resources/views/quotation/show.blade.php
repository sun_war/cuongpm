@extends('layouts.app')
@section('content')
    <ol class="breadcrumb">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('quotation.index')}}">{{trans('table.quotations')}}</a>
        </li>
        <li class="active">
            <strong>Table</strong>
        </li>
    </ol>
    <div>
        <table class="table">
            <tr>
                <th>{{__('label.image')}}</th>
                <td><img width="300px" src="{{$quotation->getImage(IMAGE_COL)}}" alt=""></td>
            </tr>
            <tr>
                <th>{{__('label.content')}}</th>
                <td>{!! $quotation->content !!}</td>
            </tr>
            <tr>
                <th>{{__('label.author')}}</th>
                <td>{!! $quotation->author !!}</td>
            </tr>
            <tr>
                <th>{{__('label.job')}}</th>
                <td>{!! $quotation->job !!}</td>
            </tr>
            <tr>
                <th>{{__('label.is_active')}}</th>
                <td>{!! $quotation->is_active ?
                '<button class="btn btn-xs btn-success"><i class="fa fa-check"></i></button>' :
                '<button class="btn btn-xs btn-default"><i class="fa fa-ban"></i></button>' !!}</td>
            </tr>
        </table>
    </div>
@endsection