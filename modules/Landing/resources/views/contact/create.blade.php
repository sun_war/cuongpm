@extends('layouts.app')
@section('content')
    <div class="row">
        <ol class="breadcrumb">
            <li>
                <a href="/"><i class="fa fa-home"></i></a>
            </li>
            <li>
                <a href="{{route('contact.index')}}">{{trans('table.contacts')}}</a>
            </li>
            <li class="active">
                <strong>{{__('action.create')}}</strong>
            </li>
        </ol>

        <form action="{{route('contact.store')}}" method="post" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group col-lg-6">
                <label for="name">{{trans('label.name')}}</label>
                <input class="form-control" name="name" id="name">
            </div>

            <div class="form-group col-lg-6">
                <label for="email">{{trans('label.email')}}</label>
                <input class="form-control" name="email" id="email">
            </div>

            <div class="form-group col-lg-6">
                <label for="phone_number">{{trans('label.phone_number')}}</label>
                <input class="form-control" name="phone_number" id="phone_number">
            </div>

            <div class="form-group col-lg-6">
                <label for="created_by">{{trans('label.created_by')}}</label>
                <input type="number" class="form-control" name="created_by" id="created_by">
            </div>
            <div class="form-group col-lg-6">
                <label for="title">{{trans('label.title')}}</label>
                <input class="form-control" name="title" id="title">
            </div>

            <div class="form-group col-lg-12">
                <label for="message">{{trans('label.message')}}</label>
                <textarea class="form-control ckeditor" name="message" id="message"></textarea>
            </div>

            <div class="col-lg-12">
                <button class="btn btn-primary">{{trans('button.done')}}</button>
                <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
                <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
                <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
            </div>
        </form>
    </div>
@endsection
@push('js')
    <script>
        Menu('#componentMenu', '#contactMenu')
    </script>
@endpush