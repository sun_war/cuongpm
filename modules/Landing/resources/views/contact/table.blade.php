<table class="table">
    <thead>
    <tr>

        <th>{{trans('label.name')}}</th>
        <th>{{trans('label.email')}}</th>
        <th>{{trans('label.phone_number')}}</th>
{{--        <th>{{trans('label.created_by')}}</th>--}}
{{--        <th>{{trans('label.title')}}</th>--}}
        <th>{{trans('label.message')}}</th>

        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($contacts as $row)
        {{--<tr class="{{$row->is_active === 1 ? : 'text-danger'}}">--}}
        <tr>

            <td>{{$row->name}}</td>
            <td>{{$row->email}}</td>
            <td>{{$row->phone_number}}</td>
{{--            <td>{{$row->created_by}}</td>--}}
{{--            <td>{{$row->title}}</td>--}}
            <td>{{$row->message}}</td>

            <td class="text-right" width="100px">
                <form method="POST" action="{{route('contact.destroy', $row->id)}}">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button class="btn btn-danger btn-xs destroyBtn">
                        <i class="fa fa-trash"></i>
                    </button>
                    {{--<a href="{{route('contact.edit', $row->id)}}" class="btn btn-info btn-xs">--}}
                        {{--<i class="fa fa-edit"></i>--}}
                    {{--</a>--}}
                    {{--<a href="{{route('contact.show', $row->id)}}" class="btn btn-info btn-xs">--}}
                        {{--<i class="fa fa-eye"></i>--}}
                    {{--</a>--}}
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$contacts->links()}}
</div>
