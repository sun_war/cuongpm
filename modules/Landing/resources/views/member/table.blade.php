<table class="table table-hover">
    <thead>
    <tr>
        <th>{{trans('label.image')}}</th>
        <th>{{trans('label.name')}}</th>
        {{--<th>{{trans('label.twitter_link')}}</th>--}}
        {{--<th>{{trans('label.facebook_link')}}</th>--}}
        {{--<th>{{trans('label.google_link')}}</th>--}}
        {{--<th>{{trans('label.is_active')}}</th>--}}
        {{--<th>{{trans('label.no')}}</th>--}}

        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach($members as $row)
        <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
            <td>
                <a href="{{route('members.edit', $row->id)}}" class="member-img">
                    <img width="150px" src="{{$row->image ? asset('storage') . $row->image : 'http://placehold.it/200x150'}}" class="img-rounded img-responsive"/>
                </a>
            </td>
            <td>{{$row->name}}</td>
            {{--<td>{{$row->twitter_link}}</td>--}}
            {{--<td>{{$row->facebook_link}}</td>--}}
            {{--<td>{{$row->google_link}}</td>--}}
            {{--<td>{{$row->is_active}}</td>--}}
            {{--<td>{{$row->no}}</td>--}}

            <td class="text-right" width="100px">
                <form method="POST" action="{{route('members.destroy', $row->id)}}">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button class="btn btn-danger btn-xs destroyBtn">
                        <i class="fa fa-trash"></i>
                    </button>
                    <a href="{{route('members.edit', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{route('members.show', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-eye"></i>
                    </a>
                </form>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$members->links()}}
</div>
