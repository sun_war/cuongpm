@extends('layouts.app')
@section('content')
    <ol class="breadcrumb bc-3">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('members.index')}}">{{trans('table.members')}}</a>
        </li>
        <li class="active">
            <strong>Show</strong>
        </li>
    </ol>
    <div>
        <table class="table">
            <tbody>
            <tr>
                <th>{{__('label.image')}}</th>
                <td>{!! $member->image !!}</td>
            </tr>
            <tr>
                <th>{{__('label.name')}}</th>
                <td>{!! $member->name !!}</td>
            </tr>
            <tr>
                <th>{{__('label.twitter_link')}}</th>
                <td>{!! $member->twitter_link !!}</td>
            </tr>
            <tr>
                <th>{{__('label.facebook_link')}}</th>
                <td>{!! $member->facebook_link !!}</td>
            </tr>
            <tr>
                <th>{{__('label.google_link')}}</th>
                <td>{!! $member->google_link !!}</td>
            </tr>
            <tr>
                <th>{{__('label.is_active')}}</th>
                <td>{!! $member->is_active !!}</td>
            </tr>
            <tr>
                <th>{{__('label.no')}}</th>
                <td>{!! $member->no !!}</td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection

@push('js')
    <script>
        Menu('#componentMenu', '#memberMenu')
    </script>
@endpush
