@extends('layouts.app')
@section('content')
    <ol class="breadcrumb bc-3">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('services.index')}}">{{trans('table.services')}}</a>
        </li>
        <li class="active">
            <strong>Show</strong>
        </li>
    </ol>
    <div>
        <table class="table">
            <tbody>
            <tr>
                <th>{{__('label.image')}}</th>
                <td><img width="300px" src="{!! $service->getImage(IMAGE_COL) !!}" alt=""></td>
            </tr>
            <tr>
                <th>{{__('label.title')}}</th>
                <td>{!! $service->title !!}</td>
            </tr>
            <tr>
                <th>{{__('label.content')}}</th>
                <td>{!! $service->content !!}</td>
            </tr>
            <tr>
                <th>{{__('label.is_active')}}</th>
                <td>{!! $service->is_active ?
                '<button class="btn btn-xs btn-success"><i class="fa fa-check"></i></button>' :
                '<button class="btn btn-xs btn-default"><i class="fa fa-ban"></i></button>' !!}</td>
            </tr>
            {{--<tr>--}}
                {{--<th>{{__('label.locale')}}</th>--}}
                {{--<td>{!! $service->locale !!}</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
                {{--<th>{{__('label.created_by')}}</th>--}}
                {{--<td>{!! $service->created_by !!}</td>--}}
            {{--</tr>--}}
            {{--<tr>--}}
                {{--<th>{{__('label.updated_by')}}</th>--}}
                {{--<td>{!! $service->updated_by !!}</td>--}}
            {{--</tr>--}}
            </tbody>
        </table>
    </div>
@endsection

@push('js')
    <script>
        Menu('#componentMenu', '#serviceMenu')
    </script>
@endpush
