<div class="form-group col-lg-12">
    <label for="locale">{{trans('label.locale')}}</label>
    <select required class="form-control" name="locale" id="locale">
        @foreach(LOCALES as $LOCALE => $name)
            <option {{auth()->user()->locale === $LOCALE ? 'selected' : ''}} value="{{$LOCALE}}">{{$name}}</option>
        @endforeach
    </select>
</div>