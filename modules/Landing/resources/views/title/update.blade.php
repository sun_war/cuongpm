@extends('layouts.app')
@section('content')
    <ol class="breadcrumb bc-3">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('titles.index')}}">{{trans('table.titles')}}</a>
        </li>
        <li class="active">
            <strong>{{__('action.update')}}</strong>
        </li>
    </ol>
    <div class="row">
        <form action="{{route('titles.update', $title->id)}}" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            {{method_field('PUT')}}
            <div class="form-group col-lg-12">
                <label for="title">{{trans('label.title')}}</label>
                <input required class="form-control" name="title" id="title" value="{{$title->title}}">
            </div>
            <div class="form-group col-lg-12">
                <label for="content">{{trans('label.content')}}</label>
                <textarea class="form-control ckeditor"  name="content" id="content">{{$title->content}}</textarea>
            </div>
            <div class="form-group col-lg-12">
                <label for="is_active">{{trans('label.is_active')}}</label>
                <div class="checkbox">
                    <label>
                        <input required type="checkbox" {{$title->is_active !== 1 ?: 'checked'}} name="is_active"
                               id="is_active" value="1">
                    </label>
                </div>
            </div>
            <div class="form-group col-lg-12">
                <label for="locale">{{trans('label.locale')}}</label>
                <select required class="form-control" name="locale" id="locale">
                    @foreach(LOCALES as $LOCALE => $name)
                        <option {{$title->locale === $LOCALE ? 'selected' : ''}} value="{{$LOCALE}}">{{$name}}</option>
                    @endforeach
                </select>
            </div>
            {{--<div class="form-group col-lg-12">--}}
                {{--<label for="no">{{trans('label.no')}}</label>--}}
                {{--<div class="checkbox">--}}
                    {{--<label>--}}
                        {{--<input required type="checkbox" {{$title->no !== 1 ?: 'checked'}} name="no" id="no" value="1">--}}
                    {{--</label>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="form-group col-lg-12">--}}
                {{--<label for="locale">{{trans('label.locale')}}</label>--}}
                {{--<input required class="form-control" name="locale" id="locale" value="{{$title->locale}}">--}}
            {{--</div>--}}
            {{--<div class="form-group col-lg-12">--}}
                {{--<label for="created_by">{{trans('label.created_by')}}</label>--}}
                {{--<input required type="number" class="form-control" name="created_by" id="created_by"--}}
                       {{--value="{{$title->created_by}}">--}}
            {{--</div>--}}
            {{--<div class="form-group col-lg-12">--}}
                {{--<label for="updated_by">{{trans('label.updated_by')}}</label>--}}
                {{--<input required type="number" class="form-control" name="updated_by" id="updated_by"--}}
                       {{--value="{{$title->updated_by}}">--}}
            {{--</div>--}}

            <div class="col-lg-12 form-group">
                <button class="btn btn-primary">{{trans('button.done')}}</button>
                <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
                <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
                <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
            </div>
        </form>
    </div>
@endsection

@push('js')
    <script>
        Menu('#componentMenu', '#titleMenu')
    </script>
@endpush
