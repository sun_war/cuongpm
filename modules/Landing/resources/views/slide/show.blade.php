@extends('resources.views.layouts.app')
@section('content')
    <ol class="breadcrumb">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('slide.index')}}">{{trans('table.slides')}}</a>
        </li>
        <li class="active">
            <strong>Table</strong>
        </li>
    </ol>
    <div>
        {{--{!! '$'.image !!}--}}
        {{--{!! '$'.content !!}--}}
        {{--{!! '$'.is_active !!}--}}

    </div>
    <a href="{{url()->previous()}}" class="btn btn-default"><i class="fa fa-backward"></i></a>
@endsection