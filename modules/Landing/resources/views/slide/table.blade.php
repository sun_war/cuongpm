<div id="sortable">
    @foreach($slides as $row)
        <div class="row form-group">
            <div class="col-lg-3">
                <a href="{{route('slide.edit', $row->id)}}" class="member-img">
                    <img src="{{ ViewFa::thumb($row->image, [600, 320]) }}" class="img-rounded img-responsive"/>
                </a>
            </div>
            <div class="col-sm-7">
                <h4><strong>No. <span class="no"></span></strong></h4>
                <p>{{$row->title}}</p>
                <p>{!! $row->content !!}</p>
            </div>
            <div class="col-sm-2 text-right">
                <form method="POST" action="{{route('slide.destroy', $row->id)}}">
                    <input type="hidden" class="ids" name="ids[]" value="{{$row->id}}">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button class="btn btn-danger btn-xs destroyBtn">
                        <i class="fa fa-trash"></i>
                    </button>
                    <a href="{{route('slide.edit', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-edit"></i>
                    </a>
                    {{--<a href="{{route('slide.show', $row->id)}}" class="btn btn-info btn-xs">--}}
                        {{--<i class="fa fa-eye"></i>--}}
                    {{--</a>--}}
                </form>
            </div>
        </div>
    @endforeach
</div>

<div class="text-center">
    <button class="btn btn-primary btn-icon" id="sortBtn"><i class="fa fa-sort-amount-asc"></i> {{__('button.save')}} </button>
</div>
