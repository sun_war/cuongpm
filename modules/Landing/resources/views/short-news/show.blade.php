@extends('layouts.app')
@section('content')
    <ol class="breadcrumb bc-3">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('short-news.index')}}">{{trans('table.short_news')}}</a>
        </li>
        <li class="active">
            <strong>Show</strong>
        </li>
    </ol>
    <div>
        <table class="table">
            <tbody>
            <tr>
                <th>{{__('label.image')}}</th>
                <td><img width="300px" src="{{$shortNews->getImage(IMAGE_COL)}}" alt=""></td>
            </tr>
            <tr>
                <th>{{__('label.title')}}</th>
                <td>{!! $shortNews->title !!}</td>
            </tr>
            <tr>
                <th>{{__('label.content')}}</th>
                <td>{!! $shortNews->content !!}</td>
            </tr>
            <tr>
                <th>{{__('label.is_active')}}</th>
                <td>{!! $shortNews->is_active ?
                '<button class="btn btn-xs btn-success"><i class="fa fa-check"></i></button>' :
                '<button class="btn btn-xs btn-default"><i class="fa fa-ban"></i></button>' !!}</td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection

@push('js')
    <script>
        Menu('#componentMenu', '#shortNewsMenu')
    </script>
@endpush
