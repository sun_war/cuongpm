<?php
/**
 * Created by PhpStorm.
 * User: cuongpm00
 * Date: 10/21/2016
 * Time: 10:07 AM
 */

namespace Landing\Facades;

use Illuminate\Support\Facades\Route;

class ViewFun
{
    public function isMenu($group, $char = '.')
    {
        $routeName = Route::currentRouteName();
        $isChar = strpos($routeName, $char);
        $name = substr($routeName, 0, $isChar);
        $arrayName = config('menu.' . $group);
        if (count(config('menu.' . $group)) == 0) {
            $arrayName = [];
        }
        return in_array($name, $arrayName);
    }

    public function isActive($group, $char = '.')
    {
        if ($this->isMenu($group, $char)) {
            return 'root-level opened';
        }
        return '';
    }

    public function thumb($image, $sizes = null)
    {
        if ($sizes !== null) {
            if ($image !== '' && $image !== null) {
                $size = '_' . implode('_', $sizes) . '.';
                $images = explode('.', $image);
                $image = config('app.asset_url') . ('/storage') . implode($size, $images);
            } else {
                $image = 'http://placehold.it/200x150';
            }
        } else {
            $image = config('app.asset_url') . ('/storage' . $image);
        }
        return $image;
    }

    public function underline($sentence, $word)
    {
        return str_replace($sentence, "<u>" . $sentence . "</u>", $word);
    }

    public function mistake($item)
    {
        $question = $item->question;
        foreach (REPLIES as $REPLY) {
            $question = str_replace($item[$REPLY], "<u>" . $item[$REPLY] . "</u>", $question);
        }
        return $question;
    }
}
