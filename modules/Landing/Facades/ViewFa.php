<?php
namespace Landing\Facades;
use Illuminate\Support\Facades\Facade;

/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 4/28/17
 * Time: 6:01 PM
 */
class ViewFa extends Facade
{
    public static function getFacadeAccessor()
    {
        return 'ViewFa';
    }
}