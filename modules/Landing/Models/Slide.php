<?php

namespace Landing\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Slide extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'slides';
    public $fillable = [IMAGE_COL, TITLE_COL, CONTENT_COL, 'is_active', NO_COL, 'created_by', 'updated_by', LOCALE_COL];

    public function scopeFilter($query, $input)
    {
        if (isset($input[IMAGE_COL])) {
            $query->where(IMAGE_COL, $input[IMAGE_COL]);
        }
        if (isset($input[TITLE_COL])) {
            $query->where(TITLE_COL, $input[TITLE_COL]);
        }
        if (isset($input[CONTENT_COL])) {
            $query->where(CONTENT_COL, $input[CONTENT_COL]);
        }
        if (isset($input['is_active'])) {
            $query->where('is_active', $input['is_active']);
        }
        return $query;
    }



    public $fileUpload = [IMAGE_COL => 1];
    protected $pathUpload = [IMAGE_COL => '/images/slides'];
    protected $thumbImage = [
        IMAGE_COL => [
            '/thumbs/' => [
                [1200, 640], [900, 500], [600, 320]
            ]
        ]
    ];
    protected $checkbox = ['is_active'];
}

