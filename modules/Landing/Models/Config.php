<?php

namespace Landing\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Config extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;
    use SoftDeletes;

    public $table = 'configs';
    public $fillable = ['title', DESCRIPTION_META_COL, IMAGE_META_COL, LOGO_COL, 'face_script', 'google_script', 'is_active', 'locale', 'created_by', 'updated_by'];

    public $fileUpload = [IMAGE_META_COL => 1, LOGO_COL => 1];
    protected $pathUpload = [
        IMAGE_META_COL => '/images/configs',
        LOGO_COL => '/images/configs'
    ];
    protected $thumbImage = [
        IMAGE_META_COL => [
            '/thumbs/' => [

            ]
        ],
        LOGO_COL => [
            '/thumbs/' => [

            ]
        ]
    ];
}

