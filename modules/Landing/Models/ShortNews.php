<?php

namespace Landing\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class ShortNews extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;
    use SoftDeletes;

    public $table = 'short_news';
    public $fillable = ['image', 'title', 'content', 'is_active', 'no', 'created_by', 'updated_by', LOCALE_COL];

    public $fileUpload = ['image' => 1];
    protected $pathUpload = ['image' => '/images/short_news'];
    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [

            ]
        ]
    ];
}

