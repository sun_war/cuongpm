<?php

namespace Landing\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Introduce extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;
    use SoftDeletes;

    public $table = 'introduces';
    public $fillable = [IMAGE_COL, 'title', 'content', 'link', 'is_active', 'no', 'created_by', 'updated_by', LOCALE_COL];

    public $fileUpload = ['image' => 1];
    protected $pathUpload = ['image' => '/images/introduces'];
    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [

            ]
        ]
    ];
}

