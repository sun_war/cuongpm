<?php

namespace Landing\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class About extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'abouts';
    public $fillable = [PHONE_NUMBER_COL, ADDRESS_COL, 'email', MAP_COL, INFO_COL, 'is_active', 'created_by', 'updated_by', LOCALE_COL];

    public function scopeFilter($query, $input)
    {
        if (isset($input[PHONE_NUMBER_COL])) {
            $query->where(PHONE_NUMBER_COL, $input[PHONE_NUMBER_COL]);
        }
        if (isset($input[ADDRESS_COL])) {
            $query->where(ADDRESS_COL, $input[ADDRESS_COL]);
        }
        if (isset($input['email'])) {
            $query->where('email', $input['email']);
        }
        if (isset($input[MAP_COL])) {
            $query->where(MAP_COL, $input[MAP_COL]);
        }
        if (isset($input[INFO_COL])) {
            $query->where(INFO_COL, $input[INFO_COL]);
        }
        if (isset($input['is_active'])) {
            $query->where('is_active', $input['is_active']);
        }

        return $query;
    }

    public $fileUpload = ['image' => 1];
    protected $pathUpload = ['image' => '/images/abouts'];
    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [
                [200, 200], [300, 300], [400, 400]
            ]
        ]
    ];
    protected $checkbox = ['is_active'];
}

