<?php

namespace Landing\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Contact extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'contacts';
    public $fillable = ['name', 'email', PHONE_NUMBER_COL, 'created_by', TITLE_COL, MESSAGE_COL, 'created_by', 'updated_by', LOCALE_COL];

    public function scopeFilter($query, $input)
    {
        if (isset($input['name'])) {
            $query->where('name', $input['name']);
        }
        if (isset($input['email'])) {
            $query->where('email', $input['email']);
        }
        if (isset($input[PHONE_NUMBER_COL])) {
            $query->where(PHONE_NUMBER_COL, $input[PHONE_NUMBER_COL]);
        }
        if (isset($input['created_by'])) {
            $query->where('created_by', $input['created_by']);
        }
        if (isset($input[TITLE_COL])) {
            $query->where(TITLE_COL, $input[TITLE_COL]);
        }
        if (isset($input[MESSAGE_COL])) {
            $query->where(MESSAGE_COL, $input[MESSAGE_COL]);
        }

        return $query;
    }


    public $fileUpload = ['image' => 1];
    protected $pathUpload = ['image' => '/images/contacts'];
    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [
                [200, 200], [300, 300], [400, 400]
            ]
        ]
    ];
    protected $checkbox = ['is_active'];
}

