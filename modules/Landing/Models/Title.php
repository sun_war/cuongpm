<?php

namespace Landing\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Title extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;
    use SoftDeletes;

    public $table = 'titles';
    public $fillable = ['title', 'content', 'is_active', 'no', 'locale', 'created_by', 'updated_by'];

    public $fileUpload = ['image' => 1];
    protected $pathUpload = ['image' => '/images/titles'];
    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [

            ]
        ]
    ];
}

