<div class="form-group text-info">{{$question->question}}</div>
<table class="table">
    @if($question->answer > 5)
        @foreach(REP_LIST as $i => $rep)
            @if(trim($question->$rep) !== '')
                <tr>
                    <td width="20px">
                        <input type="checkbox" value="{{$i}}" class="done" name="answer{{$question->id}}[]"></td>
                    <td>{{trim($question->$rep)}}</td>
                </tr>
            @endif
        @endforeach
    @else
        @foreach(REP_LIST as $i => $rep)
            @if(trim($question->$rep) !== '')
                <tr>
                    <td width="20px">
                        <input type="radio" value="{{$i}}" class="done" name="answer{{$question->id}}">
                    </td>
                    <td>{{trim($question->$rep)}}</td>
                </tr>
            @endif
        @endforeach
    @endif
</table>