<table class="table">
    <tr>

        <th width="65%">{{trans('label.question')}}</th>
{{--        <th>{{trans('label.answer')}}</th>--}}
        <th>{{trans('label.level')}}</th>
        <th>{{trans('label.knowledge')}}</th>
        <th>{{trans('label.professional')}}</th>
        <th></th>
    </tr>
    @foreach($multi_choices as $row)
        <tr>

            <td>
                <a href="{{route('multi_choices.edit', $row->id)}}">
                    {!! $row->question !!}
                  </a>
            </td>
{{--            <td>{{$row->answer}}</td>--}}
            <td>{{$row->level}}</td>
            <td>{{in_array($row->knowledge, KNOWLEDGE_KEY) ? KNOWLEDGE[$row->knowledge] : '' }}</td>
            <td>{{in_array($row->professional, PROFESSIONAL_KEY) ? PROFESSIONAL[$row->professional]: ''}}</td>
            <td class="text-right">
                <form style="display: inline" class="form-inline" method="POST"
                      action="{{route('multi_choices.destroy', $row->id)}}">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <a class="btn btn-default btn-xs" href="{{route('multi_choices.edit', $row->id)}}"><i class="fa fa-edit"></i></a>
                    <button class="btn btn-danger destroyBtn btn-xs"><i class="fa fa-trash"></i></button>
                    <a url="{{route('question.doing', $row->id)}}" data-toggle="modal" data-target="#doingQuestion" type="button" class="btn btn-xs btn-primary doingQuestionBtn">
                        <i class="fa fa-pencil"></i>
                    </a>
                </form>
            </td>
        </tr>
    @endforeach
</table>

<div id="linkPaginate">
    {{$multi_choices->links()}}
</div>
