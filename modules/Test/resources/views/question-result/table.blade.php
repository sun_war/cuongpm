<table class="table">
    <thead>
        <tr>
            <th>{{trans('label.question_id')}}</th>
<th>{{trans('label.created_by')}}</th>

            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($questionResults as $row)
        <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
            <td>{{$row->question_id}}</td>
<td>{{$row->created_by}}</td>

            <td class="text-right">
                <form method="POST" action="{{route('question-result.destroy', $row->id)}}">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button class="btn btn-danger btn-xs destroyBtn">
                        <i class="fa fa-trash"></i>
                    </button>
                    <a href="{{route('question-result.edit', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{route('question-result.show', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-eye"></i>
                    </a>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$questionResults->links()}}
</div>
