<table class="table">
    <thead>
        <tr>
            <th>{{trans('label.score')}}</th>
<th>{{trans('label.created_by')}}</th>
<th>{{trans('label.level')}}</th>
<th>{{trans('label.knowledge')}}</th>
<th>{{trans('label.professional')}}</th>
<th>{{trans('label.page')}}</th>
<th>{{trans('label.status')}}</th>

            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($testResults as $row)
        <tr class="{{$row->is_active === 1 ? : 'text-danger'}}">
            <td>{{$row->score}}</td>
<td>{{$row->created_by}}</td>
<td>{{$row->level}}</td>
<td>{{$row->knowledge}}</td>
<td>{{$row->professional}}</td>
<td>{{$row->page}}</td>
<td>{{$row->status}}</td>

            <td class="text-right">
                <form method="POST" action="{{route('test-result.destroy', $row->id)}}">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button class="btn btn-danger btn-xs destroyBtn">
                        <i class="fa fa-trash"></i>
                    </button>
                    <a href="{{route('test-result.edit', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="{{route('test-result.show', $row->id)}}" class="btn btn-info btn-xs">
                        <i class="fa fa-eye"></i>
                    </a>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<div id="linkPaginate">
    {{$testResults->links()}}
</div>
