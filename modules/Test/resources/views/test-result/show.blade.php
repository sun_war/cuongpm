@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('test-result.index')}}">{{trans('table.test_results')}}</a>
    </li>
    <li class="active">
        <strong>Table</strong>
    </li>
</ol>
<div>
    {!! $test_results ->score !!}
{!! $test_results ->created_by !!}
{!! $test_results ->level !!}
{!! $test_results ->knowledge !!}
{!! $test_results ->professional !!}
{!! $test_results ->page !!}
{!! $test_results ->status !!}

</div>
<a href="{{url()->previous()}}" class="btn btn-default"><i class="fa fa-backward"></i></a>
@endsection