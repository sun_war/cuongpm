<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TestResults extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('test_results', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('score')->default(0);
            $table->unsignedInteger('created_by');
            $table->tinyInteger('level')->default(1);
            $table->tinyInteger('knowledge')->default(0);
            $table->tinyInteger('professional')->default(0);
            $table->tinyInteger('page');
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('test_results');
    }
}
