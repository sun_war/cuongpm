<?php

namespace Test\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class TestResult extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'test_results';
    public $fillable = ['score', 'created_by', LEVEL_COL, KNOWLEDGE_COL, PROFESSIONAL_COL, PAGE_COL, STATUS_COL];

    public function scopeFilter($query, $input)
    {
        if (isset($input['score'])) {
            $query->where('score', $input['score']);
        }
        if (isset($input['created_by'])) {
            $query->where('created_by', $input['created_by']);
        }
        if (isset($input[LEVEL_COL])) {
            $query->where(LEVEL_COL, $input[LEVEL_COL]);
        }
        if (isset($input[KNOWLEDGE_COL])) {
            $query->where(KNOWLEDGE_COL, $input[KNOWLEDGE_COL]);
        }
        if (isset($input[PROFESSIONAL_COL])) {
            $query->where(PROFESSIONAL_COL, $input[PROFESSIONAL_COL]);
        }
        if (isset($input[PAGE_COL])) {
            $query->where(PAGE_COL, $input[PAGE_COL]);
        }
        if (isset($input[STATUS_COL])) {
            $query->where(STATUS_COL, $input[STATUS_COL]);
        }

        return $query;
    }

    public $fileUpload = ['image' => 1];
    protected $pathUpload = ['image' => '/images/test_results'];
    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [
                [200, 200], [300, 300], [400, 400]
            ]
        ]
    ];

    protected $checkbox = ['is_active'];
}

