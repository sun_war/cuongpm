<?php

namespace Test\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class QuestionResult extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'question_results';
    public $fillable = ['question_id', 'created_by'];

    public function scopeFilter($query, $input)
    {
        if (isset($input['question_id'])) {
            $query->where('question_id', $input['question_id']);
        }
        if (isset($input['created_by'])) {
            $query->where('created_by', $input['created_by']);
        }

        return $query;
    }

    public $fileUpload = ['image' => 1];
    protected $pathUpload = ['image' => '/images/question_results'];
    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [
                [200, 200], [300, 300], [400, 400]
            ]
        ]
    ];
    protected $checkbox = ['is_active'];
}

