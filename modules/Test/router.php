<?php
/**
 * Created by PhpStorm.
 * User: cuongpm
 * Date: 1/15/18
 * Time: 2:51 PM
 */

Route::group(['middleware' => 'web', 'namespace' => 'Test\Http\Controllers'], function () {
    Route::get('question/doing/{id}', 'MultiChoiceController@doing')->name('question.doing')->middleware('cacheable');
    Route::post('question/doing/{id}', 'MultiChoiceController@done')->name('question.done');
});

Route::group(['middleware' => ['web', 'auth:admin'], 'namespace' => 'Test\Http\Controllers'], function () {
    Route::resource('multi_choices', 'MultiChoiceController');
    Route::resource('test-result' , 'TestResultController');
    Route::resource('question-result' , 'QuestionResultController');
});

