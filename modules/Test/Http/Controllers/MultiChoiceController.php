<?php

namespace Test\Http\Controllers;

use App\Http\Controllers\Controller;
use Test\Http\Requests\MultiChoiceCreateRequest;
use Test\Http\Requests\MultiChoiceUpdateRequest;
use Test\Http\Repositories\MultiChoiceRepository;
use Illuminate\Http\Request;

class MultiChoiceController extends Controller
{
    private $repository;
    public function __construct(MultiChoiceRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['multi_choices'] = $this->repository->myPaginate($input);
        if($request->ajax())
        {
            return view('test::multi_choices.table', $data)
                ->render();
        }
        return view('test::multi_choices.index', $data);
    }

    public function create()
    {
        return view('test::multi_choices.create');
    }

    public function store(MultiChoiceCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('multi_choices.index');
    }

    public function show($id)
    {
        $multi_choices = $this->repository->find($id);
        if(empty($multi_choices))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('test::multi_choices.show', compact('multi_choices'));
    }

    public function edit($id)
    {
        $multi_choices = $this->repository->find($id);
        if(empty($multi_choices))
        {
            session()->flash('error', 'not found');
            return back();
        }
        return view('test::multi_choices.update', compact('multi_choices'));
    }

    public function update(MultiChoiceUpdateRequest $request, $id)
    {
        $input = $request->all();
        $multi_choices = $this->repository->find($id);
        if(empty($multi_choices))
        {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $multi_choices);
        session()->flash('success', 'update success');
        return redirect()->route('multi_choices.index');
    }

    public function destroy($id)
    {
        $multi_choices = $this->repository->find($id);
        if(empty($multi_choices))
        {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }

    public function doing($id) {
        $question = $this->repository->find($id);
        if(empty($question))
        {
            session()->flash('error', 'not found');
        }
        return view('test::multi_choices.do', compact('question'))->render();
    }

    public function done(Request $request, $id) {
        $multi_choices = $this->repository->find($id);
        if(empty($multi_choices))
        {
            session()->flash('error', 'not found');
        }
        return view('test::multi_choices.done', compact('multi_choices'));
    }
}
