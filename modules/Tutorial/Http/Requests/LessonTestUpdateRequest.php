<?php

namespace Tutorial\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LessonTestUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'lesson_id' => 'required',
            QUESTION_COL => 'required',
            REPLY1_COL => 'required',
            REPLY2_COL => 'required',
            REPLY3_COL => 'required',
            REPLY4_COL => 'required',
            ANSWER_COL => 'required',
        ];
    }

    public function messages()
    {
        return [

        ];
    }
}
