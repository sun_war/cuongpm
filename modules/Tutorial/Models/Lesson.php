<?php

namespace Tutorial\Models;

use ACL\Models\Admin;
use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Lesson
 * @package Tutorial\Models
 */
class Lesson extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    /**
     * @var string
     */
    public $table = 'lessons';
    /**
     * @var array
     */
    public $fillable = [TITLE_COL, INTRO_COL, CONTENT_COL, NO_COL, 'section_id', VIEWS_COL, LAST_VIEW_COL, 'created_by', 'updated_by', 'is_active'];

    /**
     * @param $query
     * @param $input
     * @return mixed
     */
    public function scopeFilter($query, $input)
    {
        if (isset($input[INTRO_COL])) {
            $query->where(INTRO_COL, $input[INTRO_COL]);
        }
        if (isset($input[CONTENT_COL])) {
            $query->where(CONTENT_COL, $input[CONTENT_COL]);
        }
        if (isset($input['section_id'])) {
            $query->where('section_id', $input['section_id']);
        }
        if (isset($input[VIEWS_COL])) {
            $query->where(VIEWS_COL, $input[VIEWS_COL]);
        }
        if (isset($input[LAST_VIEW_COL])) {
            $query->where(LAST_VIEW_COL, $input[LAST_VIEW_COL]);
        }
        if (isset($input['created_by'])) {
            $query->where('created_by', $input['created_by']);
        }
        if (isset($input['updated_by'])) {
            $query->where('updated_by', $input['updated_by']);
        }
        if (isset($input['is_active'])) {
            $query->where('is_active', $input['is_active']);
        }
        return $query;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function section()
    {
        return $this->belongsTo(Section::class, 'section_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tests()
    {
        return $this->hasMany(LessonTest::class, 'lesson_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function results()
    {
        return $this->hasMany(LessonResult::class, 'lesson_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo(Admin::class, 'created_by');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function updater()
    {
        return $this->belongsTo(Admin::class, 'updated_by');
    }

    /**
     * @var array
     */
    public $fileUpload = ['image' => 1];

    /**
     * @var array
     */
    protected $pathUpload = ['image' => '/images/lessons'];

    /**
     * @var array
     */
    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [
                [200, 200], [300, 300], [400, 400]
            ]
        ]
    ];

    /**
     * @var array
     */
    protected $checkbox = ['is_active'];
}

