<?php

namespace Tutorial\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class LessonTest extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'lesson_tests';
    public $fillable = ['lesson_id', QUESTION_COL, REPLY1_COL, REPLY2_COL, REPLY3_COL, REPLY4_COL, ANSWER_COL,
        'is_active', 'created_by', 'updated_by'];

    public function scopeFilter($query, $input)
    {
        if (isset($input['lesson_id'])) {
            $query->where('lesson_id', $input['lesson_id']);
        }
        if (isset($input[QUESTION_COL])) {
            $query->where(QUESTION_COL, $input[QUESTION_COL]);
        }
        if (isset($input[REPLY1_COL])) {
            $query->where(REPLY1_COL, $input[REPLY1_COL]);
        }
        if (isset($input[REPLY2_COL])) {
            $query->where(REPLY2_COL, $input[REPLY2_COL]);
        }
        if (isset($input[REPLY3_COL])) {
            $query->where(REPLY3_COL, $input[REPLY3_COL]);
        }
        if (isset($input[REPLY4_COL])) {
            $query->where(REPLY4_COL, $input[REPLY4_COL]);
        }
        if (isset($input[ANSWER_COL])) {
            $query->where(ANSWER_COL, $input[ANSWER_COL]);
        }
        if (isset($input['is_active'])) {
            $query->where('is_active', $input['is_active']);
        }
        if (isset($input['created_by'])) {
            $query->where('created_by', $input['created_by']);
        }
        if (isset($input['updated_by'])) {
            $query->where('updated_by', $input['updated_by']);
        }
        return $query;
    }

    public function lesson()
    {
        return $this->belongsTo(Lesson::class, 'lesson_id');
    }

    public $fileUpload = ['image' => 1];
    protected $pathUpload = ['image' => '/images/lesson_tests'];
    protected $thumbImage = [
        'image' => [
            '/thumbs/' => [
                [200, 200], [300, 300], [400, 400]
            ]
        ]
    ];
    protected $checkbox = ['is_active'];
}

