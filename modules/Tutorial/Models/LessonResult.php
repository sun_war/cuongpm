<?php

namespace Tutorial\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class LessonResult extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'lesson_results';
    public $fillable = ['created_by', 'lesson_id', 'score'];

    public function scopeFilter($query, $input)
    {
        if (isset($input['created_by'])) {
            $query->where('created_by', $input['created_by']);
        }
        if (isset($input['lesson_id'])) {
            $query->where('lesson_id', $input['lesson_id']);
        }
        if (isset($input['score'])) {
            $query->where('score', $input['score']);
        }
        return $query;
    }

    public function lesson()
    {
        return $this->belongsTo(Lesson::class, 'lesson_id');
    }
}

