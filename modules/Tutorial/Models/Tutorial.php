<?php

namespace Tutorial\Models;

use Modularization\MultiInheritance\ModelsTrait;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Tutorial extends Model implements Transformable
{
    use TransformableTrait;
    use ModelsTrait;

    public $table = 'tutorials';
    public $fillable = ['name', 'img', 'is_active', DESCRIPTION_COL];

    public function scopeFilter($query, $input)
    {
        if (isset($input['name'])) {
            $query->where('name', $input['name']);
        }
        if (isset($input['is_active'])) {
            $query->where('is_active', $input['is_active']);
        }
        return $query;
    }

    public function sections()
    {
        return $this->hasMany(Section::class, 'tutorial_id');
    }

    public $fileUpload = ['img' => 1];
    protected $pathUpload = ['img' => '/images/tutorials'];
    protected $thumbImage = [
        'img' => [
            '/thumbs/' => [
                [200, 150], [400, 300]
            ]
        ]
    ];
    protected $checkbox = ['is_active'];
}

