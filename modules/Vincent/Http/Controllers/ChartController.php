<?php

namespace Vincent\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ChartController extends Controller
{
    public function coin()
    {
        $users = app(\App\User::class)
            ->orderBy(COIN_COL, 'DESC')
            ->limit(10)
            ->get();

        return view('edu::charts', compact('users'));
    }
}
