<?php
/**
 * Created by PhpStorm.
 * User: cuongpm
 * Date: 3/22/18
 * Time: 10:55 AM
 */

namespace Vincent\Core\Services;

use Test\Models\TestResult;
use Test\Http\Repositories\MultiChoiceRepository;
use Tutorial\Http\Repositories\TutorialResultRepository;
use Tutorial\Http\Repositories\TutorialTestRepository;

class TestService
{

    private $repository, $tutorialTestRepository, $tutorialResultRepository;

    public function __construct(MultiChoiceRepository $repository, TutorialTestRepository $tutorialTestRepository,
                                TutorialResultRepository $tutorialResultRepository
    )
    {
        $this->repository = $repository;
        $this->tutorialTestRepository = $tutorialTestRepository;
        $this->tutorialResultRepository = $tutorialResultRepository;
    }

    public function countList($input)
    {
        $count = $this->repository->filterCount($input, 'id');
        return ceil((int)$count / $this->getPerPage());
    }

    public function marking($request)
    {
        $replies = $request->except(['_token']);
        $options = $this->getOptions($request);
        $level = $replies[LEVEL_COL];
        $knowledge = $replies[KNOWLEDGE_COL];
        $professional = $replies[PROFESSIONAL_COL];
        $subject_id = $replies['subject_id'];
        $questions = $this->repository->myPaginate($options);
        $result = $this->score($questions, $replies);
        $data = compact('questions', 'replies', 'options', 'level', 'knowledge', 'professional', 'subject_id');
        return array_merge($result, $data);
    }

    public function score($questions, $input)
    {
        $score = 0;
        $exactly = [];
        $questionCount = count($questions);
        foreach ($questions as $question) {
            $answerKey = 'answer' . $question->id;
            $exactly[$question->id] = false;
            if (isset($input[$answerKey])) {
                if ($this->check($question->answer, $input[$answerKey]) === true) {
                    ++$score;
                    $exactly[$question->id] = true;
                }
            }
        }
        $this->effect($input, $score, $questionCount, $questions);

        return compact('score', 'exactly');
    }

    public function scoreLesson($questions, $answers)
    {
        $score = 0;
        $exactly = [];
        foreach ($questions as $question) {
            $exactly[$question->id] = false;
            if (isset($answers[$question->id])) {
                if ($this->check($question->answer, $answers[$question->id]) === true) {
                    ++$score;
                    $exactly[$question->id] = true;
                }
            }
        }
        return compact('score', 'exactly');
    }

    public function effect($input, $score, $questionCount, $questions)
    {
        if (auth()->check()) {
            $filter = [
                'created_by' => auth()->id(),
                LEVEL_COL => isset($input[LEVEL_COL]) ? $input[LEVEL_COL] : null,
                KNOWLEDGE_COL => isset($input[KNOWLEDGE_COL]) ? $input[KNOWLEDGE_COL] : null,
                PROFESSIONAL_COL => isset($input[PROFESSIONAL_COL]) ? $input[PROFESSIONAL_COL] : null,
                PAGE_COL => isset($input[PAGE_COL]) ? $input[PAGE_COL] : 0
            ];

            $result = app(TestResult::class)->filter($filter)->first();

            if (empty($result)) {
                $filter['score'] = $score * 1000 / $questionCount;
                $result = app(TestResult::class)->create($filter);
            }

            if ($score == count($questions) && $result->score != 1000) {
                $user = auth()->user();
                $user->coin += 1000;
                $user->save();
            }
        }
    }

    private function check($answer, $reply)
    {
        if ((int)trim($answer) > 5) {
            $reply = implode('', $reply);
            if ($answer == $reply) {
                return true;
            }
        } else {
            if ($answer == $reply) {
                return true;
            }
        }
        return false;
    }

    public function getTest($input)
    {
        $input[PER_PAGE] = $this->getPerPage();
        return $this->repository->myPaginate($input);
    }

    private function getOptions($request)
    {
        $options = $request->only([LEVEL_COL, KNOWLEDGE_COL, PROFESSIONAL_COL, PAGE_COL, 'subject_id']);
        $options[PER_PAGE] = $this->getPerPage();
        return $options;
    }

    private function getPerPage()
    {
        return 10;
    }

    public function result($input)
    {
        $input['created_by'] = auth()->id();
        $tutorialResults = $this->tutorialResultRepository->myPaginate($input);
        return compact('tutorialResults');
    }
}
