@extends('edu::layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-12 form-group">
            <div class="embed-responsive embed-responsive-16by9">
                {!! $about->map !!}
            </div>
        </div>
    </div>
    <h1>Liên lạc với chúng tôi, hãy viết cho chúng tôi một tin nhắn!</h1>
    <div class="row">
        <div class="col-sm-7 sep">
            <form class="contact-form" role="form" method="post" action="{{route('contacted')}}" >
                {{ csrf_field() }}
                <div class="form-group">
                    <input name="name" class="form-control" placeholder="{{__('label.name')}}:"/>
                </div>
                <div class="form-group">
                    <input name="email" class="form-control" placeholder="{{__('label.email')}}:"/>
                </div>
                <div class="form-group">
                    <input type="number" name="phone_number" class="form-control" placeholder="{{__('label.phone_number')}}:"/>
                </div>
                <div class="form-group">
                    <input name="title" class="form-control" placeholder="{{__('label.title')}}:"/>
                </div>
                <div class="form-group">
                    <textarea class="form-control" name="message" placeholder="Tin nhắn:" rows="6"></textarea>
                </div>
                <div class="form-group text-right">
                    <button class="btn btn-primary" name="send">Gửi</button>
                </div>
            </form>
        </div>
        <div class="col-sm-offset-1 col-sm-4">
            <div class="info-entry">
                <h4>Address</h4>
                {{$about->address}}
            </div>
            <div class="info-entry">
                <h4>Liên hệ với chúng tôi</h4>
                <p>
                    Phone: {{$about->phone_number}}<br/>
                    Email: {{$about->email}}
                </p>
            </div>
        </div>
    </div>
@endsection
@push('head')
    <style>
        .contact-map {
            height: 0 !important;
            min-height: 0 !important;
        }
    </style>
@endpush
