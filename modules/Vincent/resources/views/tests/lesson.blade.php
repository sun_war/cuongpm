@extends('edu::layouts.app')
@section('title', 'Bạn đang bài làm')
@section('content')
    <ol class="breadcrumb">
        <li>
            <a href="/"><i class="entypo-home"></i></a>
        </li>
        <li>
            <a href="{{route('edu.tutorial.show', $lesson->section->tutorial->id)}}">{{$lesson->section->tutorial->name}}</a>
        </li>
        <li>
            {{$lesson->title}}
        </li>
    </ol>
    <form id="formTest" action="{{route('edu.test.lessonResult', $lesson->id)}}" method="POST">
        {{csrf_field()}}
        @foreach($questions as $k => $question)
            <div class="form-group" id="{{$k+1}}">
                <strong>{{__('label.question')}} {{++$k}}</strong>
                <input type="checkbox" data="{{$k}}" class="unsure pull-right"
                       data-toggle="tooltip" data-placement="bottom"
                       title="Bạn chưa chắc chắn ">
            </div>
            <div class="form-group text-info">@markdown($question->question)</div>
            <table class="table">
                @foreach(REP_LESSONS as $i => $rep)
                    <tr>
                        <td width="20px">
                            <input type="radio" value="{{$i}}" class="done" data="{{$k}}"
                                   name="answers[{{$question->id}}]">
                        </td>
                        <td>{{trim($question->$rep)}}</td>
                    </tr>
                @endforeach
            </table>
            <hr>
        @endforeach
    </form>
    @include('edu::tests.components.status')
    @include('edu::tests.components.nav')
@endsection

@push('head')
    <style>
        .unsure {
            height: 20px;
            width: 20px;
            margin-bottom: 0 !important;
        }

        #formTest {
            color: black;
        }
    </style>
@endpush
@push('js')
    <script src="{{ asset('build/test.js')}}"></script>
    <script>
        $('td').click(function () {
            // console.log($(this).children('input')).attr('checked', 'checked');
            $(this).find('input').attr('checked', 'checked');
        });
    </script>
@endpush