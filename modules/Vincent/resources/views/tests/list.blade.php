@extends('edu::layouts.app')
@section('title', 'Danh sách kiểm tra năng lực')
@section('content')
    <h1>Kiểm tra kiến thức công nghệ thông tin</h1>
    @include('edu::tests.components.filter', ['route' => route('edu.test.list')])
    <div class="row">
        <div class="col-sm-12">
            <form action="{{route('edu.test.doing')}}" method="GET" id="doTesting">
                <input type="hidden" name="level">
                <input type="hidden" name="knowledge">
                <input type="hidden" name="professional">
                <input type="hidden" name="subject_id">
                <input type="hidden" name="page">
            </form>
        </div>
        <div  id="table">
            @include('edu::tests.includes.list-unit')
        </div>
    </div>
@endsection
@push('css')

@endpush
@push('js')
    <script src="{{$asset_url}}/build/form-filter.js"></script>
    <script>
        const doTesting = $('#doTesting');
        selectFilter = $('.selectFilter');

        selectFilter.change(function () {
            const name = $(this).attr('name');
            const value = $(this).val();
            $('input[name="' + name + '"]').val(value);
        });

        $(document).on('click', '.list-test', function (e) {
            e.preventDefault();
            var data = $(this).attr('data');
            $('input[name="page"]').val(data);
            //doTesting.attr('action', href);
            doTesting.submit();
        });
    </script>
@endpush
