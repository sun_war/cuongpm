@extends('edu::layouts.app')
@section('title', 'Bạn đang bài làm')
@section('content')
    @include('edu::tests.components.filter', ['route' => route('test.one')])
    @include('edu::tests.components.one-table')

{{--    @include('edu::tests.components.status')--}}
{{--    @include('edu::tests.components.nav')--}}
@endsection

@push('head')
    <style>
        .unsure {
            height: 20px;
            width: 20px;
            margin-bottom: 0 !important;
        }
        #formTest {
            color: black;
        }
    </style>
@endpush
@push('js')
    <script src="{{$asset_url}}/build/form-filter.js"></script>
    <script src="{{ asset('build/test.js')}}"></script>
    <script>
        $('td').click(function () {
            $(this).children('input').prop('checked');
        });

        const mark = '#mark';
        $(mark).click(function () {
            $.ajax({

            });
        });
    </script>
@endpush