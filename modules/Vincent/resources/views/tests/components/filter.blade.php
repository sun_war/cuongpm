<form class="row" id="formFilter" action="{{$route}}" method="POST">
    <div class="col-lg-12">
        <h5>
            <i>
                <strong>
                    Thông qua bài kiểm tra có thể đánh giá được phần nào năng lực hiện có của bạn trong kho kiến thức
                    của chúng tôi.
                    Hãy lựa chọn nền tảng kiến thức phù hợp với bạn nhất
                </strong>
            </i>
        </h5>
    </div>
    <div class="form-group col-sm-4">
        <label for="">{{__('common.level')}}</label>
        <select class="form-control selectFilter" name="level" id="">
            <option value="">All</option>
            @foreach(LEVEL as $k => $v)
                <option value="{{$v}}">{{$v}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-sm-4">
        <label for="">{{__('common.knowledge')}}</label>
        <select class="form-control selectFilter" name="knowledge" id="">
            <option value="">All</option>
            @foreach(KNOWLEDGE as $k =>$v)
                <option value="{{$k}}">{{$v}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-sm-4">
        <label for="">{{__('common.professional')}}</label>
        <select class="form-control selectFilter" name="professional" id="">
            <option value="">All</option>
            @foreach(PROFESSIONAL as $k => $v)
                <option value="{{$k}}">{{$v}}</option>
            @endforeach
        </select>
    </div>
</form>