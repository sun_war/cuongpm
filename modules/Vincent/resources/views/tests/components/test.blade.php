@foreach($questions as $k=> $question)
    <div class="form-group" id="{{$k+1}}">
        <strong>{{__('label.question')}} {{++$k}}</strong>
        <input type="checkbox" data="{{$k}}" class="unsure pull-right" data-toggle="tooltip" data-placement="bottom"
               title="Bạn chưa chắc chắn ">
    </div>
    <div class="form-group text-info">@markdown($question->question)</div>
    <table class="table">
        @if($question->answer > 5)
            @foreach(REP_LIST as $i => $rep)
                @if(trim($question->$rep) !== '')
                    <tr>
                        <td width="20px">
                            <input type="checkbox" value="{{$i}}" class="done" data="{{$k}}"
                                   name="answer{{$question->id}}[]"></td>
                        <td>{{trim($question->$rep)}}</td>
                    </tr>
                @endif
            @endforeach
        @else
            @foreach(REP_LIST as $i => $rep)
                @if(trim($question->$rep) !== '')
                    <tr>
                        <td width="20px">
                            <input type="radio" value="{{$i}}" class="done" data="{{$k}}"
                                   name="answer{{$question->id}}">
                        </td>
                        <td>{{trim($question->$rep)}}</td>
                    </tr>
                @endif
            @endforeach
        @endif
    </table>
    <hr>
@endforeach