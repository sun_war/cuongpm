@foreach($questions as $k=> $question)
    <div class="form-group" id="{{$k+1}}">
        <strong>{{__('label.question')}} {{++$k}}</strong>
        <input type="checkbox" data="{{$k}}" class="unsure pull-right" data-toggle="tooltip"
               data-placement="bottom" title="Bạn chưa chắc chắn ">
    </div>
    <div class="form-group">@markdown($question->question)</div>
    <table class="table">
        @if(isset($replies[$answer = 'answer' . $question->id]))
            @if((int)trim($question->answer) > 5)
                @foreach(REP_LIST as $i => $rep)
                    @if(trim($question->$rep) !== '')
                        <tr class="@if($exactly[$question->id] && strpos((string)$question->answer, (string)$i) !== false) bg-success @endif">
                            <td width="20px">
                                <input type="checkbox" value="{{$i}}" class="done" data="{{$k}}"
                                       @if(strpos(implode('', $replies[$answer]), (string) $i) !== false)
                                       checked
                                       @endif
                                       name="answer{{$question->id}}[]">
                            </td>
                            <td>{{$question->$rep}}</td>
                        </tr>
                    @endif
                @endforeach
            @else
                @foreach(REP_LIST as $i => $rep)
                    @if(trim($question->$rep) !== '')
                        <tr class="@if($exactly[$question->id] && $replies[$answer] == $i) bg-success @endif">
                            <td width="20px">
                                <input type="radio" value="{{$i}}"
                                       @if($exactly[$question->id] && $replies[$answer] == $i) checked
                                       @endif  name="answer{{$question->id}}">
                            </td>
                            <td>{{$question->$rep}}</td>
                        </tr>
                    @endif
                @endforeach
            @endif
        @else
            @if($question->answer > 5)
                @foreach(REP_LIST as $i => $rep)
                    @if(trim($question->$rep) !== '')
                        <tr>
                            <td width="20px">
                                <input type="checkbox" value="1" name="answer{{$question->id}}[]">
                            </td>
                            <td>{{$question->$rep}}</td>
                        </tr>
                    @endif
                @endforeach
            @else
                @foreach(REP_LIST as $i => $rep)
                    @if(trim($question->$rep) !== '')
                        <tr>
                            <td width="20px">
                                <input type="radio" value="{{$i}}" name="answer{{$question->id}}">
                            </td>
                            <td>{{$question->$rep}}</td>
                        </tr>
                    @endif
                @endforeach
            @endif
        @endif
    </table>
    <hr>
@endforeach