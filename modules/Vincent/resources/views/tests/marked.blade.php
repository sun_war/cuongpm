@extends('edu::layouts.app')
@section('title', 'Kết quả làm bài')
@section('content')
    <form id="formTest" action="{{route('edu.test.marking')}}?page={{$questions->currentPage()}}"
          method="POST">
        {{csrf_field()}}
        <input type="hidden" name="level" value="{{isset($level) ? $level : ''}}">
        <input type="hidden" name="knowledge" value="{{isset($knowledge) ? $knowledge : ''}}">
        <input type="hidden" name="professional" value="{{isset($professional) ? $professional : ''}}">
        <input type="hidden" name="subject_id" value="{{isset($subject_id) ? $subject_id : ''}}">
        @foreach($questions as $k=> $question)
            @include('edu::tests.components.done')
        @endforeach

    </form>
    @include('edu::tests.components.status')
    @include('edu::tests.components.nav')
@endsection

@push('head')
<style>
    .unsure {
        height: 20px;
        width: 20px;
        margin-bottom: 0 !important;
    }
    #formTest {
        color: black;
    }
</style>
@endpush
@push('js')
    <script src="{{asset('build/test.js')}}"></script>
    <script>
        $('td').click(function () {
            $(this).children('input').prop('checked');
        });
    </script>
@endpush