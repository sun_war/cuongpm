@extends('edu::layouts.app')

@section('content')
    <ol class="breadcrumb">
        <li>
            <a href="/dashboard"><i class="fa fa-home"></i>Dashboard</a>
        </li>
        <li class="active">
            <strong>Profile</strong>
        </li>
    </ol>
    <div class="row">
        <div class="col-xs-3 col-md-2 form-group">
            @if(trim(auth()->user()->avatar) !== '')
                <img class="img-responsive img-circle img-thumbnail" src="{{ asset('storage/' . auth()->user()->avatar )}}">
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <table class="table table-bordered">
                <tr>
                    <td>
                        Information
                    </td>
                    <td>
                        <strong>{{auth()->user()->user}}</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{trans('label.first_name')}}</strong>
                    </td>
                    <td>
                        {{auth()->user()->first_name}}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{trans('label.last_name')}}</strong>
                    </td>
                    <td>
                        {{auth()->user()->last_name}}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{trans('label.email')}}</strong>
                    </td>
                    <td>
                        {{auth()->user()->email}}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{trans('label.phone_number')}}</strong>
                    </td>
                    <td>
                        {{auth()->user()->phone_number}}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{trans('label.coin')}}</strong>
                    </td>
                    <td>
                        {{auth()->user()->coin}}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{trans('label.address')}}</strong>
                    </td>
                    <td>
                        {{auth()->user()->address}}
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>{{trans('label.sex')}}</strong>
                    </td>
                    <td>
                        {{auth()->user()->sex == 1 ? 'Male' : 'Female'}}
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">

        </div>
    </div>
@endsection
@push('css')
    <link rel="stylesheet"
          href="<?php echo e($asset_url); ?>bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css"/>
@endpush
@push('js')
    <script type="text/javascript" src="<?php echo e($asset_url); ?>bower_components/moment/min/moment.min.js"></script>
    <script type="text/javascript"
            src="<?php echo e($asset_url); ?>bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript">
        $("#birthday").datetimepicker({format: 'YYYY-MM-DD'});
    </script>
    <script src="{{asset('build/forceForm.js')}}"></script>
    <script>
        $('#coin').magicFormatNumber('#number')
    </script>
@endpush