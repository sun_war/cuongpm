<div class="gallery-env row">

    <div class="feature-block col-lg-12">
        <h3>
            <i class="entypo-cog"></i>
            Tutorial Hot
        </h3>
        <p>
            Những bài hướng dẫn về các ngôn ngữ lập trình hot nhất hiện nay. Bạn có thể ôn luyện theo các chuẩn
            W3School
        </p>
    </div>
    @foreach($tutorials as $tutorial)
        <div class="col-sm-4">
            <article class="album">
                <header>
                    <a href="{{route('edu.tutorial.show', $tutorial->id)}}">
                        <img class="img-responsive" src="{{\ViewFa::thumb($tutorial->img, [400, 300])}}"/>
                    </a>
                </header>
                <section class="album-info text-center" style="min-height: 80px">
                    <h4><a href="{{route('edu.tutorial.show', $tutorial->id)}}">{{$tutorial->name}}</a></h4>
                    <p>{{$tutorial->intro}} </p>
                </section>
            </article>
        </div>
    @endforeach
</div>