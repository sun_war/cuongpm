@extends('edu::layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-12 form-group">
            <h4 style="color: green"><strong><i class="fa fa-list-alt"></i> Bảng xếp hạng khối tài sản các Stark Tank</strong></h4>
            <ul class="list-group">
                @foreach($users as $k => $user)
                    <li data="{{$user->id}}"  data-toggle="modal" data-target="#myModal" class="viewShark list-group-item {{$k === 0 ? 'active': ''}}"> {{$k + 1}}.
                        <span class="pull-right" style="color: goldenrod"><strong>{{number_format($user->coin)}}
                                <i class="fa fa-bitcoin"></i>
                            </strong></span>
                        {{$user->email}}
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
    <div class="row form-group">
        @isset($users[0])
            <div class="col-lg-12">
                <h4><strong>Thành tích người shark xuất sắc nhất</strong></h4>
            </div>
            <div class="col-lg-12">
                <h4 class="charts hidden" style="color: darkblue"><strong><i class="fa fa-line-chart"></i>  Biến động tổng tài sản Shark {{$users[0]->email}}</strong></h4>
                <div id="coin"></div>
            </div>
            <div  class="col-lg-12">
                <h4 class=" charts hidden" style="color: darkred"><i class="fa fa-line-chart"></i>  <strong>Biến động giao dịch Shark {{$users[0]->email}}</strong></h4>
                <div id="deal"></div>
            </div>
        @endisset
    </div>
    <br>
    <br>

    <!-- Modal -->
    <div class="modal fade  custom-width" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document" style="width: 90%">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Quá trình phát triển</h4>
                </div>
                <div class="modal-body">
                    <div id="content-chart">
                        <div id="target" class="text-info col-lg-12 hidden">
                            <p>Theo dõi các shark để lựa chọn người phù hợp nhất với bạn</p>
                        </div>
                        <div class="col-lg-12">
                            <h4 class="charts_shark hidden" style="color: darkblue"><strong>Biến động tổng tài sản </strong></h4>
                            <div id="coin_shark"></div>
                        </div>
                        <div  class="col-lg-12">
                            <h4 class=" charts_shark hidden" style="color: darkred"><strong>Biến động giao dịch </strong></h4>
                            <div id="deal_shark"></div>
                        </div>
                    </div>
                    <div class="text-center">
                        <button type="button" class="btn btn-icon btn-success">Ngưỡng mộ <i class="fa fa-plus"></i></button>
                        <button type="button" class="btn btn-icon btn-danger">Xin tài trợ <i class="fa fa-money"></i></button>
                        <button type="button" id="cmd" class="btn btn-icon btn-default">FDF <i class="fa fa-outdent"></i></button>
                    </div>
                </div>
            </div>
        </div>PH
    </div>
@endsection
@push('head')
    <link rel="stylesheet" href="{{$asset_url}}/assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
    <link rel="stylesheet" href="{{$asset_url}}/assets/css/neon-forms.css">
@endpush
@push('js')
    <script src="{{$asset_url}}/assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="{{$asset_url}}/assets/js/jvectormap/jquery-jvectormap-europe-merc-en.js"></script>
    <script src="{{$asset_url}}/assets/js/jquery.sparkline.min.js"></script>
    <script src="{{$asset_url}}/assets/js/rickshaw/vendor/d3.v3.js"></script>
    <script src="{{$asset_url}}/assets/js/raphael-min.js"></script>
    <script src="{{$asset_url}}/assets/js/neon-chat.js"></script>

    <!-- Bottom scripts (common) -->
    <script src="{{$asset_url}}/assets/js/gsap/TweenMax.min.js"></script>
    <script src="{{$asset_url}}/assets/js/joinable.js"></script>
    <script src="{{$asset_url}}/assets/js/resizeable.js"></script>
    <script src="{{$asset_url}}/assets/js/neon-api.js"></script>
    <!-- Imported scripts on this page -->
    <!-- JavaScripts initializations and stuff -->
    <script src="{{$asset_url}}/assets/js/neon-custom.js"></script>--}

        <script src="{{asset('bower_components/saveSvgAsPng/saveSvgAsPng.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.5/jspdf.debug.js" integrity="sha384-CchuzHs077vGtfhGYl9Qtc7Vx64rXBXdIAZIPbItbNyWIRTdG0oYAqki3Ry13Yzu" crossorigin="anonymous"></script>
        <script src="{{asset('build/form-filter.js')}}"></script>
        @isset($users[0])
        <script>
            $.ajax({
                url: "{{route('coin-log.charts')}}",
                method: 'GET',
                data: { created_by: '{{$users[0]->id}}'},
                success: function (data) {
                    Morris.Line({
                        element: 'coin',
                        data: data,
                        xkey: 'created_at',
                        ykeys: ['coin'],
                        labels: ['$'],
                        parseTime: false,
                        lineColors: ['blue']
                    });
                    Morris.Line({
                        element: 'deal',
                        data: data,
                        xkey: 'created_at',
                        ykeys: ['deal'],
                        labels: ['$'],
                        parseTime: false,
                        lineColors: ['red']
                    });
                    $('.charts').removeClass('hidden')
                },
                error: function () {
                    alert('error')
                }
            })
        </script>
        @endisset
        <script>
            $('.viewShark').click(function () {
                const id = $(this).attr('data');
                $.ajax({
                    url: "{{route('coin-log.charts')}}",
                    method: 'GET',
                    data: { created_by: id},
                    success: function (data) {
                        $('#coin_shark').html('');
                        $('#deal_shark').html('');
                        Morris.Line({
                            element: 'coin_shark',
                            data: data,
                            xkey: 'created_at',
                            ykeys: ['coin'],
                            labels: ['$'],
                            parseTime: false,
                            lineColors: ['blue']
                        });
                        Morris.Line({
                            element: 'deal_shark',
                            data: data,
                            xkey: 'created_at',
                            ykeys: ['deal'],
                            labels: ['$'],
                            parseTime: false,
                            lineColors: ['red']
                        });
                        $('.charts_shark').removeClass('hidden')
                    },
                    error: function () {
                        alert('error')
                    }
                })
            });
        </script>

        <script>
            $(function () {
                $('#cmd').click( function () {
                    let doc = new jsPDF();
                    const chart = new Promise((resolve, reject) => {
                        let promises = [];
                        var x = 10;
                        var y = 20;
                        $('#content-chart svg').each( function () {
                            const self = this;
                            const promise = new Promise((resolve, reject) => {
                                svgAsPngUri(self, {}, function (uri) {
                                    console.log('x' + x);
                                    console.log('y' + y);
                                    doc.setFontSize(20);
                                    doc.text(15, 15, "Chart");
                                    doc.addImage(uri, 'JPEG', x, y, 180, 90 );

                                    // x += 50;
                                    y += 90;
                                    resolve(uri);
                                    console.log(2);
                                });
                            });
                            promises.push(promise);
                        });
                        Promise.all(promises).then((data) => {
                           console.log(data);
                           resolve(data);
                        })
                    });
                    Promise.all([chart]).then((data) => {
                        console.log(3);
                        doc.save('charts.pdf');
                    }).catch((error) => {
                        alert('error');
                    })
                });
            });
        </script>
    @endpush