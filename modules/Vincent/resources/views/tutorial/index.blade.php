@extends('edu::layouts.app')
@section('title', 'Tutorials')
@section('content')
    <h1>Tutorials</h1>
    <div class="row">
        <div class="col-lg-12 form-group">
            <h2>Học tập với các dự án thực tế. Dẫn dắt bạn từ bước tìm hiểu, demo với khách hàng tới production.</h2>
        </div>
        @foreach($tutorials as $tutorial)
            <div class="col-sm-4">
                <article class="album">
                    <header>
                        <a href="{{route('edu.tutorial.show', $tutorial->id)}}">
                            <img class="img-responsive" src="{{$tutorial->getThumbPath('img', [400, 300])}}" />
                        </a>
                    </header>
                    <section class="album-info text-center" style="min-height: 80px">
                        <h4><a href="{{route('edu.tutorial.show', $tutorial->id)}}">{{$tutorial->name}}</a></h4>
                    </section>
                </article>
            </div>
        @endforeach
    </div>
@endsection

@push('head')
    {{--<link rel="stylesheet" href="{{$asset_url}}/assets/css/neon-core.css">--}}
@endpush
