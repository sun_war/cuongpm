@extends('edu::layouts.app')
@section('title', $tutorial->name )
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="entypo-home"></i></a>
                </li>
                <li>
                    <a href="{{route('edu.tutorial.index')}}">Tutorial</a>
                </li>
                <li class="active">
                    <strong>{{$tutorial->name}}</strong>
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 form-group">
            <h5><i><strong>Thường xuyên ôn luyện sẽ nâng cao thành tích của bạn từng nấc thang gần trở thành lập trình viên <i style="color: green">chuyên nghiệp</i>,
                tiến tới khả năng <i style="color: blueviolet">gánh team</i>, 1 <i style="color: darkred">huyền thoại</i>,
                        hoặc đỉnh cao là <i style="color: goldenrod; font-weight: bold">GOD</i></strong></i></h5>
        </div>
        <div class="col-lg-12">
            <table class="table table-hover">
                <thead>
                    <tr class=" bold italic">
                        <th><strong>SECTION</strong></th>
                        <th><strong>LESSON</strong></th>
                        <th><strong>SCORE</strong></th>
                        <th class="text-right"><strong>TEST</strong></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($sections as $row)
                    <tr>
                        <td>
                            <a href="{{route('edu.tutorial.section', $row->id)}}"> <strong>{{$row->name}} ({{count($row->lessons)}} lessons)</strong></a>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    @foreach($row->lessons as $lesson)
                        <tr>
                            <td></td>
                            <td >
                                <a data-toggle="tooltip" data-placement="top" title="Click để bắt đầu bài học" href="{{route('edu.tutorial.lesson', $lesson->id)}}">{{$lesson->title}} <i class="entypo-link"></i></a>
                            </td>
                            <th>{!! isset($lesson->results[0]) ? (int) $lesson->results[0]->score / 100 : '<i class="entypo-cc-zero"><i>' !!}</th>
                            <td class="text-right">
                                <a data-toggle="tooltip" data-placement="top" title="Ôn luyện kiến thức của bài học" class="btn btn-xs btn-white" href="{{route('edu.test.lesson', $lesson->id)}}"><i class="glyphicon-pencil glyphicon"></i></a>
                            </td>
                        </tr>
                    @endforeach
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@push('head')
    <link rel="stylesheet" href="{{$asset_url}}/assets/css/neon-core.css">
@endpush