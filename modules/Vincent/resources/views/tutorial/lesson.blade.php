@extends('edu::layouts.app')
@section('title', $lesson->title)
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <ol class="breadcrumb">
                <li>
                    <a href="/"><i class="entypo-home"></i></a>
                </li>
                {{--<li>--}}
                {{--<a href="{{route('edu.tutorial.index')}}">Tutorial</a>--}}
                {{--</li>--}}
                <li class="active">
                    <a href="{{route('edu.tutorial.show', $section->tutorial->id)}}">{{$section->tutorial->name}}</a>
                </li>
                <li class="active">
                    {{$section->name}}
                </li>
            </ol>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 text-justify">
           <div>
            @markdown($lesson->intro)
            @markdown($lesson->content)
            </div>
            <div class="form-group text-center">
                <a class="btn btn-primary btn-icon" href="{{route('edu.test.lesson', $lesson->id)}}">
                    <i class="glyphicon glyphicon-pencil"></i> Test
                </a>
                <button class="btn btn-primary btn-icon" id="word-export">
                    <i class="glyphicon glyphicon-save-file"></i> Word
                </button>
            </div>
            <div id="comments-container">

            </div>
        </div>
        <div class="col-lg-12">
            <ul class="list-group">
                <li class="list-group-item">
                    <strong>
                        Lessons
                    </strong>
                </li>
                @foreach($lessonList as $id => $name)
                    <li class="list-group-item {{$lesson->id == $id ? 'disabled ' : ''}}">
                        <a href="{{route('edu.tutorial.lesson', $id)}}">{{$name}}</a>
                    </li>
                @endforeach
            </ul>
{{--            <div class="row text-center">--}}
{{--                <div class="col-lg-8">--}}
{{--                    <h5><strong>{{$lesson->creator->last_name}}</strong></h5>--}}
{{--                    <a href="#" class="btn btn-xs btn-primary btn-icon" role="button">--}}
{{--                        <i class="fa fa-plus"></i> Follow--}}
{{--                    </a>--}}
{{--                    <a href="#" class="btn btn-xs btn-primary btn-icon" role="button">--}}
{{--                        <i class="entypo-user-add"></i> Contact--}}
{{--                    </a>--}}
{{--                </div>--}}
{{--                <div class="col-sm-4">--}}
{{--                    <img class="img-circle img-responsive"--}}
{{--                         src="{{ isset($lesson->creator->avatar) ? asset('storage/' . $lesson->creator->avatar) : 'https://scontent.fhan3-3.fna.fbcdn.net/v/t1.0-9/1380823_171886979683724_704506767_n.jpg?_nc_cat=0&oh=b2a23cae3353e54ea7d4047db855edd0&oe=5B983BF8'}}"--}}
{{--                         alt="avatar">--}}
{{--                </div>--}}
{{--            </div>--}}
        </div>
        <input type="hidden" id="lesson_id" value="{{$lesson->id}}">
    </div>
@endsection
@push('head')
    <link rel="stylesheet" type="text/css" href="{{asset('bower_components/jquery-comments')}}/css/jquery-comments.css">
    <link rel="stylesheet" type="text/css"
          href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{$asset_url}}/assets/css/neon-core.css">
@endpush
@push('js')
    <script src="{{$asset_url}}/assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
    <script type="text/javascript" src="{{asset('bower_components/jquery-comments')}}/js/jquery-comments.js"></script>
    <script src="{{$asset_url}}/assets/js/neon-api.js"></script>
    <script src="{{$asset_url}}/assets/js/neon-demo.js"></script>
    <script src="{{$asset_url}}/assets/js/neon-chat.js"></script>
    <script>
        $('#comments-container').comments({
            // profilePictureURL: '',
            getComments: function (success, error) {
                $.ajax({
                    type: 'get',
                    url: '{{route('lesson-comment-api.index')}}?lesson_id={{$lesson->id}}',
                    success: function (commentsArray) {
                        console.log(commentsArray)
                        success(commentsArray.data)
                    },
                    error: error
                });
            },
            getUsers: function (success, error) {
                $.ajax({
                    type: 'get',
                    url: '/api/users/',
                    success: function (userArray) {
                        console.log('-------------User array---------------')
                        console.log(userArray)
                        success(userArray)
                    },
                    error: error
                });
            },
            postComment: function (commentJSON, success, error) {
                console.log('create comment')
                const lesson_id = $('#lesson_id').val();
                commentJSON.lesson_id = parseInt(lesson_id);
                console.log(commentJSON)
                $.ajax({
                    type: 'post',
                    url: '{{route('lesson-comment-api.store')}}',
                    data: commentJSON,
                    success: function (comment) {
                        console.log(comment);
                        // success(comment)
                    },
                    error: function () {
                        alert("Bạn phải đăng nhập mới có thể bình luận")
                    }
                });
            },
            putComment: function (commentJSON, success, error) {
                $.ajax({
                    type: 'put',
                    url: '{{route('lesson-comment-api.index')}}' + commentJSON.id,
                    data: commentJSON,
                    success: function (comment) {
                        success(comment)
                    },
                    error: error
                });
            },
            deleteComment: function (commentJSON, success, error) {
                $.ajax({
                    type: 'delete',
                    url: '{{route('lesson-comment-api.index')}}' + commentJSON.id,
                    success: success,
                    error: error
                });
            },
            upvoteComment: function (commentJSON, success, error) {
                const commentURL = '/api/comments/' + commentJSON.id;
                const upvotesURL = commentURL + '/upvotes/';

                if (commentJSON.userHasUpvoted) {
                    $.ajax({
                        type: 'post',
                        url: upvotesURL,
                        data: {
                            comment: commentJSON.id
                        },
                        success: function () {
                            success(commentJSON)
                        },
                        error: error
                    });
                } else {
                    $.ajax({
                        type: 'delete',
                        url: upvotesURL + upvoteId,
                        success: function () {
                            success(commentJSON)
                        },
                        error: error
                    });
                }
            }
        });
    </script>

    {{--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>--}}
    <script src="{{asset('build/FileSaver.js')}}"></script>
    <script src="{{asset('build/jquery.wordexport.js')}}"></script>
    <script type="text/javascript">
        jQuery(document).ready(function ($) {
            $("#word-export").click(function (event) {
                $("#page-content").wordExport();
            });
        });

        if (typeof jQuery !== "undefined" && typeof saveAs !== "undefined") {
            (function ($) {
                $.fn.wordExport = function (fileName) {
                    fileName = typeof fileName !== 'undefined' ? fileName : "{{$lesson->title}}";
                    var static = {
                        mhtml: {
                            top: "Mime-Version: 1.0\nContent-Base: " + location.href + "\nContent-Type: Multipart/related; boundary=\"NEXT.ITEM-BOUNDARY\";type=\"text/html\"\n\n--NEXT.ITEM-BOUNDARY\nContent-Type: text/html; charset=\"utf-8\"\nContent-Location: " + location.href + "\n\n<!DOCTYPE html>\n<html>\n_html_</html>",
                            head: "<head>\n<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n<style>\n_styles_\n</style>\n</head>\n",
                            body: "<body>_body_</body>"
                        }
                    };
                    var options = {
                        maxWidth: 400
                    };
                    // Clone selected element before manipulating it
                    var markup = $(this).clone();

                    // Remove hidden elements from the output
                    markup.each(function () {
                        var self = $(this);
                        if (self.is(':hidden'))
                            self.remove();
                    });

                    // Embed all images using Data URLs
                    var images = Array();
                    var img = markup.find('img');
                    for (var i = 0; i < img.length; i++) {
                        // Calculate dimensions of output image
                        var w = Math.min(img[i].width, options.maxWidth);
                        var h = img[i].height * (w / img[i].width);
                        // Create canvas for converting image to data URL
                        var canvas = document.createElement("CANVAS");
                        canvas.width = w;
                        canvas.height = h;
                        // Draw image to canvas
                        var context = canvas.getContext('2d');
                        context.drawImage(img[i], 0, 0, w, h);
                        // Get data URL encoding of image
                        var uri = canvas.toDataURL("image/png");
                        $(img[i]).attr("src", img[i].src);
                        img[i].width = w;
                        img[i].height = h;
                        // Save encoded image to array
                        images[i] = {
                            type: uri.substring(uri.indexOf(":") + 1, uri.indexOf(";")),
                            encoding: uri.substring(uri.indexOf(";") + 1, uri.indexOf(",")),
                            location: $(img[i]).attr("src"),
                            data: uri.substring(uri.indexOf(",") + 1)
                        };
                    }

                    // Prepare bottom of mhtml file with image data
                    var mhtmlBottom = "\n";
                    for (var i = 0; i < images.length; i++) {
                        mhtmlBottom += "--NEXT.ITEM-BOUNDARY\n";
                        mhtmlBottom += "Content-Location: " + images[i].location + "\n";
                        mhtmlBottom += "Content-Type: " + images[i].type + "\n";
                        mhtmlBottom += "Content-Transfer-Encoding: " + images[i].encoding + "\n\n";
                        mhtmlBottom += images[i].data + "\n\n";
                    }
                    mhtmlBottom += "--NEXT.ITEM-BOUNDARY--";

                    //TODO: load css from included stylesheet
                    var styles = "";

                    // Aggregate parts of the file together
                    var fileContent = static.mhtml.top.replace("_html_", static.mhtml.head.replace("_styles_", styles) + static.mhtml.body.replace("_body_", markup.html())) + mhtmlBottom;

                    // Create a Blob with the file contents
                    var blob = new Blob([fileContent], {
                        type: "application/msword;charset=utf-8"
                    });
                    saveAs(blob, fileName + ".doc");
                };
            })(jQuery);
        } else {
            if (typeof jQuery === "undefined") {
                console.error("jQuery Word Export: missing dependency (jQuery)");
            }
            if (typeof saveAs === "undefined") {
                console.error("jQuery Word Export: missing dependency (FileSaver.js)");
            }
        }
    </script>

@endpush
