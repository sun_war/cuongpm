<?php
/**
 * Created by PhpStorm.
 * User: JK
 * Date: 2/12/2018
 * Time: 11:09 PM
 */

Route::group(['middleware' => ['web']], function () {
    Route::get('/one', function () {
        $questions = app(\Test\Models\MultiChoice::class)
            ->simplePaginate(1);
        if (request()->ajax()) {
            return view('edu::tests.components.one-table', compact('questions'))->render();
        }
        return view('edu::tests.one', compact('questions'));
    })->name('test.one');

    Route::post('/one/{id}', function ($id) {
        $question = app(\Test\Models\MultiChoice::class)->find($id);
        $answer = request()->get('answer');
        if ($question->answer > 5) {
            $answer = implode('', $answer);
        }
        $result = $question->answer == $answer;
        if ($result) {
            if (auth()->check()) {
                $filter = ['created_by' => auth()->id(), 'question_id' => $id];
                $question_result = app(\Test\Http\Repositories\QuestionResultRepository::class)
                    ->filterCount($filter);
                if ($question_result === 0) {
                    app(\Test\Http\Repositories\QuestionResultRepository::class)->create($filter);
                    $user = auth()->user();
                    $user->coin += 10;
                    $user->save();
                }
            }
            session()->flash('success', ' ĐÚng');
        } else {
            session()->flash('error', ' Sai');
        }
        if (request()->ajax()) {
            return response()->json($result, 200);
        }
        return back();
    })->name('test.one-mark');

    Route::group(['namespace' => 'Vincent\Http\Controllers'], function () {
        Route::get('/', 'VincentController@index')->name('home')->middleware('cacheable');

        Route::get('/home', function () {
            return redirect()->route('home');
        })->middleware('cacheable');

        Route::get('/test', 'TestController@getList')->name('edu.test.list');
        Route::get('doing', 'TestController@doing')->name('edu.test.doing');
        Route::post('marking', 'TestController@marking')->name('edu.test.marking');
        Route::get('result', 'TestController@result')->name('edu.test.result');
        Route::get('lesson/{lesson_id}', 'TestController@lesson')->name('edu.test.lesson');
        Route::post('lesson/{lesson_id}', 'TestController@lessonResult')->name('edu.test.lessonResult');

        Route::group(['prefix' => 'lang'], function () {
            Route::get('list', 'LanguageController@getList')->name('edu.language.list')->middleware('cacheable');
        });

        Route::group(['prefix' => 'tutorial', 'middleware' => 'cacheable'], function () {
            Route::get('index', 'TutorialController@index')->name('edu.tutorial.index');
            Route::get('show/{id}', 'TutorialController@show')->name('edu.tutorial.show');
            Route::get('section/{id}', 'TutorialController@section')->name('edu.tutorial.section');
            Route::get('lesson/{id}', 'TutorialController@lesson')->name('edu.tutorial.lesson');
        });

        Route::get('/charts', 'ChartController@coin')->name('charts')->middleware('cacheable');
        Route::get('/contact', 'VincentController@contact')->name('contact')->middleware('cacheable');
        Route::post('/contact', 'VincentController@contacted')->name('contacted');
    });

});
