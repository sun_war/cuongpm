<?php

namespace ReCaptcha\Rules;

use Illuminate\Contracts\Validation\Rule;
use ReCaptcha\Http\Services\ReCaptchaService;

class ReCaptchaValidRule implements Rule
{

    protected $reCaptchaService;

    public function __construct()
    {
        $this->reCaptchaService = new ReCaptchaService();
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->reCaptchaService->check($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The validation error valid recCaptcha.';
    }
}
