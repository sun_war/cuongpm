<?php
/**
 * Created by PhpStorm.
 * Date: 7/23/19
 * Time: 5:49 PM
 */

namespace ReCaptcha\Http\Services;


use GuzzleHttp\Client;

class ReCaptchaService
{
    public function check($response)
    {
        $client = new Client();

        $params = [
            'form_params' => [
                'secret' => config('re-captcha.secret'),
                'response' => $response,
                'remoteip' => $_SERVER['REMOTE_ADDR']
            ]
        ];

        $uri = config('re-captcha.uri');
        $response = $client->post($uri, $params);
        $body = json_decode((string)$response->getBody());

        return $body->success;
    }
}