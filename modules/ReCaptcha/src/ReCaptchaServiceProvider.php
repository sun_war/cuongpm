<?php

namespace ReCaptcha;

use Illuminate\Support\ServiceProvider;

class ReCaptchaServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/re-captcha.php', 're-captcha');

        $this->publishes([
            __DIR__ . '/../config/re-captcha.php' => config_path('re-captcha.php'),
        ], 're-captcha');
    }
}
