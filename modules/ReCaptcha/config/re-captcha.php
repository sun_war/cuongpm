<?php
/**
 * Created by PhpStorm.
 * Date: 7/24/19
 * Time: 2:39 PM
 */

return [
    'uri' => env('RE_CAPTCHA_URI', 'https://www.google.com/recaptcha/api/siteverify'),
    'secret' => env('RE_CAPTCHA_SECRET', '6LdeedAUAAAAADbB1GbSUFmRdT2p34YyaCDFm3PM'),
    'site' => env('RE_CAPTCHA_SITE', '6LdeedAUAAAAAKE6Y3OQ6Z34HyBDKb-bIkNFXEZ5')
];
