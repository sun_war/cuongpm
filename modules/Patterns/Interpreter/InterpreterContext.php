<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/6/18
 * Time: 11:23 AM
 */

namespace Patterns\Interpreter;


class InterpreterContext
{
    private $expressionstore = array();

    function replace(Expression $exp, $value)
    {
        $this->expressionstore[$exp->getKey()] = $value;
    }

    function lookup(Expression $exp)
    {
        return $this->expressionstore[$exp->getKey()];
    }
}