<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/6/18
 * Time: 11:38 AM
 */

namespace Patterns\Interpreter;


class VariableExpression extends Expression
{
    private $name;
    private $val;

    function __construct($name, $val = null)
    {
        $this->name = $name;
        $this->val = $val;
    }

    function interpret(InterpreterContext $context)
    {
        if (!is_null($this->val)) {
            $context->replace($this, $this->val);
            $this->val = null;
        }
    }

    function setValue($value)
    {
        $this->val = $value;
    }

    function getKey()
    {
        return $this->name;
    }
}