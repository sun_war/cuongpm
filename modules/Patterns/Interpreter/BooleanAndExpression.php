<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/6/18
 * Time: 3:04 PM
 */

namespace Patterns\Interpreter;


class BooleanAndExpression extends OperatorExpression
{
    protected function doInterpret(InterpreterContext $context, $result_l, $result_r)
    {
        $context->replace($this, $result_l && $result_r);
    }
}