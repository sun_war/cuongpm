<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/5/18
 * Time: 11:01 AM
 */

namespace Patterns\Decorator;


class PollutedPlains extends Plains
{
    function getWealthFactor() {
        return parent::getWealthFactor() - 4;
    }
}