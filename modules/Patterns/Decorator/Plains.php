<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/5/18
 * Time: 10:58 AM
 */

namespace Patterns\Decorator;


class Plains extends Tile
{
    private $wealthfactor = 2;
    function getWealthFactor() {
        return $this->wealthfactor;
    }
}