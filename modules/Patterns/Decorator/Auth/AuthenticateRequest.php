<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/5/18
 * Time: 1:53 PM
 */

namespace Patterns\Decorator\Auth;


class AuthenticateRequest extends DecorateProcess
{
    function process(RequestHelper $req)
    {
        print __CLASS__ . ": authenticating request <br/>";
        $this->processrequest->process($req);
    }
}