<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/5/18
 * Time: 11:57 AM
 */

namespace Patterns\Decorator\Auth;


abstract class ProcessRequest
{
    abstract function process( RequestHelper $req );
}