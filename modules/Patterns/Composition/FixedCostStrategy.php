<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 11/22/18
 * Time: 3:26 PM
 */

namespace Patterns\Composition;


class FixedCostStrategy extends CostStrategy
{
    function cost(Lesson $lesson)
    {
        return 30;
    }

    function chargeType()
    {
        return "fixed rate";
    }
}