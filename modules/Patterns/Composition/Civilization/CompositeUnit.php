<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/5/18
 * Time: 10:45 AM
 */

namespace Patterns\Composition\Civilization;


class CompositeUnit extends Unit
{
    private $units = array();

    function getComposite()
    {
        return $this;
    }

    protected function units()
    {
        return $this->units;
    }

    function removeUnit(Unit $unit)
    {
        $this->units = array_udiff($this->units, array($unit),
            function ($a, $b) {
                return ($a === $b) ? 0 : 1;
            });
    }

    function addUnit(Unit $unit)
    {
        if (in_array($unit, $this->units, true)) {
            return;
        }
        $this->units[] = $unit;
    }

    function bombardStrength()
    {
    }
}