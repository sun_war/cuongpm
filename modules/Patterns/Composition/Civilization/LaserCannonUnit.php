<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/5/18
 * Time: 9:36 AM
 */

namespace Patterns\Composition\Civilization;


class LaserCannonUnit extends Unit
{
    function bombardStrength()
    {
        return 44;
    }
}