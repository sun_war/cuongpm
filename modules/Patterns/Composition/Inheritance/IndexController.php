<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 11/22/18
 * Time: 2:51 PM
 */

namespace Patterns\Composition\Inheritance;


class IndexController
{
    public function __construct()
    {

    }

    public function index()
    {
        $lecture = new Lecture( 5, Lesson::FIXED );
        print "{$lecture->cost()} ({$lecture->chargeType()})\n";
        $seminar= new Seminar( 3, Lesson::TIMED );
        print "{$seminar->cost()} ({$seminar->chargeType()})\n";
    }
}