<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 11/22/18
 * Time: 2:51 PM
 */

namespace Patterns\Composition;


class IndexController
{
    public function __construct()
    {


    }

    public function index()
    {
        $lessons[] = new Seminar( 4, new TimedCostStrategy() );
        $lessons[] = new Lecture( 4, new FixedCostStrategy() );
        foreach ( $lessons as $lesson ) {
            print "lesson charge {$lesson->cost()}. <br>";
            print "Charge type: {$lesson->chargeType()} <br>\n";
        }
    }
}