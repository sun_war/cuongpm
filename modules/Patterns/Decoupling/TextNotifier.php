<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 11/23/18
 * Time: 10:18 AM
 */

namespace Patterns\Decoupling;


class TextNotifier extends Notifier
{
    function inform( $message ) {
        print "TEXT notification: {$message}\n";
    }

}