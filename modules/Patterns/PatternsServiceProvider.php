<?php
/**
 * Created by PhpStorm.
 * User: cuongpm
 * Date: 7/23/18
 * Time: 10:30 AM
 */
namespace Patterns;
use Illuminate\Support\ServiceProvider;
use Patterns\Repositories\PatternRepository;
use Patterns\Repositories\PatternRepositoryEloquent;

class PatternsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/database/migrations');
        $this->loadViewsFrom(__DIR__ . '/resources/views', 'pat');
        $this->loadRoutesFrom(__DIR__ . '/router.php');
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PatternRepository::class, PatternRepositoryEloquent::class);
    }
}