<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/4/18
 * Time: 11:31 AM
 */

namespace Patterns\AbstractFactory\Terrains\Component;


class Sea
{
    private $navigability = 0;
    function __construct( $navigability ) {
        $this->navigability = $navigability;
    }
}