<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/7/18
 * Time: 2:25 PM
 */

namespace Patterns\Strategy;


class RegexpMarker extends Marker
{
    function mark( $response ) {
        return ( preg_match( $this->test, $response ) );
    }
}