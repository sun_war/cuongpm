<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/7/18
 * Time: 2:25 PM
 */

namespace Patterns\Strategy;


class MatchMarker extends Marker
{
    function mark( $response ) {
        return ( $this->test == $response );
    }
}