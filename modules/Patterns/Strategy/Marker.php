<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/7/18
 * Time: 2:22 PM
 */

namespace Patterns\Strategy;


abstract class Marker
{
    protected $test;

    function __construct($test)
    {
        $this->test = $test;
    }

    abstract function mark($response);
}