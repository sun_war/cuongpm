<?php
/**
 * Created by PhpStorm.
 * User: cuongpm
 * Date: 7/23/18
 * Time: 10:23 AM
 */

Route::group(['namespace' => 'Patterns\Http\Controllers', 'middleware' => ['web', 'auth'], 'prefix' => 'pat'], function () {
    Route::resource('patterns', 'PatternController');
});

Route::group(['namespace' => 'Patterns\Composition', 'middleware' => ['web', 'auth'], 'prefix' => 'pat'], function () {
    Route::get('inheritance', 'Inheritance\IndexController@index');
    Route::get('composition', 'IndexController@index');
});

Route::get('decoupling', function () {
    $lessons1 = new \Patterns\Composition\Seminar(4, new \Patterns\Composition\TimedCostStrategy());
    $lessons2 = new \Patterns\Composition\Lecture(4, new \Patterns\Composition\FixedCostStrategy());
    $mgr = new \Patterns\Decoupling\RegistrationMgr();
    $mgr->register($lessons1);
    $mgr->register($lessons2);
});

Route::get('singleton', function () {
    $pref = \Patterns\Singleton\Preferences::getInstance();
    $pref->setProperty("name", "matt");
    unset($pref); // remove the reference
    $pref2 = \Patterns\Singleton\Preferences::getInstance();
    print $pref2->getProperty("name") . "<br>"; // demonstrate value is not lost
});

Route::get('abstract-factory', function () {
    $factory = new \Patterns\AbstractFactory\Terrains\TerrainFactory(
        new \Patterns\AbstractFactory\Terrains\Component\EarthSea(-1),
        new \Patterns\AbstractFactory\Terrains\Component\EarthPlains(),
        new \Patterns\AbstractFactory\Terrains\Component\EarthForest());
    echo "<pre>";
    print_r($factory->getSea());
    print_r($factory->getPlains());
    print_r($factory->getForest());

    $commsMgr = \Patterns\AbstractFactory\Terrains\AppConfig::getInstance()
        ->getCommsManager();
    echo $commsMgr->getApptEncoder()->encode();
});

Route::get('composite', function () {
// create an army
    $main_army = new \Patterns\Composition\Civilization\Army();
// add some units
    $main_army->addUnit(new \Patterns\Composition\Civilization\Archer());
    $main_army->addUnit(new \Patterns\Composition\Civilization\LaserCannonUnit());
// create a new army
    $sub_army = new \Patterns\Composition\Civilization\Army();
// add some units
    $sub_army->addUnit(new \Patterns\Composition\Civilization\Archer());
    $sub_army->addUnit(new \Patterns\Composition\Civilization\Archer());
    $sub_army->addUnit(new \Patterns\Composition\Civilization\Archer());
// add the second army to the first
    $main_army->addUnit($sub_army);
// all the calculations handled behind the scenes
    print "attacking with strength: {$main_army->bombardStrength()}<br>";
});

Route::get('decorator', function () {
    $process = new \Patterns\Decorator\Auth\AuthenticateRequest(
        new \Patterns\Decorator\Auth\StructureRequest(
            new \Patterns\Decorator\Auth\LogRequest (
                new \Patterns\Decorator\Auth\MainProcess()
            )
        )
    );
    $process->process(new \Patterns\Decorator\Auth\RequestHelper());
});

Route::get('interview', function () {
    $context = new \Patterns\Interpreter\InterpreterContext();
    $literal = new \Patterns\Interpreter\LiteralExpression('four');
    $literal->interpret($context);
    print $context->lookup($literal) . "<br>";

    $context = new \Patterns\Interpreter\InterpreterContext();
    $myvar = new \Patterns\Interpreter\VariableExpression('input', 'four');
    $myvar->interpret($context);
    print $context->lookup($myvar) . "<br>";
// output: four
    $newvar = new \Patterns\Interpreter\VariableExpression('input');
    $newvar->interpret($context);
    print $context->lookup($newvar) . "<br>";
// output: four
    $myvar->setValue("five");
    $myvar->interpret($context);
    print $context->lookup($myvar) . "<br>";
// output: five
    print $context->lookup($newvar) . "<br>";
// output: five

    $context = new \Patterns\Interpreter\InterpreterContext();
    $input = new \Patterns\Interpreter\VariableExpression('input');
    $statement = new \Patterns\Interpreter\BooleanOrExpression(
        new \Patterns\Interpreter\EqualsExpression($input, new \Patterns\Interpreter\LiteralExpression('four')),
        new \Patterns\Interpreter\EqualsExpression($input, new \Patterns\Interpreter\LiteralExpression('4'))
    );
    foreach (array("four", "4", "52") as $val) {
        $input->setValue($val);
        print "$val:<br/>";
        $statement->interpret($context);
        if ($context->lookup($statement)) {
            print "top marks<br/><br/>";
        } else {
            print "dunce hat on<br/><br/>";
        }
    }
//    $statement->interpret($context);
//    print $context->lookup($statement) . "<br>";
});

Route::get('strategy', function () {
    $markers = array(new \Patterns\Strategy\RegexpMarker("/f.ve/"),
        new \Patterns\Strategy\MatchMarker("five"),
        new \Patterns\Strategy\MarkLogicMarker('$input equals "five"')
    );
    foreach ($markers as $marker) {
        print get_class($marker) . "\n";
        $question = new \Patterns\Strategy\TextQuestion("how many beans make five", $marker);
        foreach (array("five", "four") as $response) {
            print "\tresponse: $response: </br>";
            if ($question->mark($response)) {
                print "well done </br>\n";
            } else {
                print "never mind </br>\n";
            }
        }
    }
});