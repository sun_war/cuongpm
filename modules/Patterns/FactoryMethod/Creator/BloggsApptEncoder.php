<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/4/18
 * Time: 10:03 AM
 */

namespace Patterns\FactoryMethod\Creator;


class BloggsApptEncoder extends ApptEncoder
{
    function encode()
    {
        return __CLASS__;
    }
}