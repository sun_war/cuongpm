<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/4/18
 * Time: 10:05 AM
 */

namespace Patterns\FactoryMethod\Creator;


class BloggsCommsManager extends CommsManager
{
    function getHeaderText() {
        return "BloggsCal header\n";
    }
    function getApptEncoder() {
        return new BloggsApptEncoder();
    }
    function getFooterText() {
        return "BloggsCal footer\n";
    }
}