<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/4/18
 * Time: 10:06 AM
 */

namespace Patterns\FactoryMethod\Creator;


class IndexController
{
    function __construct()
    {
        $mgr = new BloggsCommsManager();
        print $mgr->getHeaderText();

        print $mgr->getApptEncoder()->encode();
        print $mgr->getFooterText();
    }
}