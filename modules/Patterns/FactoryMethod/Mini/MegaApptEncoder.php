<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/4/18
 * Time: 9:43 AM
 */

namespace Patterns\FactoryMethod\Mini;


class MegaApptEncoder extends  ApptEncoder
{
    function encode()
    {
        return __CLASS__;
    }
}