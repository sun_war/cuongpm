<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/4/18
 * Time: 9:44 AM
 */

namespace Patterns\FactoryMethod\Mini;


abstract class CommsManager
{
    const BLOGGS = 1;
    const MEGA = 2;
    private $mode = 1;

    function __construct($mode)
    {
        $this->mode = $mode;
    }

    function getHeaderText()
    {
        switch ($this->mode) {
            case (self::MEGA):
                return "MegaCal header\n";
            default:
                return "BloggsCal header\n";
        }
    }

    function getApptEncoder()
    {
        switch ($this->mode) {
            case (self::MEGA):
                return new MegaApptEncoder();
            default:
                return new BloggsApptEncoder();
        }
    }
}

return new BloggsApptEncoder();