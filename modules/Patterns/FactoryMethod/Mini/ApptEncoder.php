<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/4/18
 * Time: 9:41 AM
 */

namespace Patterns\FactoryMethod\Mini;


abstract class ApptEncoder
{
    abstract function encode();
}