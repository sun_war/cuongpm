<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 12/4/18
 * Time: 9:42 AM
 */

namespace Patterns\FactoryMethod\Mini;


class BloggsApptEncoder extends ApptEncoder
{
    function encode()
    {
        return __CLASS__;
    }
}