<?php
/**
 * Created by PhpStorm.
 * User: diamond
 * Date: 1/11/19
 * Time: 5:51 PM
 */

namespace Media\Http\Repositories;


class MediaService
{
    public function done($dir)
    {
        $tailWhiteList = ['jpg', 'png', 'jpeg'];
        $images = [];
        $folders = [];
        $paths = [];
        if (is_dir($dir)) {
            foreach (glob($dir . '/*') as $file) {
                $path = $this->buildPath($file);
                $paths = explode('/', $path);
                $name = array_pop($paths);
                if (is_file($file)) {
                    $tail = $this->getTail($file);
                    if (in_array($tail, $tailWhiteList)) {
                        $images[$name] = $asset_url . $path;;
                    }
                } else {
                    $folders[$name] = route('media.manager', ['path' => $path]);
                }
            }
        }
        $parentFolder = $this->getRouteParentFolder($paths);
        $newFolderRoute = route('media.manager.new-folder', ['dir' => $dir]);
        $uploadRoute = route('media.manager.upload', ['dir' => $dir]);
        return compact('images', 'folders', 'parentFolder', 'newFolderRoute', 'uploadRoute');
    }

    private function buildPath($file)
    {
        $path = str_replace(public_path('/'), '', $file);
        return str_replace(('./'), '', $path);
    }

    private function getRouteParentFolder($paths)
    {
        return route('media.manager', ['path' => dirname(implode('/', $paths))]);
    }

    private function getTail($file)
    {
        $files = explode('.', $file);
        return array_pop($files);
    }
}