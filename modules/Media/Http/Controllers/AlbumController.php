<?php

namespace Media\Http\Controllers;

use App\Http\Controllers\Controller;
use Media\Http\Requests\AlbumCreateRequest;
use Media\Http\Requests\AlbumUpdateRequest;
use Media\Repositories\AlbumRepository;
use Illuminate\Http\Request;
use Modularization\MultiInheritance\ControllersTrait;

class AlbumController extends Controller
{
    use ControllersTrait;
    private $repository;

    public function __construct(AlbumRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $input = $request->all();
        $data['albums'] = $this->repository->myPaginate($input);
        if ($request->ajax()) {
            return view('media::album.table', $data)->render();
        }
        return view('media::album.index', $data);
    }

    public function create()
    {
        return view('media::album.create');
    }

    public function store(AlbumCreateRequest $request)
    {
        $input = $request->all();
        $this->repository->store($input);
        session()->flash('success', 'create success');
        return redirect()->route('albums.index');
    }

    public function show($id)
    {
        $album = $this->repository->find($id);
        if (empty($album)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('media::album.show', compact('album'));
    }

    public function edit($id)
    {
        $album = $this->repository->find($id);
        if (empty($album)) {
            session()->flash('error', 'not found');
            return back();
        }
        return view('media::album.update', compact('album'));
    }

    public function update(AlbumUpdateRequest $request, $id)
    {
        $input = $request->all();
        $album = $this->repository->find($id);
        if (empty($album)) {
            session()->flash('error', 'not found');
            return back();
        }
        $this->repository->change($input, $album);
        session()->flash('success', 'update success');
        return redirect()->route('albums.index');
    }

    public function destroy($id)
    {
        $album = $this->repository->find($id);
        if (empty($album)) {
            session()->flash('error', 'not found');
        }
        $this->repository->delete($id);
        session()->flash('success', 'delete success');
        return back();
    }
}
