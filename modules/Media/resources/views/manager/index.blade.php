@extends('layouts.app')
@section('content')
    <ol class="breadcrumb bc-3">
        <li>
            <a href="/"><i class="fa fa-home"></i></a>
        </li>
        <li>
            <a href="{{route('media.manager')}}">Manager</a>
        </li>
    </ol>
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary btn-icon" data-toggle="modal" data-target="#managerModal">
        Manager <i class="fa fa-image"></i>
    </button>
{{--    @include('media::manager.modals.show')--}}

@endsection

@push('js')
    <script src="{{asset('build/form-filter.js')}}"></script>
    <script>
        Menu('#MediaMenu', '#albumMenu');

    </script>
@endpush