@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('images.index')}}">{{trans('table.images')}}</a>
    </li>
    <li class="active">
        <strong>Show</strong>
    </li>
</ol>
<div>
    <table class="table">
        <tbody>
            <tr><th>{{__('label.title')}}</th>
<td>{!! $image->title !!}</td>
</tr><tr><th>{{__('label.album_id')}}</th>
<td>{!! $image->album_id !!}</td>
</tr><tr><th>{{__('label.image')}}</th>
<td>{!! $image->image !!}</td>
</tr><tr><th>{{__('label.link')}}</th>
<td>{!! $image->link !!}</td>
</tr><tr><th>{{__('label.is_active')}}</th>
<td>{!! $image->is_active !!}</td>
</tr>
        </tbody>
    </table>
</div>
@endsection

@push('js')
<script>
    Menu('#MediaMenu', '#imageMenu')
</script>
@endpush
