@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('images.index')}}">{{trans('table.images')}}</a>
    </li>
    <li class="active">
        <strong>{{__('action.update')}}</strong>
    </li>
</ol>
<div class="row">
    <form action="{{route('images.update', $image->id)}}" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
        {{method_field('PUT')}}
        <div class="form-group col-lg-12">
    <label for="title">{{trans('label.title')}}</label>
    <input required class="form-control" name="title" id="title" value="{{$image->title}}">
</div>
<div class="form-group col-lg-12">
    <label for="album_id">{{trans('label.album_id')}}</label>
    <input required type="number" class="form-control" name="album_id" id="album_id" value="{{$image->album_id}}">
</div>
<div class="form-group col-lg-12">
    <label for="image">{{trans('label.image')}}</label>
    <input required type="file" name="image" id="image">
    <p class="help-block">Example block-level help text here.</p>
</div>
<div class="form-group col-lg-12">
    <label for="link">{{trans('label.link')}}</label>
    <input required class="form-control" name="link" id="link" value="{{$image->link}}">
</div>
<div class="form-group col-lg-12">
    <label for="is_active">{{trans('label.is_active')}}</label>
    <div class="checkbox">
        <label>
            <input required type="checkbox" {{$image->is_active !== 1 ?: 'checked'}} name="is_active" id="is_active" value="1">
        </label>
    </div>
</div>

        <div class="col-lg-12 form-group">
            <button class="btn btn-primary">{{trans('button.done')}}</button>
            <button class="btn btn-primary isBack">{{trans('button.done_and_back')}}</button>
            <button type="reset" class="btn btn-default">{{trans('button.reset')}}</button>
            <a href="{{url()->previous()}}" class="btn btn-default">{{trans('button.cancel')}}</a>
        </div>
    </form>
</div>
@endsection

@push('js')
    <script>
        Menu('#MediaMenu', '#imageMenu')
    </script>
@endpush
