@extends('layouts.app')
@section('content')
<ol class="breadcrumb bc-3">
    <li>
        <a href="/"><i class="fa fa-home"></i></a>
    </li>
    <li>
        <a href="{{route('albums.index')}}">{{trans('table.albums')}}</a>
    </li>
    <li class="active">
        <strong>Show</strong>
    </li>
</ol>
<div>
    <table class="table">
        <tbody>
            <tr><th>{{__('label.title')}}</th>
<td>{!! $album->title !!}</td>
</tr><tr><th>{{__('label.image')}}</th>
<td>{!! $album->image !!}</td>
</tr><tr><th>{{__('label.is_active')}}</th>
<td>{!! $album->is_active !!}</td>
</tr>
        </tbody>
    </table>
</div>
@endsection

@push('js')
<script>
    Menu('#MediaMenu', '#albumMenu')
</script>
@endpush
