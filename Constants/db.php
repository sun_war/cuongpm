<?php

/**
 * Created by PhpStorm.
 * User: vincent
 * Date: 4/26/17
 * Time: 2:57 PM
 */
  const LESSONS_TB = 'lessons';
  const MULTI_CHOICES_TB = 'multi_choices';
  const ROLES_TB = 'roles';
  const SECTIONS_TB = 'sections';
  const USER_IMAGE_LIKES_TB = 'user_image_likes';
  const VOCABULARIES_TB = 'vocabularies';

  const A_COL = 'a';
  const ADDRESS_COL = 'address';
  const AMOUNT_COL = 'amount';
  const ANSWER_COL = 'answer';
  const AUTHOR_COL = 'author';
  const AVATAR_COL = 'avatar';
  const B_COL = 'b';
  const C_COL = 'c';
  const COIN_COL = 'coin';
  const CONTENT_COL = 'content';
  const COPYRIGHT_COL = 'copyright';
  const CREATE_BY_COL = 'create_by';
  const CREATED_AT_COL = 'created_at';
  const D_COL = 'd';
  const DEAL_COL = 'deal';
  const DESCRIPTION_COL = 'description';
  const DESCRIPTION_META_COL = 'description_meta';
  const DETAILS_COL = 'details';
  const DISCOUNT_COL = 'discount';
  const ICON_COL = 'icon';
  const IMAGE_COL = 'image';
  const IMAGE_META_COL = 'image_meta';
  const IMAGE_ONE_COL = 'image_one';
  const IMAGE_THREE_COL = 'image_three';
  const IMAGE_TWO_COL = 'image_two';
  const INFO_COL = 'info';
  const INTEREST_COL = 'interest';
  const INTRO_COL = 'intro';
  const JOB_COL = 'job';
  const KNOWLEDGE_COL = 'knowledge';
  const LAST_VIEW_COL = 'last_view';
  const LEVEL_COL = 'level';
  const LOCALE_COL = 'locale';
  const LOGO_COL = 'logo';
  const MAP_COL = 'map';
  const MESSAGE_COL = 'message';

  const NO_COL = 'no';
  const PAGE_COL = 'page';
  const PASSWORD_COL = 'password';
  const PHONE_NUMBER_COL = 'phone_number';
  const PRICE_COL = 'price';
  const PROFESSIONAL_COL = 'professional';
  const PRONOUNCE_COL = 'pronounce';
  const PRONUNCIATION_A_COL = 'pronunciation_a';
  const PRONUNCIATION_B_COL = 'pronunciation_b';
  const PRONUNCIATION_C_COL = 'pronunciation_c';
  const PRONUNCIATION_D_COL = 'pronunciation_d';
  const QUANTITY_COL = 'quantity';
  const QUESTION_COL = 'question';
  const REASON_COL = 'reason';
  const RECOMMENDED_COL = 'recommended';
  const REPAIR_COL = 'repair';
  const REPLACEMENT_COL = 'replacement';
  const REPLY1_COL = 'reply1';
  const REPLY2_COL = 'reply2';
  const REPLY3_COL = 'reply3';
  const REPLY4_COL = 'reply4';
  const REPLY5_COL = 'reply5';

  const STATUS_COL = 'status';
  const TIME_COL = 'time';
  const TIMEZONE_COL = 'timezone';
  const TITLE_COL = 'title';
  const TITLE_META_COL = 'title_meta';
  const TOKEN_COL = 'token';
  const TOTAL_COL = 'total';
  const UPDATED_AT_COL = 'updated_at';

  const VIEWS_COL = 'views';
  const WORD_COL = 'word';

