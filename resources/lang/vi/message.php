<?php
/**
 * Created by PhpStorm.
 * User: FightLightDiamond
 * Date: 4/20/2016
 * Time: 5:16 PM
 */
return [
    'no_data' => 'Dữ liệu trống',
    'success' => 'Thành côg',
    'fail' => 'Thất bại',
    'error' => 'Lỗi',
    'create_success' => 'Thêm thành công',
    'update_success' => 'Cập nhập thành công',
    'delete_success' => 'Xóa thành công',
    'select_singer' => 'Chọn ca sĩ',
    'select_author' => 'Chọn nhạc sĩ',

    'add_tag' => 'Thêm tag',
    'select_ringback_tone' => 'Chọn nhạc chờ',
    'select_topic' => 'Chọn chủ đề',
    'select_category' => 'Chọn thể loại',
    'select_record_copyright' => 'Chọn chủ đề',
    'select_author_copyright' => 'Chọn chủ đề',
];