<?php
/**
 * Created by PhpStorm.
 * User: CPM
 * Date: 4/28/2018
 * Time: 5:38 PM
 */

return [
    'create' => 'Tạo',
    'update' => 'Cập nhập',
    'done' => 'Xong',
    'delete' => 'Xóa',
    'save' => 'Luu',
    'show' => 'Xem',

];