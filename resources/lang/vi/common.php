<?php

return [
    'all' => 'Tất cả',
    'forgot_password_message' => 'Tôi quên mật khẩu',
    'home' => 'Trang chủ',
    'knowledge' => 'Kiến thức',
    'login' => 'Đăng nhập',
    'level' => 'Cấp độ',
    'profile' => 'Tài khoản',
    'professional' => 'Chức nghiệp',
    'register' => 'Đăng ký',
    'register_message' => 'Đăng ký tài khoản mới',
    'remember_me' => 'Ghi nhớ',
    'sign_in' => 'Đăng nhập',
    'sign_in_message' => 'Đăng nhập để sử dụng',
    'sign_out' => 'Đăng xuất',
    'site_name' => 'Keeng CMS',
];
