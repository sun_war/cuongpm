<?php
/**
 * Created by PhpStorm.
 * User: FightLightDiamond
 * Date: 4/20/2016
 * Time: 9:44 AM
 */
return [
    'add' => 'Thêm',
    'create' => 'Tạo',
    'done' => 'Xong',
    'done_and_back' => 'Thực hiện và trở lại',
    'edit' => 'Sửa',
    'update' => 'Cập nhập',
    'delete' => 'Xóa',
    'close' => 'Đóng',
    'back' => 'Trở lại',
    'save' => 'Lưu',
    'cancel' => 'Thoát',
    'reset' => 'Thiết lập lại'
];