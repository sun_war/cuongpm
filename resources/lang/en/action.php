<?php
/**
 * Created by PhpStorm.
 * User: CPM
 * Date: 4/28/2018
 * Time: 5:38 PM
 */

return [
    'create' => 'Create',
    'update' => 'Update',
    'done' => 'Done',
    'delete' => 'Delete',
    'save' => 'Save',
    'show' => 'Show',

];