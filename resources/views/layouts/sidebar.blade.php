<div class="sidebar-menu">
  <div class="sidebar-menu-inner">
    <header class="logo-env">
      <div class="logo">
        <a href="/admin">
          <h2 style="color: whitesmoke"><strong>{{config('app.name')}}</strong></h2>
        </a>
      </div>
      <div class="sidebar-collapse">
        <a href="#" class="sidebar-collapse-icon">
          <i class="entypo-menu"></i>
        </a>
      </div>
      <div class="sidebar-mobile-menu visible-xs">
        <a href="#" class="with-animation">
          <i class="entypo-menu"></i>
        </a>
      </div>
    </header>
    <ul id="main-menu" class="main-menu">
      @include('test::layouts.menu')
      @include('acl::layouts.menu')
      @include('tut::layouts.menu')
      @include('cpn::layouts.menu')
{{--      @include('eco::layouts.menu')--}}
{{--      @include('en::layouts.menu')--}}
{{--      @include('profile::layouts.menu')--}}
{{--      @include('studio::layouts.menu')--}}
    </ul>
  </div>
</div>
