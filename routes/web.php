<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('aes', function () {
    $aes = app(\Modularization\Facades\AES_Encryption::class);
    $encryption_key = openssl_random_pseudo_bytes(32);
    $str = '12344567 0000 9999';
    dump($str);
    $data = $aes->encryption($str, $encryption_key);
    $a = $aes->decryption($data, $encryption_key);
    dump($data, $a);
});

Route::get('country', function () {
    return app(\ACL\Utils\Locale::class)->getCountry();
});
