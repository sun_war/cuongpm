export class Animal {
    constructor(type, legs) {
        this.type = type;
        this.legs = legs;
    }

    makeNoise(sound = 'Loud Noise') {
        console.log(sound);
    }

    static extenstion(){
        return 'syntaxError';
    }
}


export class Cat extends Animal {

    constructor(type, legs, tail) {
        super(type, legs);
    }

    makeNoise(sound = 'run in this context') {
        console.log(sound);
    }

    async google() {
        await fetch('http://localhost:8000/lesson-comment-api?lesson_id=1011')
            .then((r) => r.json())
            .then((json) => {
                console.log(json);
            }).catch((error) => {
                console.log('failed')
        });

        console.log('Truoc data')
    }

    seter() {
        const exampleSet = new Set([1, 2, 3, 4, 5, 6]);
        exampleSet.add(3);
        exampleSet.add(5).add(17);
        console.log(exampleSet.has(3));
        exampleSet.clear();
        console.log(exampleSet.size);
    }
}

