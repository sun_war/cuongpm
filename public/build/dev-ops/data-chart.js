const ajaxDataChartRoute = '#ajaxDataChartRoute';

const scheduleBtn = '.scheduleBtn';
const setScheduleRoute = '#setScheduleRoute';

$('.selectFilter').change(function () {
    draw();
});

draw();

function draw() {
    const data = $(formFilter).serialize();
    $.ajax({
        url: $(ajaxDataChartRoute).val(),
        data: data,
        method: 'GET',
        success: function (data) {
            for (let index in data) {
                Highcharts.chart('container' + index, {
                    chart: {
                        type: 'line'
                    },
                    title: {
                        text: 'Monthly Average Temperature'
                    },
                    xAxis: {
                        title: {
                            text: null
                        },
                        labels: {
                            enabled: false,//default is true
                        }
                    },
                    yAxis: {
                        title: 'none'
                    },
                    plotOptions: {
                        line: {
                            dataLabels: {
                                enabled: true
                            },
                            enableMouseTracking: false
                        }
                    },
                    series: data[index]
                });
            }
        }
    });
}

const test_id = $('#testIdHidden').val();

$(document).on('click', scheduleBtn, function () {
    const self = $(this);
    const scheduleable_id = self.attr('data-scheduleable-id');
    const time = self.attr('data-time');
    const url = $(setScheduleRoute).attr('data-route');
    const data = {scheduleable_id: scheduleable_id, time: time, test_id: test_id};
    console.log(data);
    $.ajax({
        url: url,
        method: 'POST',
        data: data,
        success: function (data) {
            $(scheduleBtn).addClass('btn-default');
            $(scheduleBtn).removeClass('btn-primary');
            self.addClass('btn-primary');
            self.removeClass('btn-default');
            toastr.success('Update schedule success');
        }
    });
});

const probeIdInput = '#probe_id';

const exportExcelBtn = '#exportExcelBtn';

$(exportExcelBtn).click(function () {
    const probe_id = $(probeIdInput).val();
    const self = $(this);
    const url = self.attr('data-route');
    const data = {
        probe_id: probe_id
    };
    $.ajax({
        url: url,
        data: data,
        method: "POST",
        success: function (data) {
            console.log(data);
            location.href = data;
        },
    })
});