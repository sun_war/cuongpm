class Translate {
    speak(word) {
        const audio = new Audio();
        audio.src = 'http://api.voicerss.org/?key=bee4f91027d94f44b523d7bbfea9d2a1&hl=en-us&c=mp3&f=32khz_16bit_mono&src=' + word;
        audio.play();
    }
}

$(document).on('click', ".speakEnglish", function () {
    const word = $(this).text();
    const translate = new Translate();
    translate.speak(word);
});

$(document).on('click', ".englishRead", function () {
    const word = $(this).attr('data-content');
    const translate = new Translate();
    translate.speak(word);
});

$(document).on('change', '.englishInput', function () {
    const word = $(this).val();
    const translate = new Translate();
    translate.speak(word);
});

function speakEnglish(word) {
    const audio = new Audio();
    audio.src = 'http://api.voicerss.org/?key=bee4f91027d94f44b523d7bbfea9d2a1&hl=en-us&c=mp3&f=32khz_16bit_mono&src=' + word;
    audio.play();
}

const wordTranslate = $('#wordTranslate');

function selectLanguage() {
    const language = $('input:radio[name="language"]:checked').val();
    if (language === 1) {
        $('#speakEnglish').hide();
    } else {
        $('#speakEnglish').show();
    }
}

function listenOnly() {
    const word = wordTranslate.val().trim();
    if (word === '') {
        wordTranslate.val('');
        wordTranslate.focus();
        wordTranslate.attr('placeholder', 'Please key down word ...');
    } else {
        speakEnglish(word);
    }
}

function translateWord() {
    const word = wordTranslate.val().trim();
    const language = $('input:radio[name="language"]:checked').val();
    console.log(language);
    if (word === '') {
        wordTranslate.val('');
        wordTranslate.focus();
        wordTranslate.attr('placeholder', 'Please key down word ...');
    } else if (language === 0) {
        window.open('https://translate.google.com/?text=section#en/vi/' + word, "popupWindow", "width=900,height=400,scrollbars=yes");
    } else {
        window.open('https://translate.google.com/?text=section#vi/en/' + word, "popupWindow", "width=900,height=400,scrollbars=yes");
    }
}
