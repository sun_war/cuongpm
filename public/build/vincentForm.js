/**
 * Created by vincent on 5/18/17.
 */
function getChecked(input) {
    return input.map(function () {
        return $(this).val();
    }).get();
}

/**
 * rememberEnd
 * @type {{}}
 */

(function ($) {
    $.fn.checkAll = function (item) {
        const self = this;
        let id = '#' + self.attr('id');
        $(document).on('change', id, function () {
            if ($(id).is(":checked")) {
                $(item).each(function () {
                    $(this).prop('checked', true);
                })
            } else {
                $(item).each(function () {
                    $(this).prop('checked', false);
                })
            }
        });
        return this;
    };

    $.fn.magicFormer = function (config) {
        let { inputIn, selectIn, btnIn, tableIn} = config;
        const selfForm = this;
        function filterAjax(url, data) {
            $.ajax({
                url: url,
                method: 'GET',
                data: data,
                beforeSend: function () {
                    //table.html('Loading ...')
                },
                success: function (data) {
                    tableIn.html(data);
                    $('[data-toggle="popover"]').popover();
                    $('.btnPopover').hover(function () {
                        $(this).popover('show');
                    });
                },
                error: function () {
                    tableIn.html('Error..')
                }
            })
        }

        if (inputIn) {
            $(inputIn).on("keydown", function (event) {
                if (event.which === 13) {
                    event.preventDefault();
                    let data = selfForm.serialize();
                    let url = selfForm.attr('action');
                    console.log(url);
                    filterAjax(url, data);
                }
            });
        }
        if (selectIn) {
            $(selectIn).change(function () {
                let data = selfForm.serialize();
                let url = selfForm.attr('action');
                filterAjax(url, data);
            });
        }
        if (btnIn) {
            $(btnIn).click(() => {
                let data = selfForm.serialize();
                let url = selfForm.attr('action');
                filterAjax(url, data);
            });
        }
        /**
         * Paginate
         */
        $(document).on('click', '#' + tableIn.attr('id') + ' li a', function (e) {
            e.preventDefault();
            let url = $(this).attr('href');
            let data = selfForm.serialize();
            filterAjax(url, data);
        });

        return this;
    };

    $.fn.magicFormatNumber = function (config) {
        let magicNumber = this;
        magicNumber.keyup(function (event) {
            let selection = window.getSelection().toString();
            if (selection !== '') {
                return;
            }
            if ($.inArray(event.keyCode, [38, 40, 37, 39]) !== -1) {
                return;
            }
            let $this = $(this);
            let input = $this.val();
            input = input.replace(/[\D\s\._\-]+/g, "");
            input = input ? parseInt(input, 10) : 0;
            $(config).text(function () {
                return (input === 0) ? "" : input.toLocaleString("en-US");
            });
        });
        return this;
    }
}($));

const format = {
    number: (input , config) => {
        input += '';
        input = input.replace(/[\D\s\._\-]+/g, "");
        input = input ? parseInt(input, 10) : 0;
        $(config).text(function () {
            return (input === 0) ? "" : input.toLocaleString("en-US");
        });
    }
};


