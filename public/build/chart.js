$('.viewShark').click(function () {
    const id = $(this).attr('data');
    const coinShark = '#coin_shark';
    const dealShark = '#deal_shark';
    const chartsShark = '.charts_shark';
    $.ajax({
        url: "{{route('coin-log.charts')}}",
        method: 'GET',
        data: { created_by: id},
        success: function (data) {
            $(coinShark).html('');
            $(dealShark).html('');
            Morris.Line({
                element: 'coin_shark',
                data: data,
                xkey: 'created_at',
                ykeys: ['coin'],
                labels: ['$'],
                parseTime: false,
                lineColors: ['blue']
            });
            Morris.Line({
                element: 'deal_shark',
                data: data,
                xkey: 'created_at',
                ykeys: ['deal'],
                labels: ['$'],
                parseTime: false,
                lineColors: ['red']
            });
            $(chartsShark).removeClass('hidden')
        },
        error: function () {
            alert('error')
        }
    })
});
$(function () {
    $('#cmd').click( function () {
        let doc = new jsPDF();
        const chart = new Promise((resolve, reject) => {
            let promises = [];
            let x = 10;
            let y = 20;
            $('#content-chart svg').each( function () {
                const self = this;
                const promise = new Promise((resolve, reject) => {
                    svgAsPngUri(self, {}, function (uri) {
                        console.log('x' + x);
                        console.log('y' + y);
                        doc.setFontSize(20);
                        doc.text(15, 15, "Chart");
                        doc.addImage(uri, 'JPEG', x, y, 180, 90 );

                        // x += 50;
                        y += 90;
                        resolve(uri);
                        console.log(2);
                    });
                });
                promises.push(promise);
            });
            Promise.all(promises).then((data) => {
                console.log(data);
                resolve(data);
            })
        });
        Promise.all([chart]).then((data) => {
            console.log(3);
            doc.save('charts.pdf');
        }).catch((error) => {
            alert('error');
        })
    });
});