const folderShow = '.folderShow';
const contentManager = '#contentManager';

$(document).on('click', folderShow, function (e) {
    e.preventDefault();
    const self = $(this);
    const url = self.attr('href');
    $.ajax({
        url: url,
        method: 'GET',
        success: function (data) {
            $(contentManager).html(data);
        }
    });
});

const newFolderForm = '#newFolderForm';
$(document).on('submit', newFolderForm, function (e) {
    e.preventDefault();
    const self = $(this);
    const url = self.attr('action');
    const data = self.serialize();
    $.ajax({
        url: url,
        method: 'GET',
        data: data,
        success: function (data) {
            $(contentManager).html(data);
        },
        error: function () {
            alert('error');
        }
    });
});

const copyName = '.copyName';
const copied = '.copied';
$(document).on('click', copyName, function (e) {
    e.preventDefault();
    const self = $(this);
    // self.focus();
    // self.select();
    $(copied).val( self.attr('data-url')).select();
    $(copied).attr("type", "text").select();
    document.execCommand("copy");
    $(copied).attr("type", "hidden");
    toastr.success("Copied the text: " + self.val());
});

const uploadForm = '#uploadForm';
$(document).on('submit', uploadForm, function (e) {
    e.preventDefault();
    const self = $(this);
    const url = self.attr('action');
    const data = new FormData(self[0]);
    console.log(data);
    $.ajax({
        url: url,
        method: 'POST',
        data: data,
        async: false,
        cache: false,
        contentType: false,
        enctype: 'multipart/form-data',
        processData: false,
        success: function (data) {
            $(contentManager).html(data);
        },
        error: function () {
            alert('error');
        }
    });
});