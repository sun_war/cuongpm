const tutorialSelect = $('#tutorial_id');
const route = $('#sectionListRoute').val();
const config = {
    route: route,
    isSelect: '#section_id',
    name: 'tutorial_id'
};
tutorialSelect.magicSelect(config);

const sectionSelect = $('#section_id');
const routeSection = $('#lessonListRoute').val();
const configSection = {
    route: routeSection,
    isSelect: '#lesson_id',
    name: 'section_id'
};
sectionSelect.magicSelect(configSection);