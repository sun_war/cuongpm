var updateTestCaseResultBtn = $('.updateTestCaseResult');
var getTestCaseResultRoute = $('#getTestCaseResultRoute');
var tableCreate = $('#tableCreate');
var myTestLineRoute = $('#myTestLineRoute');
var testLineRadio = $('.testLineRadio');

updateTestCaseResultBtn.click(function () {
    var pppt_ids = $(this).attr('pppt_ids');
    var url = getTestCaseResultRoute.val();
    console.log(url);
    $.ajax({
        url: url,
        method: 'GET',
        data: {pppt_ids: pppt_ids},
        success: function (data) {
            console.log(data);
            tableCreate.html(data);
        },
        error: function () {
            alert('Error')
        }
    });
});

$(document).on('change', '.testLineRadio', function () {
    var select = '<option value="">Select Line</option>';
    var self = $(this);
    var value = $(this).val();
    console.log(value);
    var url = myTestLineRoute.val();
    console.log(url);
    var data = {};
    if (value == 0) {
        data = {self: 1};
    }
    $.ajax({
        url: url,
        method: 'GET',
        data: data,
        success: function (data) {
            if (data) {
                $.each(data, function (k, v) {
                    select += '<option value="' + k + '">' + v + '</option>';
                });
                self.parents('div').children('select').html(select);
            }
        },
        error: function () {
            alert('Error');
        }
    });
});

var attachmentInput = $('.attachmentInput');
var removeAttachmentRoute = $('#removeAttachmentRoute');

$(document).on('change', '.attachmentInput', function () {
    var self = $(this);
    var link = self.val();
    var img = '<img class="img-responsive" src="' + link + '">';
    self.parents('td').append(img);
});

$(document).on('click', '.rmAttachmentBtn', function () {
    var ok = confirm('Are you sure ?');
    if(ok) {
        var self = $(this);
        var id = self.attr('data');
        // self.removeClass('file-result');
        self.parents('ul').remove();
        if(id)
        {
            var data = {id: id};
            var rmUrl = removeAttachmentRoute.val();
            $.ajax({
                url: rmUrl,
                method: 'POST',
                data : data,
                success: function (data) {
                    // $('#image'+id).attr('src', 'http://placehold.it/200x150')
                    alert('Delete success')
                },
                error: function () {
                    alert('Error')
                }
            });
        }
    }

});