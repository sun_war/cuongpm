const formTest = '#formTest';
const unsure = '.unsure';
const done = '.done';

function mark() {
    const ok = confirm('Bạn thực sự muốn nộp bài');
    if(ok) {
        $(formTest).submit()
    }
}
$(unsure).change(function () {
    const id = $(this).attr('data');
    const check = '#check' + id;
    if ($(this).is(":checked")) {
        $(done).each(function () {
            if ($(this).attr('data') === id && $(this).is(":checked")) {
                $(check).removeClass('btn-default');
                $(check).removeClass('btn-info');
                $(check).addClass('btn-warning');
                return false;
            } else {
                $(check).removeClass('btn-default');
                $(check).removeClass('btn-info');
                $(check).addClass('btn-danger destroyBtn');
            }
        })
    } else {
        $(done).each(function () {
            if ($(this).attr('data') === id && $(this).is(":checked")) {
                $(check).removeClass('btn-danger destroyBtn');
                $(check).removeClass('btn-warning');
                $(check).addClass('btn-info');
                return false;
            } else {
                $(check).removeClass('btn-danger destroyBtn');
                $(check).removeClass('btn-info');
                $(check).addClass('btn-default');
            }
        })
    }
});
$(done).change(function () {
    const id = $(this).attr('data');
    const check = '#check' + id;
    if ($(this).is(":checked")) {
        $(unsure).each(function () {
            if ($(this).attr('data') === id && $(this).is(":checked")) {
                $(check).removeClass('btn-info');
                $(check).removeClass('btn-default');
                $(check).removeClass('btn-danger destroyBtn');
                $(check).addClass('btn-warning');
                return false;
            } else {
                $(check).removeClass('btn-info');
                $(check).removeClass('btn-default');
                $(check).removeClass('btn-danger destroyBtn');
                $(check).addClass('btn-info');
            }
        });
    }
    else {
        $(unsure).each(function () {
            if ($(this).attr('data') === id && $(this).is(":checked")) {
                $(check).removeClass('btn-info');
                $(check).removeClass('btn-danger destroyBtn');
                $(check).removeClass('btn-warning');
                $(check).addClass('btn-default');
                return false;
            } else {
                $(check).removeClass('btn-info');
                $(check).removeClass('btn-danger destroyBtn');
                $(check).removeClass('btn-warning');
                $(check).addClass('btn-default');
            }
        });
    }
});
