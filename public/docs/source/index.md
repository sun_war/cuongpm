---
title: API Reference

language_tabs:
- bash

- javascript


includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://127.0.0.1:3000/docs/collection.json)

<!-- END_INFO -->

#ACL

APIs for acl
<!-- START_c3fa189a6c95ca36ad6ac4791a873d23 -->
## Login a user

[Passport to login user, return token to authentication]

> Example request:

```bash
curl -X POST "http://127.0.0.1:3000/api/login" \
    -H "Content-Type: application/json" \
    -d '{"grant_type":"password","client_id":"2","client_secret":"DFF6f2IX9TOhZ2vqH73xYQlxS92ogMpxPTY1fDQx","username":"phamminhcuong1704bnfrv@gmail.com","password":"123456","provider":"users"}'

```

```javascript
const url = new URL("http://127.0.0.1:3000/api/login");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "grant_type": "password",
    "client_id": "2",
    "client_secret": "DFF6f2IX9TOhZ2vqH73xYQlxS92ogMpxPTY1fDQx",
    "username": "phamminhcuong1704bnfrv@gmail.com",
    "password": "123456",
    "provider": "users"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):


```json
{
    "id": 2,
    "first_name": null,
    "last_name": null,
    "code": null,
    "email": "phamminhcuong1704bnfrv@gmail.com",
    "phone_number": null,
    "sex": 1,
    "birthday": null,
    "address": null,
    "avatar": null,
    "is_active": 1,
    "last_login": null,
    "last_logout": null,
    "slack_webhook_url": null,
    "created_at": "2018-03-15 18:53:14",
    "updated_at": "2018-06-08 11:51:33",
    "deleted_at": null,
    "coin": "10000000000000000000",
    "locale": null,
    "group_id": 1,
    "full_name": " ",
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjI4N2Y0YzNlM2U5ZGNlMTM1NGM1YzRjOGFmNDY4ZTM4MzFhOWM3MDkzMzAzNmNiMThlZGY0Y2NlMWU1N2ZlOTk0YTI5MTc4MDY5ZjAxYWNkIn0.eyJhdWQiOiIyIiwianRpIjoiMjg3ZjRjM2UzZTlkY2UxMzU0YzVjNGM4YWY0NjhlMzgzMWE5YzcwOTMzMDM2Y2IxOGVkZjRjY2UxZTU3ZmU5OTRhMjkxNzgwNjlmMDFhY2QiLCJpYXQiOjE1NTc1NjMxNDIsIm5iZiI6MTU1NzU2MzE0MiwiZXhwIjoxNTU4ODU5MTQyLCJzdWIiOiIyIiwic2NvcGVzIjpbXX0.hIwddnMrE-PawCfA-fE581qSf3VqIi4urZTLc0xpjiq9hsRv3oRy3bZwOcpgZCUs0QFC-_dhGwhYFShgfmk5j90-dBKSIcv333rE1eYqD9tHw3qL8qTsMlyNQgUqSgRXCjS8hE0tJrRzEXgvYh8s0bnTfIEIZHYicHaVDFgcBoxSutVZd57wdhKE9C0MOk9nN-rqQIdAmPplC0FFtkLGAEpu0C19ilLKlaZC8MRphPtih0PA2djTgdoU29JB6RviU7QvTJOKAiJXp_hd9IubcE0T0ihWVtCsQwR81AZz_sB19mC6qymtiuO-1GL3o8dxHztPEKINqZ3QPVRdYHq5Hq_MFzyy9Ykx5fCj8n6IIQQrHk9sMXs25WEgIZc-75ntcDxBODy_zO-DPLSzWSkkEaT1ukCbEEnmwQD6X34nvpZYuTN8tYlLR9GfNcO7czaEn2v6dl1FIlB1iQimax-pqS2FVGalFgtqgu7utswH9gRgzMP498Y5HBpEIWVmJeWmSk6n3AidTdFfvnQnps8vp9bcgUuE_hlQB2PiwxtLTXKR3sCkY3EvW6jx-_j2vNc32HQMTR0mkld1pXpacWbYkPH5GHTmEj3IY5eZ3JRy3bxZxtu-iSu5V2ZbTTZqTgTWOmRefWytiJmJK4HELfpeHJDoLK9DiyPhCuSKBK_5uFU",
    "token_type": "Bearer",
    "expires_in": 1296000,
    "refresh_token": "def502005f6085fa5da4a5f22b446234b165b933be0d1a13a8fc6c56c94de1e0cc94ae21658fb1333ab525643827a7dece44e2a690fb000bf59606edabd5c9cc9405e874aae21f027f7ca957def0632d9ed489ec83ea7f29d9e4bf0c34f98793f55a560ae14e46bc0862679d3f414d51ad7255208e2c67708234f4b02baed0b16842e02ceeb6e6f00406043977c0ca825197c0f7868d868088b20d4b262af85e55d0860de381cdeb5671977ec40a71a576be8ed635ac2613face7910b15029c13db61acaf002230180c8a9f8af64884a0a8f96500000a1ef1297055b42bc8c5bad4d1d937a5d384be769a4f576b90658d65f17b91015e763dff406a80d88ed73347af8e0f8dcfb90c9bd94663f9b30d98de42e4db804855f92a50de7a0e0aa224c21fabdcb8a0b1841c29ce0dce6fda172ee62ad9c9603db8a0c50416a92d300d64a9d15d64eb998e84f1a9015b43711fd00e4c0c779aa6778e4b577ddd105976f"
}
```

### HTTP Request
`POST api/login`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    grant_type | required |  optional  | The grant_type of the user.
    client_id | required |  optional  | The client_id of the user.
    client_secret | required |  optional  | The id of the user.
    username | required |  optional  | The email of the user.
    password | required |  optional  | The password of the user.
    provider | required |  optional  | The provider of the user.

<!-- END_c3fa189a6c95ca36ad6ac4791a873d23 -->

<!-- START_d7b7952e7fdddc07c978c9bdaf757acf -->
## Register a user

> Example request:

```bash
curl -X POST "http://127.0.0.1:3000/api/register" \
    -H "Content-Type: application/json" \
    -d '{"email":"phamminhcuong1704bnz@gmail.com","password":"123456","password_confirmation":"123456"}'

```

```javascript
const url = new URL("http://127.0.0.1:3000/api/register");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "email": "phamminhcuong1704bnz@gmail.com",
    "password": "123456",
    "password_confirmation": "123456"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):


```json
{
    "email": "phamminhcuong1704bnz@gmail.com",
    "updated_at": "2019-05-11 08:25:43",
    "created_at": "2019-05-11 08:25:43",
    "id": 221,
    "accessToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6Ijg5OGQwNDE2YWZjYmI1ZTZmZGU0M2UzMjI4MmZiNmY4ZjdlZWIxMmIwZDMzYmU2NzZhMjA0NmZmODBlNjQ5YjhiOWZkODM0NzY3ZGY4ZGNiIn0.eyJhdWQiOiIxIiwianRpIjoiODk4ZDA0MTZhZmNiYjVlNmZkZTQzZTMyMjgyZmI2ZjhmN2VlYjEyYjBkMzNiZTY3NmEyMDQ2ZmY4MGU2NDliOGI5ZmQ4MzQ3NjdkZjhkY2IiLCJpYXQiOjE1NTc1NjMxNDMsIm5iZiI6MTU1NzU2MzE0MywiZXhwIjoxNTg5MTg1NTQzLCJzdWIiOiIyMjEiLCJzY29wZXMiOltdfQ.AUvBpb-1zpHNsy5eVLOecD5brUGLsXzDHhb_5tokNIiQjYENi6PuXc_CVsf8GXbmNBQuJ216o02pto2D1uaPLjnPspOn05CmIzBFAXyGDLIwRRO8Qin4JmxMM4X5BHom8ZEbuo_anoTQAkpceDU8oaFbLFBd5MBhmAqH796iRGFUsAbNxwO2xmESz8NtMI4D7_QA8GA0IrASKjMTrmOKM5F1b2i_lCdnM9gB2Gop0z5DvKJGzG56TTbDs047MELZu9xncn42n09wVDThHFuKRz3hId12UrII77mUsKyWUlkkD5j4BYld8FQZ9ouOxfTFaUlYphpDUVthELHdGjl4VotWVXm2Dm8gJnVOBHmgofopUEzZ5W_b6YtDbZtC81iApRj-OdPz5VnGjfzgL8NqarG64hNivAL8iLxUD2gixE-J1q2EF_w_uscrx0KCbKtCHIWARc7ZmePFuGc0kDsuaSv0rxMWc30YhdoTiS8mK2OAs2yrvFzH09tUgAH5Iv6rPrY6cIn_7ZJkPxb8MwrJjRU5h1GBioaI1LMXWmGVZLyrnYQgYo6_BlfpPZpqkVpWMF6zcSzdEOCfwR8DWTC0BKVzQtvfm6QGlt8yWZxcNUuTFh5FMaGtysAhLBVaBCAUEYF8JGZDVD3kihR3rWl7BKMcv7fKwE5_Xh0yQkuVgio",
    "full_name": " "
}
```

### HTTP Request
`POST api/register`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    email | required |  optional  | The email of the user.
    password | required |  optional  | The password of the user.
    password_confirmation | required |  optional  | The password confirmation of the user.

<!-- END_d7b7952e7fdddc07c978c9bdaf757acf -->

<!-- START_78c4b7d6388c81c68bc37ec872d44f65 -->
## Forgot Password

> Example request:

```bash
curl -X POST "http://127.0.0.1:3000/api/forgot-password" \
    -H "Content-Type: application/json" \
    -d '{"email":"phamminhcuong1704bnfrv@gmail.com"}'

```

```javascript
const url = new URL("http://127.0.0.1:3000/api/forgot-password");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "email": "phamminhcuong1704bnfrv@gmail.com"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):


```json
{
    "data": "passwords.sent"
}
```

### HTTP Request
`POST api/forgot-password`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    email | required |  optional  | The email of the user.

<!-- END_78c4b7d6388c81c68bc37ec872d44f65 -->

<!-- START_6d3061d375666b8cf6babe163b36f250 -->
## Reset the given user&#039;s password.

> Example request:

```bash
curl -X POST "http://127.0.0.1:3000/api/reset-password" \
    -H "Content-Type: application/json" \
    -d '{"email":"phamminhcuong1704bnfrv@gmail.com","token":"658fda2f0b79b92b406e4e80d8f5354ecc52e2423f99ea96d436a9e16538a962","password":"123456aA@","password_confirmation":"123456aA@"}'

```

```javascript
const url = new URL("http://127.0.0.1:3000/api/reset-password");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "email": "phamminhcuong1704bnfrv@gmail.com",
    "token": "658fda2f0b79b92b406e4e80d8f5354ecc52e2423f99ea96d436a9e16538a962",
    "password": "123456aA@",
    "password_confirmation": "123456aA@"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (422):


```json
{
    "errors": [
        "Email invalid reset password"
    ]
}
```

### HTTP Request
`POST api/reset-password`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    email | required |  optional  | The email of the user.
    token | required |  optional  | The token of the user.
    password | required |  optional  | The password of the user.
    password_confirmation | required |  optional  | The password_confirmation of the user.

<!-- END_6d3061d375666b8cf6babe163b36f250 -->

<!-- START_794a2a29ea9731efc590d399ffb66df1 -->
## api/verify-users
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/verify-users" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/verify-users");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):


```json
{
    "current_page": 1,
    "data": [
        {
            "id": 2,
            "user_id": 191,
            "code": "5c93349eab422BJ0PHaaIOC",
            "email": 0,
            "phone": 0,
            "otp_verified": 0,
            "google_authentication": null,
            "created_at": "2019-03-21 13:52:14",
            "updated_at": "2019-03-21 13:52:14"
        }
    ],
    "first_page_url": "http:\/\/localhost\/api\/verify-users?page=1",
    "from": 1,
    "last_page": 1,
    "last_page_url": "http:\/\/localhost\/api\/verify-users?page=1",
    "next_page_url": null,
    "path": "http:\/\/localhost\/api\/verify-users",
    "per_page": 10,
    "prev_page_url": null,
    "to": 1,
    "total": 1
}
```

### HTTP Request
`GET api/verify-users`


<!-- END_794a2a29ea9731efc590d399ffb66df1 -->

<!-- START_f073956b3ccaf650a5f93200ea33c4b6 -->
## api/verify-users/create
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/verify-users/create" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/verify-users/create");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):


```json
null
```

### HTTP Request
`GET api/verify-users/create`


<!-- END_f073956b3ccaf650a5f93200ea33c4b6 -->

<!-- START_c1c1ef865888a2c49a05ac6a9a4bdc17 -->
## api/verify-users
> Example request:

```bash
curl -X POST "http://127.0.0.1:3000/api/verify-users" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/verify-users");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):


```json
null
```

### HTTP Request
`POST api/verify-users`


<!-- END_c1c1ef865888a2c49a05ac6a9a4bdc17 -->

<!-- START_cc35be56d5f24e38153db947cded1159 -->
## api/verify-users/{verify_user}
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/verify-users/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/verify-users/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (404):


```json
null
```

### HTTP Request
`GET api/verify-users/{verify_user}`


<!-- END_cc35be56d5f24e38153db947cded1159 -->

<!-- START_1e7d63e79138e502e691e84a17be4e0e -->
## api/verify-users/{verify_user}/edit
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/verify-users/1/edit" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/verify-users/1/edit");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (404):


```json
null
```

### HTTP Request
`GET api/verify-users/{verify_user}/edit`


<!-- END_1e7d63e79138e502e691e84a17be4e0e -->

<!-- START_1433889f5beb3c5d5ffa746a82b26a47 -->
## api/verify-users/{verify_user}
> Example request:

```bash
curl -X PUT "http://127.0.0.1:3000/api/verify-users/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/verify-users/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (404):


```json
null
```

### HTTP Request
`PUT api/verify-users/{verify_user}`

`PATCH api/verify-users/{verify_user}`


<!-- END_1433889f5beb3c5d5ffa746a82b26a47 -->

<!-- START_460514a367541141b19151bf28481165 -->
## api/verify-users/{verify_user}
> Example request:

```bash
curl -X DELETE "http://127.0.0.1:3000/api/verify-users/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/verify-users/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (404):


```json
null
```

### HTTP Request
`DELETE api/verify-users/{verify_user}`


<!-- END_460514a367541141b19151bf28481165 -->

#AML

APIs for aml
<!-- START_897b20ff86c3c12a70e1a34ab5664d84 -->
## api/aml-settings
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/aml-settings" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/aml-settings");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):


```json
{
    "current_page": 1,
    "data": [
        {
            "id": 1,
            "amount": "84581.0000000000",
            "usd_price": "3.0000000000",
            "eth_price": "1.0000000000",
            "btc_price": "4.0000000000",
            "usd_sold_amount": "4.0000000000",
            "eth_sold_amount": "8.0000000000",
            "btc_sold_amount": "5.0000000000",
            "presenter_price": "2.0000000000",
            "presentee_price": "9.0000000000",
            "created_at": "2019-04-24 08:38:36",
            "updated_at": "2019-04-24 08:38:36"
        }
    ],
    "first_page_url": "http:\/\/localhost\/api\/aml-settings?page=1",
    "from": 1,
    "last_page": 1,
    "last_page_url": "http:\/\/localhost\/api\/aml-settings?page=1",
    "next_page_url": null,
    "path": "http:\/\/localhost\/api\/aml-settings",
    "per_page": 10,
    "prev_page_url": null,
    "to": 1,
    "total": 1
}
```

### HTTP Request
`GET api/aml-settings`


<!-- END_897b20ff86c3c12a70e1a34ab5664d84 -->

<!-- START_0d12d5e147b1ed6912f13b1134ee896c -->
## api/aml-settings/create
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/aml-settings/create" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/aml-settings/create");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):


```json
null
```

### HTTP Request
`GET api/aml-settings/create`


<!-- END_0d12d5e147b1ed6912f13b1134ee896c -->

<!-- START_6d2d2659c4c316b9c3a04d332abab519 -->
## api/aml-settings
> Example request:

```bash
curl -X POST "http://127.0.0.1:3000/api/aml-settings" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/aml-settings");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`POST api/aml-settings`


<!-- END_6d2d2659c4c316b9c3a04d332abab519 -->

<!-- START_a9491b6b845d4f19a54326b720d5618e -->
## api/aml-settings/{aml_setting}
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/aml-settings/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/aml-settings/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):


```json
{
    "data": {
        "id": 1,
        "amount": "84581.0000000000",
        "usd_price": "3.0000000000",
        "eth_price": "1.0000000000",
        "btc_price": "4.0000000000",
        "usd_sold_amount": "4.0000000000",
        "eth_sold_amount": "8.0000000000",
        "btc_sold_amount": "5.0000000000",
        "presenter_price": "2.0000000000",
        "presentee_price": "9.0000000000",
        "created_at": "2019-04-24 08:38:36",
        "updated_at": "2019-04-24 08:38:36"
    }
}
```

### HTTP Request
`GET api/aml-settings/{aml_setting}`


<!-- END_a9491b6b845d4f19a54326b720d5618e -->

<!-- START_e980f2de4a1007f87d8359815c403084 -->
## api/aml-settings/{aml_setting}/edit
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/aml-settings/1/edit" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/aml-settings/1/edit");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):


```json
{
    "data": {
        "id": 1,
        "amount": "84581.0000000000",
        "usd_price": "3.0000000000",
        "eth_price": "1.0000000000",
        "btc_price": "4.0000000000",
        "usd_sold_amount": "4.0000000000",
        "eth_sold_amount": "8.0000000000",
        "btc_sold_amount": "5.0000000000",
        "presenter_price": "2.0000000000",
        "presentee_price": "9.0000000000",
        "created_at": "2019-04-24 08:38:36",
        "updated_at": "2019-04-24 08:38:36"
    }
}
```

### HTTP Request
`GET api/aml-settings/{aml_setting}/edit`


<!-- END_e980f2de4a1007f87d8359815c403084 -->

<!-- START_af28f62249aa1b031c098962d35677de -->
## api/aml-settings/{aml_setting}
> Example request:

```bash
curl -X PUT "http://127.0.0.1:3000/api/aml-settings/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/aml-settings/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`PUT api/aml-settings/{aml_setting}`

`PATCH api/aml-settings/{aml_setting}`


<!-- END_af28f62249aa1b031c098962d35677de -->

<!-- START_93121fa3e2a8e83b3690006dadc5a802 -->
## api/aml-settings/{aml_setting}
> Example request:

```bash
curl -X DELETE "http://127.0.0.1:3000/api/aml-settings/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/aml-settings/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):


```json
null
```

### HTTP Request
`DELETE api/aml-settings/{aml_setting}`


<!-- END_93121fa3e2a8e83b3690006dadc5a802 -->

<!-- START_fa8fd546f2fcd42261664adc260b714a -->
## api/aml-transactions
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/aml-transactions" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/aml-transactions");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):


```json
{
    "current_page": 1,
    "data": [
        {
            "id": 100,
            "user_id": 10,
            "amount": "8.0000000000",
            "currency": "ETH",
            "bonus": "0.0000000000",
            "total": "2.0000000000",
            "created_at": "2019-04-24 08:38:37",
            "updated_at": "2019-04-24 08:38:37"
        },
        {
            "id": 99,
            "user_id": 9,
            "amount": "9.0000000000",
            "currency": "ETH",
            "bonus": "0.0000000000",
            "total": "3.0000000000",
            "created_at": "2019-04-24 08:38:37",
            "updated_at": "2019-04-24 08:38:37"
        },
        {
            "id": 98,
            "user_id": 6,
            "amount": "8.0000000000",
            "currency": "ETH",
            "bonus": "0.0000000000",
            "total": "2.0000000000",
            "created_at": "2019-04-24 08:38:37",
            "updated_at": "2019-04-24 08:38:37"
        },
        {
            "id": 97,
            "user_id": 3,
            "amount": "1.0000000000",
            "currency": "ETH",
            "bonus": "0.0000000000",
            "total": "10.0000000000",
            "created_at": "2019-04-24 08:38:37",
            "updated_at": "2019-04-24 08:38:37"
        },
        {
            "id": 96,
            "user_id": 1,
            "amount": "5.0000000000",
            "currency": "ETH",
            "bonus": "0.0000000000",
            "total": "6.0000000000",
            "created_at": "2019-04-24 08:38:37",
            "updated_at": "2019-04-24 08:38:37"
        },
        {
            "id": 95,
            "user_id": 5,
            "amount": "1.0000000000",
            "currency": "ETH",
            "bonus": "0.0000000000",
            "total": "3.0000000000",
            "created_at": "2019-04-24 08:38:37",
            "updated_at": "2019-04-24 08:38:37"
        },
        {
            "id": 94,
            "user_id": 3,
            "amount": "4.0000000000",
            "currency": "ETH",
            "bonus": "0.0000000000",
            "total": "6.0000000000",
            "created_at": "2019-04-24 08:38:37",
            "updated_at": "2019-04-24 08:38:37"
        },
        {
            "id": 93,
            "user_id": 2,
            "amount": "6.0000000000",
            "currency": "ETH",
            "bonus": "0.0000000000",
            "total": "6.0000000000",
            "created_at": "2019-04-24 08:38:37",
            "updated_at": "2019-04-24 08:38:37"
        },
        {
            "id": 92,
            "user_id": 6,
            "amount": "8.0000000000",
            "currency": "ETH",
            "bonus": "0.0000000000",
            "total": "1.0000000000",
            "created_at": "2019-04-24 08:38:37",
            "updated_at": "2019-04-24 08:38:37"
        },
        {
            "id": 91,
            "user_id": 7,
            "amount": "2.0000000000",
            "currency": "ETH",
            "bonus": "0.0000000000",
            "total": "9.0000000000",
            "created_at": "2019-04-24 08:38:37",
            "updated_at": "2019-04-24 08:38:37"
        }
    ],
    "first_page_url": "http:\/\/localhost\/api\/aml-transactions?page=1",
    "from": 1,
    "last_page": 10,
    "last_page_url": "http:\/\/localhost\/api\/aml-transactions?page=10",
    "next_page_url": "http:\/\/localhost\/api\/aml-transactions?page=2",
    "path": "http:\/\/localhost\/api\/aml-transactions",
    "per_page": 10,
    "prev_page_url": null,
    "to": 10,
    "total": 100
}
```

### HTTP Request
`GET api/aml-transactions`


<!-- END_fa8fd546f2fcd42261664adc260b714a -->

<!-- START_58db7bfbbefaf52405d428a8882f6a3a -->
## api/aml-transactions/create
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/aml-transactions/create" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/aml-transactions/create");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):


```json
null
```

### HTTP Request
`GET api/aml-transactions/create`


<!-- END_58db7bfbbefaf52405d428a8882f6a3a -->

<!-- START_8e119fbea629c742ab7ec6d8ea74fd36 -->
## api/aml-transactions
> Example request:

```bash
curl -X POST "http://127.0.0.1:3000/api/aml-transactions" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/aml-transactions");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`POST api/aml-transactions`


<!-- END_8e119fbea629c742ab7ec6d8ea74fd36 -->

<!-- START_20579908327d67f3fe6898812aa371c7 -->
## api/aml-transactions/{aml_transaction}
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/aml-transactions/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/aml-transactions/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):


```json
{
    "data": {
        "id": 1,
        "user_id": 5,
        "amount": "10.0000000000",
        "currency": "ETH",
        "bonus": "0.0000000000",
        "total": "5.0000000000",
        "created_at": "2019-04-24 08:38:36",
        "updated_at": "2019-04-24 08:38:36"
    }
}
```

### HTTP Request
`GET api/aml-transactions/{aml_transaction}`


<!-- END_20579908327d67f3fe6898812aa371c7 -->

<!-- START_9e9da6040f10f4296d1763eeea7dc22e -->
## api/aml-transactions/{aml_transaction}/edit
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/aml-transactions/1/edit" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/aml-transactions/1/edit");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (200):


```json
{
    "data": {
        "id": 1,
        "user_id": 5,
        "amount": "10.0000000000",
        "currency": "ETH",
        "bonus": "0.0000000000",
        "total": "5.0000000000",
        "created_at": "2019-04-24 08:38:36",
        "updated_at": "2019-04-24 08:38:36"
    }
}
```

### HTTP Request
`GET api/aml-transactions/{aml_transaction}/edit`


<!-- END_9e9da6040f10f4296d1763eeea7dc22e -->

<!-- START_72c7d85afb33b4e2da5a76ec66fd5589 -->
## api/aml-transactions/{aml_transaction}
> Example request:

```bash
curl -X PUT "http://127.0.0.1:3000/api/aml-transactions/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/aml-transactions/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`PUT api/aml-transactions/{aml_transaction}`

`PATCH api/aml-transactions/{aml_transaction}`


<!-- END_72c7d85afb33b4e2da5a76ec66fd5589 -->

<!-- START_676475d0996090f8b5e12ba9519f1cb1 -->
## api/aml-transactions/{aml_transaction}
> Example request:

```bash
curl -X DELETE "http://127.0.0.1:3000/api/aml-transactions/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/aml-transactions/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (500):


```json
null
```

### HTTP Request
`DELETE api/aml-transactions/{aml_transaction}`


<!-- END_676475d0996090f8b5e12ba9519f1cb1 -->

#Profile

APIs for profile
<!-- START_2afb288dde161e6470fd6fce73a8326f -->
## api/profile/user-albums
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/profile/user-albums" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-albums");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`GET api/profile/user-albums`


<!-- END_2afb288dde161e6470fd6fce73a8326f -->

<!-- START_f489c82db09b706ed86a11d580aef53a -->
## api/profile/user-albums/create
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/profile/user-albums/create" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-albums/create");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`GET api/profile/user-albums/create`


<!-- END_f489c82db09b706ed86a11d580aef53a -->

<!-- START_6dc07d979463010d4506180c3d3600f5 -->
## api/profile/user-albums
> Example request:

```bash
curl -X POST "http://127.0.0.1:3000/api/profile/user-albums" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-albums");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`POST api/profile/user-albums`


<!-- END_6dc07d979463010d4506180c3d3600f5 -->

<!-- START_d42d73b08f9ad11d3caa51a47d64cd65 -->
## api/profile/user-albums/{user_album}
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/profile/user-albums/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-albums/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`GET api/profile/user-albums/{user_album}`


<!-- END_d42d73b08f9ad11d3caa51a47d64cd65 -->

<!-- START_08f005ce8b26750e1a83767d9641436c -->
## api/profile/user-albums/{user_album}/edit
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/profile/user-albums/1/edit" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-albums/1/edit");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`GET api/profile/user-albums/{user_album}/edit`


<!-- END_08f005ce8b26750e1a83767d9641436c -->

<!-- START_13a45cfe300f830effe7214476d89ae0 -->
## api/profile/user-albums/{user_album}
> Example request:

```bash
curl -X PUT "http://127.0.0.1:3000/api/profile/user-albums/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-albums/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`PUT api/profile/user-albums/{user_album}`

`PATCH api/profile/user-albums/{user_album}`


<!-- END_13a45cfe300f830effe7214476d89ae0 -->

<!-- START_85ae0f7d378137e373b7c29123ddc477 -->
## api/profile/user-albums/{user_album}
> Example request:

```bash
curl -X DELETE "http://127.0.0.1:3000/api/profile/user-albums/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-albums/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`DELETE api/profile/user-albums/{user_album}`


<!-- END_85ae0f7d378137e373b7c29123ddc477 -->

<!-- START_5be252fb4e1e10a578850f984a0c15e3 -->
## api/profile/user-follows
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/profile/user-follows" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-follows");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`GET api/profile/user-follows`


<!-- END_5be252fb4e1e10a578850f984a0c15e3 -->

<!-- START_42b102fb386b46d86727b628d4de5f75 -->
## api/profile/user-follows/create
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/profile/user-follows/create" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-follows/create");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`GET api/profile/user-follows/create`


<!-- END_42b102fb386b46d86727b628d4de5f75 -->

<!-- START_e35c483b73a3f0c63e183d16d7acaa47 -->
## api/profile/user-follows
> Example request:

```bash
curl -X POST "http://127.0.0.1:3000/api/profile/user-follows" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-follows");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`POST api/profile/user-follows`


<!-- END_e35c483b73a3f0c63e183d16d7acaa47 -->

<!-- START_f3ff6497d9145327e0bc9378d347e01a -->
## api/profile/user-follows/{user_follow}
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/profile/user-follows/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-follows/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`GET api/profile/user-follows/{user_follow}`


<!-- END_f3ff6497d9145327e0bc9378d347e01a -->

<!-- START_5c3cdfffa2af59ac4ae0a0162b99948a -->
## api/profile/user-follows/{user_follow}/edit
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/profile/user-follows/1/edit" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-follows/1/edit");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`GET api/profile/user-follows/{user_follow}/edit`


<!-- END_5c3cdfffa2af59ac4ae0a0162b99948a -->

<!-- START_3c3e1c7f8f42994dc08683b26d1eba2a -->
## api/profile/user-follows/{user_follow}
> Example request:

```bash
curl -X PUT "http://127.0.0.1:3000/api/profile/user-follows/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-follows/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`PUT api/profile/user-follows/{user_follow}`

`PATCH api/profile/user-follows/{user_follow}`


<!-- END_3c3e1c7f8f42994dc08683b26d1eba2a -->

<!-- START_0ac71cfb6bf4dd2fd6ce6eac4a99b505 -->
## api/profile/user-follows/{user_follow}
> Example request:

```bash
curl -X DELETE "http://127.0.0.1:3000/api/profile/user-follows/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-follows/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`DELETE api/profile/user-follows/{user_follow}`


<!-- END_0ac71cfb6bf4dd2fd6ce6eac4a99b505 -->

<!-- START_da91fcbd0abaf708d6b9df51ac0107a5 -->
## api/profile/user-image-likes
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/profile/user-image-likes" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-image-likes");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`GET api/profile/user-image-likes`


<!-- END_da91fcbd0abaf708d6b9df51ac0107a5 -->

<!-- START_d31f2a81583b5c4169764987ef9addcb -->
## api/profile/user-image-likes/create
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/profile/user-image-likes/create" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-image-likes/create");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`GET api/profile/user-image-likes/create`


<!-- END_d31f2a81583b5c4169764987ef9addcb -->

<!-- START_2e129e7bb642910050a6293d823ac466 -->
## api/profile/user-image-likes
> Example request:

```bash
curl -X POST "http://127.0.0.1:3000/api/profile/user-image-likes" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-image-likes");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`POST api/profile/user-image-likes`


<!-- END_2e129e7bb642910050a6293d823ac466 -->

<!-- START_81793a99009fbca9ceb3872dd3801650 -->
## api/profile/user-image-likes/{user_image_like}
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/profile/user-image-likes/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-image-likes/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`GET api/profile/user-image-likes/{user_image_like}`


<!-- END_81793a99009fbca9ceb3872dd3801650 -->

<!-- START_0ad24a620a76ecba2b6c84e24cd5023c -->
## api/profile/user-image-likes/{user_image_like}/edit
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/profile/user-image-likes/1/edit" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-image-likes/1/edit");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`GET api/profile/user-image-likes/{user_image_like}/edit`


<!-- END_0ad24a620a76ecba2b6c84e24cd5023c -->

<!-- START_ed5c8dd9e1ed3e31d9cd649070bd901f -->
## api/profile/user-image-likes/{user_image_like}
> Example request:

```bash
curl -X PUT "http://127.0.0.1:3000/api/profile/user-image-likes/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-image-likes/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`PUT api/profile/user-image-likes/{user_image_like}`

`PATCH api/profile/user-image-likes/{user_image_like}`


<!-- END_ed5c8dd9e1ed3e31d9cd649070bd901f -->

<!-- START_6d5d8dbe24cc1a21187c2ac21bf6c23f -->
## api/profile/user-image-likes/{user_image_like}
> Example request:

```bash
curl -X DELETE "http://127.0.0.1:3000/api/profile/user-image-likes/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-image-likes/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`DELETE api/profile/user-image-likes/{user_image_like}`


<!-- END_6d5d8dbe24cc1a21187c2ac21bf6c23f -->

<!-- START_d063a4d2017bbe7f446398f5aa61d7f7 -->
## api/profile/user-images
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/profile/user-images" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-images");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`GET api/profile/user-images`


<!-- END_d063a4d2017bbe7f446398f5aa61d7f7 -->

<!-- START_56142713b6a2e22ab0597a67cb0b3e53 -->
## api/profile/user-images/create
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/profile/user-images/create" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-images/create");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`GET api/profile/user-images/create`


<!-- END_56142713b6a2e22ab0597a67cb0b3e53 -->

<!-- START_ffe384bf77c4c41c2524e7e456c8b07b -->
## api/profile/user-images
> Example request:

```bash
curl -X POST "http://127.0.0.1:3000/api/profile/user-images" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-images");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`POST api/profile/user-images`


<!-- END_ffe384bf77c4c41c2524e7e456c8b07b -->

<!-- START_2eacb69754cf88892f48365fddadadc0 -->
## api/profile/user-images/{user_image}
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/profile/user-images/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-images/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`GET api/profile/user-images/{user_image}`


<!-- END_2eacb69754cf88892f48365fddadadc0 -->

<!-- START_1aee93f5a6c7229a51422383656ecaeb -->
## api/profile/user-images/{user_image}/edit
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/profile/user-images/1/edit" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-images/1/edit");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`GET api/profile/user-images/{user_image}/edit`


<!-- END_1aee93f5a6c7229a51422383656ecaeb -->

<!-- START_62456d00cab674c627a4e6b46bd3bb8f -->
## api/profile/user-images/{user_image}
> Example request:

```bash
curl -X PUT "http://127.0.0.1:3000/api/profile/user-images/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-images/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`PUT api/profile/user-images/{user_image}`

`PATCH api/profile/user-images/{user_image}`


<!-- END_62456d00cab674c627a4e6b46bd3bb8f -->

<!-- START_6981ffcb47edba73a06c9797dbdbeb4e -->
## api/profile/user-images/{user_image}
> Example request:

```bash
curl -X DELETE "http://127.0.0.1:3000/api/profile/user-images/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-images/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`DELETE api/profile/user-images/{user_image}`


<!-- END_6981ffcb47edba73a06c9797dbdbeb4e -->

<!-- START_e087e66137d09974fab450658a074345 -->
## api/profile/user-rates
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/profile/user-rates" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-rates");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`GET api/profile/user-rates`


<!-- END_e087e66137d09974fab450658a074345 -->

<!-- START_f44deb391f92e21cd670aaa2d33ffd68 -->
## api/profile/user-rates/create
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/profile/user-rates/create" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-rates/create");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`GET api/profile/user-rates/create`


<!-- END_f44deb391f92e21cd670aaa2d33ffd68 -->

<!-- START_6cb555108fe92fe1eb0f484affbf1663 -->
## api/profile/user-rates
> Example request:

```bash
curl -X POST "http://127.0.0.1:3000/api/profile/user-rates" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-rates");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`POST api/profile/user-rates`


<!-- END_6cb555108fe92fe1eb0f484affbf1663 -->

<!-- START_77f95d85eea1582a860d0a2764437ad0 -->
## api/profile/user-rates/{user_rate}
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/profile/user-rates/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-rates/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`GET api/profile/user-rates/{user_rate}`


<!-- END_77f95d85eea1582a860d0a2764437ad0 -->

<!-- START_d8c2f7f3134706bc48683eaea35fa2e7 -->
## api/profile/user-rates/{user_rate}/edit
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/profile/user-rates/1/edit" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-rates/1/edit");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`GET api/profile/user-rates/{user_rate}/edit`


<!-- END_d8c2f7f3134706bc48683eaea35fa2e7 -->

<!-- START_19f2c00289eae53efb08ce241a24d6f9 -->
## api/profile/user-rates/{user_rate}
> Example request:

```bash
curl -X PUT "http://127.0.0.1:3000/api/profile/user-rates/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-rates/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`PUT api/profile/user-rates/{user_rate}`

`PATCH api/profile/user-rates/{user_rate}`


<!-- END_19f2c00289eae53efb08ce241a24d6f9 -->

<!-- START_3c12440b21d4d1c346cb175bd4701e3c -->
## api/profile/user-rates/{user_rate}
> Example request:

```bash
curl -X DELETE "http://127.0.0.1:3000/api/profile/user-rates/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-rates/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`DELETE api/profile/user-rates/{user_rate}`


<!-- END_3c12440b21d4d1c346cb175bd4701e3c -->

<!-- START_3d5f9cd5a7216c4473ce0ae33658223f -->
## api/profile/user-profiles
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/profile/user-profiles" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-profiles");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`GET api/profile/user-profiles`


<!-- END_3d5f9cd5a7216c4473ce0ae33658223f -->

<!-- START_f38d84951b1d4bf6f75f6d30e1c3a698 -->
## api/profile/user-profiles/create
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/profile/user-profiles/create" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-profiles/create");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (302):


```json
null
```

### HTTP Request
`GET api/profile/user-profiles/create`


<!-- END_f38d84951b1d4bf6f75f6d30e1c3a698 -->

<!-- START_9fe629e8207451ec9388aaa778f8d066 -->
## api/profile/user-profiles
> Example request:

```bash
curl -X POST "http://127.0.0.1:3000/api/profile/user-profiles" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-profiles");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`POST api/profile/user-profiles`


<!-- END_9fe629e8207451ec9388aaa778f8d066 -->

<!-- START_f850b92670a8c8df1b0af78f71cf6de2 -->
## api/profile/user-profiles/{user_profile}
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/profile/user-profiles/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-profiles/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`GET api/profile/user-profiles/{user_profile}`


<!-- END_f850b92670a8c8df1b0af78f71cf6de2 -->

<!-- START_1de6fc5e0d28dc3f6704c53c1d67c003 -->
## api/profile/user-profiles/{user_profile}/edit
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/profile/user-profiles/1/edit" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-profiles/1/edit");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`GET api/profile/user-profiles/{user_profile}/edit`


<!-- END_1de6fc5e0d28dc3f6704c53c1d67c003 -->

<!-- START_6693ba4b4bbf7e7facb24bc6e5777acd -->
## api/profile/user-profiles/{user_profile}
> Example request:

```bash
curl -X PUT "http://127.0.0.1:3000/api/profile/user-profiles/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-profiles/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`PUT api/profile/user-profiles/{user_profile}`

`PATCH api/profile/user-profiles/{user_profile}`


<!-- END_6693ba4b4bbf7e7facb24bc6e5777acd -->

<!-- START_5173ac284d7d94c97979a5c680acf8fc -->
## api/profile/user-profiles/{user_profile}
> Example request:

```bash
curl -X DELETE "http://127.0.0.1:3000/api/profile/user-profiles/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/profile/user-profiles/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`DELETE api/profile/user-profiles/{user_profile}`


<!-- END_5173ac284d7d94c97979a5c680acf8fc -->

#Studio

APIs for studio
<!-- START_a162b41a818e6314ced11b56ca7f743d -->
## api/studio-follows
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/studio-follows" \
    -H "Content-Type: application/json" \
    -d '{"title":"aspernatur","body":"sunt","type":"quod","author_id":7,"thumbnail":"expedita"}'

```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studio-follows");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "title": "aspernatur",
    "body": "sunt",
    "type": "quod",
    "author_id": 7,
    "thumbnail": "expedita"
}

fetch(url, {
    method: "GET",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`GET api/studio-follows`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    title | string |  required  | The title of the post.
    body | string |  required  | The title of the post.
    type | string |  optional  | The type of post to create. Defaults to 'textophonious'.
    author_id | integer |  optional  | the ID of the author
    thumbnail | image |  optional  | This is required if the post type is 'imagelicious'.

<!-- END_a162b41a818e6314ced11b56ca7f743d -->

<!-- START_335948df412d52d3ae08737381de3123 -->
## api/studio-follows/create
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/studio-follows/create" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studio-follows/create");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`GET api/studio-follows/create`


<!-- END_335948df412d52d3ae08737381de3123 -->

<!-- START_24c04081da6e1cac65982fd12b8bde92 -->
## api/studio-follows
> Example request:

```bash
curl -X POST "http://127.0.0.1:3000/api/studio-follows" \
    -H "Content-Type: application/json" \
    -d '{"title":"dicta","body":"ut","type":"fugit","author_id":9,"thumbnail":"et"}'

```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studio-follows");

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
}

let body = {
    "title": "dicta",
    "body": "ut",
    "type": "fugit",
    "author_id": 9,
    "thumbnail": "et"
}

fetch(url, {
    method: "POST",
    headers: headers,
    body: body
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`POST api/studio-follows`

#### Body Parameters

Parameter | Type | Status | Description
--------- | ------- | ------- | ------- | -----------
    title | string |  required  | The title of the post.
    body | string |  required  | The title of the post.
    type | string |  optional  | The type of post to create. Defaults to 'textophonious'.
    author_id | integer |  optional  | the ID of the author
    thumbnail | image |  optional  | This is required if the post type is 'imagelicious'.

<!-- END_24c04081da6e1cac65982fd12b8bde92 -->

<!-- START_d2f899b9ecc3a28c60845ebae58f2563 -->
## api/studio-follows/{studio_follow}
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/studio-follows/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studio-follows/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`GET api/studio-follows/{studio_follow}`


<!-- END_d2f899b9ecc3a28c60845ebae58f2563 -->

<!-- START_453e64b50f8217faca8202fd8a06a5ff -->
## api/studio-follows/{studio_follow}/edit
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/studio-follows/1/edit" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studio-follows/1/edit");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`GET api/studio-follows/{studio_follow}/edit`


<!-- END_453e64b50f8217faca8202fd8a06a5ff -->

<!-- START_f5f40d27d54b1e024b8baaa93878994c -->
## api/studio-follows/{studio_follow}
> Example request:

```bash
curl -X PUT "http://127.0.0.1:3000/api/studio-follows/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studio-follows/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`PUT api/studio-follows/{studio_follow}`

`PATCH api/studio-follows/{studio_follow}`


<!-- END_f5f40d27d54b1e024b8baaa93878994c -->

<!-- START_7a7c486b63a7c27a59b0e747b5013851 -->
## api/studio-follows/{studio_follow}
> Example request:

```bash
curl -X DELETE "http://127.0.0.1:3000/api/studio-follows/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studio-follows/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`DELETE api/studio-follows/{studio_follow}`


<!-- END_7a7c486b63a7c27a59b0e747b5013851 -->

<!-- START_e28c1145b04d3c42056b56515ddc7563 -->
## api/studio-rates
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/studio-rates" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studio-rates");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`GET api/studio-rates`


<!-- END_e28c1145b04d3c42056b56515ddc7563 -->

<!-- START_1829f1e2eaa8c26e3b30769b8287efe5 -->
## api/studio-rates/create
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/studio-rates/create" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studio-rates/create");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`GET api/studio-rates/create`


<!-- END_1829f1e2eaa8c26e3b30769b8287efe5 -->

<!-- START_4faeb26f6cd53a6c72d7e27c67a8f93b -->
## api/studio-rates
> Example request:

```bash
curl -X POST "http://127.0.0.1:3000/api/studio-rates" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studio-rates");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`POST api/studio-rates`


<!-- END_4faeb26f6cd53a6c72d7e27c67a8f93b -->

<!-- START_c38ebc0e4150fee71c1475f6bccb2d27 -->
## api/studio-rates/{studio_rate}
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/studio-rates/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studio-rates/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`GET api/studio-rates/{studio_rate}`


<!-- END_c38ebc0e4150fee71c1475f6bccb2d27 -->

<!-- START_7169b494a098638f3679a7279604cd98 -->
## api/studio-rates/{studio_rate}/edit
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/studio-rates/1/edit" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studio-rates/1/edit");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`GET api/studio-rates/{studio_rate}/edit`


<!-- END_7169b494a098638f3679a7279604cd98 -->

<!-- START_a9776981ea9c6c17ab0598a6cce434e1 -->
## api/studio-rates/{studio_rate}
> Example request:

```bash
curl -X PUT "http://127.0.0.1:3000/api/studio-rates/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studio-rates/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`PUT api/studio-rates/{studio_rate}`

`PATCH api/studio-rates/{studio_rate}`


<!-- END_a9776981ea9c6c17ab0598a6cce434e1 -->

<!-- START_ba71562a8b7eadb81e6cb00f24d7b211 -->
## api/studio-rates/{studio_rate}
> Example request:

```bash
curl -X DELETE "http://127.0.0.1:3000/api/studio-rates/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studio-rates/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`DELETE api/studio-rates/{studio_rate}`


<!-- END_ba71562a8b7eadb81e6cb00f24d7b211 -->

<!-- START_0ac69c00dd9462c70c8c6dc4ad42b436 -->
## api/studio-reports
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/studio-reports" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studio-reports");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`GET api/studio-reports`


<!-- END_0ac69c00dd9462c70c8c6dc4ad42b436 -->

<!-- START_fa236382438b9e6db5b35c5e96d5cd5b -->
## api/studio-reports/create
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/studio-reports/create" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studio-reports/create");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`GET api/studio-reports/create`


<!-- END_fa236382438b9e6db5b35c5e96d5cd5b -->

<!-- START_2cf0a321f5c98903944cab44ed5e80d7 -->
## api/studio-reports
> Example request:

```bash
curl -X POST "http://127.0.0.1:3000/api/studio-reports" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studio-reports");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`POST api/studio-reports`


<!-- END_2cf0a321f5c98903944cab44ed5e80d7 -->

<!-- START_77033b209218f246dcce2181c42d12d3 -->
## api/studio-reports/{studio_report}
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/studio-reports/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studio-reports/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`GET api/studio-reports/{studio_report}`


<!-- END_77033b209218f246dcce2181c42d12d3 -->

<!-- START_383629f3a7f8f6973f0eb1ef680468d2 -->
## api/studio-reports/{studio_report}/edit
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/studio-reports/1/edit" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studio-reports/1/edit");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`GET api/studio-reports/{studio_report}/edit`


<!-- END_383629f3a7f8f6973f0eb1ef680468d2 -->

<!-- START_e56bc9dd70bf8665475b3f6553bb56dd -->
## api/studio-reports/{studio_report}
> Example request:

```bash
curl -X PUT "http://127.0.0.1:3000/api/studio-reports/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studio-reports/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`PUT api/studio-reports/{studio_report}`

`PATCH api/studio-reports/{studio_report}`


<!-- END_e56bc9dd70bf8665475b3f6553bb56dd -->

<!-- START_48e580bf4d2cb9f3e8a5b3bdb06b21ee -->
## api/studio-reports/{studio_report}
> Example request:

```bash
curl -X DELETE "http://127.0.0.1:3000/api/studio-reports/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studio-reports/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`DELETE api/studio-reports/{studio_report}`


<!-- END_48e580bf4d2cb9f3e8a5b3bdb06b21ee -->

<!-- START_9d47c148bb693403d6220cd61c47769b -->
## api/studios
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/studios" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studios");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`GET api/studios`


<!-- END_9d47c148bb693403d6220cd61c47769b -->

<!-- START_34545b978e32d19965834e23501b045a -->
## api/studios/create
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/studios/create" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studios/create");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`GET api/studios/create`


<!-- END_34545b978e32d19965834e23501b045a -->

<!-- START_f1265991d915125bfbceb623d1ce79e2 -->
## api/studios
> Example request:

```bash
curl -X POST "http://127.0.0.1:3000/api/studios" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studios");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "POST",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`POST api/studios`


<!-- END_f1265991d915125bfbceb623d1ce79e2 -->

<!-- START_9dc8cc91a68de3273f31fd57eef7319d -->
## api/studios/{studio}
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/studios/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studios/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`GET api/studios/{studio}`


<!-- END_9dc8cc91a68de3273f31fd57eef7319d -->

<!-- START_7e2bd4df1607c3e32134268312bc890a -->
## api/studios/{studio}/edit
> Example request:

```bash
curl -X GET -G "http://127.0.0.1:3000/api/studios/1/edit" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studios/1/edit");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "GET",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`GET api/studios/{studio}/edit`


<!-- END_7e2bd4df1607c3e32134268312bc890a -->

<!-- START_49d2408b09b3952430b2e5ae93ff0676 -->
## api/studios/{studio}
> Example request:

```bash
curl -X PUT "http://127.0.0.1:3000/api/studios/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studios/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "PUT",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`PUT api/studios/{studio}`

`PATCH api/studios/{studio}`


<!-- END_49d2408b09b3952430b2e5ae93ff0676 -->

<!-- START_be5910918719a114f69e297e1a100ab3 -->
## api/studios/{studio}
> Example request:

```bash
curl -X DELETE "http://127.0.0.1:3000/api/studios/1" 
```

```javascript
const url = new URL("http://127.0.0.1:3000/api/studios/1");

let headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
}

fetch(url, {
    method: "DELETE",
    headers: headers,
})
    .then(response => response.json())
    .then(json => console.log(json));
```


> Example response (429):


```json
null
```

### HTTP Request
`DELETE api/studios/{studio}`


<!-- END_be5910918719a114f69e297e1a100ab3 -->


