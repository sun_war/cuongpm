const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
   .sass('resources/sass/app.scss', 'public/css');

mix.js('modules/BlockChain/resources/assets/js/app.js', 'public/modules/BlockChain/js');
mix.js('modules/ACL/resources/assets/js/app.js', 'public/modules/ACL/js');
mix.js('modules/IO/resources/assets/js/app.js', 'public/modules/IO/js');
mix.js('modules/English/resources/assets/js/app.js', 'public/modules/English/js');
