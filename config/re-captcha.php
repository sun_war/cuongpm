<?php
/**
 * Created by PhpStorm.
 * Date: 7/24/19
 * Time: 2:39 PM
 */

return [
    'uri' => env('RE_CAPTCHA_URI', 'https://www.google.com/recaptcha/api/siteverify'),
    'secret' => env('RE_CAPTCHA_SECRET', '6LcKDq8UAAAAACzkEqcIfmpdX6eP3t8rBrcUK5Ht')
];